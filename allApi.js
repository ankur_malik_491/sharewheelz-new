import * as Facebook from "expo-facebook";
import * as Google from "expo-google-app-auth";
import { showImagePicker } from "react-native-image-picker";


const API = {
    signupWithEmail: (FirstName,LastName,DOB,Gender,Email,Password,Refferal) => {
        let result = {}
        return new Promise(async(resolve,reject)=>{
            try{
                fetch('http://stsmentor.com/zozocar_admin/api/v1/users/registeruser', 
                    {
                        method: 'POST',
                        headers: {
                            Accept: 'application/json',
                            'Content-Type': 'application/json',
                        },
                        body: JSON.stringify({
                                                API_KEY : "test@123*",
                                                provider : "email",
                                                firstname : FirstName,
                                                lastname : LastName,
                                                birthyear : DOB, 
                                                gender : Gender,
                                                email : Email, 
                                                password:Password,
                                                refferal_code : Refferal
                                }),     
                    })
                    .then(response => response.json())
                    .then(response2 => {
                        //console.log(response2)
                        result.status = response2.Status;
                        result.message = response2.Message;
                        result.data = response2.data;
                        resolve(result)
                    }).catch((error) =>{
                        result.status = false;
                        result.data = undefined;
                        reject(result)
                    });
                
            }
            catch(error){
                result.status = false;
                result.message = error;
                result.data = undefined;
                resolve(result)
            }
        })
    },
    loginWithEmail: (Email,Password) => {
        let result = {}
        return new Promise(async(resolve,reject)=>{
            try{
                fetch('http://stsmentor.com/zozocar_admin/api/v1/users/login', 
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                                            API_KEY : "test@123*",
                                            user_email : Email, 
                                            user_password :Password
                            }),     
                    })
                    .then(response => response.json())
                    .then(response2 => {
                        //console.log(response2)
                        result.status = response2.Status;
                        result.message = response2.Message;
                        result.data = response2.data;
                        resolve(result)
                    }).catch((error) =>{
                        result.status = false;
                        result.data = undefined;
                        reject(result)
                    });
                
            }
            catch(error){
                result.status = false;
                result.message = error;
                result.data = undefined;
                resolve(result)
            }
        })
    }, 
    SocialLoginData: (loginSource) => {
        let result = {};
        return new Promise(async(resolve,reject)=>{
            try{ 
                if(loginSource=="facebook")
                {   
                    await Facebook.initializeAsync('223957445346702');
                    const {type,token,expires,permissions,declinedPermissions,} = await Facebook.logInWithReadPermissionsAsync({
                        permissions: ['public_profile','email'],
                        });
                    if (type === 'success') {
                       fetch(`https://graph.facebook.com/me?access_token=${token}&fields=id,email,first_name,last_name,picture.width(600).height(600)`) 
                        .then(response => response.json())
                        .then(response2 => {
                           //console.log(response2)
                            result.status = true;
                            result.firstname = response2.first_name;
                            result.lastname = response2.last_name;
                            result.email = response2.email;
                            result.photoUrl = response2.picture.data.url;
                            result.refferal = "";
                            resolve(result)
                        
                        }).catch((error) =>{
                            console.log(error)
                        });
                    }
                    else{
                        result.status = false;
                        result.message = "User Cancelled Request";
                        result.data = undefined;
                        resolve(result)
                    }
                    
                }
                else{
                    const response = await Google.logInAsync({
                        androidClientId: "303051574343-7feb4b95j6nioqfr52taant3hf6pr7rn.apps.googleusercontent.com",
                        //       //iosClientId: YOUR_CLIENT_ID_HERE,  <-- if you use iOS
                        scopes: ["profile", "email"]
                    })
                    if (response.type === 'success') {
                           // console.log(response)
                        result.status = true;
                        result.firstname = response.user.givenName;
                        result.lastname = response.user.familyName;
                        result.email = response.user.email;
                        result.photoUrl = response.user.photoUrl;
                        result.refferal = "";
                        resolve(result)
                         
                    }
                    else{
                         result.status = false;
                         result.message = "User Cancelled Request";
                         result.data = undefined;
                         resolve(result)
                     }
                } 
            } 
            catch(error){
                result.status = false;
                result.message = error;
                result.data = undefined;
                resolve(result)
            }
        })
    },
    loginWithSocial: (first_name,last_name,Email,PhotoUrl,Refferal) => {
        let result = {}
        return new Promise(async(resolve,reject)=>{
            try{
                fetch('http://stsmentor.com/zozocar_admin/api/v1/users/registeruser', 
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                                            API_KEY : "test@123*",
                                            provider : "facebook", 
                                            firstname : first_name,
                                            lastname:last_name,
                                            email:Email,
                                            photourl:PhotoUrl,
                                            refferal_code:Refferal,
                            }),     
                    })
                    .then(response => response.json())
                    .then(response2 => {
                        //console.log(response2)
                        result.status = response2.Status;
                        result.message = response2.Message;
                        result.data = response2.data;
                        resolve(result)
                    }).catch((error) =>{
                        result.status = false;
                        result.data = undefined;
                        reject(result)
                    });
                
            }
            catch(error){
                result.status = false;
                result.message = error;
                result.data = undefined;
                resolve(result)
            }
        })
    },
    resetPassword: (Email) => {
        let result = {}
        return new Promise(async(resolve,reject)=>{
            try{
                fetch('http://stsmentor.com/zozocar_admin/api/v1/users/forgotpassword', 
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                                            API_KEY : "test@123*",
                                            user_email : Email 
                            }),     
                    })
                    .then(response => response.json())
                    .then(response2 => {
                        //console.log(response2)
                        result.status = response2.Status;
                        result.message = response2.Message;
                        result.data = undefined;
                        resolve(result)
                    }).catch((error) =>{
                        result.status = false;
                        result.data = undefined;
                        reject(result)
                    });
                
            }
            catch(error){
                result.status = false;
                result.message = error;
                result.data = undefined;
                resolve(result)
            }
        })
    },
    getUserDetails : (uid) => {
        let result = {}
        return new Promise(async(resolve,reject)=>{
            try{
                fetch('http://stsmentor.com/zozocar_admin/api/v1/user', 
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                                            API_KEY : "test@123*",
                                            user_id : uid,
                            }),     
                    })
                    .then(response => response.json())
                    .then(response2 => {
                        //console.log(response2)
                        result.status = response2.Status;
                        result.message = response2.Message;
                        result.data = response2.data;
                        resolve(result)
                    }).catch((error) =>{
                        result.status = false;
                        result.data = undefined;
                        reject(result)
                    });
                
            }
            catch(error){
                result.status = false;
                result.message = error;
                result.data = undefined;
                resolve(result)
            }
        })
    },
    updateUserDetails : (uid,FirstName,LastName,Dob,Gender,DL) => {
        let result = {}
        return new Promise(async(resolve,reject)=>{
            try{
                fetch('http://stsmentor.com/zozocar_admin/api/v1/users/updateinfo', 
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                                            API_KEY : "test@123*",
                                            user_id : uid, 
                                            firstname:FirstName,
                                            lastname:LastName,
                                            birthyear:Dob,
                                            gender:Gender,
                                            bio:DL
                            }),     
                    })
                    .then(response => response.json())
                    .then(response2 => {
                        //console.log(response2)
                        result.status = response2.Status;
                        result.message = response2.Message;
                        result.data = response2.data;
                        resolve(result)
                    }).catch((error) =>{
                        result.status = false;
                        result.data = undefined;
                        reject(result)
                    });
                
            }
            catch(error){
                result.status = false;
                result.message = error;
                result.data = undefined;
                resolve(result)
            }
        })
    }, 
    updateUserEmail : (uid,Email) => {
        let result = {}
        return new Promise(async(resolve,reject)=>{
            try{
                fetch('http://stsmentor.com/zozocar_admin/api/v1/users/addemail', 
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                                            API_KEY : "test@123*",
                                            user_id : uid, 
                                            user_email:Email
                            }),     
                    })
                    .then(response => response.json())
                    .then(response2 => {
                        //console.log(response2)
                        result.status = response2.Status;
                        result.message = response2.Message;
                        result.data = response2.data;
                        resolve(result)
                    }).catch((error) =>{
                        result.status = false;
                        result.data = undefined;
                        reject(result)
                    });
                
            }
            catch(error){
                result.status = false;
                result.message = error;
                result.data = undefined;
                resolve(result)
            }
        })
    }, 
    updateUserPhone : (uid,Phone) => {
        let result = {}
        return new Promise(async(resolve,reject)=>{
            try{
                fetch('http://stsmentor.com/zozocar_admin/api/v1/users/addphone', 
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                                            API_KEY : "test@123*",
                                            user_id : uid, 
                                            user_phone:Phone
                            }),     
                    })
                    .then(response => response.json())
                    .then(response2 => {
                        //console.log(response2)
                        result.status = response2.Status;
                        result.message = response2.Message;
                        result.data = response2.data;
                        resolve(result)
                    }).catch((error) =>{
                        result.status = false;
                        result.data = undefined;
                        reject(result)
                    });
                
            }
            catch(error){
                result.status = false;
                result.message = error;
                result.data = undefined;
                resolve(result)
            }
        })
    },
    updateUserPhoto : (uid,photoUrl) => {
        let result = {}
        return new Promise(async(resolve,reject)=>{
            try{
                fetch('http://stsmentor.com/zozocar_admin/api/v1/users/updateprofilepic', 
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                                            API_KEY : "test@123*",
                                            user_id : uid, 
                                            profile_pic : photoUrl
                            }),     
                    })
                    .then(response => response.json())
                    .then(response2 => {
                        //console.log(response2)
                        result.status = response2.Status;
                        result.message = response2.Message;
                        result.data = response2.data;
                        resolve(result)
                    }).catch((error) =>{
                        result.status = false;
                        result.data = undefined;
                        reject(result)
                    });
                
            }
            catch(error){
                result.status = false;
                result.message = error;
                result.data = undefined;
                resolve(result)
            }
        })
    },
    updateUserPassword : (uid,Password) => {
        let result = {}
        return new Promise(async(resolve,reject)=>{
            try{
                fetch('http://stsmentor.com/zozocar_admin/api/v1/users/changeuserpassword', 
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                                            API_KEY : "test@123*",
                                            provider : "email",
                                            user_id : uid, 
                                            user_password : Password
                            }),     
                    })
                    .then(response => response.json())
                    .then(response2 => {
                        //console.log(response2)
                        result.status = response2.Status;
                        result.message = response2.Message;
                        result.data = response2.data;
                        resolve(result)
                    }).catch((error) =>{
                        result.status = false;
                        result.data = undefined;
                        reject(result)
                    });
                
            }
            catch(error){
                result.status = false;
                result.message = error;
                result.data = undefined;
                resolve(result)
            }
        })
    },
    getUserCars : (uid) => {
        let result = {}
        return new Promise(async(resolve,reject)=>{
            try{
                fetch('http://stsmentor.com/zozocar_admin/api/v1/users/getusercars', 
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                                            API_KEY : "test@123*",
                                            user_id : uid,
                            }),     
                    })
                    .then(response => response.json())
                    .then(response2 => {
                        //console.log(response2)
                        result.status = response2.Status;
                        result.message = response2.Message;
                        result.data = response2.data;
                        resolve(result)
                    }).catch((error) =>{
                        result.status = false;
                        result.data = undefined;
                        reject(result)
                    });
                
            }
            catch(error){
                result.status = false;
                result.message = error;
                result.data = undefined;
                resolve(result)
            }
        })
    },
    getCarsBrands : () => {
        let result = {}
        return new Promise(async(resolve,reject)=>{
            try{
                fetch('http://stsmentor.com/zozocar_admin/api/v1/car/getallbrands', 
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                                            API_KEY : "test@123*"
                            }),     
                    })
                    .then(response => response.json())
                    .then(response2 => {
                        //console.log(response2)
                        result.status = response2.Status;
                        result.message = response2.Message;
                        result.data = response2.data;
                        resolve(result)
                    }).catch((error) =>{
                        result.status = false;
                        result.data = undefined;
                        reject(result)
                    });
                
            }
            catch(error){
                result.status = false;
                result.message = error;
                result.data = undefined;
                resolve(result)
            }
        })
    },
    getCarsBody : () => {
        let result = {}
        return new Promise(async(resolve,reject)=>{
            try{
                fetch('http://stsmentor.com/zozocar_admin/api/v1/car/carbody', 
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                                            API_KEY : "test@123*"
                            }),     
                    })
                    .then(response => response.json())
                    .then(response2 => {
                        //console.log(response2)
                        result.status = response2.Status;
                        result.message = response2.Message;
                        result.data = response2.data;
                        resolve(result)
                    }).catch((error) =>{
                        result.status = false;
                        result.data = undefined;
                        reject(result)
                    });
                
            }
            catch(error){
                result.status = false;
                result.message = error;
                result.data = undefined;
                resolve(result)
            }
        })
    },
    getCarscolor : () => {
        let result = {}
        return new Promise(async(resolve,reject)=>{
            try{
                fetch('http://stsmentor.com/zozocar_admin/api/v1/car/carcolor', 
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                                            API_KEY : "test@123*"
                            }),     
                    })
                    .then(response => response.json())
                    .then(response2 => {
                        //console.log(response2)
                        result.status = response2.Status;
                        result.message = response2.Message;
                        result.data = response2.data;
                        resolve(result)
                    }).catch((error) =>{
                        result.status = false;
                        result.data = undefined;
                        reject(result)
                    });
                
            }
            catch(error){
                result.status = false;
                result.message = error;
                result.data = undefined;
                resolve(result)
            }
        })
    },
    getCarsModal : (brand) => {
        let result = {}
        return new Promise(async(resolve,reject)=>{
            try{
                fetch('http://stsmentor.com/zozocar_admin/api/v1/car/getcarsofbrand', 
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                                            API_KEY : "test@123*",
                                            car_brand: brand
                            }),     
                    })
                    .then(response => response.json())
                    .then(response2 => {
                        //console.log(response2)
                        result.status = response2.Status;
                        result.message = response2.Message;
                        result.data = response2.data;
                        resolve(result)
                    }).catch((error) =>{
                        result.status = false;
                        result.data = undefined;
                        reject(result)
                    });
                
            }
            catch(error){
                result.status = false;
                result.message = error;
                result.data = undefined;
                resolve(result)
            }
        })
    },
    addUserCars : (uid,brand,modal,bodyType,bodyColor,regYear,regNumber) => {
        let result = {}
        return new Promise(async(resolve,reject)=>{
            try{
                fetch('http://stsmentor.com/zozocar_admin/api/v1/users/addcar', 
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                                            API_KEY : "test@123*",
                                            user_id: uid,
                                            car_brand:brand,
                                            car_model:modal,
                                            body_type:bodyType,
                                            body_color:bodyColor,
                                            reg_year:regYear,
                                            number_plate:regNumber
                            }),     
                    })
                    .then(response => response.json())
                    .then(response2 => {
                        //console.log(response2)
                        result.status = response2.Status;
                        result.message = response2.Message;
                        result.data = response2.data;
                        resolve(result)
                    }).catch((error) =>{
                        result.status = false;
                        result.data = undefined;
                        reject(result)
                    });
                
            }
            catch(error){
                result.status = false;
                result.message = error;
                result.data = undefined;
                resolve(result)
            }
        })
    },
    deleteUserCars : (carid) => {
        let result = {}
        return new Promise(async(resolve,reject)=>{
            try{
                fetch('http://stsmentor.com/zozocar_admin/api/v1/users/deleteusercar', 
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                                            API_KEY : "test@123*",
                                            car_id:carid
                            }),     
                    })
                    .then(response => response.json())
                    .then(response2 => {
                        //console.log(response2)
                        result.status = response2.Status;
                        result.message = response2.Message;
                        result.data = response2.data;
                        resolve(result)
                    }).catch((error) =>{
                        result.status = false;
                        result.data = undefined;
                        reject(result)
                    });
                
            }
            catch(error){
                result.status = false;
                result.message = error;
                result.data = undefined;
                resolve(result)
            }
        })
    },
    getUserRating : (uid,type) => {
        let result = {}
        return new Promise(async(resolve,reject)=>{
            try{
                fetch('http://stsmentor.com/zozocar_admin/api/v1/users/get_rating', 
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                                            API_KEY : "test@123*",
                                            rating_type : type,
                                            user_id:uid 
                            }),     
                    })
                    .then(response => response.json())
                    .then(response2 => {
                        //console.log(response2)
                        result.status = response2.Status;
                        result.message = response2.Message;
                        result.data = response2.data;
                        resolve(result)
                    }).catch((error) =>{
                        result.status = false;
                        result.data = undefined;
                        reject(result)
                    });
                
            }
            catch(error){
                result.status = false;
                result.message = error;
                result.data = undefined;
                resolve(result)
            }
        })
    },
    getUserPref : (uid) => {
        let result = {}
        return new Promise(async(resolve,reject)=>{
            try{
                fetch('http://stsmentor.com/zozocar_admin/api/v1/user', 
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                                            API_KEY : "test@123*",
                                            user_id:uid 
                            }),     
                    })
                    .then(response => response.json())
                    .then(response2 => {
                        //console.log(response2)
                        result.status = response2.Status;
                        result.message = response2.Message;
                        result.data = response2.data;
                        resolve(result)
                    }).catch((error) =>{
                        result.status = false;
                        result.data = undefined;
                        reject(result)
                    });
                
            }
            catch(error){
                result.status = false;
                result.message = error;
                result.data = undefined;
                resolve(result)
            }
        })
    },
    updateUserPref : (uid,Chatting,Smoking,Music,Pets) => {
        let result = {}
        return new Promise(async(resolve,reject)=>{
            try{
                fetch('http://stsmentor.com/zozocar_admin/api/v1/users/updateprefrence', 
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                                            API_KEY : "test@123*",
                                            user_id:uid ,
                                            chatting:Chatting,
                                            smoking:Smoking,
                                            music:Music,
                                            pets:Pets

                            }),     
                    })
                    .then(response => response.json())
                    .then(response2 => {
                        //console.log(response2)
                        result.status = response2.Status;
                        result.message = response2.Message;
                        result.data = response2.data;
                        resolve(result)
                    }).catch((error) =>{
                        result.status = false;
                        result.data = undefined;
                        reject(result)
                    });
                
            }
            catch(error){
                result.status = false;
                result.message = error;
                result.data = undefined;
                resolve(result)
            }
        })
    },
    getUserMessage: (uid) => {
        let result = {}
        return new Promise(async(resolve,reject)=>{
            try{
                fetch('http://stsmentor.com/zozocar_admin/api/v1/ride/getusermessages', 
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                                            API_KEY : "test@123*",
                                            user_id:uid 

                            }),     
                    })
                    .then(response => response.json())
                    .then(response2 => {
                        //console.log(response2)
                        result.status = response2.Status;
                        result.message = response2.Message;
                        result.data = response2.data;
                        resolve(result)
                    }).catch((error) =>{
                        result.status = false;
                        result.message = error
                        result.data = undefined;
                        reject(result)
                    });
                
            }
            catch(error){
                result.status = false;
                result.message = error;
                result.data = undefined;
                resolve(result)
            }
        })
    },
    getUserNotification: (uid) => {
        let result = {}
        return new Promise(async(resolve,reject)=>{
            try{
                fetch('', 
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                                            API_KEY : "test@123*",
                                            user_id:uid 

                            }),     
                    })
                    .then(response => response.json())
                    .then(response2 => {
                        //console.log(response2)
                        result.status = response2.Status;
                        result.message = response2.Message;
                        result.data = response2.data;
                        resolve(result)
                    }).catch((error) =>{
                        result.status = false;
                        result.message = error
                        result.data = undefined;
                        reject(result)
                    });
                
            }
            catch(error){
                result.status = false;
                result.message = error;
                result.data = undefined;
                resolve(result)
            }
        })
    },
    getAllCities: (city) => {
        let result = {}
        return new Promise(async(resolve,reject)=>{
            try{
                fetch(`https://maps.googleapis.com/maps/api/place/autocomplete/json?input=${city}&components=country:in&offset=3&types=geocode&key=AIzaSyDbashfxtPTBcYUx5OoAXcGxc1xwVJl2Os`
                    )
                    .then(response => response.json())
                    .then(response2 => {
                        //console.log(response2)
                        result.status = true;
                        result.message = "city found";
                        result.data = response2.predictions;
                        resolve(result)
                    }).catch((error) =>{
                        result.status = false;
                        result.message = error
                        result.data = undefined;
                        reject(result)
                    });
                
            }
            catch(error){
                console.log(error)
                result.status = false;
                result.message = error;
                result.data = undefined;
                resolve(result)
            }
        })
    },
    getCitiesLatLong: (place) => {
        let result = {}
        return new Promise(async(resolve,reject)=>{
            try{
                fetch(`https://maps.googleapis.com/maps/api/geocode/json?address=${place}&key=AIzaSyDbashfxtPTBcYUx5OoAXcGxc1xwVJl2Os`
                    
                    )
                    .then(response => response.json())
                    .then(response2 => {
                        //console.log(response2.result.geometry)
                        result.status = true;
                        result.message = "lat long found";
                        result.data = response2.results;
                        resolve(result)
                    }).catch((error) =>{
                        result.status = false;
                        result.message = error
                        result.data = undefined;
                        reject(result)
                    });
                
            }
            catch(error){
                result.status = false;
                result.message = error;
                result.data = undefined;
                resolve(result)
            }
        })
    },
    getGpsCity: (lat,long) => {
        let result = {}
        return new Promise(async(resolve,reject)=>{
            try{
                fetch(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${lat},${long}&key=AIzaSyDbashfxtPTBcYUx5OoAXcGxc1xwVJl2Os`
                    )
                    .then(response => response.json())
                    .then(response2 => {
                        //console.log(response2)
                        result.status = true;
                        result.message = "city place id found";
                        result.data = response2.results;
                        resolve(result)
                    }).catch((error) =>{
                        result.status = false;
                        result.message = error
                        result.data = undefined;
                        reject(result)
                    });
                
            }
            catch(error){
                console.log(error)
                result.status = false;
                result.message = error;
                result.data = undefined;
                resolve(result)
            }
        })
    },
    distanceCalculate: (pickLat,pickLong,dropLat,dropLong) => {
        let result = {}
        return new Promise(async(resolve,reject)=>{
            try{
                fetch(`https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=${pickLat},${pickLong}&destinations=${dropLat},${dropLong}&key=AIzaSyDbashfxtPTBcYUx5OoAXcGxc1xwVJl2Os`
                    )
                    .then(response => response.json())
                    .then(response2 => {
                        //console.log(response2)
                        result.status = true;
                        result.message = "distance Found";
                        result.data = response2;
                        resolve(result)
                    }).catch((error) =>{
                        result.status = false;
                        result.message = error
                        result.data = undefined;
                        reject(result)
                    });
                
            }
            catch(error){
                console.log(error)
                result.status = false;
                result.message = error;
                result.data = undefined;
                resolve(result)
            }
        })
    },
    offerUserRide: (uid,pickLat,pickLong,pickUpVal,pickUpAdd,dropLat,dropLong,dropUpVal,dropUpAdd,
                    ridePickTime,rideDropTime,rideDistance,carid,seat,fare) => {
        let result = {}
        return new Promise(async(resolve,reject)=>{
            try{ 
                fetch('http://stsmentor.com/zozocar_admin/api/v1/ride/addride', 
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                    'Content-Type': 'application/json',
                    },
                    body:JSON.stringify( {
                        API_KEY:"test@123*",
                        ride_json :JSON.stringify([{"ride_user_id":uid,
                                    "ride_pick_lat":pickLat,
                                    "ride_pick_long":pickLong,
                                    "ride_pick_city":pickUpVal,
                                    "ride_pick_address":pickUpAdd,
                                    "ride_drop_lat":dropLat,
                                    "ride_drop_long":dropLong,
                                    "ride_drop_city":dropUpVal,
                                    "ride_drop_address":dropUpAdd,
                                    "inbetween_cities":"",
                                    "ride_pick_timestamp":ridePickTime,
                                    "ride_drop_timestamp":rideDropTime,
                                    "ride_distance":rideDistance,
                                    "ride_is_stopover":[],
                                    "ride_car_id":carid,
                                    "ride_middleseat_empty":"0",
                                    "ride_seats_offered":seat,
                                    "ride_book_instant":"1",
                                    "ride_seat_price":fare,
                                    "ride_about":"about is here",
                                    "ride_status":"online"}
                                ])
                            
                            })  
                    })
                    .then(response => response.json())
                    .then(response2 => {
                        //console.log(ride_json)
                        result.status = response2.Status;
                        result.message = response2.Message;
                        result.data = response2.data;
                        resolve(result)
                    }).catch((error) =>{
                        result.status = false;
                        result.message = error
                        result.data = undefined;
                        reject(result)
                    });
                
            }
            catch(error){
                result.status = false;
                result.message = error;
                result.data = undefined;
                resolve(result)
            }
        })
    },
    searchRide: (pickLat,pickLong,pickUpCity,pickUpAddress,dropLat,dropLong,dropUpCity,dropUpAddress,pickTime) => {
        let result = {}
        return new Promise(async(resolve,reject)=>{
            try{ 
                fetch('http://stsmentor.com/zozocar_admin/api/v1/ride/search', 
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                                            API_KEY : "test@123*",
                                            src_lat:pickLat,
                                            src_long:pickLong,
                                            src_city:pickUpCity,
                                            src_address:pickUpAddress,
                                            dest_lat:dropLat,
                                            dest_long:dropLong,
                                            dest_city:dropUpCity,
                                            dest_address:dropUpAddress,
                                            pickup_time:pickTime

                            
                            }),     
                    })
                    .then(response => response.json())
                    .then(response2 => {
                        //console.log(response2)
                        result.status = response2.Status;
                        result.message = response2.Message;
                        result.data = response2.data;
                        resolve(result)
                    }).catch((error) =>{
                        result.status = false;
                        result.message = error
                        result.data = undefined;
                        reject(result)
                    });
                
            }
            catch(error){
                result.status = false;
                result.message = error;
                result.data = undefined;
                resolve(result)
            }
        })
    },
    bookUserRide: (rideId,OnrId,uid,pickLat,pickLong,pickUpAddress,dropLat,dropLong,dropUpAddress,
                    seatBook,fare,seatAvl) => {
        let result = {}
        return new Promise(async(resolve,reject)=>{
            try{ 
                fetch('http://stsmentor.com/zozocar_admin/api/v1/ride/bookride', 
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                                            API_KEY : "test@123*",
                                            ride_id:rideId,
                                            ride_owner_id:OnrId,
                                            user_id:uid,
                                            src_lat:pickLat,
                                            src_long:pickLong,
                                            src_city:pickUpAddress,
                                            dest_lat:dropLat,
                                            dest_long:dropLong,
                                            dest_city:dropUpAddress,
                                            seats:seatBook,
                                            seat_price:fare,
                                            ride_book_instant:"1",
                                            src_match_type:"origin",
                                            dest_match_type:"origin",
                                            seats_availability:seatAvl


                            
                            }),     
                    })
                    .then(response => response.json())
                    .then(response2 => {
                        //console.log(response2)
                        result.status = response2.Status;
                        result.message = response2.Message;
                        result.data = response2.data;
                        resolve(result)
                    }).catch((error) =>{
                        result.status = false;
                        result.message = error
                        result.data = undefined;
                        reject(result)
                    });
                
            }
            catch(error){
                result.status = false;
                result.message = error;
                result.data = undefined;
                resolve(result)
            }
        })
    },
    getUserRides: (uid,mode) => {
        let result = {}
        return new Promise(async(resolve,reject)=>{
            try{ 
                fetch('http://stsmentor.com/zozocar_admin/api/v1/ride/getuserrides', 
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                                            API_KEY : "test@123*",
                                            user_id: uid,
                                            ride_status: mode
    
                            
                            }),     
                    })
                    .then(response => response.json())
                    .then(response2 => {
                        //console.log(response2)
                        result.status = response2.Status;
                        result.message = response2.Message;
                        result.data = response2.data;
                        resolve(result)
                    }).catch((error) =>{
                        result.status = false;
                        result.message = error
                        result.data = undefined;
                        reject(result)
                    });
                
            }
            catch(error){
                result.status = false;
                result.message = error;
                result.data = undefined;
                resolve(result)
            }
        })
    },

}

export default API;