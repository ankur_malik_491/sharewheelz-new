import React from 'react'; 
import{Text,View} from "react-native";
import AppIntroSlider from 'react-native-app-intro-slider';
import { responsiveWidth, responsiveFontSize ,responsiveHeight} from 'react-native-responsive-dimensions';


export default class Walk extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }
  _onDone = () => {
    this.props.navigation.navigate("auth");
  };
  renderfun=() =>{
   var slides=[];
    return(
      slides = [
        {
          key: 's1',
          title:"Welcome To YOYO Rooms",
          text: 'YOYO Rooms is a hotel booking service that helps you find and book one of our unique hotel to escape the ordinary.',
          titleStyle: {color:"#0246b1",fontSize:responsiveFontSize(3.5),fontWeight:"bold"},
          textStyle: {color:"#000",fontSize:responsiveFontSize(2.2)},
          image: require('../../../assets/splash.png'),
          imageStyle: {height:responsiveWidth(60),width:responsiveWidth(45)},
          backgroundColor:"#fff"
        },
        {
          key: 's2',
          title: 'Find best deals',
          text: 'Find best deals for any season from cosy country homes to cities best hotels.',
          titleStyle: {color:"#0246b1",fontSize:responsiveFontSize(3.5),fontWeight:"bold"},
          textStyle: {color:"#000",fontSize:responsiveFontSize(2.2)},
          image: require('../../../assets/hoteldeals.png'),
          imageStyle: {height:responsiveHeight(25),width:responsiveWidth(75)},
          backgroundColor:"#fff"
        },
        
        {
          key: 's3',
          title: "Book your date ",
          text: 'Choose the Inn,Hotels that suits on the day you want, you are the boss! ',
          titleStyle: {color:"#0246b1",fontSize:responsiveFontSize(3.5),fontWeight:"bold"},
          textStyle: {color:"#000",fontSize:responsiveFontSize(2.2)},
          image: require('../../../assets/Hoteldate.png'),
          imageStyle: {height:responsiveHeight(30),width:responsiveWidth(70),borderRadius:responsiveWidth(5)},
          backgroundColor:"#fff"
        },
        {
          key: 's4',
          title: "Refer and earn money",
          text: 'Invite your friends to YOYO Rooms and get 100 yoyo rupee both for each referal. ',
          titleStyle: {color:"#0246b1",fontSize:responsiveFontSize(3.5),fontWeight:"bold"},
          textStyle: {color:"#000",fontSize:responsiveFontSize(2.2)},
          image: require('../../assets/refer.png'),
          imageStyle: {height:responsiveHeight(25),width:responsiveWidth(60),borderRadius:responsiveWidth(5)},
          backgroundColor:"#fff"
        },
      
      ]
    )
  }
  _renderNextButton = () => {return(<View style={{display:"none"}}></View>)};
  _renderDoneButton = () => {
    return (
      <View style={{alignItems:"flex-end",justifyContent:"center"}}>
        <View style={{height:responsiveHeight(5),width:responsiveWidth(30),backgroundColor:"#0652dd",
                  alignItems:"center",justifyContent:"center",borderRadius:responsiveWidth(3)}}>
          <Text style={{color:"#fff",fontSize:responsiveFontSize(2.5)}}>Get Started</Text>
        </View>
      </View>
    );
  };
  render() {
      return (
          <AppIntroSlider
            slides={this.renderfun()}
            onDone={this._onDone}
            showNextButton={false}
            showNextButton={false}
            bottomButton={true}
            renderDoneButton={this._renderDoneButton}
            renderNextButton={this._renderNextButton}
            activeDotStyle={{backgroundColor:"#0652dd"}}
            dotStyle={{borderColor:"#0652dd",borderWidth:1}}
          />
      );
  }
}


