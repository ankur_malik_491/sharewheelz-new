import React from 'react';
import {Text, View,TouchableOpacity,TouchableWithoutFeedback,Keyboard,ImageBackground} from 'react-native';
import {Container,Icon,Input,Content,Toast,Card,CardItem} from "native-base";
import {responsiveWidth,responsiveHeight,responsiveFontSize} from "react-native-responsive-dimensions";
import KeyboardSpacer from 'react-native-keyboard-spacer';
import Spinner from 'react-native-loading-spinner-overlay';
import Display from 'react-native-display';
import API from "../../../../allApi";

export default class Forgot extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      userEmail : "",
      loading : false,
      showLoader:false,
      uEmailColor:"grey",
      enable:false,
      enableBase:false,
      enableImagView:false,
      enableImg:false,
      enableCard:false

    };
  }
  componentDidMount = () => {
   this.setState({enable:true})
    setTimeout(()=>{
      this.setState({enableCard:true}) 
      setTimeout(()=>{
        this.setState({enableClose:true}) 
    },500);
  },1500);
  } 
  validateUserEmail = ()=>{
    var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    if(emailPattern.test(this.state.userEmail)){
        return true;
    }
    return false;
  }
  
  resetUserPassword = async () =>{
    if(!this.validateUserEmail()){
      this.setState({uEmailColor:"red"});
      Toast.show({
        text:"please fill email in correct format (abc@email.com)",
        textStyle:{textAlign:"center"},
        duration:2000,
        style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
      });
    }
    else{
          try { 
                this.setState({showLoader:true,loading:true,uEmailColor:'green'})
                API.resetPassword(this.state.userEmail)
                .then((response)=>{
                  //console.log(response)
                  if(response.status){
                    this.setState({uEmailColor:'green'})
                    this.setState({showLoader:false,loading:false})
                      Toast.show({
                        text:response.message,
                        textStyle:{textAlign:"center"},
                        duration:2000,
                        style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"green"}
                      });
                    }
                  else {
                    this.setState({uEmailColor:'red'})
                    this.setState({showLoader:false,loading:false})
                    Toast.show({
                        text:response.message,
                        textStyle:{textAlign:"center"},
                        duration:2000,
                        style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
                      });
                  }
                })
              } 
              catch (error) {
                    this.setState({uEmailColor:'red'})
                    this.setState({loading:false,showLoader:false})
                    Toast.show({
                      text:error,
                      textStyle:{textAlign:"center"},
                      duration:2000,
                      style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
                    });
              }
    }
  }
 render(){
 return ( 
      <View style={{flex:1}} >
        <Container >
          <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <ImageBackground source={require("../../../assets/auth.png")} resizeMode="stretch" 
                            style={{width:responsiveWidth(100),height:responsiveHeight(100)}}>
              <View style={{marginTop:responsiveHeight(5),flexDirection:"row",justifyContent:"space-between"}}>
                <View style={{marginLeft:responsiveWidth(5)}}>
                  <Display enable={this.state.enable} 
                          enterDuration={2000} 
                          enter="slideInLeft">
                    <Text style={{color:'#fff',fontSize:responsiveFontSize(3.5),fontWeight:"bold",fontFamily:""}}>
                    Trouble Logging In?</Text>
                  </Display>
                </View>
                <View style={{marginRight:responsiveWidth(2),alignItems:"center",justifyContent:"center"}}>
                  <Display enable={this.state.enableClose} 
                          enterDuration={2000} 
                          enter="fadeIn">
                    <TouchableOpacity onPress={()=> this.props.navigation.goBack()} >
                      <Icon name="ios-close" style={{fontSize:responsiveFontSize(5),marginRight:10,color:"#fff"}}/>
                    </TouchableOpacity>
                  </Display>
                </View>
              </View>
              <Content >
                <View style={{justifyContent:"center",margin:responsiveHeight(3),marginTop:responsiveHeight(4)}}>
                  <Display enable={this.state.enableCard} 
                          enterDuration={2000} 
                          enter="bounceInUp">
                    <Card style={{backgroundColor:"#fff",borderRadius:responsiveWidth(5)}}>
                      <CardItem header bordered 
                          style={{backgroundColor:"#fff",borderRadius:responsiveWidth(5),borderBottomLeftRadius:0,
                                  borderBottomRightRadius:0}}>
                        <Text style={{fontWeight:"bold",fontSize:responsiveFontSize(2.5),color:"#0652dd"}}>Forgot Password ?</Text>
                      </CardItem>
                      <CardItem bordered>
                        <View>
                          <Text style={{fontSize:responsiveFontSize(2.3)}}>
                            Please enter the email you signed up with.We will send you a link to change your password.
                          </Text>
                          <View style={{paddingHorizontal:responsiveWidth(3),borderRadius: responsiveWidth(2),borderWidth:1,
                                    borderColor: this.state.uEmailColor,marginTop:responsiveHeight(4),marginBottom:responsiveHeight(4)}}>
                            <Text style={{position: 'absolute',top:responsiveHeight(-1.5),left: responsiveHeight(2),
                                        fontWeight: 'bold',backgroundColor:"#fff"}}>
                              Registered Email
                            </Text>
                            <View style={{flex:1,flexDirection:"row",justifyContent:"center",alignItems:"center"}}>
                              <Input value={this.state.userEmail}
                                    onChangeText={(userEmail) => this.setState({userEmail})}
                                    autoCapitalize = "none"
                                />
                              <Icon name="ios-mail" style={{color:"#000",fontSize:responsiveFontSize(4)}}/>
                            </View>
                          </View> 
                        </View> 
                      </CardItem>
                      <CardItem footer bordered style={{borderRadius:responsiveWidth(5),alignItems:"center",justifyContent:"center"}}>
                        <TouchableOpacity onPress={()=>   this.resetUserPassword()}
                          style={{alignItems:"center",justifyContent:"center",width:responsiveWidth(60),height:responsiveHeight(7),
                          backgroundColor:!this.state.loading?"#0652dd":"grey",borderRadius:responsiveWidth(3)}}>
                          <Text style={{fontSize:responsiveFontSize(2),color: '#fff'}}>
                            {this.state.loading?"Please Wait...":"Send Password Reset Link"}
                          </Text>
                        </TouchableOpacity>
                      </CardItem>
                    </Card>
                  </Display>
                </View>
              </Content>  
            <KeyboardSpacer/>
            </ImageBackground>
          </TouchableWithoutFeedback>
        </Container>
        <Spinner animation="fade"
                visible={this.state.showLoader}
                textContent={'Loading...'}
                textStyle={{color:"#fff"}}
              />
      </View>
 )}
}


