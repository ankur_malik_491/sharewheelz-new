import React from 'react';
import {Text, View,TouchableOpacity,AsyncStorage,TouchableWithoutFeedback,Keyboard,ImageBackground,Image} from 'react-native';
import {Container,Icon,Input, Content,Toast,Picker} from "native-base";
import {responsiveWidth,responsiveHeight,responsiveFontSize} from "react-native-responsive-dimensions";
import KeyboardSpacer from 'react-native-keyboard-spacer';
import Spinner from 'react-native-loading-spinner-overlay';
import Display from 'react-native-display';
import API from "../../../../allApi";
import Localstorage from "../../../../async";

export default class Signup extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      username: '',
                    userFirstName:"",
                    userLastName:"",
                    userEmail:"",
                    userDobYear:"",
                    userGender:"",
                    userPassword:"",
                    userConPassword:"",
                    userReferCode:"",
                    selectedDobValue:"Select your D.O.B year",
                    selectedGenderValue:"select your gender",
                    userPasswordShow:"ios-eye",
                    userConPasswordShow:"ios-eye",
                    userPasswordVisibility:true,
                    userConPasswordVisibility:true,
                    userDobYearItems:[],
                    userGenderItems:[{ label: 'Select your gender', value: '' },
                                    { label: 'Male', value: 'Male' },
                                    { label: 'Female', value: 'Female' },
                                    { label: 'Other', value: 'Other' }
                                  ],
                    loading: false,
                    showLoader:false,
                    enable:false,
                    enableBase:false,
                    enableImagView:false,
                    enableImg:false,
                    enableCard:false

    };
    this.handleUserDobYear();
  }
  componentDidMount = () => {
  this.setState({enable:true})
    setTimeout(()=>{
      this.setState({enableBase:true})
      setTimeout(()=>{
        this.setState({enableImagView:true}) 
        setTimeout(()=>{
          this.setState({enableImg:true}) 
          setTimeout(()=>{
            this.setState({enableCard:true}) 
           
        },500);
      },500);
    },700);
  },1500);
  } 
  handleUserPasswordVisibility = () => {
    this.setState({
      userPasswordShow: this.state.userPasswordShow === 'ios-eye' ? 'ios-eye-off' : 'ios-eye',
      userPasswordVisibility: !this.state.userPasswordVisibility
    })
  }
  handleUserDobYear = () => {
    this.state.userDobYearItems.push({label:"Select your D.O.B year",value:""}) 
    for(var i =2002;i>=1940;i--){
        var year = i.toString();
        this.state.userDobYearItems.push({label:year,value:year}) 
    }
  }
  handleUserConfirmPasswordVisibility = () => {
    this.setState({
      userConPasswordShow: this.state.userConPasswordShow === 'ios-eye' ? 'ios-eye-off' : 'ios-eye',
      userConPasswordVisibility: !this.state.userConPasswordVisibility
    })
  }
  validateUserEmail = ()=>{
    var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    if(emailPattern.test(this.state.userEmail)){
        return true;
      }
    return false;
  }
  validateUserPassword = ()=>{
    var passPattern = /^(?=.*[a-z])(?=.*\d)(?=.*[!@#$%^&*_-])[A-Za-z\d!@#$%^&*_-]{8,}$/;
    if(passPattern.test(this.state.userPassword)){
        return true;
      }
    return false;
  }
  handleOnSingup = async () => {
    const { userFirstName,userLastName,
          userEmail,userDobYear,userGender,
          userPassword,userConPassword,userReferCode } = this.state
    if( userFirstName == "" || userLastName == "" || userEmail == "" || userDobYear == "" || userGender == "" 
       || userPassword == "" || userConPassword == "")
       {
        this.setState({ufnColor:"red",ulnColor:"red",uEmailColor:"red",uDobColor:"red",uGenderColor:"red",uPassColor:"red",
                      uConPassColor:"red",uReferalColor:"#000"})
        Toast.show({
              text:"Please fill the required fields",
              textStyle:{textAlign:"center"},
              duration:2000,
              style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
            });
      }
    else
      {
        if(!this.validateUserEmail())
          {
            this.setState({ufnColor:"green",ulnColor:"green",uEmailColor:"red",uDobColor:"green",uGenderColor:"green",
                          uPassColor:"#000",uConPassColor:"#000",uReferalColor:"#000"})
            Toast.show({
                  text:"Please fill email in correct format (abc@email.com)",
                  textStyle:{textAlign:"center"},
                  duration:2000,
                  style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
                });
          }
        else if(!this.validateUserPassword())
          {
            this.setState({ufnColor:"green",ulnColor:"green",uEmailColor:"green",uDobColor:"green",uGenderColor:"green",
                          uPassColor:"red",uConPassColor:"#000",uReferalColor:"#000"})
            Toast.show({
              text:"Password must include minimum eight characters, at least one letter, one number and one special character",
              textStyle:{textAlign:"center"},
              duration:2000,
              style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
            }); 
          }
        else if(userPassword !== userConPassword)
          {
            this.setState({ufnColor:"green",ulnColor:"green",uEmailColor:"green",uDobColor:"green",uGenderColor:"green",
                          uPassColor:"red",uConPassColor:"red",uReferalColor:"#000"})
            Toast.show({
              text:"Password must include be same",
              textStyle:{textAlign:"center"},
              duration:2000,
              style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
            }); 
          }
        else
          { 
            try { 
                this.setState({ufnColor:"green",ulnColor:"green",uEmailColor:"red",uDobColor:"green",uGenderColor:"green",
                              uPassColor:"green",uConPassColor:"green",uReferalColor:"#000"})
                this.setState({showLoader:true,loading:true})
                API.signupWithEmail(userFirstName,userLastName,userDobYear,userGender,userEmail,userPassword,userReferCode)
                .then((response)=>{
                  //console.log(response);
                  if(response.status){
                    Toast.show({
                      text:"Account Created Successfully",
                      textStyle:{textAlign:"center"},
                      duration:2000,
                      style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"green"}
                    });
                    this.setState({showLoader:false,loading:false})
                    Localstorage.storeItem("UID",response.data.u_id);
                    let picUrl = "https://i.ya-webdesign.com/images/profile-avatar-png-4.png"
                    Localstorage.storeItem("PhotoUrl",picUrl);
                    globalThis.profileimg = picUrl;
                    this.props.navigation.navigate("main")
                  }
                  else {
                    this.setState({ufnColor:"red",ulnColor:"red",uEmailColor:"red",uDobColor:"red",uGenderColor:"red",
                                    uPassColor:"red",uConPassColor:"red",uReferalColor:"#000"})
                    Toast.show({
                        text:response.message,
                        textStyle:{textAlign:"center"},
                        duration:2000,
                        style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
                      });
                      this.setState({showLoader:false,loading:false})
                  }
              
              }).catch((error) =>{
                  Toast.show({
                    text:error,
                    textStyle:{textAlign:"center"},
                    duration:2000,
                    style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
                  });
                  this.setState({showLoader:false,loading:false})
              });
            } 
          catch (error) {
              this.setState({ufnColor:"red",ulnColor:"red",uEmailColor:"red",uDobColor:"red",uGenderColor:"red",
                            uPassColor:"red",uConPassColor:"red",uReferalColor:"#000"})
              this.setState({loading:false,showLoader:false})
              Toast.show({
                text:error,
                textStyle:{textAlign:"center"},
                duration:2000,
                style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
              });
            }
        }
      }
   }
render(){
 return ( 
      <View style={{flex:1}} >
        <Container >
          <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <ImageBackground source={require("../../../assets/auth.png")} resizeMode="stretch" 
                            style={{width:responsiveWidth(100),height:responsiveHeight(100)}}>
              <View style={{marginTop:responsiveHeight(12),marginLeft:responsiveWidth(12)}}>
                <Display enable={this.state.enable} 
                        enterDuration={2000} 
                        enter="slideInLeft">
                  <Text style={{color:'#fff',fontSize:responsiveFontSize(4),fontWeight:"bold",fontFamily:""}}>SHAREWHEELZ</Text>
                  <Display enable={this.state.enableBase} 
                      enterDuration={2000} 
                      enter="fadeIn">
                    <Text style={{color:'#fff',fontSize:responsiveFontSize(2.5),fontWeight:"bold",fontFamily:""}}>
                      Save Fuel Car Pool</Text>
                  </Display>
                </Display>
              </View>
              <Display enable={this.state.enableImagView} 
                    enterDuration={2000} 
                    enter="pulse">
                <View style={{backgroundColor:"#0652dd",height:responsiveWidth(20),width:responsiveWidth(20),marginLeft:responsiveWidth(40),
                              marginTop:responsiveHeight(10),borderRadius:responsiveWidth(5),borderWidth:2,borderColor:"#fff"}}>
                  <Display enable={this.state.enableImg} 
                          enterDuration={2000} 
                          enter="zoomIn">
                    <Image source={require("../../../assets/sharewheelzlogo1.png")} 
                          style={{height:responsiveWidth(18),width:responsiveWidth(18),borderRadius:responsiveWidth(5)}}/>
                  </Display>
                </View>
              </Display>
              <Content style={{marginTop:responsiveHeight(5)}}>
              <View style={{justifyContent:"center",margin:responsiveHeight(3),marginTop:responsiveHeight(2)}}>
                <Display enable={this.state.enableCard} 
                        enterDuration={2000} 
                        enter="bounceInUp">
                    <Text style={{color:'#000',fontWeight:"bold",fontSize:responsiveFontSize(2.5)}}>
                      Welcome,</Text>
                    <Text style={{color:'#000',fontSize:responsiveFontSize(2.5)}}>
                      Sign Up to continue.
                    </Text>
                    <View style={ {margin:responsiveWidth(1),paddingHorizontal:responsiveWidth(3),borderRadius: responsiveWidth(2),
                      borderWidth:1,borderColor: this.state.ufnColor,marginTop:responsiveHeight(4)}}>
                        <Text style={{position: 'absolute',top:responsiveHeight(-1.5),left: responsiveHeight(2),
                                    fontWeight: 'bold',backgroundColor:"#fff"}}>
                          First Name
                        </Text>
                        <View style={{flex:1,flexDirection:"row",justifyContent:"center",alignItems:"center"}}>
                          <Input value={this.state.userFirstName}
                                    onChangeText={(userFirstName) => this.setState({userFirstName})}
                                    autoCapitalize = "characters"
                                    placeholder="Enter your First Name"
                                    keyboardType="ascii-capable"


                              />
                          <Icon name="user" type="Entypo" style={{fontSize:responsiveFontSize(4),color:"#000"}} />
                        </View>
                    </View>
                    <View style={ {margin:responsiveWidth(1),paddingHorizontal:responsiveWidth(3),borderRadius: responsiveWidth(2),
                      borderWidth:1,borderColor: this.state.ulnColor,marginTop:responsiveHeight(4)}}>
                        <Text style={{position: 'absolute',top:responsiveHeight(-1.5),left: responsiveHeight(2),
                                    fontWeight: 'bold',backgroundColor:"#fff"}}>
                          Last Name
                        </Text>
                        <View style={{flex:1,flexDirection:"row",justifyContent:"center",alignItems:"center"}}>
                          <Input value={this.state.userLastName}
                                    onChangeText={(userLastName) => this.setState({userLastName})}
                                    autoCapitalize = "words"
                                    placeholder="Enter your Last Name"
                                    keyboardType="ascii-capable"
                              />
                          <Icon name="user" type="Entypo" style={{fontSize:responsiveFontSize(4),color:"#000"}} />
                        </View>
                    </View>  
                    <View style={ {margin:responsiveWidth(1),paddingHorizontal:responsiveWidth(3),borderRadius: responsiveWidth(2),
                      borderWidth:1,borderColor: this.state.uEmailColor,marginTop:responsiveHeight(4)}}>
                        <Text style={{position: 'absolute',top:responsiveHeight(-1.5),left: responsiveHeight(2),
                                    fontWeight: 'bold',backgroundColor:"#fff"}}>
                          Email
                        </Text>
                        <View style={{flex:1,flexDirection:"row",justifyContent:"center",alignItems:"center"}}>
                          <Input value={this.state.userEmail}
                                    onChangeText={(userEmail) => this.setState({userEmail})}
                                    autoCapitalize = "none"
                                    placeholder="Enter your email"
                                    keyboardType="email-address"
                              />
                          <Icon name="ios-mail" style={{color:"#000",fontSize:responsiveFontSize(4)}}/>
                        </View>
                    </View>
                    <View style={{margin:responsiveWidth(1),paddingHorizontal:responsiveWidth(3),borderRadius: responsiveWidth(2),
                      borderWidth:1,borderColor: this.state.uDobColor,marginTop:responsiveHeight(4)}}>
                        <Text style={{position: 'absolute',top:responsiveHeight(-1.5),left: responsiveHeight(2),
                                    fontWeight: 'bold',backgroundColor:"#fff"}}>
                          D.O.B Year
                        </Text>
                        <Picker
                          mode="dropdown"
                          selectedValue={this.state.userDobYear}
                          onValueChange={(value)=> this.setState({userDobYear:value})}
                        >
                          {this.state.userDobYearItems.map((item, i) => (
                            <Picker.Item key={i} label={item.label} value={item.value} />
                          ))}
                        </Picker>
                    </View> 
                    <View style={{margin:responsiveWidth(1),paddingHorizontal:responsiveWidth(3),borderRadius: responsiveWidth(2),
                      borderWidth:1,borderColor: this.state.uGenderColor,marginTop:responsiveHeight(4)}}>
                        <Text style={{position: 'absolute',top:responsiveHeight(-1.5),left: responsiveHeight(2),
                                    fontWeight: 'bold',backgroundColor:"#fff"}}>
                          Gender
                        </Text>
                        <Picker
                          mode="dropdown"
                          selectedValue={this.state.userGender}
                          onValueChange={(value)=> this.setState({userGender:value})}
                        >
                          {this.state.userGenderItems.map((item, i) => (
                            <Picker.Item key={i} label={item.label} value={item.value} />
                          ))}
                        </Picker>
                    </View> 
                    <View style={{margin:responsiveWidth(1),paddingHorizontal:responsiveWidth(3),borderRadius: responsiveWidth(2),
                      borderWidth:1,borderColor: this.state.uPassColor,marginTop:responsiveHeight(4)}}>
                        <Text style={{position: 'absolute',top:responsiveHeight(-1.5),left: responsiveHeight(2),
                                    fontWeight: 'bold',backgroundColor:"#fff"}}>
                          Password
                        </Text>
                        <View style={{flex:1,flexDirection:"row",justifyContent:"center",alignItems:"center"}}>
                            <Input value={this.state.userPassword}
                                    onChangeText={(userPassword)=>this.setState({userPassword})}
                                    secureTextEntry={this.state.userPasswordVisibility}
                                    placeholder="Enter your password"
                                    keyboardType="ascii-capable"
                              />
                          <TouchableOpacity onPress={() => this.handleUserPasswordVisibility()}>
                            <Icon name={this.state.userPasswordShow} style={{color:"#000",fontSize:responsiveFontSize(4)}}/>
                          </TouchableOpacity>
                        </View>
                    </View> 
                    <View style={{margin:responsiveWidth(1),paddingHorizontal:responsiveWidth(3),borderRadius: responsiveWidth(2),
                      borderWidth:1,borderColor: this.state.uConPassColor,marginTop:responsiveHeight(4)}}>
                        <Text style={{position: 'absolute',top:responsiveHeight(-1.5),left: responsiveHeight(2),
                                    fontWeight: 'bold',backgroundColor:"#fff"}}>
                          Confirm Password
                        </Text>
                        <View style={{flex:1,flexDirection:"row",justifyContent:"center",alignItems:"center"}}>
                            <Input value={this.state.userConPassword}
                                    onChangeText={(userConPassword)=>this.setState({userConPassword})}
                                    secureTextEntry={this.state.userConPasswordVisibility}
                                    placeholder="Enter your password"
                                    keyboardType="ascii-capable"
                              />
                          <TouchableOpacity onPress={() => this.handleUserConfirmPasswordVisibility()}>
                            <Icon name={this.state.userConPasswordShow} style={{color:"#000",fontSize:responsiveFontSize(4)}}/>
                          </TouchableOpacity>
                        </View>
                    </View>
                    <View style={{margin:responsiveWidth(1),paddingHorizontal:responsiveWidth(3),borderRadius: responsiveWidth(2),
                      borderWidth:1,borderColor: this.state.uReferalColor,marginTop:responsiveHeight(4)}}>
                        <Text style={{position: 'absolute',top:responsiveHeight(-1.5),left: responsiveHeight(2),
                                    fontWeight: 'bold',backgroundColor:"#fff"}}>
                          Referal Code
                        </Text>
                        <View style={{flex:1,flexDirection:"row",justifyContent:"center",alignItems:"center"}}>
                            <Input value={this.state.userReferCode}
                                    onChangeText={(userReferCode)=>this.setState({userReferCode})}
                                    placeholder="Enter your refer code (Optional)"
                                    keyboardType="numeric"
                              />
                            <Icon name="ios-pricetags" style={{color:"#000",fontSize:responsiveFontSize(4)}}/>
                        </View>
                    </View>
                    <View style={{alignItems:"center",marginTop:responsiveHeight(2)}}>
                      <TouchableOpacity onPress={()=>  this.handleOnSingup()}
                        style={{alignItems:"center",justifyContent:"center",width:responsiveWidth(50),height:responsiveHeight(7),
                        backgroundColor:"#0652dd",borderRadius:responsiveWidth(3)}}>
                        <Text style={{fontSize:responsiveFontSize(2),color: '#fff'}}>
                          {this.state.loading?"Please Wait...":"Sign Up"}
                        </Text>
                      </TouchableOpacity>
                    </View>
                    <View style={{alignItems:"center",marginTop:responsiveHeight(2)}}>
                      <TouchableOpacity onPress={()=> this.props.navigation.navigate('login')} style={{flexDirection:"row"}}>
                        <Text style={{fontSize:responsiveFontSize(2)}}>Already have an account ?</Text>
                        <Text style={{fontSize:responsiveFontSize(2),color:"#0652dd"}}> Sign In</Text>
                      </TouchableOpacity>
                    </View>
                </Display>
                
             </View>
             </Content>  
            <KeyboardSpacer/>
            
            </ImageBackground>
          </TouchableWithoutFeedback>
        </Container>
        <Spinner animation="fade"
                visible={this.state.showLoader}
                textContent={'Loading...'}
                textStyle={{color:"#fff"}}
              />
      </View>
 )}
}


