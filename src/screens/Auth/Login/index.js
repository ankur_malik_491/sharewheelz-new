import React from 'react';
import {Text, View,TouchableOpacity,AsyncStorage,TouchableWithoutFeedback,Keyboard,StatusBar,Image, Platform,StyleSheet,TextInput,Modal,Dimensions, ImageBackground} from 'react-native';
import {SocialIcon} from 'react-native-elements';
import {Container,Icon,Input, Content,Toast,Spinner} from "native-base";
import { SafeAreaView } from 'react-native-safe-area-context';
import {responsiveWidth,responsiveHeight,responsiveFontSize} from "react-native-responsive-dimensions";
import KeyboardSpacer from 'react-native-keyboard-spacer';
//import Spinner from 'react-native-loading-spinner-overlay';
import Carousel from 'react-native-looped-carousel';
import Display from 'react-native-display';
import API from "../../../../allApi";
import Localstorage from "../../../../async";


export default class Login extends React.Component {
  constructor(props){
      
    super(props)
    this.state = {
      images : [],
      mobile:"",
      loader:false,
      loader1:false,
      sliderloader:false,
      modal:false,
      seconds:120,
      OTP:"",
      otp1:"",otp2:"",otp3:"",otp4:"",

    }

}
componentDidMount= () => {
  this.setDevId();
  this.getslider();
}
  // componentDidMount = () => {
  //  this.setState({enable:true})
  //  this.setDevId();
  //   setTimeout(()=>{
  //     this.setState({enableBase:true})
  //     setTimeout(()=>{
  //       this.setState({enableImagView:true}) 
  //       setTimeout(()=>{
  //         this.setState({enableImg:true}) 
  //         setTimeout(()=>{
  //          this.setState({enableCard:true}) 
           
  //       },500);
  //     },500);
  //   },700);
     
  // },1500);
  // } 
  setDevId = () => {
    if(Platform.OS == "android"){
      let DevId ="android";
      Localstorage.storeItem("DevId",DevId);
    }
    else{
      let DevId ="ios";
      Localstorage.storeItem("DevId",DevId);
    }
  }
  getslider = async() => {
    await fetch('https://yoyorooms.net/app/Appapi/getbannerapi')
    .then(response => response.json())
   .then(response2 => {
    
    if(response2!=0){
        this.setState({images: response2,sliderloader:true})
        
    }
    else {
     
    }
   })
}

  _onLayoutDidChange = e => {
    const layout = e.nativeEvent.layout;
    this.setState({ size: { width: responsiveWidth(100), height :responsiveHeight(70)}});
  };

checkMob = () =>{
  var mob = this.state.mobile;
  if(mob.length != 10 || isNaN(mob) || mob < 6000000000 || mob.indexOf(" ")!=-1){
    
    Toast.show({
      text:"Enter a valid number!",
      textStyle:{textAlign:"center"},
      duration:2000,
      style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
    });
    
  }
  else{
    try{
      this.setState({loader:true})
      fetch("https://app.yoyorooms.net/Appapi/login_user", 
          {
              method: 'POST',
              headers: {
                  Accept: 'application/json',
                  'Content-Type': 'application/json',
              },
              body: JSON.stringify({
                          mobile  : mob   
                      }),     
          })
          .then(response => response.json())
          .then(response2 => {
            //console.log(response2)
              if(response2.status==1){
                this.setState({loader:false,modal:true})
                this.setState({userDetails:response2.user_Details,seconds:120})
                this.countSeconds();
                
               
              }
              else{
                this.setState({loader:false})
                Toast.show({
                  text:"Something Went Wrong!",
                  textStyle:{textAlign:"center"},
                  duration:2000,
                  style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
                });
              }
          }).catch((error) =>{
            this.setState({loader:false})
              // result.status = false;
              // result.data = undefined;
              // reject(result)
          });
      
  }
  catch(error){
    this.setState({loader:false})
      // result.status = false;
      // result.message = error;
      // result.data = undefined;
      // resolve(result)
  }
  }

}
resendOTP = () =>{
  var mob = this.state.mobile;
    try{
      fetch("https://app.yoyorooms.net/Appapi/login_user", 
          {
              method: 'POST',
              headers: {
                  Accept: 'application/json',
                  'Content-Type': 'application/json',
              },
              body: JSON.stringify({
                          mobile  : mob   
                      }),     
          })
          .then(response => response.json())
          .then(response2 => {
           // console.log(response2)
              if(response2.status==1){
                this.setState({userDetails:response2.user_Details,seconds:120})
                this.countSeconds();
               
              }
              else{
                this.setState({loader:false})
                Toast.show({
                  text:"Something Went Wrong!",
                  textStyle:{textAlign:"center"},
                  duration:2000,
                  style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
                });
              }
          }).catch((error) =>{
            this.setState({loader:false})
              // result.status = false;
              // result.data = undefined;
              // reject(result)
          });
      
  }
  catch(error){
    this.setState({loader:false})
      // result.status = false;
      // result.message = error;
      // result.data = undefined;
      // resolve(result)
  }
  

}
countSeconds = () => {
  
  let timer = setInterval(() => {
    
    var num = (this.state.seconds - 1).toString()
    //console.log(num)
  
    this.setState({seconds: num});
  }, 1000);
  this.setState({ timer });
}
validateOTP = async () =>{
  if(this.state.otp1=="" || this.state.otp2=="" || this.state.otp3=="" || this.state.otp4=="")
  {
      alert("Please enter a OTP!")
      // Toast.show({
      //   text:"Please enter a OTP!",
      //   textStyle:{textAlign:"center"},
      //   duration:2000,
      //   style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
      // });
  }
  else{
      this.setState({loader1:true})
      const userOtp = this.state.otp1+this.state.otp2+this.state.otp3+this.state.otp4;
      //console.log(userOtp)
      //console.log(this.state.userDetails[0].phone)
      
      if(this.state.userDetails[0].password == userOtp)
      {
        await AsyncStorage.setItem('uid',this.state.userDetails[0].id);
        // var uid = await AsyncStorage.getItem('uid');
        // var userid = uid;
        // console.log(userid)
        if(this.state.userDetails[0].name!=""){
          await AsyncStorage.setItem('uname',this.state.userDetails[0].name);
        //   var uname = await AsyncStorage.getItem('uname');
        // var username =uname;
        // console.log(username)
        }
        else{
          await AsyncStorage.setItem('uname','YOYO Guest');
        //   var uname = await AsyncStorage.getItem('uname');
        // var username =uname;
        // console.log(username)
        }
        await AsyncStorage.setItem('uPhone',this.state.userDetails[0].phone);
        // var uphone = await AsyncStorage.getItem('uPhone');
        // var userphone =uphone;
        // console.log(userphone)
        this.props.navigation.navigate("main")
        this.setState({modal:false})
        this.setState({loader1:false})

      }
      else{
        this.setState({loader1:false})
        alert("You entered a wrong OTP! Please enter a Valid OTP.")
        // Toast.show({
        //   text:"You entered a wrong OTP! Please enter a Valid OTP",
        //   textStyle:{textAlign:"center"},
        //   duration:2000,
        //   style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
        // });
      }
  }
}

  Slider = () => {
    if(this.state.images.length>0){
    return(  this.state.images.map((x,i) => {
      return(  
      <TouchableOpacity key={i} >
        <View  style={[{ backgroundColor: '#fff',alignContent:"center" }, this.state.size]}>
          <ImageBackground source={{uri : x.img_path}} resizeMode="stretch"
                    style={{height:responsiveHeight(70),width:responsiveWidth(100),
                    alignItems:"center",justifyContent:"flex-end"}} >
          <Text style={{bottom:30,color:'#fff',fontSize:18,textAlign:'center',margin:15,fontWeight:'bold',backgroundColor:'rgba(10,29,59,0.8)',padding:10}}>{x.img_text}</Text>
          </ImageBackground>
        </View>
      </TouchableOpacity>
      )
    }))
  }
  else {
    return(  
      <TouchableOpacity >
        <View  style={[{ backgroundColor: '#e6e6e6',alignItems:"center" ,justifyContent:"center",height:responsiveHeight(100),width:responsiveWidth(100),}, this.state.size]}>
          {/* <Image source={require("../../../../assets/splash.png")} style={{width:responsiveWidth(70),height:responsiveHeight(10)}} /> */}
          <Spinner color="#000" size="large" />
        </View>
      </TouchableOpacity>
  
      )
  }
}

// socialLogin = async (source) =>{
//  try { 
//       this.setState({showLoader:true})
//       API.SocialLoginData(source)
//       .then((response)=>{
//           if(response.status){
//             API.loginWithSocial(response.firstname,response.lastname,response.email,response.photoUrl,response.refferal)
//             .then((response)=>{
//               //console.log(response);
//               if(response.status){
//                 Toast.show({
//                   text:"Login Successfully",
//                   textStyle:{textAlign:"center"},
//                   duration:2000,
//                   style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"green"}
//                 });
//                 this.setState({showLoader:false})
//                 Localstorage.storeItem("UID",response.data.u_id);
//                 Localstorage.storeItem("PhotoUrl",response.data.u_profile_pic);
//                 globalThis.profileimg = response.data.u_profile_pic;
//                 this.props.navigation.navigate("main")
//               }
//               else {
//                 Toast.show({
//                     text:response.message,
//                     textStyle:{textAlign:"center"},
//                     duration:2000,
//                     style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
//                   });
//                   this.setState({showLoader:false})
//               }
//             }).catch((error) =>{
//               Toast.show({
//                 text:error,
//                 textStyle:{textAlign:"center"},
//                 duration:2000,
//                 style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
//               });
//               this.setState({showLoader:false,loading:false})
//             }); 
//           }
//           else{
//             Toast.show({
//               text:response.message,
//               textStyle:{textAlign:"center"},
//               duration:2000,
//               style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
//             });
//           }

//       }).catch((error) =>{
//         console.log(error)
//       });
//     }
//   catch (error) {
//       this.setState({showLoader:false})
//       Toast.show({
//         text:error,
//         textStyle:{textAlign:"center"},
//         duration:2000,
//         style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
//         });
//     }
// }
render(){
 return ( 
  <Container>
    <Content >
      <StatusBar barStyle='light-content' backgroundColor='#0a1d3b'></StatusBar>
      <View style={{flex:1}}>
        <View style={{ alignItems:'center' }} onLayout={this._onLayoutDidChange}>
          {(this.state.sliderloader) &&
            <Image source={{uri :'https://app.yoyorooms.net/assets/logo/front_logo.png'}} 
                  style={{height:40,width:117,alignSelf:'center',position:'absolute',top:20,zIndex:9999}}
          />}
          <Carousel
                delay={4000}
                style={this.state.size}
                autoplay
                //pageInfo
                scrollEventThrottle={0}
                bullets
                currentPage={2}
                //onAnimateNextPage={p => console.log(p)}
                >
                {this.Slider()}
              </Carousel>
        </View>
        <View  style={{backgroundColor:'rgba(255,255,255,1)',height:responsiveHeight(30),paddingHorizontal:20,borderTopRightRadius:20,borderTopLeftRadius:20}}>
          <View style={{flexDirection:'row',borderWidth:1,borderColor:'#0246B1',padding:5,marginTop:15,borderRadius:25}}>
            <TextInput style={{width:'15%',
            height:40,paddingLeft : 10,
              fontSize : 18,

            borderRightWidth:0,}}
            value = "+91"
            editable = {false}
            />  
            <TextInput style={styles.input}
                  maxLength = {10}
                  placeholder = 'Mobile Number'
                  placeholderTextColor = 'rgba(0,0,0,0.6)'
                  keyboardType = 'numeric'
                  returnKeyType = 'go'
                  autoCorrect ={false}
                  onChangeText={(text) => this.setState({mobile:text})}
                  //onSubmitEditing={(mobile) => this.checkMob(mobile)}
                  />
                {(!this.state.loader) &&
                  <View style={{height:40,backgroundColor:'#0246b1',width:'20%',borderRadius:20,justifyContent:'center',alignItems:'center'}}>
                    <TouchableOpacity onPress={()=>this.checkMob()}>
                      <Icon name="ios-arrow-forward" style={{fontWeight:'bold',fontSize:35,marginHorizontal:15,color:'#fff'}} />
                    </TouchableOpacity>
                  </View>
                }
                {(this.state.loader) &&
                  <View style={{height:40,backgroundColor:'#0246b1',width:'20%',borderRadius:20,justifyContent:'center',alignItems:'center'}}>
                    <Spinner color="#fff" size="small" />
                  </View>
                }     
          </View>
          <View style={{marginTop:10}}>
            <Text style={{textAlign:'center'}}>Or Login With</Text>
          </View>
          <View style={{flexDirection:'row',justifyContent:"space-between",marginTop:10}}>
            <View style={{width:'45%'}}>
              <Image source={{uri : 'https://i.imgur.com/EyjZoLP.png'}} style={{width:'100%',height:50,borderRadius:25}} />
            </View>
            <View style={{width:'45%'}}>
              <Image source={{uri : 'https://i.imgur.com/OHnsgdd.png'}} style={{width:'100%',height:50,borderRadius:25}} />
            </View>
          </View>
        </View> 
      </View>
    </Content>
    <Modal
        animationType="slide"
        transparent={true}
        visible={this.state.modal}
        onRequestClose={() => this.setState({modal:false,})}
    >
        <Container>
          <View style={{flexDirection:"row",alignItems:"center",paddingHorizontal:"5%",marginTop:responsiveHeight(2)}} >
            <TouchableOpacity onPress={() => this.setState({modal:false,})}>
              <Icon name="ios-arrow-back" style={{fontWeight:'bold',fontSize:30,color:'#000'}} />
            </TouchableOpacity>
            <Text style={{fontWeight:'bold',fontSize:18,color:'#000',marginLeft:"2%"}}>
              WELCOME BACK!
            </Text>
          </View>
          <Content>
            <View style={{alignItems:"center",justifyContent:"center"}}>
              <Image source={require("../../../../assets/otp.png")} 
              style={{width:responsiveWidth(30),height:responsiveHeight(20),marginTop:"5%"}} />
            </View>
            <View style={{paddingHorizontal:responsiveWidth(5),justifyContent:"space-between",marginTop:responsiveHeight(3)}}>
              <Text style={{fontWeight:'bold',fontSize:18,color:'#000',}}>
                We have sent a 4 digit OTP on
              </Text>
              <View style={{flexDirection:"row",justifyContent:"space-between",marginTop:responsiveHeight(1)}}>
                <Text style={{fontWeight:'bold',fontSize:18,color:'#000',}}>
                  +91 {this.state.mobile}
                </Text>
                <TouchableOpacity onPress={() => this.setState({modal:false,seconds:120})}>
                  <Text style={{fontWeight:'bold',fontSize:18,color:'#fff',color:"#0246B1"}}>
                    Edit
                  </Text>
                </TouchableOpacity>
              </View>
              <View style={{alignItems:"center",marginTop:"10%"}}>
                <Text style={{fontSize:20,color:'#000',}}>
                  Enter the OTP 
                </Text>
              </View>
              <View style={{alignItems:"center",marginTop:"10%",flexDirection:"row",justifyContent:"space-evenly"}}>
                <View style={{borderWidth:2,alignItems:"center",justifyContent:"center",height:50,width:50}}>
                  <TextInput style={{fontSize:22}}
                    maxLength = {1}
                    ref="first"
                    keyboardType = 'numeric'
                    returnKeyType="go"
                    autoFocus={true}
                    autoCorrect ={false}
                    onChangeText={(val) => {this.refs['second'].focus(),this.setState({otp1:val})}}
                    />
                  </View>
                  <View style={{borderWidth:2,alignItems:"center",justifyContent:"center",height:50,width:50}}>
                    <TextInput style={{fontSize:22}}
                      maxLength = {1}
                      ref="second"
                      keyboardType = 'numeric'
                      returnKeyType="go"
                      blurOnSubmit={false}
                      autoCorrect ={false}
                      onChangeText={(val) => {this.refs['third'].focus(),this.setState({otp2:val})}}
                    />
                  </View>
                  <View style={{borderWidth:2,alignItems:"center",justifyContent:"center",height:50,width:50}}>
                    <TextInput style={{fontSize:22}}
                      maxLength = {1}
                      ref="third"
                      keyboardType = 'numeric'
                      keyboardType = 'numeric'
                      returnKeyType="go"
                      blurOnSubmit={false}
                      autoCorrect ={false}
                      onChangeText={(val) => {this.refs['fourth'].focus(),this.setState({otp3:val})}}
                    />
                  </View>
                  <View style={{borderWidth:2,alignItems:"center",justifyContent:"center",height:50,width:50}}>
                    <TextInput style={{fontSize:22}}
                      maxLength = {1}
                      ref="fourth"
                      placeholderTextColor = 'rgba(0,0,0,0.6)'
                      keyboardType = 'numeric'
                      returnKeyType = 'go'
                      autoCorrect ={false}
                      onChangeText={(text) => this.setState({otp4:text})}
                    />
                  </View>
              </View>
              {(this.state.seconds<=0) &&
                <View style={{marginTop:"10%"}}>
                
                  <TouchableOpacity onPress={() => this.resendOTP()}
                        style={{alignItems:"center"}}>
                    <Text style={{fontWeight:'bold',fontSize:18,color:'#0246B1',}}>
                      Resend OTP
                    </Text>
                  </TouchableOpacity>
                </View>
              }
              {(this.state.seconds>0) &&
                <View style={{marginTop:"10%",alignItems:"center"}}>
                    <Text style={{fontSize:18,color:'#000',}}>
                      Resend OTP after : {this.state.seconds} sec
                    </Text>
                </View>
              }
              <View style={{marginTop:responsiveHeight(10)}}>
                <TouchableOpacity onPress={() => {this.validateOTP()}}
                      style={{alignItems:"center",paddingHorizontal:10,paddingVertical:10,
                      backgroundColor:this.state.loader1?"grey":"#0246B1",borderRadius:15}}>
                  <Text style={{fontWeight:'bold',fontSize:20,color:'#fff',}}>
                    {this.state.loader1?"Verifying ...":"Verify & Continue"}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          </Content>

        </Container>
    </Modal>
  </Container>
);
}
}

const styles = StyleSheet.create({
  container: {
      flex :1,
      backgroundColor : 'rgba(255,255,255,1)',
      //backgroundColor: 'rgba(246,248,224,0.5)',
      flexDirection : 'column',
  },
  logoContainer :
  {
      //alignItems : 'center',
      //justifyContent : 'center',
      flex : 1,
      marginTop : 40,
  },
  logo: {
      width : 100,
      height : 95,
  },
  otp :{
    width : 200,
    height :150,
  },
  title: {
      color : '#000',
      fontSize : 35,
      alignSelf : 'flex-start',
      marginTop : 5,
      marginLeft : 0,
      opacity : 0.9,
      //fontFamily : "Poppins-ExtraBold",
      fontWeight : '600',
  },
  welcometitle: {
      color : '#000',
      fontSize : 20,
      alignSelf : 'flex-start',
      
      marginLeft : 0,
      opacity : 0.9,
     // fontFamily : "BellotaText-Regular",
      fontWeight : '400',
  },
  txt: {
      color : '#000',
      fontSize : 16,
      alignSelf : 'flex-start',
      marginTop : 3,
      marginLeft : 0,
      opacity : 0.5,
     // fontFamily : "BellotaText-Regular",
      //fontFamily : "Poppins-ExtraBold",
      fontWeight : '400',
  },
  txt2: {
      color : '#000',
      fontSize : 16,
      alignSelf : 'flex-start',
      marginTop : 3,
      marginLeft : 0,
      
    //  fontFamily : "BellotaText-Regular",
      //fontFamily : "Poppins-ExtraBold",
      fontWeight : '400',
  },
  maxtitle: {
    color : '#000',
    fontSize : 28,
    textAlign : 'center',
    marginTop : 5,
    opacity : 0.9,
},
  uniname : {
    color : '#000',
      fontSize : 17,
      textAlign : 'center',
      marginTop : 5,
      opacity : 1,
      fontWeight : '500',
      //fontFamily: 'Raleway-Bold',
  },
  celebration : {
    color : '#000',
      fontSize : 20,
      textAlign : 'center',
      marginBottom : 10,
      opacity : 1,
  },
  subtitle : {
    color : '#ff6f31',
      fontSize : 15,
      textAlign : 'center',
      marginTop : -10,
      opacity : 0.9,
      marginBottom: 10,
  },
  infoConyainer : {
    position : "absolute",
    left: 0,
    right :0 ,
    bottom :0,
    height :120,
    padding :20, 
    //backgroundColor: 'red',
  },
  input :{
    height :40,
    backgroundColor : 'rgba(255,255,255,0.9)',
    color : '#000',
    paddingHorizontal : 10,
    fontSize : 18,
    
    borderColor : '#000',
    // borderBottomWidth:1,
    // borderTopWidth : 1,
    width : '65%',
  },
  button :{
    marginBottom: 0,
    alignItems : 'center',
    backgroundColor : '#3A8BBB',
    marginTop : 30,
    borderRadius : 5,
  },
  buttontext :{
    padding :10, 
    color : '#fff',
    fontSize : 18,
      }
})



