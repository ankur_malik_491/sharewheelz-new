import React, { Component } from 'react';
import { View, Text, Image,StyleSheet,} from 'react-native';
import { responsiveWidth, responsiveHeight,responsiveFontSize } from 'react-native-responsive-dimensions';
import Display from 'react-native-display';
import * as Progress from 'react-native-progress';
import Localstorage from  "../../../async";

console. disableYellowBox = true;
class Splash extends Component {
  constructor(props) {
    super(props);
    this.state = {
                  enable:false,
                  enablelogo:false,
                  enabletext:false,
                  enableprogress:false,
                  progressValue:0
                };
  }
componentDidMount = () => {
    this.setState({enable:true,enabletext:true})
    setTimeout(()=>{
      this.setState({enablelogo:true,enable:false,enableprogress:true})
      setTimeout(()=>{
        Localstorage.retrieveItem("uid").then((response)=>{
         // console.log(response)
              if(!response){
                Localstorage.retrieveItem("DevId").then((response)=>{
                      if(!response){
                          this.props.navigation.navigate("walk");
                      }
                      else{
                          this.props.navigation.navigate("auth");
                      }
                }).catch((error) =>{
                        console.log(error)
                    });
              } 
              else{
                this.props.navigation.navigate("main");
                
              }
          }).catch((error) =>{
                  console.log(error)
              });
       
      },3000);
    },2000);
} 
  
render() {
   setTimeout((function() {
      this.setState({ progressValue: this.state.progressValue + (0.1 * Math.random())});
    }).bind(this), 1000);
  return (
      <View style={styles.container}>
        <View style={{alignItems:"center",justifyContent:"flex-end"}}>
          
          <Display enable={this.state.enabletext} 
                  enterDuration={2000} 
                  enter="zoomIn" >
            <Image source={require("../../../assets/splash.png")}
                style={{width:responsiveWidth(70),height:responsiveHeight(10)}}/>
          </Display>
        </View>
        {/* <View style={{flex:1,alignItems:"center",justifyContent:"flex-end"}}>
          <Display enable={this.state.enableprogress} 
                  enterDuration={2000} 
                  enter="fadeIn" >
            <Progress.Bar progress={this.state.progressValue} width={responsiveWidth(30)} color="#0a1d3b" 
                style={{marginBottom:responsiveHeight(5)}}/>
          </Display>
        </View> */}
      </View> 
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor:"#fff"
    
  },
  loader : {
    position:"absolute",
    bottom:100,
    alignSelf : "center",
  }

})

export default Splash;