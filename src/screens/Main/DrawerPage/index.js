import Main from "./Main";
import History from './History';
import ReferEarn from "./ReferEarn";
import Setting from "./Setting";
import About from './About';
import Support from './Support';

export {
   Main,History,ReferEarn,Setting,About,Support
}