
import React from 'react';
import {View, Text, StyleSheet, Button, TouchableOpacity} from "react-native";
import BottomTabContainer from "../../../../routes/BottomTab";
import { createAppContainer } from 'react-navigation';
const BottonNav = createAppContainer(BottomTabContainer);

export default class Main extends React.Component {
    constructor(props){
        super(props);
    }
    render(){
        return (
            <View style={styles.container}>
                <BottonNav/>
            
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
    }
});