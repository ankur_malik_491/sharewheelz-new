import React, { Component } from 'react';
import {View, Text, StyleSheet, Button, TouchableOpacity,Alert} from "react-native";
import { Container, Content, List, ListItem, Left, Body, Right, Thumbnail ,Icon,Separator, Card,CardItem} from 'native-base';
import { responsiveWidth,responsiveHeight,responsiveFontSize } from 'react-native-responsive-dimensions';

export default class HistoryList extends Component {
  render() {
    return (
  <Container>
    <Content padder>
      <Card>
      <TouchableOpacity onPress={()=>this.props.navigation.navigate('historylistcontent')}>
        <CardItem header bordered style={{backgroundColor:"#1CB5E0",height:responsiveHeight(8)}}>
          <View style={{flexDirection:"row"}}>
              <Icon  name="car-side" type="FontAwesome5" />
              <View style={{marginLeft:responsiveWidth(3)}}>
                  <Text style={{fontSize:responsiveFontSize(2),fontWeight:"bold"}}>Wed, Jan 29, 06:40 PM</Text>
                  <Text style={{fontSize:responsiveFontSize(2)}}>Car - CRN 46597336538</Text>
                  
              </View>
          </View>
          <Right>
            <Text style={{fontWeight:"bold",fontSize:responsiveFontSize(2.5)}}> $30</Text>
          </Right>
        </CardItem>
        <CardItem style={{height:responsiveHeight(20),backgroundColor:"#f2f2f2"}}>
          <View style={{flexDirection:"row"}}>
              <Icon />
              <View style={{marginLeft:responsiveWidth(3)}}>
                <Text style={{margin:responsiveWidth(2.5)}}>Start Address</Text>
                <Text style={{margin:responsiveWidth(2.5)}}>End Address</Text>   
              </View>
          </View>
          <Body/>
          <Right>
            <Thumbnail source={require('../../../../../assets/backimg.jpg')} />
          </Right>
        </CardItem>
        </TouchableOpacity>
       </Card>
       <Text/>
       <Card>
      <TouchableOpacity onPress={()=>this.props.navigation.navigate('historylistcontent')}>
        <CardItem header bordered style={{backgroundColor:"#1CB5E0",height:responsiveHeight(8)}}>
          <View style={{flexDirection:"row"}}>
              <Icon  name="car-side" type="FontAwesome5" />
              <View style={{marginLeft:responsiveWidth(3)}}>
                  <Text style={{fontSize:responsiveFontSize(2),fontWeight:"bold"}}>Wed, Jan 29, 06:40 PM</Text>
                  <Text style={{fontSize:responsiveFontSize(2)}}>Car - CRN 46597336538</Text>
                  
              </View>
          </View>
          <Right>
            <Text style={{fontWeight:"bold",fontSize:responsiveFontSize(2.5)}}> $30</Text>
          </Right>
        </CardItem>
        <CardItem style={{height:responsiveHeight(20),backgroundColor:"#f2f2f2"}}>
          <View style={{flexDirection:"row"}}>
              <Icon />
              <View style={{marginLeft:responsiveWidth(3)}}>
                <Text style={{margin:responsiveWidth(2.5)}}>Start Address</Text>
                <Text style={{margin:responsiveWidth(2.5)}}>End Address</Text>   
              </View>
          </View>
          <Body/>
          <Right>
            <Thumbnail source={require('../../../../../assets/backimg.jpg')} />
          </Right>
        </CardItem>
        </TouchableOpacity>
       </Card>
       <Text/>
       <Card>
      <TouchableOpacity onPress={()=>this.props.navigation.navigate('historylistcontent')}>
        <CardItem header bordered style={{backgroundColor:"#1CB5E0",height:responsiveHeight(8)}}>
          <View style={{flexDirection:"row"}}>
              <Icon  name="car-side" type="FontAwesome5" />
              <View style={{marginLeft:responsiveWidth(3)}}>
                  <Text style={{fontSize:responsiveFontSize(2),fontWeight:"bold"}}>Wed, Jan 29, 06:40 PM</Text>
                  <Text style={{fontSize:responsiveFontSize(2)}}>Car - CRN 46597336538</Text>
                  
              </View>
          </View>
          <Right>
            <Text style={{fontWeight:"bold",fontSize:responsiveFontSize(2.5)}}> $30</Text>
          </Right>
        </CardItem>
        <CardItem style={{height:responsiveHeight(20),backgroundColor:"#f2f2f2"}}>
          <View style={{flexDirection:"row"}}>
              <Icon />
              <View style={{marginLeft:responsiveWidth(3)}}>
                <Text style={{margin:responsiveWidth(2.5)}}>Start Address</Text>
                <Text style={{margin:responsiveWidth(2.5)}}>End Address</Text>   
              </View>
          </View>
          <Body/>
          <Right>
            <Thumbnail source={require('../../../../../assets/backimg.jpg')} />
          </Right>
        </CardItem>
        </TouchableOpacity>
       </Card>
     </Content>
   </Container>
    );
  }
}