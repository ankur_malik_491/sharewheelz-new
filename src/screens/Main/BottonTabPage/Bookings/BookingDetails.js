import React from 'react';
import {Text, View,TouchableOpacity,TouchableWithoutFeedback,Keyboard,ImageBackground,Image,AsyncStorage} from 'react-native';
import {Container,Icon,Input, Content,Toast,Card,CardItem,Spinner,Segment,Thumbnail} from "native-base";
import {responsiveWidth,responsiveHeight,responsiveFontSize} from "react-native-responsive-dimensions";
import { SafeAreaView } from 'react-native-safe-area-context';
import { ScrollView } from 'react-native-gesture-handler';
import BottomDrawer from 'rn-bottom-drawer';


export default class BookingDetail extends React.Component {
    constructor(props){
        super(props);
        this.state={
          loading:true,
          cancelLoader:false,
          offer:true,
          cancelView:false,
          bookingInfo:[],
          facilities:[],
          cancelOptions:[{reason:"Change in plan",icon:"ios-swap",color:"red",text:"Change in plan"},
                        {reason:"Found a better deal",icon:"ios-briefcase",color:"blue",text:"Found a better deal"},
                        {reason:"Want to book different hotel",icon:"ios-business",color:"green",text:"Want to book different hotel"},
                        {reason:"Booking done by mistake",icon:"ios-close-circle",color:"red",text:"Booking done by mistake"}]
        }
        this.getBookingsDetails();
    }
  getBookingsDetails = async() =>{
    var userid = await AsyncStorage.getItem('uid');
    var bookId = this.props.navigation.getParam('booking_id', 'NO-booking-id');
    var hotelId = this.props.navigation.getParam('hotel_id', 'NO-hotel-id');
    // console.log(userid)
    // console.log(bookId)
    // console.log(hotelId)
    try{
        fetch("https://app.yoyorooms.net/Appapi/get_booking_details    ", 
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                            uid : userid,
                            booking_id: bookId,
                            hotel_id:hotelId   
                        }),     
            })
            .then(response => response.json())
            .then(response2 => {
            //console.log(response2)
                if(response2){
                    this.setState({bookingInfo:response2.booking_info,facilities:response2.facilities})
                    this.setState({loading:false})               
                }
                else{
                    this.setState({loading:false})  
                //MerryToast.show('Something went wrong! Please try again.', MerryToast.SHORT);
                }
            }).catch((error) =>{
                this.setState({loading:false})  
            //MerryToast.show('Something went wrong! Please try again.', MerryToast.SHORT);
            console.log(error)
                // result.status = false;
                // result.data = undefined;
                // reject(result)
            });
        
    }
    catch(error){
        this.setState({loading:false}) 
    //MerryToast.show('Something went wrong! Please try again.', MerryToast.SHORT);
        // result.status = false;
        // result.message = error;
        // result.data = undefined;
        // resolve(result)
    }
}
openExternalApp(url) {
    if(url!=""){
    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        Linking.openURL(url);
      } 
      else {
        alert("maps not supported please vist" + url);
      }
    });
    }
    else{
        alert("maps url not found");
    }
  }
  cancelBooking =(res)=>{
    this.setState({cancelLoader:true,reason:res,cancelView:false})
    try{
        fetch("https://app.yoyorooms.net/Appapi/cancel_user_booking", 
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                            reason : res,
                            booking_id:this.state.bookingInfo.booking_id
                        }),     
            })
            .then(response => response.json())
            .then(response2 => {
            //console.log(response2)
                if(response2){
                    this.setState({cancelLoader:false}) 
                    alert("Booking Cancelled Successfully!")
                    this.props.navigation.goBack();              
                }
                else{
                    this.setState({cancelLoader:false})  
                    alert('Something went wrong! Please try again.');
                }
            }).catch((error) =>{
                this.setState({cancelLoader:false})  
            //MerryToast.show('Something went wrong! Please try again.', MerryToast.SHORT);
                console.log(error)
                alert('Something went wrong! Please try again.');
                // result.status = false;
                // result.data = undefined;
                // reject(result)
            });
        
    }
    catch(error){
        this.setState({loading:false}) 
        alert('Something went wrong! Please try again.');
    //MerryToast.show('Something went wrong! Please try again.', MerryToast.SHORT);
        // result.status = false;
        // result.message = error;
        // result.data = undefined;
        // resolve(result)
    }
  }


render(){
   //console.log(this.state.searchHistory)
 return (
    <SafeAreaView style={{flex:1}}>
        <Container >
            <View>
                <View style={{flexDirection:"row",alignItems:"center",justifyContent:"space-between",paddingHorizontal:responsiveWidth(5),
                            paddingVertical:responsiveHeight(2),height:"auto",backgroundColor: "#0246b1"}}>
                    <TouchableOpacity onPress={()=>this.props.navigation.goBack()}>
                        <Icon name='arrow-back'  style={{color:"#000",fontSize:responsiveFontSize(4),color:"#fff"}}/>
                    </TouchableOpacity>
                    <TouchableOpacity style={{width:responsiveWidth(80)}}>
                        <Text style={{fontSize:responsiveFontSize(2.5),fontWeight:"bold",color:"#fff"}}>
                            Booked
                        </Text>
                    </TouchableOpacity>
                    {/* <TouchableOpacity >
                        <Icon name='ios-heart-empty'   style={{color:"red",fontSize:responsiveFontSize(4)}}/>
                    </TouchableOpacity> */}
                </View>
            </View>
            <View style={{flex:1,backgroundColor:"#e6e6e6"}}>
                {(!this.state.loading) &&
                    <View  style={{flex:1,}}>
                        {(this.state.bookingInfo.length>0) && 
                            <ScrollView style={{backgroundColor:"#0246b1"}}>
                                <View style={{alignItems:"center",justifyContent:"center",marginTop:responsiveHeight(2)}}>
                                    {(this.state.bookingInfo[0].booking_status==0||this.state.bookingInfo[0].booking_status==1) &&
                                        <>
                                        <View style={{backgroundColor:"green",alignItems:"center",justifyContent:"center",borderRadius:responsiveWidth(10),
                                                    paddingHorizontal:responsiveWidth(4.3),marginBottom:responsiveHeight(2)}}>
                                            <Icon name='ios-checkmark' style={{color:"#fff",fontSize:responsiveFontSize(7)}}/>
                                        </View>
                                        <Text style={{color:"#fff",fontSize:responsiveFontSize(2.8),fontWeight:"600",textAlign:"center"}}>
                                            Success! Your stay is confirmed 
                                        </Text>
                                        </>
                                    }
                                    {(this.state.bookingInfo[0].booking_status==2) &&
                                        <>
                                        <View style={{backgroundColor:"green",alignItems:"center",justifyContent:"center",borderRadius:responsiveWidth(10),
                                                    paddingHorizontal:responsiveWidth(4.3),marginBottom:responsiveHeight(2)}}>
                                            <Icon name='ios-checkmark' style={{color:"#fff",fontSize:responsiveFontSize(7)}}/>
                                        </View>
                                        <Text style={{color:"#fff",fontSize:responsiveFontSize(2.8),fontWeight:"600",textAlign:"center"}}>
                                            Success! Your stay is completed 
                                        </Text>
                                        </>
                                    }
                                    {(this.state.bookingInfo[0].booking_status==4||this.state.bookingInfo[0].booking_status==5) &&
                                        <>
                                        <View style={{backgroundColor:"red",alignItems:"center",justifyContent:"center",borderRadius:responsiveWidth(10),
                                                    paddingHorizontal:responsiveWidth(4.3),marginBottom:responsiveHeight(2)}}>
                                            <Icon name='ios-close' style={{color:"#fff",fontSize:responsiveFontSize(7)}}/>
                                        </View>
                                        <Text style={{color:"#fff",fontSize:responsiveFontSize(2.8),fontWeight:"600",textAlign:"center"}}>
                                            Your booking has been cancelled 
                                        </Text>
                                        </>
                                    }
                                    <View style={{flexDirection:"row"}}>
                                        <Text style={{color:"#fff",fontSize:responsiveFontSize(2.8),fontWeight:"100",textAlign:"center"}}>
                                            Booking ID : 
                                        </Text>
                                        <Text style={{color:"#fff",fontSize:responsiveFontSize(2.8),fontWeight:"bold",textAlign:"center",marginLeft:responsiveWidth(3)}}>
                                            {this.state.bookingInfo[0].booking_id} 
                                        </Text>
                                    </View>
                                </View>
                                <View style={{margin:responsiveWidth(5),paddingHorizontal:responsiveWidth(5),paddingVertical:responsiveHeight(2),
                                            backgroundColor:"#fff",borderRadius:responsiveWidth(5)}}>
                                    <Text style={{color:"#000",fontSize:responsiveFontSize(2.8),fontWeight:"bold",textAlign:"center",marginBottom:responsiveHeight(2)}}>
                                        {this.state.bookingInfo[0].customer_name}
                                    </Text>
                                    <View style={{justifyContent:"center",alignItems:"center"}}>
                                        <View style={{flexDirection:"row",alignItems:"center",justifyContent:"space-between"}}>
                                            <Text style={{fontSize:responsiveFontSize(2.5),color:"#000",opacity:0.7,fontWeight:"500",
                                                        marginBottom:responsiveHeight(1),textAlign:"center"}}>
                                                {this.state.bookingInfo[0].check_in} 
                                            </Text>
                                            <Text style={{fontSize:responsiveFontSize(2.5),color:"#000",opacity:0.7,fontWeight:"500",
                                                        marginBottom:responsiveHeight(1),marginLeft:responsiveWidth(3),textAlign:"center"}}>
                                                ({this.state.bookingInfo[0].check_in_time}) 
                                            </Text>
                                        </View>
                                        <Text style={{fontSize:responsiveFontSize(2.2),color:"#000",fontWeight:"500",
                                                    marginBottom:responsiveHeight(1),backgroundColor:"#0246b1",paddingVertical:responsiveHeight(1),
                                                    paddingHorizontal:responsiveWidth(3),color:"#fff"}}>
                                            {this.state.bookingInfo[0].total_night==0?(this.state.bookingInfo[0].total_hours+' H '):(this.state.bookingInfo[0].total_night+' N ')}
                                        </Text>
                                        <View style={{flexDirection:"row",alignItems:"center",justifyContent:"space-between"}}>
                                            <Text style={{fontSize:responsiveFontSize(2.5),color:"#000",opacity:0.7,fontWeight:"500",
                                                        marginBottom:responsiveHeight(1),textAlign:"center"}}>
                                                {this.state.bookingInfo[0].check_out} 
                                            </Text>
                                            <Text style={{fontSize:responsiveFontSize(2.5),color:"#000",opacity:0.7,fontWeight:"500",
                                                        marginBottom:responsiveHeight(1),marginLeft:responsiveWidth(3),textAlign:"center"}}>
                                                ({this.state.bookingInfo[0].check_out_time}) 
                                            </Text>
                                        </View>
                                    </View>
                                    <View style={{flexDirection:"row",borderTopColor:"#ccc",borderTopWidth:2,marginTop:responsiveHeight(1),paddingVertical:responsiveWidth(2)}}>
                                        <View style={{alignItems:"center",justifyContent:"center",width:responsiveWidth(25)}}>
                                            <Image
                                                source={{uri:this.state.bookingInfo[0].best_img}} 
                                                resizeMode="stretch"
                                                style={{height:responsiveHeight(15),width:responsiveWidth(25),borderRadius:responsiveWidth(3)}}
                                                        />
                                        </View>
                                        <View style={{alignItems:"center",justifyContent:"center",width:responsiveWidth(55),paddingHorizontal:responsiveWidth(5)}}>
                                            <Text style={{fontSize:responsiveFontSize(2.5),color:"#000",fontWeight:"bold",
                                                        marginBottom:responsiveHeight(1),}}>
                                                {this.state.bookingInfo[0].full_name}
                                            </Text>
                                            <Text style={{fontSize:responsiveFontSize(2.2),color:"#000",opacity:0.7,fontWeight:"500",
                                                        marginBottom:responsiveHeight(1),}}>
                                                {this.state.bookingInfo[0].description} 
                                            </Text>
                                        </View>
                                    </View>
                                    <View style={{flexDirection:"row",marginTop:responsiveHeight(1),paddingVertical:responsiveWidth(2),alignItems:"center",justifyContent:"space-between",width:responsiveWidth(80)}}>
                                        <View style={{alignItems:"center",justifyContent:"center",backgroundColor:"#e6e6e6",paddingHorizontal:responsiveWidth(3),paddingVertical:responsiveHeight(1),
                                                    borderRadius:responsiveWidth(3)}}>
                                            <Text style={{fontSize:responsiveFontSize(2.5),color:"#0246b1",fontWeight:"bold",
                                                        marginBottom:responsiveHeight(1),}}>
                                                GUESTS
                                            </Text>
                                            <View style={{flexDirection:"row",alignItems:"center",justifyContent:"center"}}>
                                                <Text style={{fontSize:responsiveFontSize(2.3),color:"#000",fontWeight:"bold",
                                                            marginBottom:responsiveHeight(1),}}>
                                                    {this.state.bookingInfo[0].total_adults} 
                                                </Text>
                                                <Icon name="account-outline" type="MaterialCommunityIcons" 
                                                    style={{fontSize:responsiveFontSize(3),color:"#000",fontWeight:"bold",
                                                        marginBottom:responsiveHeight(1),}}/>
                                                <Text style={{fontSize:responsiveFontSize(2.3),color:"#000",fontWeight:"bold",
                                                    marginBottom:responsiveHeight(1),marginLeft:responsiveWidth(3)}}>
                                                    {this.state.bookingInfo[0].total_childs} 
                                                </Text>
                                                <Icon name="human-child" type="MaterialCommunityIcons" 
                                                    style={{fontSize:responsiveFontSize(3),color:"#000",fontWeight:"bold",
                                                        marginBottom:responsiveHeight(1),}}/>
                                            </View>
                                        </View>
                                        <View style={{alignItems:"center",justifyContent:"center",backgroundColor:"#e6e6e6",paddingHorizontal:responsiveWidth(3),paddingVertical:responsiveHeight(1),
                                                    borderRadius:responsiveWidth(3)}}>
                                            <Text style={{fontSize:responsiveFontSize(2.5),color:"#0246b1",fontWeight:"bold",
                                                        marginBottom:responsiveHeight(1),}}>
                                                ROOMS
                                            </Text>
                                            <View style={{flexDirection:"row",alignItems:"center",justifyContent:"center"}}>
                                                <Text style={{fontSize:responsiveFontSize(2.3),color:"#000",fontWeight:"bold",
                                                            marginBottom:responsiveHeight(1),}}>
                                                    {this.state.bookingInfo[0].total_rooms} 
                                                </Text>
                                                <Icon name="ios-home" 
                                                    style={{fontSize:responsiveFontSize(2.5),color:"#000",fontWeight:"bold",
                                                        marginBottom:responsiveHeight(1),}}/>
                                            </View>
                                        </View>
                                        <View style={{alignItems:"center",justifyContent:"center",backgroundColor:"#e6e6e6",paddingHorizontal:responsiveWidth(3),paddingVertical:responsiveHeight(1),
                                                    borderRadius:responsiveWidth(3)}}>
                                            <Text style={{fontSize:responsiveFontSize(2.5),color:"#0246b1",fontWeight:"bold",
                                                        marginBottom:responsiveHeight(1),}}>
                                                PRICE
                                            </Text>
                                            <View style={{flexDirection:"row",alignItems:"center",justifyContent:"center"}}>
                                                <Text style={{fontSize:responsiveFontSize(2.3),color:"#000",fontWeight:"bold",
                                                            marginBottom:responsiveHeight(1),}}>
                                                    ₹ {this.state.bookingInfo[0].payable_amount} 
                                                </Text>
                                            </View>
                                        </View>
                                    </View>
                                    {(this.state.bookingInfo[0].booking_status==0||this.state.bookingInfo[0].booking_status==1) &&
                                        <>
                                        <View style={{flexDirection:"row",alignItems:"center",justifyContent:"center",marginTop:responsiveHeight(3),borderTopWidth:2,borderTopColor:"#ccc",
                                                    paddingVertical:responsiveHeight(2),paddingHorizontal:responsiveWidth(8)}}>
                                            <View style={{backgroundColor:"#e6e6e6",alignItems:"center",justifyContent:"center",borderRadius:responsiveWidth(3)}}>
                                                <Thumbnail square source={{uri:"https://app.yoyorooms.net/assets/images/map-pin2.jpg"}} 
                                                        style={{borderRadius:responsiveWidth(3),height:responsiveWidth(18),width:responsiveWidth(18)}}/>
                                            </View>
                                            <Text style={{fontSize:responsiveFontSize(2.2),color:"#000",fontWeight:"600",opacity:0.7,
                                                            marginBottom:responsiveHeight(1),marginLeft:responsiveWidth(3)}}>
                                                {this.state.bookingInfo[0].full_name},{this.state.bookingInfo[0].full_address}
                                            </Text>
                                        </View>
                                        <View style={{flexDirection:"row",alignItems:"center",justifyContent:"center",marginTop:responsiveHeight(2),
                                                    paddingVertical:responsiveHeight(1),paddingHorizontal:responsiveWidth(5)}}>
                                            <TouchableOpacity onPress={()=>this.openExternalApp(this.state.bookingInfo[0].location_url)}>
                                                <View style={{justifyContent:"center",alignItems:"center",flexDirection:"row",backgroundColor:"#0246b1",
                                                            paddingVertical:responsiveHeight(1),paddingHorizontal:responsiveWidth(3),borderRadius:responsiveWidth(5)}}>
                                                    <Icon name='directions'  type="MaterialCommunityIcons"  style={{color:"#fff",fontSize:responsiveFontSize(3)}}/>
                                                    <Text style={{fontSize:responsiveFontSize(2.3),color:"#fff",fontWeight:"bold",marginLeft:responsiveWidth(2)}}>
                                                    DIRECTIONS
                                                    </Text>
                                                </View>
                                            </TouchableOpacity>
                                            <TouchableOpacity onPress={()=>alert("open calling module ")}>
                                                <View style={{justifyContent:"center",alignItems:"center",flexDirection:"row",backgroundColor:"#0246b1",marginLeft:responsiveWidth(3),
                                                            paddingVertical:responsiveHeight(1),paddingHorizontal:responsiveWidth(3),borderRadius:responsiveWidth(5)}}>
                                                    <Icon name='ios-call'   style={{color:"#fff",fontSize:responsiveFontSize(3)}}/>
                                                    <Text style={{fontSize:responsiveFontSize(2.3),color:"#fff",fontWeight:"bold",marginLeft:responsiveWidth(2)}}>
                                                    CALL HOTEL
                                                    </Text>
                                                </View>
                                            </TouchableOpacity>
                                        </View>
                                        </>
                                    }
                                    {(this.state.bookingInfo[0].booking_status==2||this.state.bookingInfo[0].booking_status==4||this.state.bookingInfo[0].booking_status==5) &&
                                        <>
                                        <View style={{flexDirection:"row",alignItems:"center",justifyContent:"center",marginTop:responsiveHeight(3),borderTopWidth:2,borderTopColor:"#ccc",
                                                    paddingVertical:responsiveHeight(2),paddingHorizontal:responsiveWidth(8)}}>
                                            <View style={{backgroundColor:"#e6e6e6",alignItems:"center",justifyContent:"center",borderRadius:responsiveWidth(3)}}>
                                                <Thumbnail square source={{uri:"https://app.yoyorooms.net/assets/images/map-pin2.jpg"}} 
                                                        style={{borderRadius:responsiveWidth(3),height:responsiveWidth(18),width:responsiveWidth(18)}}/>
                                            </View>
                                            <Text style={{fontSize:responsiveFontSize(2.2),color:"#000",fontWeight:"600",opacity:0.7,
                                                            marginBottom:responsiveHeight(1),marginLeft:responsiveWidth(3)}}>
                                                {this.state.bookingInfo[0].full_name},{this.state.bookingInfo[0].full_address}
                                            </Text>
                                        </View>
                                        <View style={{alignItems:"center",justifyContent:"center",marginTop:responsiveHeight(2),
                                                    paddingVertical:responsiveHeight(1),paddingHorizontal:responsiveWidth(5)}}>
                                            <TouchableOpacity onPress={()=>this.props.navigation.navigate("hotelDeatilsOuter",{hotel_id:this.state.bookingInfo[0].hotel_id,hotel_name:this.state.bookingInfo[0].full_name})}>
                                                <View style={{justifyContent:"center",alignItems:"center",flexDirection:"row",backgroundColor:"#0246b1",
                                                            paddingVertical:responsiveHeight(2),paddingHorizontal:responsiveWidth(5),borderRadius:responsiveWidth(5)}}>
                                                    <Icon name='ios-refresh'  style={{color:"#fff",fontSize:responsiveFontSize(3)}}/>
                                                    <Text style={{fontSize:responsiveFontSize(2.3),color:"#fff",fontWeight:"bold",marginLeft:responsiveWidth(2)}}>
                                                        Book Again
                                                    </Text>
                                                </View>
                                            </TouchableOpacity>
                                        </View>
                                        </>
                                    }
                                </View>
                                {(this.state.bookingInfo[0].booking_status==0||this.state.bookingInfo[0].booking_status==1) &&
                                <>
                                <View style={{margin:responsiveWidth(5),marginTop:0,paddingHorizontal:responsiveWidth(5),paddingVertical:responsiveHeight(2),
                                            backgroundColor:"#fff",borderRadius:responsiveWidth(5)}}>
                                    <Text style={{color:"#000",fontSize:responsiveFontSize(2.5),fontWeight:"200",opacity:0.7,textAlign:"center"}}>
                                        Your booking details are sent as SMS to {this.state.bookingInfo[0].customer_phone}
                                    </Text>
                                </View>
                                <View style={{margin:responsiveWidth(5),marginTop:0,paddingHorizontal:responsiveWidth(5),paddingVertical:responsiveHeight(2),
                                            backgroundColor:"#fff",borderRadius:responsiveWidth(5),alignItems:"center",justifyContent:"center"}}>
                                    <Text style={{color:"#000",fontSize:responsiveFontSize(2.8),fontWeight:"bold",textAlign:"center"}}>
                                        Pay Online
                                    </Text>
                                    <Text style={{color:"#000",fontSize:responsiveFontSize(2.1),fontWeight:"200",opacity:0.7,textAlign:"center",marginTop:responsiveHeight(1)}}>
                                        Get additional 5.0% off by paying online*
                                    </Text>
                                    <View style={{borderWidth:2,borderColor:"#e6e6e6",borderRadius:responsiveWidth(5),
                                                paddingHorizontal:responsiveWidth(5),width:responsiveWidth(80),marginTop:responsiveHeight(2)}}>
                                        <View style={{alignItems:"center",justifyContent:"space-between",flexDirection:"row",paddingVertical:responsiveHeight(2)}}>
                                            <Text style={{color:"#000",fontSize:responsiveFontSize(2.3),fontWeight:"400",opacity:0.7,textAlign:"center"}}>
                                                Balance Amount
                                            </Text>
                                            <Text style={{color:"#000",fontSize:responsiveFontSize(2.5),fontWeight:"700",opacity:0.7,textAlign:"center"}}>
                                                ₹ {this.state.bookingInfo[0].payable_amount}
                                            </Text>
                                        </View>
                                        <View style={{alignItems:"center",justifyContent:"space-between",flexDirection:"row",marginTop:responsiveHeight(1),
                                                    borderTopColor:"#e6e6e6",borderTopWidth:1,paddingVertical:responsiveHeight(2),}}>
                                            <Text style={{color:"#000",fontSize:responsiveFontSize(2.3),fontWeight:"400",opacity:0.7,textAlign:"center"}}>
                                                Additional Discount
                                            </Text>
                                            <Text style={{color:"#000",fontSize:responsiveFontSize(2.5),fontWeight:"700",opacity:0.7,textAlign:"center"}}>
                                                - ₹ {(this.state.bookingInfo[0].payable_amount*5)/100}
                                            </Text>
                                        </View>
                                        <View style={{alignItems:"center",justifyContent:"space-between",flexDirection:"row",marginTop:responsiveHeight(1),
                                                    borderTopColor:"#e6e6e6",borderTopWidth:1,paddingVertical:responsiveHeight(2),}}>
                                            <Text style={{color:"#000",fontSize:responsiveFontSize(2.3),fontWeight:"400",opacity:0.7,textAlign:"center"}}>
                                                Amount Payable
                                            </Text>
                                            <Text style={{color:"#000",fontSize:responsiveFontSize(2.5),fontWeight:"700",opacity:0.7,textAlign:"center"}}>
                                                ₹ {this.state.bookingInfo[0].payable_amount-((this.state.bookingInfo[0].payable_amount*5)/100)}
                                            </Text>
                                        </View>
                                    </View>
                                    <Text style={{color:"#000",fontSize:responsiveFontSize(2.1),fontWeight:"200",opacity:0.7,textAlign:"center",marginTop:responsiveHeight(1)}}>
                                        *Discount is not applicable if you pay at hotel
                                    </Text>
                                    <TouchableOpacity onPress={()=>alert("open pay onile module ")}>
                                        <View style={{justifyContent:"center",alignItems:"center",flexDirection:"row",backgroundColor:"green",marginTop:responsiveHeight(2),
                                                    paddingVertical:responsiveHeight(1),paddingHorizontal:responsiveWidth(5),borderRadius:responsiveWidth(5)}}>
                                            <Text style={{fontSize:responsiveFontSize(2.5),color:"#fff",fontWeight:"bold",marginLeft:responsiveWidth(2)}}>
                                                PAY NOW
                                            </Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={()=>alert("open pay onile module ")}>
                                        <View style={{display:this.state.offer?"flex":"none",alignItems:"center",flexDirection:"row",backgroundColor:"#da4453",width:responsiveWidth(90),
                                                    marginVertical:responsiveHeight(3),paddingVertical:responsiveHeight(2)}}>
                                            <View style={{alignItems:"center",justifyContent:"center",paddingHorizontal:responsiveWidth(3),borderRightWidth:2,borderRightColor:"#ccc"}}>
                                                <Icon name="ios-gift" style={{color:"#fff",fontSize:responsiveFontSize(4)}}/>
                                            </View>
                                            <View style={{paddingHorizontal:responsiveWidth(3),}}>
                                                <Text style={{fontSize:responsiveFontSize(3),color:"#fff",fontWeight:"bold",marginLeft:responsiveWidth(2)}}>
                                                    OFFER
                                                </Text>
                                                <Text style={{fontSize:responsiveFontSize(2),color:"#fff",fontWeight:"600",marginLeft:responsiveWidth(2)}}>
                                                    Save ₹ {(this.state.bookingInfo[0].payable_amount*5)/100} instantly, pay now!
                                                </Text>
                                            </View>
                                            <TouchableOpacity onPress={()=>this.setState({offer:false})}>
                                                <View style={{alignItems:"center",justifyContent:"center",paddingHorizontal:responsiveWidth(3),}}>
                                                    <Icon name="ios-close" style={{color:"#fff",fontSize:responsiveFontSize(4)}}/>
                                                </View>
                                            </TouchableOpacity>
                                        </View>
                                    </TouchableOpacity>
                                    
                                </View>
                                <View style={{margin:responsiveWidth(5),marginTop:0,paddingHorizontal:responsiveWidth(5),paddingVertical:responsiveHeight(2),
                                            backgroundColor:"#fff",borderRadius:responsiveWidth(5),alignItems:"center",justifyContent:"center"}}>
                                    <Text style={{color:"#000",fontSize:responsiveFontSize(2.8),fontWeight:"bold",textAlign:"center"}}>
                                        Need Help?
                                    </Text>
                                    <Text style={{color:"#000",fontSize:responsiveFontSize(2.5),fontWeight:"200",opacity:0.7,textAlign:"center"}}>
                                        Contact your YOYO Captain for assistance
                                    </Text>
                                    <TouchableOpacity onPress={()=>alert("open Asst module ")}>
                                        <View style={{justifyContent:"center",alignItems:"center",flexDirection:"row",backgroundColor:"#0246b1",marginTop:responsiveHeight(2),
                                                    paddingVertical:responsiveHeight(1),paddingHorizontal:responsiveWidth(5),borderRadius:responsiveWidth(5)}}>
                                            <Text style={{fontSize:responsiveFontSize(2.3),color:"#fff",fontWeight:"bold",marginLeft:responsiveWidth(2)}}>
                                                GET ASSISTANCE
                                            </Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                                <View style={{margin:responsiveWidth(5),marginTop:0,paddingHorizontal:responsiveWidth(5),paddingVertical:responsiveHeight(2),
                                            backgroundColor:"#fff",borderRadius:responsiveWidth(5),alignItems:"center",justifyContent:"center"}}>
                                    <Text style={{color:"#000",fontSize:responsiveFontSize(2.8),fontWeight:"bold",textAlign:"center"}}>
                                        Our Promises to You
                                    </Text>
                                    <ScrollView horizontal={true}>
                                        <View style={{flexDirection:"row",alignItems:"center",justifyContent:"center",paddingVertical:responsiveWidth(5)}}>
                                            {(this.state.facilities.length>0) &&
                                            this.state.facilities.map((item,i) => (
                                                <View key={i} style={{alignItems:"center",justifyContent:"space-between",paddingHorizontal:responsiveWidth(3),
                                                                    paddingVertical:responsiveHeight(2),width:responsiveWidth(30),backgroundColor:"#e6e6e6",marginRight:responsiveWidth(3),
                                                                    borderRadius:responsiveWidth(3)}} >
                                                    <Icon name={item.app_icon} 
                                                        type="MaterialCommunityIcons"
                                                        style={{color:"#000",fontSize:responsiveFontSize(4)}}/>
                                                    <Text style={{fontSize:responsiveFontSize(1.8),marginTop:responsiveHeight(2),textAlign:"center"}}>
                                                        {item.facility}
                                                    </Text>
                                                </View>
                                            ))}
                                        </View>
                                    </ScrollView>
                                    <View style={{flexDirection:"row",alignItems:"center",justifyContent:"space-between",marginTop:responsiveHeight(2),
                                                paddingVertical:responsiveHeight(1),paddingHorizontal:responsiveWidth(5)}}>
                                        <TouchableOpacity onPress={()=>this.setState({cancelView:true})}>
                                            <View style={{justifyContent:"center",alignItems:"center",backgroundColor:"red",width:responsiveWidth(35),
                                                        paddingVertical:responsiveHeight(1),paddingHorizontal:responsiveWidth(5),borderRadius:responsiveWidth(5)}}>
                                                <Text style={{fontSize:responsiveFontSize(2.3),color:"#fff",fontWeight:"bold",textAlign:"center"}}>
                                                   CANCEL BOOKING
                                                </Text>
                                            </View>
                                        </TouchableOpacity>
                                        <TouchableOpacity onPress={()=>alert("open Share module ")}>
                                            <View style={{justifyContent:"center",alignItems:"center",backgroundColor:"green",width:responsiveWidth(35),marginLeft:responsiveWidth(5),
                                                        paddingVertical:responsiveHeight(1),paddingHorizontal:responsiveWidth(5),borderRadius:responsiveWidth(5)}}>
                                                <Text style={{fontSize:responsiveFontSize(2.3),color:"#fff",fontWeight:"bold",textAlign:"center"}}>
                                                   SHARE BOOKING
                                                </Text>
                                            </View>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                                </>
                                }
                                
                            </ScrollView>
                        } 
                        {(this.state.bookingInfo==0) &&
                            <View  style={{alignItems:"center",justifyContent:"center",height:responsiveHeight(100)}}>
                                <Image source={require("../../../../../assets/splash.png")} style={{height:responsiveHeight(20),width:responsiveWidth(70),}} />
                                <Text style={{color:"#0a1d3b",fontSize:responsiveFontSize(2.5),fontWeight:"bold"}}>
                                    Sorry! No Details found for this Booking</Text>
                            </View>
                        } 
                    </View>
                }
                {(this.state.loading) &&
                    <View  style={{alignItems:"center",justifyContent:"center",height:responsiveHeight(100)}}>
                        {/* <Image source={require("../../../../../assets/splash.png")} style={{height:responsiveHeight(40),width:responsiveWidth(60),}} /> */}
                        <Spinner color="#0246b1" size="large"/>
                    </View>
                }
                {this.state.cancelView &&
                <BottomDrawer
                    containerHeight={responsiveHeight(70)}
                    startUp={true}
                    onCollapsed={()=>this.setState({cancelView:false})}
                >
                    <View style={{backgroundColor:"#fff",paddingHorizontal:responsiveWidth(5),paddingVertical:responsiveHeight(3)}}>
                        <View style={{flexDirection:"row",alignItems:"center",justifyContent:"space-between",marginBottom:responsiveHeight(5)}}>
                            <View>
                                <Text style={{fontSize:responsiveFontSize(4),fontWeight:"bold",color:"#000"}}>
                                    Cancel Booking
                                </Text>
                                <Text style={{fontSize:responsiveFontSize(2),fontWeight:"bold",color:"#0246b1"}}>
                                    No cancellation Charge
                                </Text>
                            </View>
                            <TouchableOpacity onPress={()=>this.setState({cancelView:false})}>
                                <View style={{alignItems:"center",justifyContent:"center",paddingHorizontal:responsiveWidth(3),}}>
                                    <Icon name="ios-close-circle" style={{color:"red",fontSize:responsiveFontSize(4)}}/>
                                </View>
                            </TouchableOpacity>
                        </View>
                        <ScrollView>
                        {this.state.cancelOptions.map((item,i)=>(
                        <TouchableOpacity key ={i} onPress={()=>this.cancelBooking(item.reason)}>
                            <View style={{flexDirection:"row",alignItems:"center",borderBottomColor:"#ccc",borderBottomWidth:1,
                                        paddingVertical:responsiveHeight(2)}}>
                                <Icon name={item.icon} style={{color:item.color,fontSize:responsiveFontSize(5)}}/>    
                                <Text style={{fontSize:responsiveFontSize(2.5),fontWeight:"800",color:"#000",marginLeft:responsiveWidth(3)}}>
                                    {item.text}
                                </Text>
                            </View>
                        </TouchableOpacity>
                        ))}
                        </ScrollView>
                    </View> 
                </BottomDrawer>
                }
            </View>
        </Container>
    </SafeAreaView>
)}
} 




