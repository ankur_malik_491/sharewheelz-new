import React from 'react';
import {Text, View,TouchableOpacity,TouchableWithoutFeedback,Keyboard,ImageBackground,Image,AsyncStorage} from 'react-native';
import {Container,Icon,Input, Content,Toast,Card,CardItem,Spinner,Segment} from "native-base";
import {responsiveWidth,responsiveHeight,responsiveFontSize} from "react-native-responsive-dimensions";
import { SafeAreaView } from 'react-native-safe-area-context';
import { ScrollView } from 'react-native-gesture-handler';

export default class Bookings extends React.Component {
    constructor(props){
        super(props);
        this.state={
          activePage:1,
          loading:true,
          bookingsCancelled:[],
          bookingPending:[],
          bookingCompleted:[]
        }
        this.getBookings();
    }
  getBookings = async() =>{
    var userid = await AsyncStorage.getItem('uid');
    try{
        fetch("https://app.yoyorooms.net/Appapi/user_bookings    ", 
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                            uid : userid   
                        }),     
            })
            .then(response => response.json())
            .then(response2 => {
            //console.log(response2)
                if(response2){
                    this.setState({bookingsCancelled:response2.cancelled,bookingPending:response2.pending,bookingCompleted:response2.completed})
                    this.setState({loading:false})               
                }
                else{
                    this.setState({loading:false})  
                //MerryToast.show('Something went wrong! Please try again.', MerryToast.SHORT);
                }
            }).catch((error) =>{
                this.setState({loading:false})  
            //MerryToast.show('Something went wrong! Please try again.', MerryToast.SHORT);
            console.log(error)
                // result.status = false;
                // result.data = undefined;
                // reject(result)
            });
        
    }
    catch(error){
        this.setState({loading:false}) 
    //MerryToast.show('Something went wrong! Please try again.', MerryToast.SHORT);
        // result.status = false;
        // result.message = error;
        // result.data = undefined;
        // resolve(result)
    }
}
selectComponent = (activePage) => () => this.setState({activePage}) 

_renderComponent = () => {
    //console.log(this.state.currentDate)
  if(this.state.activePage === 1)
    return (
            <View style={{flex:1,backgroundColor:"#e6e6e6"}}>
                {(!this.state.loading) &&
                    <ScrollView>
                        {(this.state.bookingPending.length>0) &&
                            this.state.bookingPending.map((item,i) => (
                            <View key={i} style={{backgroundColor:"#fff",margin:responsiveWidth(5),
                                        marginTop:responsiveHeight(3),borderRadius:responsiveWidth(3)}}>
                                <TouchableOpacity onPress={()=>this.props.navigation.navigate("bookingDetails",{hotel_id:item.hotel_id,booking_id:item.booking_id})}>
                                    <View style={{alignItems:"center",justifyContent:"space-between",flexDirection:"row",
                                                paddingHorizontal:responsiveWidth(5),paddingVertical:responsiveHeight(1),borderBottomWidth:1,borderBottomColor:"#ccc"}}>
                                        <Text style={{fontSize:responsiveFontSize(2.5),fontWeight:"bold"}}>
                                            {item.city_name}
                                        </Text>
                                        <Text style={{fontSize:responsiveFontSize(2),opacity:0.7,fontWeight:"600"}}>
                                            Booked on {item.booked_on}
                                        </Text>
                                    </View>
                                    <View style={{paddingVertical:responsiveHeight(3),paddingHorizontal:responsiveWidth(5),flexDirection:"row"}}>
                                        <View style={{alignItems:"center",justifyContent:"center"}}>
                                            <Image
                                                source={{uri:item.best_img}} 
                                                resizeMode="stretch"
                                                style={{height:responsiveHeight(15),width:responsiveWidth(25),borderRadius:responsiveWidth(3)}}
                                                        />
                                        </View>
                                        <View style={{paddingHorizontal:responsiveWidth(5),justifyContent:"center",alignItems:"center",
                                                    width:responsiveWidth(55),borderLeftColor:"#ccc",borderLeftWidth:3,marginLeft:responsiveWidth(3)}}>
                                            <Text style={{fontSize:responsiveFontSize(2.5),color:"#000",fontWeight:"bold",
                                                        marginBottom:responsiveHeight(1),}}>
                                                {item.full_name}
                                            </Text>
                                            <View style={{flexDirection:"row",alignItems:"center",justifyContent:"space-between"}}>
                                                <Text style={{fontSize:responsiveFontSize(2.2),color:"#000",opacity:0.7,fontWeight:"500",
                                                            marginBottom:responsiveHeight(1),textAlign:"center"}}>
                                                    {item.check_in} 
                                                </Text>
                                                <Text style={{fontSize:responsiveFontSize(2.2),color:"#000",opacity:0.7,fontWeight:"500",
                                                            marginBottom:responsiveHeight(1),marginLeft:responsiveWidth(3),textAlign:"center"}}>
                                                    ({item.check_in_time}) 
                                                </Text>
                                            </View>
                                            <Text style={{fontSize:responsiveFontSize(2.2),color:"#000",fontWeight:"500",
                                                        marginBottom:responsiveHeight(1),backgroundColor:"#0246b1",paddingVertical:responsiveHeight(1),
                                                        paddingHorizontal:responsiveWidth(3),color:"#fff"}}>
                                                {item.total_night==0?item.total_hours+' H ':item.total_night+' N '}
                                            </Text>
                                            <View style={{flexDirection:"row",alignItems:"center",justifyContent:"space-between"}}>
                                                <Text style={{fontSize:responsiveFontSize(2.2),color:"#000",opacity:0.7,fontWeight:"500",
                                                            marginBottom:responsiveHeight(1),textAlign:"center"}}>
                                                    {item.check_out} 
                                                </Text>
                                                <Text style={{fontSize:responsiveFontSize(2.2),color:"#000",opacity:0.7,fontWeight:"500",
                                                            marginBottom:responsiveHeight(1),marginLeft:responsiveWidth(3),textAlign:"center"}}>
                                                    ({item.check_out_time}) 
                                                </Text>
                                            </View>
                                        </View>
                                    </View>
                                    <View style={{flexDirection:"row",paddingHorizontal:responsiveWidth(5),marginBottom:responsiveHeight(2),width:responsiveWidth(50)}}> 
                                        <Text style={{fontSize:responsiveFontSize(2.2),color:"#000",fontWeight:"900",marginRight:responsiveWidth(2)}}>
                                            Booked for : 
                                        </Text>
                                        <Text style={{fontSize:responsiveFontSize(2.2),color:"#000",fontWeight:"bold",textAlign:"center"}}>
                                            {item.customer_name} 
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={()=>alert("yoyo assist")}>
                                    <View style={{alignItems:"center",justifyContent:"center",paddingVertical:responsiveHeight(2),flexDirection:"row",
                                                borderTopWidth:1,borderTopColor:"#ccc"}}>
                                        <Icon name="ios-help-circle" style={{fontSize:responsiveFontSize(4),color:"#0256b1",marginRight:responsiveWidth(2)}}/>
                                        <Text style={{fontSize:responsiveFontSize(2.5),color:"#0246b1"}}>
                                            YOYO Assist 
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        ))}
                        {(this.state.bookingPending ==0) &&
                            <View  style={{alignItems:"center",justifyContent:"center",height:responsiveHeight(100)}}>
                                <Image source={require("../../../../../assets/splash.png")} style={{height:responsiveHeight(20),width:responsiveWidth(70),}} />
                                <Text style={{color:"#0a1d3b",fontSize:responsiveFontSize(2.5),fontWeight:"bold"}}>
                                    Sorry! No Upcoming Bookings Found</Text>
                            </View>

                        }
                    </ScrollView>  
                }
                {(this.state.loading) &&
                    <View  style={{alignItems:"center",justifyContent:"center",flex:1}}>
                    {/* <Image source={require("../../../../../assets/splash.png")} style={{height:responsiveHeight(40),width:responsiveWidth(60),}} /> */}
                        <Spinner color="#0246b1" size="large"/>
                    </View>
                }
            </View>
    )
  else if(this.state.activePage === 2)
    return (
        <View style={{flex:1,backgroundColor:"#e6e6e6"}}>
            {(!this.state.loading) &&
                <ScrollView>
                    {(this.state.bookingCompleted.length>0) &&
                        this.state.bookingCompleted.map((item,i) => (
                        <View key={i} style={{backgroundColor:"#fff",margin:responsiveWidth(5),
                                    marginTop:responsiveHeight(3),borderRadius:responsiveWidth(3)}}>
                            <TouchableOpacity onPress={()=>this.props.navigation.navigate("bookingDetails",{hotel_id:item.hotel_id,booking_id:item.booking_id})}>
                                <View style={{alignItems:"center",justifyContent:"space-between",flexDirection:"row",
                                            paddingHorizontal:responsiveWidth(5),paddingVertical:responsiveHeight(1),borderBottomWidth:1,borderBottomColor:"#ccc"}}>
                                    <Text style={{fontSize:responsiveFontSize(2.5),fontWeight:"bold"}}>
                                        {item.city_name}
                                    </Text>
                                    <Text style={{fontSize:responsiveFontSize(2),opacity:0.7,fontWeight:"600"}}>
                                        Booked on {item.booked_on}
                                    </Text>
                                </View>
                                <View style={{paddingVertical:responsiveHeight(3),paddingHorizontal:responsiveWidth(5),flexDirection:"row"}}>
                                    <View style={{alignItems:"center",justifyContent:"center"}}>
                                        <Image
                                            source={{uri:item.best_img}} 
                                            resizeMode="stretch"
                                            style={{height:responsiveHeight(15),width:responsiveWidth(25),borderRadius:responsiveWidth(3)}}
                                                    />
                                    </View>
                                    <View style={{paddingHorizontal:responsiveWidth(5),justifyContent:"center",alignItems:"center",
                                                width:responsiveWidth(55),borderLeftColor:"#ccc",borderLeftWidth:3,marginLeft:responsiveWidth(3)}}>
                                        <Text style={{fontSize:responsiveFontSize(2.5),color:"#000",fontWeight:"bold",
                                                    marginBottom:responsiveHeight(1),}}>
                                            {item.full_name}
                                        </Text>
                                        <View style={{flexDirection:"row",alignItems:"center",justifyContent:"space-between"}}>
                                            <Text style={{fontSize:responsiveFontSize(2.2),color:"#000",opacity:0.7,fontWeight:"500",
                                                        marginBottom:responsiveHeight(1),textAlign:"center"}}>
                                                {item.check_in} 
                                            </Text>
                                            <Text style={{fontSize:responsiveFontSize(2.2),color:"#000",opacity:0.7,fontWeight:"500",
                                                        marginBottom:responsiveHeight(1),marginLeft:responsiveWidth(3),textAlign:"center"}}>
                                                ({item.check_in_time}) 
                                            </Text>
                                        </View>
                                        <Text style={{fontSize:responsiveFontSize(2.2),color:"#000",fontWeight:"500",
                                                    marginBottom:responsiveHeight(1),backgroundColor:"#0246b1",paddingVertical:responsiveHeight(1),
                                                    paddingHorizontal:responsiveWidth(3),color:"#fff"}}>
                                            {item.total_night==0?item.total_hours+' H ':item.total_night+' N '}
                                        </Text>
                                        <View style={{flexDirection:"row",alignItems:"center",justifyContent:"space-between"}}>
                                            <Text style={{fontSize:responsiveFontSize(2.2),color:"#000",opacity:0.7,fontWeight:"500",
                                                        marginBottom:responsiveHeight(1),textAlign:"center"}}>
                                                {item.check_out} 
                                            </Text>
                                            <Text style={{fontSize:responsiveFontSize(2.2),color:"#000",opacity:0.7,fontWeight:"500",
                                                        marginBottom:responsiveHeight(1),marginLeft:responsiveWidth(3),textAlign:"center"}}>
                                                ({item.check_out_time}) 
                                            </Text>
                                        </View>
                                    </View>
                                </View>
                                <View style={{flexDirection:"row",paddingHorizontal:responsiveWidth(5),marginBottom:responsiveHeight(2),width:responsiveWidth(50)}}> 
                                    <Text style={{fontSize:responsiveFontSize(2.2),color:"#000",fontWeight:"900",marginRight:responsiveWidth(2)}}>
                                        Booked for : 
                                    </Text>
                                    <Text style={{fontSize:responsiveFontSize(2.2),color:"#000",fontWeight:"bold",textAlign:"center"}}>
                                        {item.customer_name} 
                                    </Text>
                                </View>
                            </TouchableOpacity>
                            <View style={{borderTopWidth:1,borderTopColor:"#ccc",flexDirection:"row",alignItems:"center",justifyContent:"space-between",
                                        paddingHorizontal:responsiveWidth(5)}}>
                                <TouchableOpacity 
                                    onPress={()=>this.props.navigation.navigate("hotelDeatilsOuter",
                                                                                {hotel_id:item.hotel_id,
                                                                                hotel_name:item.full_name})}
                                    >
                                    <View style={{alignItems:"center",justifyContent:"center",paddingVertical:responsiveHeight(2),flexDirection:"row",
                                                }}>
                                        <Icon name="ios-clock" style={{fontSize:responsiveFontSize(4),color:"#0256b1",marginRight:responsiveWidth(2)}}/>
                                        <Text style={{fontSize:responsiveFontSize(2.5),color:"#0246b1"}}>
                                            Book Again 
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={()=>alert("yoyo assist")}>
                                    <View style={{alignItems:"center",justifyContent:"center",paddingVertical:responsiveHeight(2),flexDirection:"row",
                                                }}>
                                        <Icon name="ios-help-circle" style={{fontSize:responsiveFontSize(4),color:"#0256b1",marginRight:responsiveWidth(2)}}/>
                                        <Text style={{fontSize:responsiveFontSize(2.5),color:"#0246b1"}}>
                                            YOYO Assist 
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                    ))}
                    {(this.state.bookingCompleted == 0) &&
                        <View  style={{alignItems:"center",height:responsiveHeight(100),marginTop:responsiveHeight(20)}}>
                            <Image source={require("../../../../../assets/splash.png")} style={{height:responsiveHeight(20),width:responsiveWidth(70),}} />
                            <Text style={{color:"#0a1d3b",fontSize:responsiveFontSize(2.5),fontWeight:"bold"}}>
                                Sorry! No Completed Bookings Found</Text>
                        </View>

                    }
                </ScrollView>  
            }
            {(this.state.loading) &&
                <View  style={{alignItems:"center",justifyContent:"center",flex:1}}>
                {/* <Image source={require("../../../../../assets/splash.png")} style={{height:responsiveHeight(40),width:responsiveWidth(60),}} /> */}
                    <Spinner color="#0246b1" size="large"/>
                </View>
            }
        </View>
    )
    else 
    return (
        <View style={{flex:1,backgroundColor:"#e6e6e6"}}>
            {(!this.state.loading) &&
                <ScrollView>
                    {(this.state.bookingsCancelled.length>0) &&
                        this.state.bookingsCancelled.map((item,i) => (
                        <View key={i} style={{backgroundColor:"#fff",margin:responsiveWidth(5),
                                    marginTop:responsiveHeight(3),borderRadius:responsiveWidth(3)}}>
                            <TouchableOpacity onPress={()=>this.props.navigation.navigate("bookingDetails",{hotel_id:item.hotel_id,booking_id:item.booking_id})}>
                                <View style={{alignItems:"center",justifyContent:"space-between",flexDirection:"row",
                                            paddingHorizontal:responsiveWidth(5),paddingVertical:responsiveHeight(1),borderBottomWidth:1,borderBottomColor:"#ccc"}}>
                                    <Text style={{fontSize:responsiveFontSize(2.5),fontWeight:"bold"}}>
                                        {item.city_name}
                                    </Text>
                                    <Text style={{fontSize:responsiveFontSize(2),opacity:0.7,fontWeight:"600"}}>
                                        Booked on {item.booked_on}
                                    </Text>
                                </View>
                                <View style={{paddingVertical:responsiveHeight(3),paddingHorizontal:responsiveWidth(5),flexDirection:"row"}}>
                                    <View style={{alignItems:"center",justifyContent:"center"}}>
                                        <Image
                                            source={{uri:item.best_img}} 
                                            resizeMode="stretch"
                                            style={{height:responsiveHeight(15),width:responsiveWidth(25),borderRadius:responsiveWidth(3)}}
                                                    />
                                    </View>
                                    <View style={{paddingHorizontal:responsiveWidth(5),justifyContent:"center",alignItems:"center",
                                                width:responsiveWidth(55),borderLeftColor:"#ccc",borderLeftWidth:3,marginLeft:responsiveWidth(3)}}>
                                        <Text style={{fontSize:responsiveFontSize(2.5),color:"#000",fontWeight:"bold",
                                                    marginBottom:responsiveHeight(1),}}>
                                            {item.full_name}
                                        </Text>
                                        <View style={{flexDirection:"row",alignItems:"center",justifyContent:"space-between"}}>
                                            <Text style={{fontSize:responsiveFontSize(2.2),color:"#000",opacity:0.7,fontWeight:"500",
                                                        marginBottom:responsiveHeight(1),textAlign:"center"}}>
                                                {item.check_in} 
                                            </Text>
                                            <Text style={{fontSize:responsiveFontSize(2.2),color:"#000",opacity:0.7,fontWeight:"500",
                                                        marginBottom:responsiveHeight(1),marginLeft:responsiveWidth(3),textAlign:"center"}}>
                                                ({item.check_in_time}) 
                                            </Text>
                                        </View>
                                        <Text style={{fontSize:responsiveFontSize(2.2),color:"#000",fontWeight:"500",
                                                    marginBottom:responsiveHeight(1),backgroundColor:"#0246b1",paddingVertical:responsiveHeight(1),
                                                    paddingHorizontal:responsiveWidth(3),color:"#fff"}}>
                                            {item.total_night==0?item.total_hours+' H ':item.total_night+' N '}
                                        </Text>
                                        <View style={{flexDirection:"row",alignItems:"center",justifyContent:"space-between"}}>
                                            <Text style={{fontSize:responsiveFontSize(2.2),color:"#000",opacity:0.7,fontWeight:"500",
                                                        marginBottom:responsiveHeight(1),textAlign:"center"}}>
                                                {item.check_out} 
                                            </Text>
                                            <Text style={{fontSize:responsiveFontSize(2.2),color:"#000",opacity:0.7,fontWeight:"500",
                                                        marginBottom:responsiveHeight(1),marginLeft:responsiveWidth(3),textAlign:"center"}}>
                                                ({item.check_out_time}) 
                                            </Text>
                                        </View>
                                    </View>
                                </View>
                                <View style={{flexDirection:"row",paddingHorizontal:responsiveWidth(5),marginBottom:responsiveHeight(2),width:responsiveWidth(50)}}> 
                                    <Text style={{fontSize:responsiveFontSize(2.2),color:"#000",fontWeight:"900",marginRight:responsiveWidth(2)}}>
                                        Booked for : 
                                    </Text>
                                    <Text style={{fontSize:responsiveFontSize(2.2),color:"#000",fontWeight:"bold",textAlign:"center"}}>
                                        {item.customer_name} 
                                    </Text>
                                </View>
                            </TouchableOpacity>
                            <View style={{borderTopWidth:1,borderTopColor:"#ccc",flexDirection:"row",alignItems:"center",justifyContent:"space-between",
                                        paddingHorizontal:responsiveWidth(5)}}>
                                <TouchableOpacity 
                                    onPress={()=>this.props.navigation.navigate("hotelDeatilsOuter",
                                                                                {hotel_id:item.hotel_id,
                                                                                hotel_name:item.full_name})}
                                    >
                                    <View style={{alignItems:"center",justifyContent:"center",paddingVertical:responsiveHeight(2),flexDirection:"row",
                                                }}>
                                        <Icon name="ios-clock" style={{fontSize:responsiveFontSize(4),color:"#0256b1",marginRight:responsiveWidth(2)}}/>
                                        <Text style={{fontSize:responsiveFontSize(2.5),color:"#0246b1"}}>
                                            Book Again 
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                                <TouchableOpacity onPress={()=>alert("yoyo assist")}>
                                    <View style={{alignItems:"center",justifyContent:"center",paddingVertical:responsiveHeight(2),flexDirection:"row",
                                                }}>
                                        <Icon name="ios-help-circle" style={{fontSize:responsiveFontSize(4),color:"#0256b1",marginRight:responsiveWidth(2)}}/>
                                        <Text style={{fontSize:responsiveFontSize(2.5),color:"#0246b1"}}>
                                            YOYO Assist 
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        </View>
                    ))}
                    {(this.state.bookingsCancelled == 0) &&
                        <View  style={{alignItems:"center",height:responsiveHeight(100),marginTop:responsiveHeight(20)}}>
                            <Image source={require("../../../../../assets/splash.png")} style={{height:responsiveHeight(20),width:responsiveWidth(70),}} />
                            <Text style={{color:"#0a1d3b",fontSize:responsiveFontSize(2.5),fontWeight:"bold"}}>
                                Sorry! No Cancelled Bookings Found</Text>
                        </View>

                    }
                </ScrollView>  
            }
            {(this.state.loading) &&
                <View  style={{alignItems:"center",justifyContent:"center",flex:1}}>
                {/* <Image source={require("../../../../../assets/splash.png")} style={{height:responsiveHeight(40),width:responsiveWidth(60),}} /> */}
                    <Spinner color="#0246b1" size="large"/>
                </View>
            }
        </View>
    )
}

render(){
   //console.log(this.state.searchHistory)
 return (
    <SafeAreaView style={{flex:1}}>
        <Container >
            <View>
                <View style={{flexDirection:"row",alignItems:"center",justifyContent:"space-between",borderBottomWidth:1,
                        borderBottomColor:"#a6a6a6",paddingHorizontal:responsiveWidth(5),paddingVertical:responsiveHeight(2),height:"auto",backgroundColor: "#fff"}}>
                    <TouchableOpacity onPress={()=>this.props.navigation.goBack()}>
                        <Icon name='arrow-back'  style={{color:"#000",fontSize:responsiveFontSize(4)}}/>
                    </TouchableOpacity>
                    <TouchableOpacity style={{width:responsiveWidth(80)}}>
                        <Text style={{fontSize:responsiveFontSize(2.5),fontWeight:"bold"}}>
                            Bookings
                        </Text>
                    </TouchableOpacity>
                    {/* <TouchableOpacity >
                        <Icon name='ios-heart-empty'   style={{color:"red",fontSize:responsiveFontSize(4)}}/>
                    </TouchableOpacity> */}
                </View>
            </View>
            <View style={{backgroundColor: "#e6e6e6",paddingHorizontal:responsiveWidth(5)}}>
                <Segment style={{backgroundColor: "#e6e6e6",height:"auto",marginTop:responsiveHeight(3)}}>
                    <View style={{flexDirection:"row"}}>
                        <TouchableOpacity onPress={this.selectComponent(1)}
                            style={{alignItems:"center",justifyContent:"center",paddingVertical:responsiveHeight(1),paddingHorizontal:responsiveWidth(3),width:"auto",
                                    height:"auto",backgroundColor:this.state.activePage==1?"#0246b1":"#fff",
                                    borderTopLeftRadius:responsiveWidth(5),borderBottomLeftRadius:responsiveWidth(5),borderWidth:1}}>
                            <Text style={{fontWeight:'bold',fontSize:responsiveFontSize(2.5),color:this.state.activePage==1?"#fff":"#0246b1"}}>
                                Upcoming
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.selectComponent(2)}
                            style={{alignItems:"center",justifyContent:"center",paddingVertical:responsiveHeight(1),paddingHorizontal:responsiveWidth(3),width:"auto",
                                    height:"auto",backgroundColor:this.state.activePage==2?"#0246b1":"#fff",borderWidth:1}}>
                            <Text style={{fontWeight:'bold',fontSize:responsiveFontSize(2.5),color:this.state.activePage==2?"#fff":"#0246b1",textAlign:"center"}}>
                                Completed
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={this.selectComponent(3)}
                            style={{alignItems:"center",justifyContent:"center",paddingVertical:responsiveHeight(1),paddingHorizontal:responsiveWidth(3),width:"auto",
                                    height:"auto",backgroundColor:this.state.activePage==3?"#0246b1":"#fff",borderWidth:1,
                                    borderTopRightRadius:responsiveWidth(5),borderBottomRightRadius:responsiveWidth(5)}}>
                            <Text style={{fontWeight:'bold',fontSize:responsiveFontSize(2.5),color:this.state.activePage==3?"#fff":"#0246b1",textAlign:"center"}}>
                                Cancelled
                            </Text>
                        </TouchableOpacity>
                    </View>
                </Segment>
            </View>
            <View style={{flex:1}}>
               {this._renderComponent()}
            </View>
        </Container>
    </SafeAreaView>
)}
} 




