import React from 'react';
import {Text, View,TouchableOpacity,AsyncStorage,ImageBackground,Image} from 'react-native';
import {Container,Icon,Input, Content,Toast,Card,CardItem,Spinner} from "native-base";
import {responsiveWidth,responsiveHeight,responsiveFontSize} from "react-native-responsive-dimensions";
import { SafeAreaView } from 'react-native-safe-area-context';
import { ScrollView } from 'react-native-gesture-handler';
import { NavigationActions } from 'react-navigation'

const backAction = NavigationActions.back({
  key: null
})


export default class HotelList extends React.Component {
    constructor(props){
        super(props);
        this.state={
          city:this.props.navigation.getParam('city_name', 'NO-city-Selected'),
          hotels:[],
          loading:true,
          heartToggle:false,
          heartid:null,
        }
        this.getHotels();
    } 
  
  getHotels = async() =>{
      var cityName = this.props.navigation.getParam('city_name', 'NO-city-Selected');
      //console.log(cityName)
        try{
          fetch("https://app.yoyorooms.net/Appapi/search_Hotel_By_City", 
              {
                  method: 'POST',
                  headers: {
                      Accept: 'application/json',
                      'Content-Type': 'application/json',
                  },
                  body: JSON.stringify({
                              city_name  : cityName    
                          }),     
              })
              .then(response => response.json())
              .then(response2 => {
                //console.log(response2)
                  if(response2){
                    this.setState({hotels:response2})
                    this.setState({loading:false})               
                  }
                  else{
                      this.setState({loading:false})  
                    //MerryToast.show('Something went wrong! Please try again.', MerryToast.SHORT);
                  }
              }).catch((error) =>{
                  this.setState({loading:false})  
                //MerryToast.show('Something went wrong! Please try again.', MerryToast.SHORT);
                console.log(error)
                  // result.status = false;
                  // result.data = undefined;
                  // reject(result)
              });
          
      }
      catch(error){
          this.setState({loading:false}) 
        //MerryToast.show('Something went wrong! Please try again.', MerryToast.SHORT);
          // result.status = false;
          // result.message = error;
          // result.data = undefined;
          // resolve(result)
      }
      
  
    }
    saveHotel = async(hid,city,i)=>{
        if(i==this.state.heartid){
            this.setState({heartToggle:!this.state.heartToggle})
        }
        else{
            this.setState({heartid:i})
        }
        var user_id = await AsyncStorage.getItem('uid');
        //console.log(cityName)
          try{
            fetch("https://app.yoyorooms.net/Appapi/save_user_hotel", 
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                                uid  : user_id ,
                                hotelid :hid,
                                city :city 
                            }),     
                })
                .then(response => response.json())
                .then(response2 => {
                  //console.log(response2)
                    if(response2==1){
                        this.setState({loading:false})
                        alert("added suceesfully")
                      //console.log(this.state.Savedhotels[0].hotelinfo[0].short_name)              
                    }
                    else{
                        this.setState({loading:false})  
                      //MerryToast.show('Something went wrong! Please try again.', MerryToast.SHORT);
                    }
                }).catch((error) =>{
                    this.setState({loading:false})  
                  //MerryToast.show('Something went wrong! Please try again.', MerryToast.SHORT);
                  console.log(error)
                    // result.status = false;
                    // result.data = undefined;
                    // reject(result)
                });
            
        }
        catch(error){
            this.setState({loading:false}) 
          //MerryToast.show('Something went wrong! Please try again.', MerryToast.SHORT);
            // result.status = false;
            // result.message = error;
            // result.data = undefined;
            // resolve(result)
        }
      }
 render(){
   //console.log(this.state.hotels)
 return (
    <SafeAreaView style={{flex:1}}>
        <Container >
            <View>
              <View style={{flexDirection:"row",alignItems:"center",justifyContent:"space-between",borderBottomWidth:1,
                        borderBottomColor:"#a6a6a6",paddingHorizontal:responsiveWidth(5),paddingVertical:responsiveHeight(2),height:"auto",backgroundColor: "#fff"}}>
                    <TouchableOpacity onPress={()=> this.props.navigation.dispatch(backAction)}>
                        <Icon name='arrow-back'  style={{color:"#000",fontSize:responsiveFontSize(4)}}/>
                    </TouchableOpacity>
                    <TouchableOpacity style={{width:responsiveWidth(80)}} >
                        <Text style={{fontSize:responsiveFontSize(2.5),fontWeight:"bold"}}>
                            Hotels in {this.state.city}
                        </Text>
                    </TouchableOpacity>
                    {/* <TouchableOpacity >
                        <Icon name='ios-heart'   style={{color:"red",fontSize:responsiveFontSize(4)}}/>
                    </TouchableOpacity> */}
                </View>
            </View>
            <View style={{flex:1,backgroundColor:"#e6e6e6"}}>
                {(!this.state.loading) &&
                    <ScrollView>
                        {(this.state.hotels.length>0) &&
                            this.state.hotels.map((item,i) => (
                            <View key={i} style={{backgroundColor:"#fff",margin:responsiveWidth(5),
                                        marginTop:responsiveHeight(3),borderRadius:responsiveWidth(3)}}>
                                <TouchableOpacity onPress={()=>this.props.navigation.navigate("hotelDeatils",{hotel_id:item.id,hotel_name:item.full_name})}>
                                    <View>
                                        <ImageBackground source={{uri:item.best_img}} resizeMode="stretch"
                                                style={{height:"auto",width:responsiveWidth(90),borderTopLeftRadius:responsiveWidth(3),
                                                        borderTopRightRadius:responsiveWidth(3),overflow:"hidden"}}>
                                            <View style={{alignItems:"flex-end"}}>
                                                <TouchableOpacity onPress={()=>this.saveHotel(item.id,item.city_name,i)} style={{margin:responsiveWidth(5)}}>
                                                    <Icon 
                                                        name={this.state.heartToggle || this.state.heartid==i?'ios-heart':'ios-heart-empty'}   
                                                        style={{color:"red",fontSize:responsiveFontSize(4)}}/>
                                                </TouchableOpacity>
                                            </View>
                                            <View style={{justifyContent:"center",alignItems:"center",flexDirection:"row",backgroundColor:"red",margin:responsiveWidth(5),
                                                        width:responsiveWidth(20),padding:responsiveWidth(1),marginTop:responsiveHeight(13)}}>
                                                <Text style={{fontSize:responsiveFontSize(2.2),color:"#fff",fontWeight:"bold",}}>
                                                    {item.rating}
                                                </Text>
                                                <Icon name='star'   style={{color:"#fff",fontSize:responsiveFontSize(3),marginLeft:responsiveWidth(3)}}/>
                                            </View>
                                        </ImageBackground>
                                    </View>
                                    <View style={{paddingHorizontal:responsiveWidth(5),paddingBottom:responsiveHeight(1),marginTop:responsiveHeight(2),justifyContent:"space-between"}}>
                                        <Text style={{fontSize:responsiveFontSize(2.5),color:"#000",fontWeight:"bold"}}>
                                            {item.full_name}
                                        </Text>

                                        <Text style={{fontSize:responsiveFontSize(2),color:"#000",opacity:0.5,fontWeight:"bold",}}>
                                            {item.full_address}
                                        </Text>
                                        <View style={{flexDirection:"row",alignItems:"center"}}>
                                            <Text style={{fontSize:responsiveFontSize(2.1),color:"#000",fontWeight:"bold",}}>
                                                ₹ {item.price-((item.price*item.discount)/100)}
                                            </Text>
                                            <Text style={{fontSize:responsiveFontSize(1.7),color:"#000",opacity:0.7,marginLeft:responsiveWidth(3),textDecorationLine:"line-through"}}>
                                                ₹ {item.price}
                                            </Text>
                                            <Text style={{fontSize:responsiveFontSize(1.7),color:"#ff9900",fontWeight:"bold",marginLeft:responsiveWidth(3)}}>
                                                {item.discount} % OFF
                                            </Text>
                                        </View>
                                    </View>
                                </TouchableOpacity>
                            </View>
                        ))}
                        {(this.state.hotels.length<=0) &&
                            <View  style={{alignItems:"center",justifyContent:"center",height:responsiveHeight(100)}}>
                                <Image source={require("../../../../../../assets/splash.png")} style={{height:responsiveHeight(20),width:responsiveWidth(70),}} />
                                <Text style={{color:"#0a1d3b",fontSize:responsiveFontSize(2.5),fontWeight:"bold"}}>Sorry! No Hotels Found in {this.state.city}</Text>
                            </View>

                        }
                    </ScrollView>  
                }
                {(this.state.loading) &&
                    <View  style={{alignItems:"center",justifyContent:"center",flex:1}}>
                    {/* <Image source={require("../../../../../assets/splash.png")} style={{height:responsiveHeight(40),width:responsiveWidth(60),}} /> */}
                        <Spinner color="#0246b1" size="large"/>
                    </View>
                }
            </View>
        </Container>
    </SafeAreaView>
)}
} 




