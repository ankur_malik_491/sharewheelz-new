import React, {Component} from 'react';
import {Text,View} from 'react-native';
import {responsiveWidth,responsiveHeight,responsiveFontSize} from "react-native-responsive-dimensions";
import ImageView from 'react-native-image-view';
import { SafeAreaView } from 'react-native-safe-area-context';

const images = [];
const arr= [{source:{uri:"https://i.imgur.com/oz0vfuU.png"},
                        title:"No images",
                        height:responsiveHeight(40),
                        width:responsiveWidth(100)}]
var arrval= false;

export default class App extends Component {
    constructor(props) {
        super(props);
        
        this.state = {
            imageIndex: 0,
            isImageViewVisible: true,
            arrval:0,
           
        };
        this.setimg();
       
    }
    setimg = () =>{
        const imgarray = this.props.navigation.getParam('hotelimages', 'NO-Image-Selected');
        if(imgarray!=0){
            arrval=true
            //console.log(arrval)
        imgarray.map((item,i)=>(
            images.push({source:{uri:item.image_path},title:item.category,height:responsiveHeight(40),width:responsiveWidth(100)})
        ))
        }
        else {
            
            arrval=false
        }
        //console.log(images)
    }

    renderFooter({title}) {
        return (
            <View style={{alignItems:"center",justifyContent:"center"}}>
                <Text style={{fontSize:responsiveFontSize(3),color:"#fff",margin:responsiveWidth(10)}}>{title}</Text>
            </View>
        );
    }

    render() {
        const {isImageViewVisible,imageIndex} = this.state;
        //console.log(arrval)
        return (
            <SafeAreaView style={{flex:1,}}>
                <View style={{flex:1,alignItems:"center",justifyContent:"center",backgroundColor:"#000"}}>
                    <ImageView
                        images={arrval?images:arr}
                        imageIndex={imageIndex}
                        animationType="fade"
                        isVisible={isImageViewVisible}
                        renderFooter={this.renderFooter}
                        controls={{next:true,prev:true}}
                        onClose={() => {this.setState({isImageViewVisible: false}),this.props.navigation.goBack()}}
                    
                    />
                </View>
            </SafeAreaView>
        );
    }
}