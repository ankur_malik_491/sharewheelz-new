import React from 'react';
import {Text, View,TouchableOpacity,Linking,ImageBackground,Image,Modal,TextInput,AsyncStorage} from 'react-native';
import {Container,Icon,Spinner, Thumbnail,Toast} from "native-base";
import {responsiveWidth,responsiveHeight,responsiveFontSize} from "react-native-responsive-dimensions";
import { ScrollView } from 'react-native-gesture-handler';
import { SafeAreaView } from 'react-native-safe-area-context';
import BookingModal from '../.../../../../../../components/Modals/BookingMOdal';
import Loader from 'react-native-loading-spinner-overlay';
 
export default class HotelDetails extends React.Component {
    constructor(props){
      super(props);
      this.state={
        hotelID:this.props.navigation.getParam('hotel_id', 'NO-id-Selected'),
        hotelname:this.props.navigation.getParam('hotel_name', 'NO-Name-Selected'),
        Hotel:[],
        HotelDetails:[],
        Hotelfacilities:[],
        HotelImages:[],
        HotelRooms:[],
        HotelSections:[],
        loading:true,
        bookLoader:false,
        submitLoader:false,
        loader:false,
        select:false,
        activeTab:1,
        rooms:1,
        adults:1,
        childs:0,
        nights:1,
        selectRoomDetails:[],
        roomId:0,
        bookingType:"normal",
        selectprice:false,
        showGuestModal:false,
        name:"",
        phone:"",
        mobile:"",
        userName:"",
        bookingLoader:false,
      }
      this.getHotels();
     
  }
  getHotels = async() =>{
      
    var userid = await AsyncStorage.getItem('uid');
    var uname = await AsyncStorage.getItem('uname');
    var uphone = await AsyncStorage.getItem('uPhone');
    this.setState({uid:userid,name:uname,userName:uname,phone:uphone,mobile:uphone})
    var hotelid = this.props.navigation.getParam('hotel_id', 'NO-id-Selected');
    //console.log(hotelid )
      try{
        fetch("https://app.yoyorooms.net/Appapi/hotel_details", 
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                            hotel_id : hotelid   
                        }),     
            })
            .then(response => response.json())
            .then(response2 => {
                //console.log(response2)
                if(response2){
                  this.setState({Hotel:response2})
                  this.setState({HotelDetails:response2.hotel_details})
                  this.setState({Hotelfacilities:response2.facilities})
                  this.setState({HotelImages:response2.images})
                  this.setState({HotelRooms:response2.rooms})
                  this.setState({HotelSections:response2.section})
                  this.setState({loading:false}) 
                }
                else{
                    this.setState({loading:false})  
                    Toast.show({
                        text:"Something Went Wrong. Try Again!",
                        textStyle:{textAlign:"center"},
                        duration:2000,
                        style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
                    });
                }
            }).catch((error) =>{
                this.setState({loading:false})
                Toast.show({
                    text:"Something Went Wrong. Try Again!",
                    textStyle:{textAlign:"center"},
                    duration:2000,
                    style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
                });
            });
        
    }
    catch(error){
        this.setState({loading:false}) 
        Toast.show({
            text:"Something Went Wrong. Try Again!",
            textStyle:{textAlign:"center"},
            duration:2000,
            style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
        });
    }
    

  } 

  openExternalApp(url) {
    if(url!=""){
    Linking.canOpenURL(url).then(supported => {
      if (supported) {
        Linking.openURL(url);
      } 
      else {
        alert("maps not supported please vist" + url);
      }
    });
    }
    else{
        alert("maps url not found");
    }
  }
setModalVal = (modalVal)=>{
    this.setState({bookLoader:!modalVal})
}
setModalValue = (modalVal,slctRoomsDetails)=>{
    if(this.state.bookingType=="normal"){
        this.setState({select:true,loader:true,bookLoader:!modalVal,})
        this.setState({selectRoomDetails:{
                                        startDate:slctRoomsDetails.srtDate,
                                        endDate:slctRoomsDetails.endDate,
                                        totalRooms:slctRoomsDetails.tRooms,
                                        totalRoomsLeng:slctRoomsDetails.tRoomsLeng,
                                        totalAdults:slctRoomsDetails.tAdults,
                                        totalChilds:slctRoomsDetails.tChilds,
                                        totalNights:slctRoomsDetails.tNights,
                                        roomID:slctRoomsDetails.roomID,
                                        roomType:slctRoomsDetails.roomType

                                    }
        })
        this.getfare();
    }
    else{
        this.setState({select:true,loader:true,bookLoader:!modalVal})
        this.setState({selectRoomDetails:{
                                        startDate:slctRoomsDetails.srtDate,
                                        endDate:slctRoomsDetails.endDate,
                                        StartTime:slctRoomsDetails.startTime,
                                        endTime:slctRoomsDetails.endTime,
                                        totalRooms:slctRoomsDetails.tRooms,
                                        totalRoomsLeng:slctRoomsDetails.tRoomsLeng,
                                        totalAdults:slctRoomsDetails.tAdults,
                                        totalChilds:slctRoomsDetails.tChilds,
                                        totalHours:slctRoomsDetails.tHours,
                                        roomID:slctRoomsDetails.roomID,
                                        roomType:slctRoomsDetails.roomType

                                    }
                    })
                //console.log(this.state.selectRoomDetails)   
    this.getfare();
    }
}
getfare = ()=>{
    var roomDetails = this.state.selectRoomDetails;
    try{
        if(this.state.bookingType=="normal"){
            fetch("https://app.yoyorooms.net/Appapi/calculateprice_normal", 
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                                        roomid:roomDetails.roomID,
                                        checkin:roomDetails.startDate,
                                        checkout:roomDetails.endDate,
                                        adult:roomDetails.totalAdults,
                                        child:roomDetails.totalChilds,
                                        night:roomDetails.totalNights,
                                        rooms :roomDetails.totalRooms 
                            }),     
                })
                .then(response => response.json())
                .then(response2 => {
                //console.log(response2)
                this.setState({loader:false})
                    if(response2){
                    this.setState({priceDetails:response2.price,selectprice:true})            
                    }
                    else{
                        this.setState({loader:false})
                        Toast.show({
                            text:"Something Went Wrong. Try Again!",
                            textStyle:{textAlign:"center"},
                            duration:2000,
                            style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
                        });
                    }
                }).catch((error) =>{
                    this.setState({loader:false}) 
                    Toast.show({
                        text:"Something Went Wrong. Try Again!",
                        textStyle:{textAlign:"center"},
                        duration:2000,
                        style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
                    });
                });
        }
        else{
            fetch("https://app.yoyorooms.net/Appapi/calculateprice_payperhour", 
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                                        roomid:roomDetails.roomID,
                                        checkin:roomDetails.startDate,
                                        checkout:roomDetails.endDate,
                                        checkinTime:roomDetails.StartTime,
                                        checkoutTime:roomDetails.endTime,
                                        adult:roomDetails.totalAdults,
                                        child:roomDetails.totalChilds,
                                        hour:roomDetails.totalHours,
                                        rooms :roomDetails.totalRooms 
                            }),     
                })
                .then(response => response.json())
                .then(response2 => {
                    //console.log(response2)
                    if(response2){
                        this.setState({loader:false})
                        this.setState({priceDetails:response2.price,selectprice:true})            
                    }
                    else{
                        console.log("else")
                        this.setState({loader:false,selectprice:false})
                        Toast.show({
                            text:"Something Went Wrong. Try Again!",
                            textStyle:{textAlign:"center"},
                            duration:2000,
                            style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
                        })
                    }
                }).catch((error) =>{
                    console.log(error)
                    this.setState({loader:false,selectprice:false}) 
                    Toast.show({
                        text:"Something Went Wrong. Try Again!",
                        textStyle:{textAlign:"center"},
                        duration:2000,
                        style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
                    });
                });
        }
        
    }
    catch(error){
        console.log(error)
        this.setState({loader:false,selectprice:false})
        Toast.show({
            text:"Something Went Wrong. Try Again!",
            textStyle:{textAlign:"center"},
            duration:2000,
            style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
        });
    }
}
UpdateBookingUser = () => {
    this.setState({mobile:this.state.phone,userName:this.state.name,showGuestModal:false})
}
booking = async() =>{
    this.setState({bookingLoader:true})
    var hotel_Details = this.state.selectRoomDetails;
    var price_details = this.state.priceDetails;                                 
    //console.log(price_details)
    try{
        if(this.state.bookingType=="normal")
        { 
            fetch("https://app.yoyorooms.net/Appapi/save_booking_normal", 
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                                uid:this.state.uid,
                                hotelid : this.state.hotelID,
                                roomid:hotel_Details.roomID,
                                totalroom:hotel_Details.totalRoomsLeng,
                                totaladults:hotel_Details.totalAdults,
                                totalchilds:hotel_Details.totalChilds,
                                check_in:hotel_Details.startDate,
                                check_out:hotel_Details.endDate,
                                totalnight:hotel_Details.totalNights,
                                gname:this.state.userName,
                                gphone:this.state.mobile,
                                ttlprice:price_details.maxpayable,
                                discount:price_details.discount,
                                ttlpayable:price_details.payable,


                          }),     
                })
                .then(response => response.json())
                .then(response2 => {
                    //console.log(response2.booking_Details[0].booking_id)
                    if(response2.status==1){
                        alert(`Thankyou for booking, your booking id:${response2.booking_Details[0].booking_id}. Please keep this for refference!`)
                        this.setState({bookingLoader:false}) 
                        this.props.navigation.goBack();
                    }
                    else{
                        this.setState({bookingLoader:false})  
                        Toast.show({
                            text:"Something Went Wrong. Try Again!",
                            textStyle:{textAlign:"center"},
                            duration:2000,
                            style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
                        });
                    }
                }).catch((error) =>{
                    this.setState({bookingLoader:false})
                    Toast.show({
                        text:"Something Went Wrong. Try Again!",
                        textStyle:{textAlign:"center"},
                        duration:2000,
                        style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
                    });
                });
        }
        else{
            fetch("https://app.yoyorooms.net/Appapi/save_booking_payperhour", 
                {
                    method: 'POST',
                    headers: {
                        Accept: 'application/json',
                        'Content-Type': 'application/json',
                    },
                    body: JSON.stringify({
                                uid:this.state.uid,
                                hotelid : this.state.hotelID,
                                roomid:hotel_Details.roomID,
                                totalroom:hotel_Details.totalRoomsLeng,
                                totaladults:hotel_Details.totalAdults,
                                totalchilds:hotel_Details.totalChilds,
                                check_in:hotel_Details.startDate,
                                check_out:hotel_Details.endDate,
                                check_in_time:hotel_Details.StartTime,
                                check_out_time:hotel_Details.endTime,
                                totalhour:hotel_Details.totalHours,
                                gname:this.state.userName,
                                gphone:this.state.mobile,
                                ttlprice:price_details.maxpayable,
                                discount:price_details.discount,
                                ttlpayable:price_details.payable,


                          }),     
                })
                .then(response => response.json())
                .then(response2 => {
                    //console.log(response2.booking_Details[0].booking_id)
                    if(response2.status==1){
                        alert(`Thankyou for booking, your booking id:${response2.booking_Details[0].booking_id}. Please keep this for refference!`)
                        this.setState({bookingLoader:false}) 
                        this.props.navigation.goBack();
                    }
                    else{
                        this.setState({bookingLoader:false})  
                        Toast.show({
                            text:"Something Went Wrong. Try Again!",
                            textStyle:{textAlign:"center"},
                            duration:2000,
                            style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
                        });
                    }
                }).catch((error) =>{
                    this.setState({bookingLoader:false})
                    Toast.show({
                        text:"Something Went Wrong. Try Again!",
                        textStyle:{textAlign:"center"},
                        duration:2000,
                        style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
                    });
                });

        }
      
    }
    catch(error){
        this.setState({bookingLoader:false})  
      Toast.show({
          text:"Something Went Wrong. Try Again!",
          textStyle:{textAlign:"center"},
          duration:2000,
          style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
    });
}
    

  } 
 

render() {
  //console.log(this.state.HotelRooms[0])
return (
    <SafeAreaView style={{flex:1}}>
      <Container >
        <View>
            <View style={{flexDirection:"row",alignItems:"center",justifyContent:"space-between",borderBottomWidth:1,
                    borderBottomColor:"#a6a6a6",paddingHorizontal:responsiveWidth(5),paddingVertical:responsiveHeight(2),height:"auto",backgroundColor: "#fff"}}>
                <TouchableOpacity onPress={()=>this.props.navigation.goBack()}>
                    <Icon name='arrow-back'  style={{color:"#000",fontSize:responsiveFontSize(4)}}/>
                </TouchableOpacity>
                <TouchableOpacity style={{width:responsiveWidth(70)}}>
                    <Text style={{fontSize:responsiveFontSize(2.5),fontWeight:"bold"}}>
                        {this.state.hotelname}
                    </Text>
                </TouchableOpacity>
                <TouchableOpacity onPress={()=>alert("open share module")}>
                    <Icon name='md-share'   style={{color:"#000",fontSize:responsiveFontSize(4),}}/>
                </TouchableOpacity>
            </View>
        </View>
        <View style={{flex:1,backgroundColor:"#e6e6e6"}}>
            {(!this.state.loading) &&
                <View  style={{flex:1}}>
                    {(this.state.HotelDetails.length>0) && 
                        <ScrollView>
                            <TouchableOpacity 
                                onPress={()=>this.props.navigation.navigate('imageViewer',{hotelimages:this.state.HotelImages})}>
                                <ImageBackground source={{uri:this.state.HotelDetails[0].best_img}} 
                                    style={{height:"auto",width:responsiveWidth(100),backgroundColor:"#000"}}>
                                    <View style={{height:"auto",padding:responsiveWidth(5),paddingTop:responsiveHeight(10),
                                            backgroundColor:"rgba(0,0,0,0.7)"}}>
                                        <Text style={{color:"#fff",fontSize:responsiveFontSize(2.5),fontWeight:"bold"}}>
                                            YOYO {this.state.HotelDetails[0].hotel_type}
                                        </Text>
                                        <Text style={{color:"#fff",fontSize:responsiveFontSize(3),fontWeight:"bold",marginVertical:responsiveHeight(2)}}>
                                            {this.state.HotelDetails[0].full_name}
                                        </Text>
                                        {(this.state.HotelSections!=0) &&
                                            <View style={{flexDirection:"row",justifyContent:"space-between",alignItems:"center"}}>
                                                <View style={{justifyContent:"center",alignItems:"center",backgroundColor:"red",width:responsiveWidth(50)}}>
                                                    <Text style={{fontSize:responsiveFontSize(2),color:"#fff",fontWeight:"bold",marginVertical:responsiveHeight(1)}}>
                                                        {this.state.HotelSections[0].title}
                                                    </Text>
                                                </View>
                                                <View >
                                                    {(this.state.HotelImages!=0) &&
                                                        <Text style={{fontSize:responsiveFontSize(2.2),color:"#fff",marginVertical:responsiveHeight(1)}}>
                                                            {this.state.HotelImages.length} PHOTOS
                                                        </Text>
                                                    }
                                                    {(this.state.HotelImages==0) &&
                                                        <Text style={{fontSize:responsiveFontSize(2.2),color:"#fff",marginVertical:responsiveHeight(1)}}>
                                                            0 PHOTOS
                                                        </Text>
                                                    }
                                                </View>
                                        
                                            </View>
                                        }
                                        {(this.state.HotelSections==0) &&
                                            <View style={{flexDirection:"row",justifyContent:"space-between",alignItems:"center"}}>
                                                <View style={{justifyContent:"center",alignItems:"center",width:responsiveWidth(50)}}>
                                                    <Text style={{fontSize:responsiveFontSize(2),color:"#fff",fontWeight:"bold",marginVertical:responsiveHeight(1)}}>
                                                        
                                                    </Text>
                                                </View>
                                                <View >
                                                    {(this.state.HotelImages!=0) &&
                                                        <Text style={{fontSize:responsiveFontSize(2.2),color:"#fff",marginVertical:responsiveHeight(1)}}>
                                                            {this.state.HotelImages.length} PHOTOS
                                                        </Text>
                                                    }
                                                    {(this.state.HotelImages==0) &&
                                                        <Text style={{fontSize:responsiveFontSize(2.2),color:"#fff",marginVertical:responsiveHeight(1)}}>
                                                            0 PHOTOS
                                                        </Text>
                                                    }
                                                </View>
                                            </View>
                                        }
                                    </View>
                                </ImageBackground>
                            </TouchableOpacity>
                            <View style={{marginTop:responsiveHeight(2),backgroundColor:"#fff",paddingHorizontal:responsiveWidth(3)}}>
                                <View style={{flexDirection:"row",alignItems:"center",justifyContent:"space-between",borderBottomWidth:1,borderBottomColor:"#e6e6e6",paddingVertical:responsiveWidth(5)}}>
                                    <TouchableOpacity onPress={()=>this.openExternalApp(this.state.HotelDetails[0].location_url)}>
                                        <View style={{backgroundColor:"#e6e6e6",alignItems:"center",justifyContent:"center",borderRadius:responsiveWidth(3)}}>
                                            <Thumbnail square source={{uri:"https://app.yoyorooms.net/assets/images/map-pin2.jpg"}} 
                                                    style={{borderRadius:responsiveWidth(3)}}/>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={()=>this.openExternalApp(this.state.HotelDetails[0].location_url)}>
                                        <View style={{width:responsiveWidth(50)}}>
                                            <Text style={{fontSize:responsiveFontSize(1.8),opacity:0.7}}>
                                                {this.state.HotelDetails[0].full_address}
                                            </Text>
                                        </View>
                                    </TouchableOpacity>
                                    <TouchableOpacity onPress={()=>alert("open rating page ")}>
                                        <View style={{justifyContent:"center",alignItems:"center",flexDirection:"row",backgroundColor:"#0246b1",
                                                    paddingVertical:responsiveHeight(1),paddingHorizontal:responsiveWidth(3),borderRadius:responsiveWidth(3)}}>
                                            <Icon name='star'   style={{color:"#fff",fontSize:responsiveFontSize(3)}}/>
                                            <Text style={{fontSize:responsiveFontSize(2.2),color:"#fff",fontWeight:"bold",marginLeft:responsiveWidth(3)}}>
                                                {this.state.HotelDetails[0].rating}
                                            </Text>
                                        </View>
                                    </TouchableOpacity>
                                </View>
                                <ScrollView horizontal={true}>
                                    <View style={{flexDirection:"row",alignItems:"center",justifyContent:"center",paddingVertical:responsiveWidth(5)}}>
                                        {(this.state.Hotelfacilities.length>0) &&
                                        this.state.Hotelfacilities.map((item,i) => (
                                            <View key={i} style={{alignItems:"center",justifyContent:"space-between",paddingHorizontal:responsiveWidth(3),width:responsiveWidth(30)}} >
                                                <Icon name={item.app_icon} 
                                                    type="MaterialCommunityIcons"
                                                    style={{color:"#000",fontSize:responsiveFontSize(4)}}/>
                                                <Text style={{fontSize:responsiveFontSize(1.8),marginTop:responsiveHeight(2),textAlign:"center"}}>
                                                    {item.facility}
                                                </Text>
                                            </View>
                                        ))}
                                    </View>
                                </ScrollView>
                            </View>
                            <View style={{marginTop:responsiveHeight(2),backgroundColor:"#fff",padding:responsiveWidth(5),paddingVertical:responsiveWidth(10),justifyContent:"space-between"}}>
                                <Text style={{fontSize:responsiveFontSize(3),fontWeight:"bold"}}>
                                    Description
                                </Text> 
                                {(this.state.HotelDetails[0].description!=0) &&
                                    <Text style={{fontSize:responsiveFontSize(2.3),marginTop:responsiveHeight(2)}}>
                                        {this.state.HotelDetails[0].description}
                                    </Text>                                
                                }
                                 {(this.state.HotelDetails[0].description==0) &&
                                    <Text style={{fontSize:responsiveFontSize(2.3),marginTop:responsiveHeight(2)}}>
                                        No Description Found !
                                    </Text>                                
                                }
                            </View>
                            <View style={{marginTop:responsiveHeight(2),backgroundColor:"#fff",paddingHorizontal:responsiveWidth(5)}}>
                                <View style={{flexDirection:"row",alignItems:"center",justifyContent:"space-between",borderBottomWidth:1,borderBottomColor:"#e6e6e6",paddingVertical:responsiveWidth(5)}}>
                                    <TouchableOpacity onPress={()=>this.setState({bookLoader:true,activeTab:1})}>
                                        <View style={{alignItems:"center",justifyContent:"space-between",width:responsiveWidth(30)}}>
                                            
                                            {!this.state.select &&
                                                <>
                                                <Text style={{color:"red",fontSize:responsiveFontSize(2.3),textAlign:"center",fontWeight:"bold"}}>
                                                    Today
                                                </Text>
                                                <Text style={{fontSize:responsiveFontSize(2),textAlign:"center",opacity:0.7,fontWeight:"bold",marginTop:responsiveHeight(2)}}>
                                                    {">>"} 12PM
                                                </Text>
                                                </>
                                            }
                                            {this.state.select && 
                                                <>
                                                <Text style={{color:"red",fontSize:responsiveFontSize(2.3),textAlign:"center",fontWeight:"bold"}}>
                                                    { this.state.selectRoomDetails.startDate}
                                                </Text>
                                                {this.state.bookingType=="normal" &&
                                                    <Text style={{fontSize:responsiveFontSize(2),textAlign:"center",opacity:0.7,fontWeight:"bold",marginTop:responsiveHeight(2)}}>
                                                        {">>"} 12PM
                                                    </Text>
                                                }
                                                {this.state.bookingType=="section" &&
                                                    <Text style={{fontSize:responsiveFontSize(2),textAlign:"center",opacity:0.7,fontWeight:"bold",marginTop:responsiveHeight(2)}}>
                                                        {">>"} {this.state.selectRoomDetails.StartTime} {this.state.selectRoomDetails.StartTime<"12:00"?"AM":"PM"}
                                                    </Text>
                                                }
                                                </>
                                            }
                                        </View>
                                    </TouchableOpacity>
                                    <View style={{alignItems:"center",justifyContent:"space-between",width:responsiveWidth(30)}}>
                                        <Icon name="ios-calendar" style={{color:"#000",fontSize:responsiveFontSize(4)}}/>
                                        {!this.state.select &&
                                            <Text style={{fontSize:responsiveFontSize(2),textAlign:"center",opacity:0.7,fontWeight:"bold",marginTop:responsiveHeight(2)}}>
                                                {this.state.nights} Night
                                            </Text>
                                        }
                                        {this.state.select && this.state.bookingType=="normal" &&
                                            <Text style={{fontSize:responsiveFontSize(2),textAlign:"center",opacity:0.7,fontWeight:"bold",marginTop:responsiveHeight(2)}}>
                                                {this.state.selectRoomDetails.totalNights} Night
                                            </Text>
                                        }
                                        {this.state.select && this.state.bookingType=="section" &&
                                            <Text style={{fontSize:responsiveFontSize(2),textAlign:"center",opacity:0.7,fontWeight:"bold",marginTop:responsiveHeight(2)}}>
                                                {this.state.selectRoomDetails.totalHours} Hours
                                            </Text>
                                        }
                                    </View>
                                    <TouchableOpacity onPress={()=>this.setState({bookLoader:true,activeTab:1})}>
                                        <View style={{alignItems:"center",justifyContent:"space-between",width:responsiveWidth(30)}}>
                                            {!this.state.select &&
                                                <>
                                                <Text style={{color:"red",fontSize:responsiveFontSize(2.3),textAlign:"center",fontWeight:"bold"}}>
                                                    Tomorrow
                                                </Text>
                                                <Text style={{fontSize:responsiveFontSize(2),textAlign:"center",opacity:0.7,fontWeight:"bold",marginTop:responsiveHeight(2)}}>
                                                    {">>"} 11AM
                                                </Text>
                                                </>
                                            }
                                            {this.state.select && this.state.bookingType=="normal" &&
                                                <>
                                                <Text style={{color:"red",fontSize:responsiveFontSize(2.3),textAlign:"center",fontWeight:"bold"}}>
                                                    { this.state.selectRoomDetails.endDate}
                                                </Text>
                                                <Text style={{fontSize:responsiveFontSize(2),textAlign:"center",opacity:0.7,fontWeight:"bold",marginTop:responsiveHeight(2)}}>
                                                    {">>"} 11AM
                                                </Text>
                                                </>
                                            }
                                            {this.state.select && this.state.bookingType=="section" &&
                                                    <>
                                                    <Text style={{color:"red",fontSize:responsiveFontSize(2.3),textAlign:"center",fontWeight:"bold"}}>
                                                        { this.state.selectRoomDetails.endDate}
                                                    </Text>
                                                    <Text style={{fontSize:responsiveFontSize(2),textAlign:"center",opacity:0.7,fontWeight:"bold",marginTop:responsiveHeight(2)}}>
                                                        {">>"} {this.state.selectRoomDetails.endTime} {this.state.selectRoomDetails.endTime<"12:00"?"AM":"PM"}
                                                    </Text>
                                                    </>
                                            }
                                        </View>
                                    </TouchableOpacity>
                                </View>
                                <View style={{flexDirection:"row",alignItems:"center",justifyContent:"center",paddingVertical:responsiveWidth(5)}}>
                                    { !this.state.select &&   
                                        <TouchableOpacity onPress={()=>this.setState({bookLoader:true,activeTab:1})}>
                                            <Text style={{color:"red",fontSize:responsiveFontSize(2.5),textAlign:"center"}}>
                                                {this.state.rooms} Room, {this.state.adults} Adult, {this.state.childs} Childs {">"}
                                            </Text>
                                        </TouchableOpacity>
                                    }
                                    { this.state.select &&   
                                        <TouchableOpacity onPress={()=>this.setState({bookLoader:true,activeTab:1})}>
                                            <Text style={{color:"red",fontSize:responsiveFontSize(2.5),textAlign:"center"}}>
                                                {this.state.selectRoomDetails.totalRoomsLeng} {this.state.selectRoomDetails.roomType}, {this.state.selectRoomDetails.totalAdults} Adult, {this.state.selectRoomDetails.totalChilds} Childs {">"}
                                            </Text>
                                        </TouchableOpacity>
                                    }
                                </View>
                            </View>
                            <View style={{marginTop:responsiveHeight(2),backgroundColor:"#fff",paddingHorizontal:responsiveWidth(5)}}>
                                <View style={{flexDirection:"row",alignItems:"center",justifyContent:"space-between",paddingVertical:responsiveWidth(5)}}>
                                    <Text style={{color:"#000",fontWeight:"bold",fontSize:responsiveFontSize(2.3),textAlign:"center"}}>
                                        Amount Payable
                                    </Text>
                                    {! this.state.select &&
                                        <View style={{flexDirection:"row",alignItems:"center",justifyContent:"center"}}>
                                            <Text style={{fontSize:responsiveFontSize(2),textAlign:"center",opacity:0.7,fontWeight:"bold",textDecorationLine:"line-through"}}>
                                                ₹ {this.state.HotelDetails[0].price}
                                            </Text>
                                            <Text style={{fontSize:responsiveFontSize(2.5),textAlign:"center",fontWeight:"bold",marginLeft:responsiveWidth(3)}}>
                                                ₹ {this.state.HotelDetails[0].price-((this.state.HotelDetails[0].price*this.state.HotelDetails[0].discount)/100)}
                                            </Text>
                                        </View>
                                    }
                                    {this.state.select && !this.state.selectprice &&
                                        <View style={{flexDirection:"row",alignItems:"center",justifyContent:"center"}}>
                                            <Text style={{fontSize:responsiveFontSize(2),textAlign:"center",opacity:0.7,fontWeight:"bold",textDecorationLine:"line-through"}}>
                                                ₹ ---
                                            </Text>
                                            <Text style={{fontSize:responsiveFontSize(2.5),textAlign:"center",fontWeight:"bold",marginLeft:responsiveWidth(3)}}>
                                                ₹ ---
                                            </Text>
                                        </View>
                                    }
                                    {this.state.select && this.state.selectprice &&
                                        <View style={{flexDirection:"row",alignItems:"center",justifyContent:"center"}}>
                                            <Text style={{fontSize:responsiveFontSize(2),textAlign:"center",opacity:0.7,fontWeight:"bold",textDecorationLine:"line-through"}}>
                                                ₹ {parseInt(this.state.priceDetails.maxpayable)}
                                            </Text>
                                            <Text style={{fontSize:responsiveFontSize(2.5),textAlign:"center",fontWeight:"bold",marginLeft:responsiveWidth(3)}}>
                                                ₹ {parseInt(this.state.priceDetails.payable)}
                                            </Text>
                                        </View>
                                    }
                                </View>
                                {! this.state.select &&
                                    <View style={{flexDirection:"row",alignItems:"center",justifyContent:"space-between",paddingVertical:responsiveWidth(5),
                                                borderTopWidth:1,borderTopColor:"#e6e6e6"}}>
                                        <Text style={{color:"#000",opacity:0.7,fontSize:responsiveFontSize(2.3),textAlign:"center"}}>
                                            Price Dropped ({this.state.HotelDetails[0].discount} % OFF)
                                        </Text>
                                        <Text style={{fontSize:responsiveFontSize(2.5),textAlign:"center",fontWeight:"bold",opacity:0.7}}>
                                            - ₹ {(this.state.HotelDetails[0].price*this.state.HotelDetails[0].discount)/100}
                                        </Text>
                                    </View>
                                }
                                { this.state.select && !this.state.selectprice &&
                                    <View style={{flexDirection:"row",alignItems:"center",justifyContent:"space-between",paddingVertical:responsiveWidth(5),
                                                borderTopWidth:1,borderTopColor:"#e6e6e6"}}>
                                        <Text style={{color:"#000",opacity:0.7,fontSize:responsiveFontSize(2.3),textAlign:"center"}}>
                                            Price Dropped (-- % OFF)
                                        </Text>
                                        <Text style={{fontSize:responsiveFontSize(2.5),textAlign:"center",fontWeight:"bold",opacity:0.7}}>
                                            - ₹ ---
                                        </Text>
                                    </View>
                                }
                                { this.state.select && this.state.selectprice &&
                                    <View style={{flexDirection:"row",alignItems:"center",justifyContent:"space-between",paddingVertical:responsiveWidth(5),
                                                borderTopWidth:1,borderTopColor:"#e6e6e6"}}>
                                        <Text style={{color:"#000",opacity:0.7,fontSize:responsiveFontSize(2.3),textAlign:"center"}}>
                                            Price Dropped ({parseInt(this.state.priceDetails.discount)} % OFF)
                                        </Text>
                                        <Text style={{fontSize:responsiveFontSize(2.5),textAlign:"center",fontWeight:"bold",opacity:0.7}}>
                                            - ₹ {parseInt(this.state.priceDetails.discounted)}
                                        </Text>
                                    </View>
                                }
                                <View style={{paddingBottom:responsiveWidth(5),paddingTop:0}}>
                                    <View style={{alignItems:"flex-start",justifyContent:"space-between"}}>
                                        <Text style={{color:"#000",opacity:0.7,fontSize:responsiveFontSize(2.3),textAlign:"center"}}>
                                            Room Tariff
                                        </Text>
                                    </View>
                                    {!this.state.select &&
                                        <View style={{flexDirection:"row",alignItems:"center",justifyContent:"space-between"}}>
                                            <Text style={{fontSize:responsiveFontSize(2),textAlign:"center",opacity:0.7,}}>
                                                ₹ {this.state.HotelDetails[0].price-((this.state.HotelDetails[0].price*this.state.HotelDetails[0].discount)/100)} x {this.state.rooms} Room X {this.state.nights} Night
                                            </Text>
                                            <Text style={{fontSize:responsiveFontSize(2.5),textAlign:"center",fontWeight:"bold",opacity:0.7}}>
                                                ₹ {this.state.HotelDetails[0].price-((this.state.HotelDetails[0].price*this.state.HotelDetails[0].discount)/100)}
                                            </Text>
                                        </View>
                                    }
                                    {this.state.select && !this.state.selectprice &&
                                        <View style={{flexDirection:"row",alignItems:"center",justifyContent:"space-between"}}>
                                            {this.state.bookingType=="normal" &&
                                                <>
                                                <Text style={{fontSize:responsiveFontSize(2),textAlign:"center",opacity:0.7,}}>
                                                ₹ --- x {this.state.selectRoomDetails.totalRoomsLeng} Room X {this.state.selectRoomDetails.totalNights} Night
                                                </Text>
                                                <Text style={{fontSize:responsiveFontSize(2.5),textAlign:"center",fontWeight:"bold",opacity:0.7}}>
                                                    ₹ {this.state.HotelDetails[0].price-((this.state.HotelDetails[0].price*this.state.HotelDetails[0].discount)/100)}
                                                </Text>
                                                </>
                                            }
                                            {this.state.bookingType=="section" &&
                                                <>
                                                <Text style={{fontSize:responsiveFontSize(2),textAlign:"center",opacity:0.7,}}>
                                                ₹ --- x {this.state.selectRoomDetails.totalRoomsLeng} Room X {this.state.selectRoomDetails.totalHours} hours
                                                </Text>
                                                <Text style={{fontSize:responsiveFontSize(2.5),textAlign:"center",fontWeight:"bold",opacity:0.7}}>
                                                    ₹ ---
                                                </Text>
                                                </>
                                            }
                                        </View>
                                    }
                                    {this.state.select && this.state.selectprice &&
                                        <View style={{flexDirection:"row",alignItems:"center",justifyContent:"space-between"}}>
                                            {this.state.bookingType=="normal" &&
                                                <>
                                                <Text style={{fontSize:responsiveFontSize(2),textAlign:"center",opacity:0.7,}}>
                                                ₹ {parseInt(this.state.priceDetails.roomPrice)} x {this.state.selectRoomDetails.totalRoomsLeng} Room X {this.state.selectRoomDetails.totalNights} Night
                                                </Text>
                                                <Text style={{fontSize:responsiveFontSize(2.5),textAlign:"center",fontWeight:"bold",opacity:0.7}}>
                                                    ₹ {parseInt(this.state.priceDetails.maxpayable)}
                                                </Text>
                                                </>
                                            }
                                            {this.state.bookingType=="section" &&
                                                <>
                                                <Text style={{fontSize:responsiveFontSize(2),textAlign:"center",opacity:0.7,}}>
                                                ₹ {parseInt(this.state.priceDetails.roomPrice)} x {this.state.selectRoomDetails.totalRoomsLeng} Room X {this.state.selectRoomDetails.totalHours} Hours
                                                </Text>
                                                <Text style={{fontSize:responsiveFontSize(2.5),textAlign:"center",fontWeight:"bold",opacity:0.7}}>
                                                    ₹ {parseInt(this.state.priceDetails.maxpayable)}
                                                </Text>
                                                </>
                                            }
                                        </View>
                                    }
                                </View>
                            </View>
                            <View style={{marginTop:responsiveHeight(2),backgroundColor:"#fff",paddingHorizontal:responsiveWidth(5)}}>
                                <View style={{flexDirection:"row",alignItems:"center",justifyContent:"space-between",paddingVertical:responsiveWidth(5)}}>
                                    <Text style={{color:"#000",opacity:0.7,fontWeight:"bold",fontSize:responsiveFontSize(2.3),textAlign:"center"}}>
                                        Booking for
                                    </Text>
                                    <TouchableOpacity onPress={()=>this.setState({showGuestModal:true})}>
                                        <Icon name="ios-create" style={{color:"red",fontSize:responsiveFontSize(4)}}/>
                                    </TouchableOpacity>
                                </View>
                                <View style={{alignItems:"center",justifyContent:"center",marginBottom:responsiveHeight(2)}}>
                                    <Text style={{color:"#000",fontSize:responsiveFontSize(2.3),textAlign:"center"}}>
                                        {this.state.userName}
                                    </Text>
                                    <Text style={{color:"#000",fontSize:responsiveFontSize(2.3),textAlign:"center",marginTop:responsiveHeight(0.5)}}>
                                        {this.state.mobile}
                                    </Text>
                                </View>
                                <View style={{flexDirection:"row",alignItems:"center",justifyContent:"space-between",paddingVertical:responsiveWidth(5),
                                            borderTopWidth:1,borderTopColor:"#e6e6e6"}}>
                                    <Thumbnail square source={{uri:"https://app.yoyorooms.net/assets/icon/gst.png"}} resizeMode="stretch"
                                                style={{height:responsiveHeight(7),width:responsiveWidth(11)}}/>
                                    <Text style={{fontSize:responsiveFontSize(2),opacity:0.7,width:responsiveWidth(70)}}>
                                        You can add oe edit your GSTIN after your booking confirmation
                                    </Text>
                                </View>
                            </View>
                            <View style={{marginTop:responsiveHeight(2),backgroundColor:"#fff",paddingHorizontal:responsiveWidth(5)}}>
                                <View style={{alignItems:"flex-start",justifyContent:"space-between",paddingVertical:responsiveWidth(5)}}>
                                    <Text style={{color:"#000",fontWeight:"bold",fontSize:responsiveFontSize(2.3),textAlign:"center"}}>
                                        Important
                                    </Text>
                                </View>
                                <View style={{alignItems:"flex-start",justifyContent:"center",paddingVertical:responsiveWidth(5),
                                            borderTopWidth:1,borderTopColor:"#e6e6e6"}}>
                                    {(this.state.HotelDetails[0].important_text != 0) &&
                                        <Text style={{fontSize:responsiveFontSize(2),opacity:0.7,}}>
                                            {this.state.HotelDetails[0].important_text}
                                        </Text>
                                    }
                                    {(this.state.HotelDetails[0].important_text == 0) &&
                                        <Text style={{fontSize:responsiveFontSize(2),opacity:0.7,}}>
                                            Nothing Much Required !
                                        </Text>
                                    }
                                    
                                </View>
                            </View>
                            <View style={{marginTop:responsiveHeight(2),backgroundColor:"#fff",paddingHorizontal:responsiveWidth(5)}}>
                                <View style={{alignItems:"flex-start",justifyContent:"space-between",paddingVertical:responsiveWidth(5)}}>
                                    <Text style={{color:"#000",fontWeight:"bold",fontSize:responsiveFontSize(2.3),textAlign:"center"}}>
                                        Cancellation Policy
                                    </Text>
                                </View>
                                <View style={{alignItems:"flex-start",justifyContent:"center",paddingVertical:responsiveWidth(5),
                                            borderTopWidth:1,borderTopColor:"#e6e6e6"}}>
                                    {(this.state.HotelDetails[0].cancellation_policies != 0) &&
                                        <Text style={{fontSize:responsiveFontSize(2),opacity:0.7,}}>
                                            {this.state.HotelDetails[0].cancellation_policies}
                                        </Text>
                                    }
                                    {(this.state.HotelDetails[0].cancellation_policies == 0) &&
                                        <Text style={{fontSize:responsiveFontSize(2),opacity:0.7,}}>
                                            No Cancellation Policy !
                                        </Text>
                                    }
                                </View>
                            </View>
                            <View style={{marginTop:responsiveHeight(2),backgroundColor:"#fff",padding:responsiveWidth(5),paddingHorizontal:responsiveWidth(10)}}>
                                <TouchableOpacity onPress={()=>alert("open guest policy")}>
                                    <View style={{width:responsiveWidth(80),alignItems:"center",justifyContent:"center"}}>
                                        <Text style={{color:"#000",opacity:0.7,fontWeight:"bold",fontSize:responsiveFontSize(2.3),textAlign:"center"}}>
                                            By proceeding, you agree to our 
                                        </Text>
                                        <Text style={{color:"red",opacity:0.7,fontWeight:"bold",fontSize:responsiveFontSize(2.3),textAlign:"center"}}>
                                            Guest Policies
                                        </Text>
                                    </View>
                                </TouchableOpacity>
                            </View>
                            {this.state.select && !this.state.price &&
                                <View style={{alignItems:"center",justifyContent:"center",padding:responsiveWidth(3),height:"auto",backgroundColor: "#fff",
                                                borderTopWidth:1,borderColor:"#a6a6a6"}}>
                                     {!this.state.bookingLoader &&
                                        <TouchableOpacity onPress={() => this.booking()}
                                            style={{alignItems:"center",justifyContent:"center",height:responsiveHeight(11),width:responsiveWidth(80),
                                            backgroundColor:"#0246B1",borderRadius:responsiveWidth(5)}}>
                                            <Text style={{fontWeight:'bold',fontSize:responsiveFontSize(2.5),color:'#fff',}}>
                                                Submit Booking
                                            </Text>
                                        </TouchableOpacity>
                                    }
                                    {this.state.bookingLoader &&
                                        <TouchableOpacity 
                                            style={{alignItems:"center",justifyContent:"center",width:responsiveWidth(80),height:responsiveHeight(11),
                                            backgroundColor:"#0246B1",borderRadius:responsiveWidth(5)}}>
                                            <Spinner color="#fff" size="small"/>
                                        </TouchableOpacity>
                                    }
                                </View>
                            }
                        </ScrollView>
                        } 
                        {(this.state.HotelDetails==0) &&
                            <View  style={{alignItems:"center",justifyContent:"center",height:responsiveHeight(100)}}>
                                <Image source={require("../../../../../../assets/splash.png")} style={{height:responsiveHeight(20),width:responsiveWidth(70),}} />
                                <Text style={{color:"#0a1d3b",fontSize:responsiveFontSize(2.5),fontWeight:"bold"}}>
                                    Sorry! No Details found for this hotel</Text>
                            </View>
                        } 
                    </View>
                }
                {(this.state.loading) &&
                    <View  style={{alignItems:"center",justifyContent:"center",height:responsiveHeight(100)}}>
                        {/* <Image source={require("../../../../../assets/splash.png")} style={{height:responsiveHeight(40),width:responsiveWidth(60),}} /> */}
                        <Spinner color="#0246b1" size="large"/>
                    </View>
                }
                {!this.state.select && this.state.HotelSections == 0 && !this.state.loading && this.state.HotelDetails.length>0 && !this.state.price &&
                    <View style={{alignItems:"center",justifyContent:"center",borderTopWidth:1,
                            borderColor:"#a6a6a6",padding:responsiveWidth(3),height:"auto",backgroundColor: "#fff"}}>
                        <TouchableOpacity onPress={() => this.setState({bookLoader:true,activeTab:1,bookingType:"normal"})}
                            style={{alignItems:"center",justifyContent:"center",paddingVertical:responsiveHeight(1),width:responsiveWidth(80),
                            backgroundColor:this.state.loader?"grey":"#0246B1",borderRadius:responsiveWidth(4)}}>
                            <Text style={{fontSize:responsiveFontSize(2),color:'#fff',}}>
                                Book Now
                            </Text>
                            <Text style={{fontWeight:'bold',fontSize:responsiveFontSize(2.5),color:'#fff',}}>
                                Normal Booking
                            </Text>
                        </TouchableOpacity>
                    </View>
                }
                {!this.state.select && this.state.HotelSections != 0 && !this.state.loading && this.state.HotelDetails.length>0 && !this.state.price &&
                    <View style={{alignItems:"center",justifyContent:"center",borderTopWidth:1,flexDirection:"row",
                            borderColor:"#a6a6a6",padding:responsiveWidth(3),height:"auto",backgroundColor: "#fff"}}>
                        <TouchableOpacity onPress={() => this.setState({bookLoader:true,activeTab:1,bookingType:"normal"})}
                            style={{alignItems:"center",justifyContent:"center",paddingVertical:responsiveHeight(1),width:responsiveWidth(47),
                            backgroundColor:"#0246B1",borderTopLeftRadius:responsiveWidth(5),borderBottomLeftRadius:responsiveWidth(5)}}>
                            <Text style={{fontSize:responsiveFontSize(2),color:'#fff',}}>
                                Book Now
                            </Text>
                            <Text style={{fontWeight:'bold',fontSize:responsiveFontSize(2.5),color:'#fff',textAlign:"center"}}>
                                Normal Booking
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.setState({bookLoader:true,activeTab:1,bookingType:"section"})}
                            style={{alignItems:"center",justifyContent:"center",paddingVertical:responsiveHeight(1),width:responsiveWidth(47),
                            backgroundColor:"#e6e6e6",borderTopRightRadius:responsiveWidth(5),borderBottomRightRadius:responsiveWidth(5)}}>
                            <Text style={{fontSize:responsiveFontSize(2),color:'#0246b1',}}>
                                Book Now
                            </Text>
                            <Text style={{fontWeight:'bold',fontSize:responsiveFontSize(2.5),color:'#0246b1',textAlign:"center"}}>
                                {this.state.HotelSections[0].title}
                            </Text>
                        </TouchableOpacity>
                    </View>
                }
                {this.state.select && this.state.HotelSections != 0 && !this.state.loading && this.state.HotelDetails.length>0 && this.state.price &&
                    <View style={{alignItems:"center",justifyContent:"center",borderTopWidth:1,flexDirection:"row",
                            borderColor:"#a6a6a6",padding:responsiveWidth(3),height:"auto",backgroundColor: "#fff"}}>
                        <TouchableOpacity onPress={() => this.setState({bookLoader:true,activeTab:1,bookingType:"normal"})}
                            style={{alignItems:"center",justifyContent:"center",paddingVertical:responsiveHeight(1),width:responsiveWidth(47),
                            backgroundColor:"#0246B1",borderTopLeftRadius:responsiveWidth(5),borderBottomLeftRadius:responsiveWidth(5)}}>
                            <Text style={{fontSize:responsiveFontSize(2),color:'#fff',}}>
                                Book Now
                            </Text>
                            <Text style={{fontWeight:'bold',fontSize:responsiveFontSize(2.5),color:'#fff',textAlign:"center"}}>
                                Normal Booking
                            </Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={() => this.setState({bookLoader:true,activeTab:1,bookingType:"section"})}
                            style={{alignItems:"center",justifyContent:"center",paddingVertical:responsiveHeight(1),width:responsiveWidth(47),
                            backgroundColor:"#e6e6e6",borderTopRightRadius:responsiveWidth(5),borderBottomRightRadius:responsiveWidth(5)}}>
                            <Text style={{fontSize:responsiveFontSize(2),color:'#0246b1',}}>
                                Book Now
                            </Text>
                            <Text style={{fontWeight:'bold',fontSize:responsiveFontSize(2.5),color:'#0246b1',textAlign:"center"}}>
                                {this.state.HotelSections[0].title}
                            </Text>
                        </TouchableOpacity>
                    </View>
                }
                { this.state.bookLoader &&
                    <BookingModal roomDetails={this.state.HotelRooms} activeTab={this.state.activeTab} bookingType={this.state.bookingType}
                    onClose={(mVal)=>this.setModalVal(mVal)}
                    onAction={(mVal,selectRoomDetails)=>this.setModalValue(mVal,selectRoomDetails)}/> 
                } 
                <Modal
                    animationType="slide"
                    transparent={true}
                    visible={this.state.showGuestModal}
                    onRequestClose={() => this.setState({showGuestModal:false})}
                >
                    <View style={{alignItems:"center",justifyContent:"center",backgroundColor:"rgba(0,0,0,0.7)",height:"auto",flex:1,}} >
                        <View style={{justifyContent:"space-between",backgroundColor:"#fff",marginHorizontal:responsiveWidth(10),height:"auto",paddingHorizontal:responsiveWidth(5),
                                    paddingVertical:responsiveHeight(3),borderRadius:responsiveWidth(3)}} >
                            <View style={{alignContent:"center",justifyContent:"space-between",flexDirection:"row"}}> 
                                <Text style={{fontSize:responsiveFontSize(3),fontWeight:"bold"}}>
                                    Add
                                </Text>
                                <TouchableOpacity onPress={() => this.setState({showGuestModal:false,})}
                                    style={{alignItems:"center",justifyContent:"center",}}>
                                    <Icon name='ios-close-circle'   style={{color:"red",fontSize:responsiveFontSize(3)}}/>
                                </TouchableOpacity>
                            </View>
                            <View style={{flexDirection:'row',marginTop:responsiveHeight(5),borderWidth:1,borderColor:'#0246B1',
                                    paddingHorizontal:responsiveWidth(3),paddingVertical:responsiveHeight(1),borderRadius:responsiveWidth(10)}}>
                                <Icon name='ios-person'   style={{color:"#000",fontSize:responsiveFontSize(3)}}/>  
                                <TextInput 
                                    style={{marginLeft:responsiveWidth(3),fontSize:responsiveFontSize(2.2),width:responsiveWidth(50)}}
                                    placeholder = 'Guest Name'
                                    value={this.state.name}
                                    placeholderTextColor = 'rgba(0,0,0,0.9)'
                                    autoCorrect ={false}
                                    onChangeText={(text) => this.setState({name:text})}
                                    />    
                            </View>
                            <View style={{flexDirection:'row',marginTop:responsiveHeight(3),borderWidth:1,borderColor:'#0246B1',
                                    paddingHorizontal:responsiveWidth(3),paddingVertical:responsiveHeight(1),borderRadius:responsiveWidth(10)}}>
                                <Icon name='ios-call'   style={{color:"#000",fontSize:responsiveFontSize(3)}}/>  
                                <TextInput 
                                    style={{marginLeft:responsiveWidth(3),fontSize:responsiveFontSize(2.2),width:responsiveWidth(50)}}
                                    placeholder = 'Phone'
                                    value={this.state.phone}
                                    placeholderTextColor = 'rgba(0,0,0,0.9)'
                                    autoCorrect ={false}
                                    onChangeText={(text) => this.setState({phone:text})}
                                    />    
                            </View>
                            <View style={{justifyContent:"center",alignItems:"center",marginTop:responsiveHeight(3)}}>
                                    <Text style={{fontSize:responsiveFontSize(2),color: '#000',fontWeight:"600"}}>
                                        All communications regarding this booking will be sent to this number.
                                    </Text>
                            </View>
                            <View style={{justifyContent:"center",alignItems:"center",marginTop:responsiveHeight(5)}}>
                                <TouchableOpacity onPress={()=> this.UpdateBookingUser()}
                                    style={{alignItems:"center",justifyContent:"center",width:responsiveWidth(70),height:responsiveHeight(7),
                                    backgroundColor:"#0652dd",borderRadius:responsiveWidth(3)}}>
                                    <Text style={{fontSize:responsiveFontSize(2.5),color: '#fff',fontWeight:"bold"}}>
                                        Update Details
                                    </Text>
                                </TouchableOpacity>
                            </View>
                        </View>
                    </View>
                </Modal>
            </View>   
        </Container>
        <Loader animation="fade"
                visible={this.state.loader}
                textContent={'Calculate Price...'}
                textStyle={{color:"#fff"}}
        />
    </SafeAreaView>
);
}
}
