import {createStackNavigator} from "react-navigation-stack";
import {createAppContainer,createSwitchNavigator} from "react-navigation";
import HotelList from "./hotelList";
import HotelDetails from './hotelDetails';
import ImageViewer from './ImageViewer';

const searchHotelRoutes = createStackNavigator({
    hotelList:HotelList,
    hotelDeatils:HotelDetails,
    imageViewer: ImageViewer
    
   
},{
    initialRouteName:'hotelList',
    headerMode:null
});

export default createAppContainer(searchHotelRoutes );