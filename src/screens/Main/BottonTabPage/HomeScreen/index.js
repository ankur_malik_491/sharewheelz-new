
import React from 'react';
import {Text, View,TouchableOpacity,Image, ImageBackground,AsyncStorage,StatusBar, Platform, ViewBase,} from 'react-native';
import {Toast,Icon,Input, Content,Spinner} from "native-base";
import {responsiveWidth,responsiveHeight,responsiveFontSize} from "react-native-responsive-dimensions";
import ReactNativeParallaxHeader from 'react-native-parallax-header';
import MapModal from "../../../../components/MapModal";
import { SafeAreaView } from 'react-native-safe-area-context';
import Shimmer from "../../../../components/shimmer"

export default class HomeScreen extends React.Component { 
    constructor() {
    super();
    this.state = {
      locationView:[],
      defaultCities:[],
      sectionsData:[],
      refer:[],
      limitedOffer:[],
      myForever:[],
      existingOffer:[],
      weekend:[],
      latest:[],
      yoyoSpecial:[],
      wallets:[],
      walletsBalance:[],
      loader:true,
      showModal:false,
      shimmerHolder:[1,2,3,4,5,6,7,8,9,10]

      
      }
      
     this._getsectionsbanners()
  }
  getWalletBal = async() =>{
    const uid = await AsyncStorage.getItem('uid');
    var userid = uid;
      try{
        fetch("https://app.yoyorooms.net/Appapi/getuserwalletbalance", 
            {
                method: 'POST',
                headers: {
                    Accept: 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                            uid  : userid 
                        }),     
            })
            .then(response => response.json())
            .then(response2 => {
              //console.log(response2)
                if(response2.status==1){
                  this.setState({walletsBalance:response2})
                  this.setState({loader:false})               
                }
                else{
                  Toast.show({
                    text:"Something went wrong! Please try again.",
                    textStyle:{textAlign:"center"},
                    duration:2000,
                    style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
                  });
                }
            }).catch((error) =>{
                Toast.show({
                  text:"Something went wrong! Please try again.",
                  textStyle:{textAlign:"center"},
                  duration:2000,
                  style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
                });
            });
        
    }
    catch(error){
      Toast.show({
        text:"Something went wrong! Please try again.",
        textStyle:{textAlign:"center"},
        duration:2000,
        style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
      });
    }
    

  }
  _getsectionsbanners = async() => {
    await fetch('https://yoyorooms.net/app/Appapi/default_cities')
    .then(response => response.json())
   .then(response2 => {
    //console.log(response2)
    if(response2!=0){
        this.setState({defaultCities: response2})
        
    }
    else {
      console.log("error in default city")
    }
   
   })
  await fetch('https://app.yoyorooms.net/Appapi/get_section_data')
  .then(response => response.json())
 .then(response2 => {
  //console.log(response2)
  if(response2!=0){
      this.setState({sectionsData: response2})
      
  }
  else {
    console.log("error in get uper sections")
  }
 
 })
 await fetch('https://app.yoyorooms.net/Appapi/get_refer_banner')
  .then(response => response.json())
 .then(response2 => {
  //console.log(response2)
  if(response2!=0){
      this.setState({refer: response2})
      
  }
  else {
    console.log("error in refer banner")
  }
 })
 await fetch('https://app.yoyorooms.net/Appapi/get_limited_offer_banner')
  .then(response => response.json())
 .then(response2 => {
  //console.log(response2)
  if(response2!=0){
      this.setState({limitedOffer: response2})
      
  }
  else {
    console.log("error in limited banner")
  }
 })
 await fetch('https://app.yoyorooms.net/Appapi/get_wizard_banner')
  .then(response => response.json())
 .then(response2 => {
  //console.log(response2)
  if(response2!=0){
      this.setState({myForever: response2})
      
  }
  else {
    console.log("error in wizard banner")
  }
 })
 await fetch('https://app.yoyorooms.net/Appapi/get_existing_offer_banner')
  .then(response => response.json())
 .then(response2 => {
  //console.log(response2)
  if(response2!=0){
      this.setState({existingOffer: response2})
      
  }
  else {
    console.log("error in existing offer")
  }
 })
 await fetch('https://app.yoyorooms.net/Appapi/get_weekend_banner')
  .then(response => response.json())
 .then(response2 => {
  //console.log(response2)
  if(response2!=0){
      this.setState({weekend: response2})
      
  }
  else {
    console.log("error in weekend banner")
  }
 })
 await fetch('https://app.yoyorooms.net/Appapi/get_yoyolatest_banner')
 .then(response => response.json())
.then(response2 => {
 //console.log(response2)
 if(response2!=0){
     this.setState({latest: response2})
     
 }
 else {
  console.log("error in yoyolatest banner")
 }
})
await fetch('https://app.yoyorooms.net/Appapi/get_yoyospecial_banner')
.then(response => response.json())
.then(response2 => {
//console.log(response2)
if(response2!=0){
    this.setState({yoyoSpecial: response2})
    
}
else {
  console.log("error in yoyospecial banner")
}
})
await fetch('https://app.yoyorooms.net/Appapi/get_wallet_banner')
.then(response => response.json())
.then(response2 => {
//console.log(response2)
if(response2!=0){
    this.setState({wallets: response2})
    
}
else {
  console.log("error in wallet banner")
}
})
this.getWalletBal()
}

setval =(val,name)=>{
    // console.log(val)
     //console.log(name)
    this.setState({showModal:!val});
    if(name){
    this.props.navigation.navigate('searchHotel',{city_name:name})}
}
renderNavBar = () => (
    <View style={{height: responsiveHeight(12),alignItems: 'center', flexDirection: 'row',justifyContent:"space-between",paddingVertical:responsiveHeight(1.5)}}>
      <View>
        <TouchableOpacity onPress={() => alert("open drawer ")}>
          <Icon name="menu" style={{color:"#fff",fontSize:responsiveFontSize(4),marginLeft:responsiveWidth(3)}}/>
        </TouchableOpacity>
      </View>
      <TouchableOpacity onPress={()=>this.setState({showModal:true})}>
        <View style={{flexDirection:"row",alignItems:"center",borderWidth:1,borderRadius:responsiveWidth(3),
                        paddingHorizontal:responsiveWidth(3),backgroundColor:"#fff",width:responsiveWidth(75)}}>
            <Icon active name='search'  type="FontAwesome5" style={{color:"grey",fontSize:responsiveFontSize(3)}}/>
            <Input placeholder="Search For Hotels, Location"
                    disabled
                    style={{width:responsiveWidth(30),marginLeft:responsiveWidth(1)}}
            />
        </View>
      </TouchableOpacity> 
      <View>
        <TouchableOpacity onPress={() => alert("open notigfication page")}>
          <Icon name="bell" type="MaterialCommunityIcons" style={{color:"#fff",fontSize:responsiveFontSize(4),marginRight:responsiveWidth(3)}}/>
        </TouchableOpacity>
      </View>
    </View>
        
)
renderContent = () => (
      
  <View style={{backgroundColor:"#e6e6e6"}}>
    {
      this.state.loader &&
      <View style={{height:responsiveHeight(100)}}>
        <Spinner color="#0246b1" size="small"/>

      </View>
    }
       
        {!(this.state.loader) &&
        <View>
        {(this.state.defaultCities.length>0) &&
        <Content horizontal>
        <View style={{flexDirection:"row",height:"auto",alignItems:"center",backgroundColor:"#fff",paddingVertical:responsiveHeight(2)}}>
          
          <View style={{alignItems:"center"}}>
            <TouchableOpacity onPress={()=>{alert("open location")}} style={{alignItems:"center",justifyContent:"center"}}>
                <View style={{backgroundColor:'#0246B1',width:responsiveWidth(17),height:responsiveWidth(17),borderRadius:responsiveWidth(3),
                            justifyContent:'center',alignItems:'center',marginLeft:responsiveWidth(3)}}>
                  <Icon name="md-locate" style={{color:"#fff",fontSize:responsiveFontSize(4)}}/>
                  
                </View>
            </TouchableOpacity>
            <Text style={{fontSize:responsiveFontSize(1.5),marginLeft:responsiveWidth(3)}}>NearBy</Text>
          </View>
          {this.state.defaultCities.map((item,i) => (
            <View key={i} style={{alignItems:"center"}}>
              <TouchableOpacity onPress={()=>this.props.navigation.navigate('searchHotel',{city_name:item.city_name})} 
                    style={{alignItems:"center",justifyContent:"center"}}>
                  <View style={{backgroundColor:'#0246B1',width:responsiveWidth(17),height:responsiveWidth(17),borderRadius:responsiveWidth(3),
                              justifyContent:'center',alignItems:'center',marginLeft:responsiveWidth(3)}}>
                    <Image source={{uri:item.img_path}} 
                        style={{width:responsiveWidth(17),height:responsiveWidth(17),borderRadius:responsiveWidth(3),}}/>
                    
                  </View>
              </TouchableOpacity>
              <Text style={{fontSize:responsiveFontSize(1.5),marginLeft:responsiveWidth(3)}}>
                {item.city_name}
              </Text>
            </View>
          ))}
          <View style={{alignItems:"center"}}>
            <TouchableOpacity onPress={()=>{alert("open location")}} style={{alignItems:"center",justifyContent:"center"}}>
                <View style={{backgroundColor:'#0246B1',width:responsiveWidth(17),height:responsiveWidth(17),borderRadius:responsiveWidth(3),
                            justifyContent:'center',alignItems:'center',marginLeft:responsiveWidth(3),marginRight:responsiveWidth(3)}}>
                  <Icon name="ios-arrow-forward" style={{color:"#fff",fontSize:responsiveFontSize(4)}}/>
                  
                </View>
            </TouchableOpacity>
            <Text style={{fontSize:responsiveFontSize(1.5),marginLeft:responsiveWidth(3)}}>More</Text>
          </View>
          
        </View>
        </Content>
          }
        { (this.state.sectionsData.length>0) &&
          this.state.sectionsData.map((item,i)=>(
         
        <View key = {i}>
          <View style={{flexDirection:"row"}}>
            <View style={{width:responsiveWidth(2),backgroundColor:"#0246B1",margin:responsiveWidth(3)}}>

            </View>
            <Text style={{margin:responsiveWidth(3),marginLeft:0,fontSize:responsiveFontSize(3),fontWeight:"bold"}}>
              {item.section_name}
            </Text>
          </View>
        <Content horizontal>
          <View style={{alignItems:"center",justifyContent:"center",flexDirection:"row",margin:responsiveWidth(5),marginTop:responsiveHeight(1)}}>
            {(item.images.length>0) &&
             item.images.map((itm,i) => (
                <TouchableOpacity key={i} onPress={()=>{alert("open location")}} style={{alignItems:"center",justifyContent:"center",marginRight:responsiveWidth(5),marginLeft:responsiveWidth(5)}}>
                    <View >
                      <Image source={{uri:itm.image_path}} resizeMode="stretch"
                        style={{width:responsiveWidth(70),height:responsiveHeight(20),borderRadius:responsiveWidth(3),}}/>
                      
                    </View>
                </TouchableOpacity>
            ))}
            {(item.images.length == 0) &&
                <TouchableOpacity key={i} onPress={()=>{alert("open location")}} 
                    style={{alignItems:"center",justifyContent:"center",marginRight:responsiveWidth(5),marginLeft:responsiveWidth(5)}}>
                    <View 
                        style={{backgroundColor:"#ccc",alignItems:"center",justifyContent:"center",width:responsiveWidth(70),height:responsiveHeight(20),borderRadius:responsiveWidth(3),}}>
                      <Text style={{fontSize:responsiveFontSize(1.5),margin:responsiveWidth(10)}}>
                        Oops Something went wrong!
                        </Text>
                    </View>
                </TouchableOpacity>
            }
          </View>
        </Content>
        </View>
          ))}
        {(this.state.refer.length>0) &&
        <View>
          <View style={{flexDirection:"row"}}>
              <View style={{width:responsiveWidth(2),backgroundColor:"#0246B1",margin:responsiveWidth(3)}}>

              </View>
              <Text style={{margin:responsiveWidth(3),marginLeft:0,fontSize:responsiveFontSize(3),fontWeight:"bold"}}>
                Refer and Win
              </Text>
          </View>
          <Content horizontal>
            <View style={{alignItems:"center",justifyContent:"center",flexDirection:"row",margin:responsiveWidth(5),marginTop:responsiveHeight(1)}}>
              {this.state.refer.map((item,i) => (
                <View key={i} style={{alignItems:"center"}}>
                  <TouchableOpacity onPress={()=>{alert("open location")}} style={{alignItems:"center",justifyContent:"center",marginRight:responsiveWidth(5),marginLeft:responsiveWidth(5)}}>
                      <View >
                        <Image source={{uri:item.image_path}} resizeMode="stretch"
                          style={{width:responsiveWidth(80),height:responsiveHeight(25),borderRadius:responsiveWidth(3),}}/>
                      
                      </View>
                  </TouchableOpacity>
              </View>
              ))}
            </View>
          </Content> 
        </View>
        }
        {(this.state.limitedOffer.length>0) &&
        <View>
          <View style={{flexDirection:"row"}}>
              <View style={{width:responsiveWidth(2),backgroundColor:"#0246B1",margin:responsiveWidth(3)}}>

              </View>
              <Text style={{margin:responsiveWidth(3),marginLeft:0,fontSize:responsiveFontSize(3),fontWeight:"bold"}}>
                Limited period offers
              </Text>
          </View>
          <Content horizontal>
            <View style={{alignItems:"center",justifyContent:"center",flexDirection:"row",margin:responsiveWidth(5),marginTop:responsiveHeight(1)}}>
              {this.state.limitedOffer.map((item,i) => (
                <View key={i} style={{alignItems:"center"}}>
                  <TouchableOpacity onPress={()=>{alert("open location")}} style={{alignItems:"center",justifyContent:"center",marginRight:responsiveWidth(5),marginLeft:responsiveWidth(5)}}>
                      <View >
                        <Image source={{uri:item.image_path}} resizeMode="stretch"
                          style={{width:responsiveWidth(50),height:responsiveHeight(30),borderRadius:responsiveWidth(3),}}/>
                      
                      </View>
                  </TouchableOpacity>
              </View>
              ))}
            </View>
          </Content> 
        </View>
        }
        {(this.state.myForever.length>0) &&
        <View>
          <View style={{flexDirection:"row"}}>
              <View style={{width:responsiveWidth(2),backgroundColor:"#0246B1",margin:responsiveWidth(3)}}>

              </View>
              <Text style={{margin:responsiveWidth(3),marginLeft:0,fontSize:responsiveFontSize(3),fontWeight:"bold"}}>
                My Forever - Blue Phoenix
              </Text>
          </View>
          <Content horizontal>
            <View style={{alignItems:"center",justifyContent:"center",flexDirection:"row",margin:responsiveWidth(5),marginTop:responsiveHeight(1)}}>
              {this.state.myForever.map((item,i) => (
                <View key={i} style={{alignItems:"center"}}>
                  <TouchableOpacity onPress={()=>{alert("open location")}} style={{alignItems:"center",justifyContent:"center",marginRight:responsiveWidth(5)}}>
                      <View >
                        <Image source={{uri:item.image_path}} resizeMode="stretch"
                          style={{width:responsiveWidth(90),height:responsiveHeight(30),borderRadius:responsiveWidth(3),}}/>
                      
                      </View>
                  </TouchableOpacity>
              </View>
              ))}
            </View>
          </Content> 
        </View>
        }
        {(this.state.existingOffer.length>0) &&
        <View>
          <View style={{flexDirection:"row"}}>
              <View style={{width:responsiveWidth(2),backgroundColor:"#0246B1",margin:responsiveWidth(3)}}>

              </View>
              <Text style={{margin:responsiveWidth(3),marginLeft:0,fontSize:responsiveFontSize(3),fontWeight:"bold"}}>
                Existing offers
              </Text>
          </View>
          <Content horizontal>
            <View style={{alignItems:"center",justifyContent:"space-between",flexDirection:"row",margin:responsiveWidth(5),marginTop:responsiveHeight(1)}}>
              {this.state.existingOffer.map((item,i) => (
                <View key={i} style={{backgroundColor:"#fff",borderRadius:responsiveWidth(3),
                                      marginRight:responsiveWidth(5),marginLeft:responsiveWidth(5)}}>
                  <TouchableOpacity onPress={()=>{alert("open location")}} style={{alignItems:"center",justifyContent:"center",margin:responsiveWidth(3)}}>
                      <View >
                        <Image source={{uri:item.image_path}} resizeMode="stretch"
                          style={{width:responsiveWidth(50),height:responsiveHeight(30),borderRadius:responsiveWidth(3),}}/>
                      
                      </View>
                  </TouchableOpacity>
                  <View style={{alignItems:"center",justifyContent:"center",borderTopWidth:2,borderTopColor:"#ccc",borderStyle:"dashed"}}>
                  <TouchableOpacity onPress={()=>{alert("open location")}} 
                      style={{alignItems:"center",justifyContent:"center",margin:responsiveWidth(3)}}>
                      <View >
                        <Text style={{fontSize:responsiveFontSize(3),color:"#0246B1"}}>
                          {item.image_code}
                        </Text>
                      
                      </View>
                  </TouchableOpacity>
                  </View>
                </View>
              ))}
            </View>
          </Content> 
        </View>
        }
        {(this.state.weekend.length>0) &&
        <View>
          <View style={{flexDirection:"row"}}>
              <View style={{width:responsiveWidth(2),backgroundColor:"#0246B1",margin:responsiveWidth(3)}}>

              </View>
              <Text style={{margin:responsiveWidth(3),marginLeft:0,fontSize:responsiveFontSize(3),fontWeight:"bold"}}>
                Weekend Gateway
              </Text>
          </View>
          <Content horizontal>
            <View style={{alignItems:"center",justifyContent:"center",flexDirection:"row",margin:responsiveWidth(5),marginTop:responsiveHeight(1)}}>
              {this.state.weekend.map((item,i) => (
                <View key={i} style={{alignItems:"center"}}>
                  <TouchableOpacity onPress={()=>{alert("open location")}} style={{alignItems:"center",justifyContent:"center",marginRight:responsiveWidth(5),marginLeft:responsiveWidth(5)}}>
                      <View >
                        <ImageBackground source={{uri:item.image_path}}
                          style={{width:responsiveWidth(70),height:responsiveHeight(50),overflow:"hidden",borderRadius:responsiveWidth(3),alignItems:"center",justifyContent:"flex-end"}}>
                          <View >
                            <Text style={{fontSize:responsiveFontSize(3),fontWeight:"bold",color:"#fff"}}>
                              {item.image_title}
                            </Text>
                          </View>
                        </ImageBackground>
                      
                      </View>
                  </TouchableOpacity>
              </View>
              ))}
            </View>
          </Content> 
        </View>
        }
        {(this.state.latest.length>0) &&
        <View>
          <View style={{flexDirection:"row"}}>
              <View style={{width:responsiveWidth(2),backgroundColor:"#0246B1",margin:responsiveWidth(3)}}>

              </View>
              <Text style={{margin:responsiveWidth(3),marginLeft:0,fontSize:responsiveFontSize(3),fontWeight:"bold"}}>
                Lastest at YOYO
              </Text>
          </View>
          <Content horizontal>
            <View style={{alignItems:"center",justifyContent:"center",flexDirection:"row",margin:responsiveWidth(5),marginTop:responsiveHeight(1)}}>
              {this.state.latest.map((item,i) => (
                <View key={i} style={{marginRight:responsiveWidth(5),marginLeft:responsiveWidth(5),backgroundColor:"#fff",borderRadius:responsiveWidth(3)}}>
                  <TouchableOpacity onPress={()=>{alert("open location")}} style={{alignItems:"center",justifyContent:"center"}}>
                    <View >
                      <Image source={{uri:item.image_path}} resizeMode="stretch"
                        style={{width:responsiveWidth(50),height:responsiveHeight(20),borderTopLeftRadius:responsiveWidth(3),borderTopRightRadius:responsiveWidth(3)}}/>

                    </View>
                  </TouchableOpacity>
                  <View style={{alignItems:"center",justifyContent:"center",}}>
                    <TouchableOpacity onPress={()=>{alert("open location")}} 
                        style={{alignItems:"center",justifyContent:"center",margin:responsiveWidth(3)}}>
                        <View style={{width:responsiveWidth(40)}}>
                          <Text style={{fontSize:responsiveFontSize(2),color:"#000",fontWeight:"bold"}}>
                            { item.image_title}...
                          </Text>

                        </View>
                    </TouchableOpacity>
                  </View>
                </View>
              ))}
            </View>
          </Content> 
        </View>
        }
        {(this.state.yoyoSpecial.length>0) &&
        <View>
          <View style={{flexDirection:"row"}}>
              <View style={{width:responsiveWidth(2),backgroundColor:"#0246B1",margin:responsiveWidth(3)}}>

              </View>
              <Text style={{margin:responsiveWidth(3),marginLeft:0,fontSize:responsiveFontSize(3),fontWeight:"bold"}}>
                YOYO Special
              </Text>
          </View>
          <Content horizontal>
            <View style={{alignItems:"center",justifyContent:"center",flexDirection:"row",margin:responsiveWidth(5),marginTop:responsiveHeight(1)}}>
              {this.state.yoyoSpecial.map((item,i) => (
                <View key={i} style={{marginRight:responsiveWidth(5),marginLeft:responsiveWidth(5),backgroundColor:"#fff",borderRadius:responsiveWidth(3)}}>
                  <TouchableOpacity onPress={()=>{alert("open location")}} style={{alignItems:"center",justifyContent:"center"}}>
                    <View >
                      <Image source={{uri:item.image_path}} resizeMode="stretch"
                        style={{width:responsiveWidth(50),height:responsiveHeight(20),borderTopLeftRadius:responsiveWidth(3),borderTopRightRadius:responsiveWidth(3)}}/>

                    </View>
                  </TouchableOpacity>
                  <View style={{alignItems:"center",justifyContent:"center",}}>
                    <TouchableOpacity onPress={()=>{alert("open location")}} 
                        style={{alignItems:"center",justifyContent:"center",margin:responsiveWidth(3)}}>
                        <View style={{width:responsiveWidth(40)}}>
                          <Text style={{fontSize:responsiveFontSize(2),color:"#000",fontWeight:"bold"}}>
                            {item.image_title}...
                          </Text>

                        </View>
                        <View style={{width:responsiveWidth(40)}}>
                          <Text style={{fontSize:responsiveFontSize(1.5),color:"#000",opacity:0.5,fontWeight:"bold"}}>
                            {item.image_sub_title}...
                          </Text>

                        </View>
                    </TouchableOpacity>
                  </View>
                </View>
              ))}
            </View>
          </Content> 
        </View>
        }
        {(this.state.wallets.length>0) &&
        <View>
          <View style={{flexDirection:"row"}}>
              <View style={{width:responsiveWidth(2),backgroundColor:"#0246B1",margin:responsiveWidth(3)}}>

              </View>
              <Text style={{margin:responsiveWidth(3),marginLeft:0,fontSize:responsiveFontSize(3),fontWeight:"bold"}}>
                Your wallets
              </Text>
          </View>
          <Content horizontal>
            <View style={{alignItems:"center",justifyContent:"center",flexDirection:"row",margin:responsiveWidth(5),marginTop:responsiveHeight(1)}}>
              {this.state.wallets.map((item,i) => (
                <View key={i} style={{alignItems:"center"}}>
                  <TouchableOpacity onPress={()=>{alert("open location")}} style={{alignItems:"center",justifyContent:"center",marginRight:responsiveWidth(5),marginLeft:responsiveWidth(5)}}>
                      <View >
                        <ImageBackground source={{uri:item.image_path}}
                          style={{width:responsiveWidth(70),height:responsiveHeight(20),overflow:"hidden",borderRadius:responsiveWidth(3),justifyContent:"center"}}>
                          <View style={{backgroundColor:"#fff",borderRadius:responsiveWidth(5),margin:responsiveWidth(5),width:responsiveWidth(30),marginTop:responsiveHeight(8)}}>
                            <Text style={{fontSize:responsiveFontSize(3),fontWeight:"bold",color:"#000",marginVertical:responsiveWidth(2),marginHorizontal:responsiveWidth(5)}}>
                              ₹ {(i==0) &&
                                 this.state.walletsBalance.yoyo_money}
                                 {(i==1) &&
                                 this.state.walletsBalance.yoyo_ruppee}
                            </Text>
                          </View>
                        </ImageBackground>
                      
                      </View>
                  </TouchableOpacity>
              </View>
              ))}
            </View>
          </Content> 
        </View>
        }
    </View>
    }
  </View>
)

render() {
return (
  <SafeAreaView style={{flex:1}}>
    <View style={{flex:1}}>
      <StatusBar barStyle={Platform.OS=="ios"?"dark-content":"light-content"}/>
      {this.state.showModal && 
          <MapModal onAction={(val,city_name)=>this.setval(val,city_name)}/>
      }
     <ReactNativeParallaxHeader
        headerMinHeight={responsiveHeight(12)}
        headerMaxHeight={responsiveHeight(26)}
        navbarColor="#0246B1"
        backgroundColor="#0246B1"
        title={(
                <View style={{width:responsiveWidth(100),height:"auto",justifyContent:"center"}}>
                  <View style={{height: "auto",alignItems:"center",flexDirection: 'row',justifyContent:"space-between"}}>
                    <View>
                      <TouchableOpacity >
                        <Icon name="menu" style={{color:"#fff",fontSize:responsiveFontSize(4),marginLeft:responsiveWidth(3)}}/>
                      </TouchableOpacity>
                    </View>
                    <View>
                      <Image source={{uri:"https://i.imgur.com/tJNppaR.png"}} 
                        style={{width:responsiveWidth(40),height:responsiveHeight(7)}}/>
                    </View>
                    <View>
                      <TouchableOpacity >
                        <Icon name="bell" type="MaterialCommunityIcons" style={{color:"#fff",fontSize:responsiveFontSize(4),marginRight:responsiveWidth(3)}}/>
                      </TouchableOpacity>
                    </View>
                  </View>
                  <View style={{alignItems: 'center',justifyContent:"center",paddingHorizontal:responsiveWidth(5),marginTop:responsiveHeight(1)}}>
                    <TouchableOpacity onPress={()=>this.setState({showModal:true})}
                            style={{flexDirection:"row",alignItems:"center",borderWidth:1,borderRadius:responsiveWidth(3),
                            paddingHorizontal:responsiveWidth(3),marginTop:responsiveHeight(3),backgroundColor:"#fff"}}>
                      <View style={{flexDirection:"row",alignItems:"center",backgroundColor:"#fff"}}>
                        <Icon active name='search'  type="FontAwesome5" style={{color:"grey",fontSize:responsiveFontSize(3)}}/>
                        <Input placeholder="Search For Hotels, Location" 
                            disabled
                            style={{width:responsiveWidth(70),marginLeft:responsiveWidth(5)}}
                        />
                      </View>
                    </TouchableOpacity>
                  </View>
                </View>
        )}
        alwaysShowTitle={false}
        alwaysShowNavBar={false}
        renderNavBar={this.renderNavBar}
        renderContent={this.renderContent}
      />
    </View>
  </SafeAreaView>
  )}
}