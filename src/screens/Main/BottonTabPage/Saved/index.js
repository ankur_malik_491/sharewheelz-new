import React from 'react';
import {Text, View,TouchableOpacity,TouchableWithoutFeedback,Keyboard,ImageBackground,Image,AsyncStorage} from 'react-native';
import {Container,Icon,Input, Content,Toast,Card,CardItem,Spinner} from "native-base";
import {responsiveWidth,responsiveHeight,responsiveFontSize} from "react-native-responsive-dimensions";
import { SafeAreaView } from 'react-native-safe-area-context';
import { ScrollView } from 'react-native-gesture-handler';



export default class Saved extends React.Component {
    constructor(props){
        super(props);
        this.state={
          Savedhotels:[],
          loading:true,
        }
        this.getSavedHotels();
    } 
  
  getSavedHotels = async() =>{
      this.setState({loading:true})
      var user_id = await AsyncStorage.getItem('uid');
      //console.log(cityName)
        try{
          fetch("https://app.yoyorooms.net/Appapi/saved_user_Bookings", 
              {
                  method: 'POST',
                  headers: {
                      Accept: 'application/json',
                      'Content-Type': 'application/json',
                  },
                  body: JSON.stringify({
                              uid  : user_id   
                          }),     
              })
              .then(response => response.json())
              .then(response2 => {
                //console.log(response2)
                  if(response2){
                    this.setState({Savedhotels:response2})
                    this.setState({loading:false}) 
                    //console.log(this.state.Savedhotels[0].hotelinfo[0].short_name)              
                  }
                  else{
                      this.setState({loading:false})  
                    //MerryToast.show('Something went wrong! Please try again.', MerryToast.SHORT);
                  }
              }).catch((error) =>{
                  this.setState({loading:false})  
                //MerryToast.show('Something went wrong! Please try again.', MerryToast.SHORT);
                console.log(error)
                  // result.status = false;
                  // result.data = undefined;
                  // reject(result)
              });
          
      }
      catch(error){
          this.setState({loading:false}) 
        //MerryToast.show('Something went wrong! Please try again.', MerryToast.SHORT);
          // result.status = false;
          // result.message = error;
          // result.data = undefined;
          // resolve(result)
      }
      
  
    }
    saveHotel = async(hid,city)=>{
      var user_id = await AsyncStorage.getItem('uid');
      //console.log(cityName)
        try{
          fetch("https://app.yoyorooms.net/Appapi/save_user_hotel", 
              {
                  method: 'POST',
                  headers: {
                      Accept: 'application/json',
                      'Content-Type': 'application/json',
                  },
                  body: JSON.stringify({
                              uid  : user_id ,
                              hotelid :hid,
                              city :city 
                          }),     
              })
              .then(response => response.json())
              .then(response2 => {
                //console.log(response2)
                  if(response2==1){
                    this.getSavedHotels();
                    //console.log(this.state.Savedhotels[0].hotelinfo[0].short_name)              
                  }
                  else{
                      this.setState({loading:false})  
                    //MerryToast.show('Something went wrong! Please try again.', MerryToast.SHORT);
                  }
              }).catch((error) =>{
                  this.setState({loading:false})  
                //MerryToast.show('Something went wrong! Please try again.', MerryToast.SHORT);
                console.log(error)
                  // result.status = false;
                  // result.data = undefined;
                  // reject(result)
              });
          
      }
      catch(error){
          this.setState({loading:false}) 
        //MerryToast.show('Something went wrong! Please try again.', MerryToast.SHORT);
          // result.status = false;
          // result.message = error;
          // result.data = undefined;
          // resolve(result)
      }
    }
    
 render(){
   
 return (
    <SafeAreaView style={{flex:1}}>
        <Container >
            <View>
              <View style={{flexDirection:"row",alignItems:"center",justifyContent:"space-between",borderBottomWidth:1,
                        borderBottomColor:"#a6a6a6",paddingHorizontal:responsiveWidth(5),paddingVertical:responsiveHeight(2),height:"auto",backgroundColor: "#fff"}}>
                    <TouchableOpacity onPress={()=>this.props.navigation.goBack()}>
                        <Icon name='arrow-back'  style={{color:"#000",fontSize:responsiveFontSize(4)}}/>
                    </TouchableOpacity>
                    <TouchableOpacity style={{width:responsiveWidth(70)}}>
                        <Text style={{fontSize:responsiveFontSize(2.5),fontWeight:"bold"}}>
                            Saved
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity >
                        <Icon name='ios-heart'   style={{color:"red",fontSize:responsiveFontSize(4)}}/>
                    </TouchableOpacity>
                </View>
            </View>
            <View style={{flex:1,backgroundColor:"#e6e6e6"}}>
                {(!this.state.loading) &&
                    <ScrollView>
                        {(this.state.Savedhotels.length>0) &&
                            this.state.Savedhotels.map((item,i) => {
                              return(
                              <View key={i}  style={{margin:responsiveWidth(5),borderRadius:responsiveWidth(3)}}>
                                <View style={{flexDirection:"row",alignItems:"center",justifyContent:"space-between",paddingHorizontal:responsiveWidth(3),
                                              paddingVertical:responsiveHeight(1),borderLeftColor:"#0246b1",borderLeftWidth:5}}>
                                  <View style={{flexDirection:"row",alignItems:"center",}}>
                                    <Text style={{fontSize:responsiveFontSize(3),fontWeight:"bold",color:"#000"}}>
                                      {item.city[0].city}
                                    </Text>
                                  </View>
                                  <View style={{flexDirection:"row",alignItems:"center",}}>
                                    <Text style={{fontSize:responsiveFontSize(2.5),fontWeight:"bold",color:"#0246b1",marginRight:responsiveWidth(3)}}>
                                      {item.hotelinfo.length}
                                    </Text>
                                    <Icon name="ios-arrow-forward" style={{fontSize:responsiveFontSize(3),color:"#0246b1"}}/>
                                  </View>
                                </View>
                                <ScrollView horizontal={true}>
                                  {item.hotelinfo.map((item,i) => (
                                    <TouchableOpacity  key={i}
                                      onPress={()=>this.props.navigation.navigate("hotelDeatilsOuter",
                                                                                  {hotel_id:item.hotel_id,
                                                                                  hotel_name:item.full_name})}
                                    >
                                      <View style={{backgroundColor:"#fff",margin:responsiveWidth(5),borderRadius:responsiveWidth(3),
                                                    height:"auto",width:responsiveWidth(55),}}>
                                        <ImageBackground source={{uri:"https://unqinfotech.com/yoyo/images/uploads/1560564597.png"}} resizeMode="stretch"
                                                style={{height:"auto",width:responsiveWidth(55),borderTopLeftRadius:responsiveWidth(3),
                                                        borderTopRightRadius:responsiveWidth(3),overflow:"hidden"}}>
                                            <View style={{alignItems:"flex-end"}}>
                                                <TouchableOpacity 
                                                  onPress={()=>this.saveHotel(item.id,item.city_name)} 
                                                  style={{margin:responsiveWidth(3)}}>
                                                    <Icon name='ios-heart'   style={{color:"red",fontSize:responsiveFontSize(4)}}/>
                                                </TouchableOpacity>
                                            </View>
                                            <View style={{justifyContent:"center",alignItems:"center",flexDirection:"row",backgroundColor:"red",marginTop:responsiveWidth(10),
                                                        margin:responsiveWidth(3),width:responsiveWidth(15),padding:responsiveWidth(1)}}>
                                                <Text style={{fontSize:responsiveFontSize(2),color:"#fff",fontWeight:"bold",}}>
                                                    {item.rating}
                                                </Text>
                                                <Icon name='star' style={{color:"#fff",fontSize:responsiveFontSize(2),marginLeft:responsiveWidth(3)}}/>
                                            </View>
                                        </ImageBackground>
                                        <View style={{paddingHorizontal:responsiveWidth(5),paddingBottom:responsiveHeight(2),marginTop:responsiveHeight(2),
                                                      justifyContent:"space-between",width:responsiveWidth(55)}}>
                                          <Text style={{fontSize:responsiveFontSize(2.5),color:"#000",fontWeight:"bold"}}>
                                            {item.short_name}
                                          </Text>
                                          <Text style={{fontSize:responsiveFontSize(2),color:"#000",opacity:0.5,fontWeight:"bold",}}>
                                            {item.short_address}
                                          </Text>
                                          <View style={{flexDirection:"row",alignItems:"center",width:responsiveWidth(45),}}>
                                              <Text style={{fontSize:responsiveFontSize(2.1),color:"#000",fontWeight:"bold",}}>
                                                  ₹ {item.price}
                                              </Text>
                                              <Text style={{fontSize:responsiveFontSize(1.7),color:"#000",opacity:0.7,marginLeft:responsiveWidth(3),textDecorationLine:"line-through"}}>
                                                  ₹ {item.price-((item.price*item.discount)/100)}
                                              </Text>
                                              <Text style={{fontSize:responsiveFontSize(1.7),color:"#ff9900",fontWeight:"bold",marginLeft:responsiveWidth(3)}}>
                                                  {item.discount} % OFF
                                              </Text>
                                          </View>
                                        </View>
                                      </View>
                                    </TouchableOpacity>
                                  ))}
                                </ScrollView>
                                    
                            </View>
                              )}
                        )} 
                        {(this.state.Savedhotels.length<=0) &&
                            <View  style={{alignItems:"center",justifyContent:"center",height:responsiveHeight(100)}}>
                                <Image source={require("../../../../../assets/splash.png")} style={{height:responsiveHeight(20),width:responsiveWidth(70),}} />
                                <Text style={{color:"#0a1d3b",fontSize:responsiveFontSize(2.5),fontWeight:"bold"}}>
                                  Sorry! No Saved Hotels Found.
                                </Text>
                            </View>

                        }
                    </ScrollView>  
                }
                {(this.state.loading) &&
                    <View  style={{alignItems:"center",justifyContent:"center",flex:1}}>
                      {/* <Image source={require("../../../../../assets/splash.png")} style={{height:responsiveHeight(40),width:responsiveWidth(60),}} /> */}
                      <Spinner color="#0246b1" size="large"/>
                    </View>
                }
            </View>
        </Container>
    </SafeAreaView>
)}
} 




