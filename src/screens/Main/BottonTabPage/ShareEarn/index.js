import React from 'react';
import {Text, View,TouchableOpacity,TouchableWithoutFeedback,Keyboard,ImageBackground,Image,AsyncStorage,Share} from 'react-native';
import {Container,Icon,Input, Content,Toast,Card,CardItem,Spinner,Segment} from "native-base";
import {responsiveWidth,responsiveHeight,responsiveFontSize} from "react-native-responsive-dimensions";
import { SafeAreaView } from 'react-native-safe-area-context';
import { ScrollView } from 'react-native-gesture-handler';

export default class ShareEarn extends React.Component {
    constructor(props){
      super(props);
      this.state={
       
      }
  } 

onSharePress = () => {
  const shareOptions = {
    title: 'Refer and earn!',
    message: 'I recommend you use the YOYO app for hassle-free stays at affordable prices. Download from https://app.nxgenrooms.com/welcome/share/W7GEM18IT1 , use my code: W7GEM18IT1', 
    // url: 'www.example.com',
    // subject: 'Subject'
  };
  Share.share(shareOptions);
}

render() {
  //console.log(this.state.userData)
return (
  <SafeAreaView style={{flex:1}}>
        <Container style={{backgroundColor:"#e6e6e6"}}>
          <View style={{flexDirection:"row",alignItems:"center",justifyContent:"space-between",borderBottomWidth:1,
                  borderBottomColor:"#a6a6a6",paddingHorizontal:responsiveWidth(5),paddingVertical:responsiveHeight(2),height:"auto",backgroundColor: "#fff"}}>
              <TouchableOpacity onPress={()=>this.props.navigation.goBack()}>
                  <Icon name='arrow-back'  style={{color:"#000",fontSize:responsiveFontSize(4)}}/>
              </TouchableOpacity>
              <TouchableOpacity style={{width:responsiveWidth(70)}}>
                  <Text style={{fontSize:responsiveFontSize(2.5),fontWeight:"bold"}}>
                      INVITE & EARN
                  </Text>
              </TouchableOpacity>
              <TouchableOpacity >
                  <Icon name='ios-help-circle'   style={{color:"#000",fontSize:responsiveFontSize(4)}}/>
              </TouchableOpacity>
          </View>
          <ScrollView>
            <Image source={{uri:"https://app.yoyorooms.net/assets/images/invite.png"}} 
                    resizeMode="stretch"
                    style={{height:responsiveHeight(30),width:responsiveWidth(100)}}/>
            <View style={{backgroundColor:"#fff",margin:responsiveWidth(5),
                        marginTop:responsiveHeight(-3),borderRadius:responsiveWidth(3)}}>
              <View style={{paddingHorizontal:responsiveWidth(5),paddingVertical:responsiveHeight(2)}}>
                  <Text style={{fontSize:responsiveFontSize(2.8),color:"#000",fontWeight:"bold"}}>
                    How this works?
                  </Text>
                  <Text style={{fontSize:responsiveFontSize(2.3),color:"#000",fontWeight:"800",opacity:0.7,marginTop:responsiveHeight(1)}}>
                    When you invite freinds to YOYO
                  </Text>
              </View>
              <View style={{flexDirection:"row",alignItems:"center",justifyContent:"space-between",paddingHorizontal:responsiveWidth(5),
                            paddingVertical:responsiveHeight(2)}}>
                <View>
                  <Text style={{fontSize:responsiveFontSize(2.5),color:"#000",fontWeight:"bold"}}>
                    You Get
                  </Text>
                  <View style={{paddingHorizontal:responsiveWidth(5),marginTop:responsiveHeight(1),width:responsiveWidth(37),
                                paddingVertical:responsiveHeight(2),backgroundColor:"#e6e6e6",borderRadius:responsiveWidth(5),}}>
                    <Text style={{fontSize:responsiveFontSize(2.5),color:"#000",fontWeight:"bold"}}>
                      ₹ 250
                    </Text>
                    <Text style={{fontSize:responsiveFontSize(2),color:"#000",fontWeight:"500",opacity:0.7}}>
                      On their first Check-in
                    </Text>
                  </View>
                </View>
                <View>
                  <Text style={{fontSize:responsiveFontSize(2.5),color:"#000",fontWeight:"bold"}}>
                    You Get
                  </Text>
                  <View style={{paddingHorizontal:responsiveWidth(5),marginTop:responsiveHeight(1),width:responsiveWidth(37),
                                paddingVertical:responsiveHeight(2),backgroundColor:"#e6e6e6",borderRadius:responsiveWidth(5),}}>
                    <Text style={{fontSize:responsiveFontSize(2.5),color:"#000",fontWeight:"bold"}}>
                      ₹ 250
                    </Text>
                    <Text style={{fontSize:responsiveFontSize(2),color:"#000",fontWeight:"500",opacity:0.7}}>
                      When they Sign up
                    </Text>
                  </View>
                </View>
              </View>
              <View style={{alignItems:"center",justifyContent:"center",paddingHorizontal:responsiveWidth(5),
                            paddingVertical:responsiveHeight(3)}}>
                <TouchableOpacity onPress={()=>this.onSharePress()}>
                    <View style={{flexDirection:"row",justifyContent:"center",alignItems:"center",backgroundColor:"green",
                                paddingVertical:responsiveHeight(2),paddingHorizontal:responsiveWidth(5),borderRadius:responsiveWidth(5)}}>
                        <Icon name='logo-whatsapp'   style={{color:"#fff",fontSize:responsiveFontSize(3)}}/>
                        <Text style={{fontSize:responsiveFontSize(2.3),color:"#fff",fontWeight:"bold",marginLeft:responsiveWidth(3)}}>
                            SHARE VIA WHATSAPP
                        </Text>
                    </View>
                </TouchableOpacity>
              </View>
              <View style={{paddingHorizontal:responsiveWidth(5),marginBottom:responsiveHeight(5)}}>
                <View style={{flexDirection:"row",alignItems:"center",borderTopColor:"#e6e6e6",borderTopWidth:1,borderStyle:"dashed",paddingVertical:responsiveHeight(2),}}>
                  <Text style={{fontSize:responsiveFontSize(2.3),color:"#000",fontWeight:"800",opacity:0.7,}}>
                    Your invite code :
                  </Text>
                  <Text style={{fontSize:responsiveFontSize(2.3),color:"#000",fontWeight:"bold",marginLeft:responsiveWidth(2)}}>
                    W7GEM18IT1
                  </Text> 
                </View>
                <View style={{alignItems:"center",justifyContent:"center",paddingHorizontal:responsiveWidth(5)}}>
                  <TouchableOpacity onPress={()=>alert(" copied : W7GEM18IT1")}>
                      <View style={{justifyContent:"center",alignItems:"center",backgroundColor:"#0246b1",
                                  paddingVertical:responsiveHeight(1),paddingHorizontal:responsiveWidth(5),borderRadius:responsiveWidth(5)}}>
                          <Text style={{fontSize:responsiveFontSize(2.3),color:"#fff",fontWeight:"bold",}}>
                              COPY
                          </Text>
                      </View>
                  </TouchableOpacity>
                </View>          
              </View>
            </View>
          </ScrollView>
        </Container>
  </SafeAreaView>
);
}
}
