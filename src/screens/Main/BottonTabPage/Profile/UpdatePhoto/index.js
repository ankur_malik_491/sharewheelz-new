import React from 'react';
import { Icon,Text,Toast} from 'native-base';
import { View ,StyleSheet,TouchableOpacity,Alert,Image,Modal} from 'react-native';
import {responsiveHeight,responsiveWidth, responsiveFontSize} from'react-native-responsive-dimensions';
import * as ImagePicker from 'expo-image-picker';
import Constants from 'expo-constants';
import * as Permissions from 'expo-permissions';
import Spinner from 'react-native-loading-spinner-overlay';
import API from "../../../../../../allApi";
import Localstorage from "../../../../../../async";


export default class UpdateUserPhoto extends React.Component {
    constructor(props){
        super(props);
        this.state = { 
            photoUrl:"https://i.imgur.com/jeCVrJJ.png",
            pickedImage:false,
            showModal:false,
            showLoader:false,
            loading:false
        };
        this.getUserPhoto();
    }
getUserPhoto =async () =>{
    try{ 
        Localstorage.retrieveItem("UID").then((userId) =>{
        if(userId){
            API.getUserDetails(userId).then((response)=>{
            if(response.status){
                if(!response.data.u_profile_pic == "")
                    { this.setState({photoUrl:response.data.u_profile_pic})}
                    
                else{
                    this.setState({photoUrl:"https://i.ya-webdesign.com/images/profile-avatar-png-4.png"})}
                }
                else{
                Toast.show({
                    text:response.message,
                    textStyle:{textAlign:"center"},
                    duration:2000,
                    style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
                });
                }
            }).catch((error) =>{
            console.log(error)
        });
        }
        else{
            alert("You are not logged in! please login first.")
            this.props.navigation.navigate('auth');
        }
        }).catch((error) =>{
            console.log(error)
        });
    }
    catch (error) {
        Toast.show({
            text:error,
            textStyle:{textAlign:"center"},
            duration:2000,
            style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
            });
    }
}
openCamera = async () => {
    try { 
        this.setState({showModal:false})
        const { status } = await Permissions.askAsync(Permissions.CAMERA_ROLL);
        if (status !== 'granted') {
            alert('Sorry, we need camera roll permissions to make this work!');
        }
        else{
            let result = await ImagePicker.launchCameraAsync({
            mediaTypes: ImagePicker.MediaTypeOptions.All,
            allowsEditing: true,
            aspect: [4, 3],
            quality: 1,
            });
            if (!result.cancelled) {
            this.setState({ photoUrl : result.uri,pickedImage : true });
            }
        }
      } catch (error) {
            Toast.show({
                text:error,
                textStyle:{textAlign:"center"},
                duration:2000,
                style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
            });
      }
}
openGallery = async () => {
    try { 
        this.setState({showModal:false})
        let result = await ImagePicker.launchImageLibraryAsync({
          mediaTypes: ImagePicker.MediaTypeOptions.Images,
          allowsEditing: true,
          aspect: [4, 3],
          quality: 1,
         // base64:true
        });
        if (!result.cancelled) {
          this.setState({ photoUrl : result.uri,pickedImage : true });
        }
  
        console.log(result);
      } catch (error) {
            Toast.show({
                text:error,
                textStyle:{textAlign:"center"},
                duration:2000,
                style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
            });
      }
}
updatePhoto = () => {
    try{
        var pic = this.state.photoUrl
        Localstorage.retrieveItem("UID").then((userId) =>{
        if(userId){
            this.setState({showLoader:true,loading:true})
            API.updateUserPhoto(userId,pic).then((response)=>{
                //console.log(response)
            if(response.status){
                Toast.show({
                    text:response.message,
                    textStyle:{textAlign:"center"},
                    duration:2000,
                    style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
                });
                this.setState({showLoader:false,loading:false})
                this.props.navigation.goBack();
            }
            else{
                this.setState({showLoader:false,loading:false})
                Toast.show({
                    text:response.message,
                    textStyle:{textAlign:"center"},
                    duration:2000,
                    style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
                });
            }
            }).catch((error) =>{
            console.log(error)
        });
        }
        else{
            alert("You are not logged in! please login first.")
            this.props.navigation.navigate('auth');
        }
        }).catch((error) =>{
            console.log(error)
        });
    }
    catch (error) {
        this.setState({showLoader:false,loading:false})
        Toast.show({
            text:error,
            textStyle:{textAlign:"center"},
            duration:2000,
            style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
            });
    }
}
render(){
     // console.log(this.state.color1)
  return (
          
    <View style={{felx:1}}>
        <View style={{flexDirection:"row",alignItems:"center",backgroundColor:"#0652dd",padding:responsiveWidth(5)}}>
            <TouchableOpacity onPress={()=>this.props.navigation.goBack()}>
                <Icon name="ios-arrow-back" style={{color:"#fff",fontSize:responsiveFontSize(4)}}/>
            </TouchableOpacity>
            <Text style={{color:"#fff",fontSize:responsiveFontSize(3),marginLeft:responsiveWidth(5)}}>Choose profile photo</Text>
        </View>
        <View style={{alignItems:"center",justifyContent:"center"}}>
            <Image source={{uri: this.state.photoUrl}}
                style={{height:responsiveWidth(35),width:responsiveWidth(35),
                         borderRadius:responsiveWidth(20),marginTop:responsiveHeight(5)}} />

            <View style={{marginTop:responsiveHeight(5)}}>
                <Text style={{opacity:0.7,fontSize:responsiveFontSize(2.5),}}>Example of a good photo</Text>
            </View>
        </View>
        <View style={{justifyContent:"center",marginTop:responsiveHeight(3),marginLeft:responsiveWidth(5)}}>
            <View style={{flexDirection:"row",alignItems:"center"}}>
                <Icon name="ios-checkmark-circle-outline" style={{fontSize:responsiveFontSize(4),opacity:0.7}}/>
                <Text style={{opacity:0.7,fontSize:responsiveFontSize(2.3),marginLeft:responsiveWidth(3)}}>
                    No Sunglasses
                </Text>
            </View>
            <View style={{flexDirection:"row",alignItems:"center",marginTop:responsiveHeight(2)}}>
                <Icon name="ios-checkmark-circle-outline" style={{fontSize:responsiveFontSize(4),opacity:0.7}}/>
                <Text style={{opacity:0.7,fontSize:responsiveFontSize(2.3),marginLeft:responsiveWidth(3)}}>
                    Facing the camera
                </Text>
            </View>
            <View style={{flexDirection:"row",alignItems:"center",marginTop:responsiveHeight(2)}}>
                <Icon name="ios-checkmark-circle-outline" style={{fontSize:responsiveFontSize(4),opacity:0.7}}/>
                <Text style={{opacity:0.7,fontSize:responsiveFontSize(2.3),marginLeft:responsiveWidth(3)}}>
                    You alone
                </Text>
            </View>
            <View style={{flexDirection:"row",alignItems:"center",marginTop:responsiveHeight(2)}}>
                <Icon name="ios-checkmark-circle-outline" style={{fontSize:responsiveFontSize(4),opacity:0.7}}/>
                <Text style={{opacity:0.7,fontSize:responsiveFontSize(2.3),marginLeft:responsiveWidth(3)}}>
                    Clear and bright
                </Text>
            </View>
        </View>
        <View style={{alignItems:"center",justifyContent:"flex-end",marginTop:responsiveHeight(10)}}>
           {(!this.state.pickedImage) && <TouchableOpacity onPress={()=> this.setState({showModal:true})}
              style={{alignItems:"center",justifyContent:"center",width:responsiveWidth(50),height:responsiveHeight(7),
              backgroundColor:"#0652dd",borderRadius:responsiveWidth(3)}}>
              <Text style={{fontSize:responsiveFontSize(2),color: '#fff'}}>
                Choose a photo
              </Text>
            </TouchableOpacity>}
            {(this.state.pickedImage)&&
            <TouchableOpacity onPress={()=> this.updatePhoto()}
              style={{alignItems:"center",justifyContent:"center",width:responsiveWidth(50),height:responsiveHeight(7),
              backgroundColor:(!this.state.loading)?"#0652dd":"grey",borderRadius:responsiveWidth(3)}}>
              <Text style={{fontSize:responsiveFontSize(2),color: '#fff'}}>
                {(this.state.loading)?"please wait":"Upload photo"}
              </Text>
            </TouchableOpacity>
            }
        </View>
        <Modal
            animationType="slide"
            transparent={true}
            visible={this.state.showModal}
            onRequestClose={() => this.setState({showModal:false})}
            animationType="fade"
            transparent={true}
        >
            <View style={{flex:1,alignItems:"center",justifyContent:"center",backgroundColor:"#000",opacity:0.9}}>
            <View style={{backgroundColor:"#fff",height:"auto",width:responsiveWidth(80)}} >
                <View style={{marginLeft:responsiveWidth(10),margin:responsiveHeight(3)}}>  
                    <Text style={{fontSize:responsiveFontSize(3)}}>Select profile image</Text>
                    <TouchableOpacity onPress={()=>this.openCamera()}>
                        <Text style={{fontSize:responsiveFontSize(2.5),marginTop:responsiveHeight(2)}}>
                            Take a picture
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=>this.openGallery()}>
                        <Text style={{fontSize:responsiveFontSize(2.5),marginTop:responsiveHeight(2)}}>
                            Choose from gallery
                        </Text>
                    </TouchableOpacity>
                    </View>
                </View>
            </View>
        </Modal> 
        <Spinner animation="fade"
                visible={this.state.showLoader}
                textContent={'Loading...'}
                textStyle={{color:"#fff"}}
              />    
    </View>
            
        );
    }
}

const styles = StyleSheet.create({
    container:{
        flex:1,
        alignItems:"center",
        justifyContent:"center"
    }
});