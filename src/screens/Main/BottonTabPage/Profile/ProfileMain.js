import React from 'react';
import {Text, View,TouchableOpacity,Image} from 'react-native';
import {Toast,Icon} from "native-base";
import {responsiveWidth,responsiveHeight,responsiveFontSize} from "react-native-responsive-dimensions";
import Spinner from 'react-native-loading-spinner-overlay';
import ReactNativeParallaxHeader from 'react-native-parallax-header';
import API from "../../../../../allApi";
import Localstorage from "../../../../../async";

export default class ProfileMain extends React.Component {
  constructor(props){
    super(props);
    
    this.state={
      userData:[],
      photoUrl:"https://i.imgur.com/jeCVrJJ.png",
      prof:true,
      name:false,
      showLoader:false,
    }
    this.getUserData()
    //console.log("constructor")
}
signout = async () =>{
  try{
      this.setState({showLoader:true});
      Localstorage.removeItem("UID").then((response) =>{
        if(!response){
          this.setState({showLoader:false});
          Toast.show({
            text:"Logout Successfully",
            textStyle:{textAlign:"center"},
            duration:2000,
            style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"green"}
          });
          this.props.navigation.navigate('auth');
        }
        else{
          this.setState({showLoader:false});
          Toast.show({
            text:"Something Went wrong!",
            textStyle:{textAlign:"center"},
            duration:2000,
            style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
          });
        }
      })
    }
  catch (error) {
    Toast.show({
        text:error,
        textStyle:{textAlign:"center"},
        duration:2000,
        style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
      });
    }
}
updateUserPassword = () =>{
  if(this.state.userData.u_login_source == "facebook"){
    Toast.show({
      text:"Can't change password, logged in with Social account",
      textStyle:{textAlign:"center"},
      duration:2000,
      style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
    });
  }
  else{
    this.props.navigation.navigate("passwordUpdate")
  }
}
getUserData =async () =>{
  try{ 
    Localstorage.retrieveItem("UID").then((userId) =>{
      if(userId){
        API.getUserDetails(userId).then((response)=>{
          console.log(response)
          if(response.status){
              this.setState({userData:response.data})
              if(!response.data.u_profile_pic == "")
                { this.setState({photoUrl:response.data.u_profile_pic})}
                
              else{
                this.setState({photoUrl:"https://i.ya-webdesign.com/images/profile-avatar-png-4.png"})}
              }
            else{
              Toast.show({
                text:response.message,
                textStyle:{textAlign:"center"},
                duration:2000,
                style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
              });
            }
        }).catch((error) =>{
          console.log(error)
      });
      }
      else{
        alert("You are not logged in! please login first.")
        this.props.navigation.navigate('auth');
      }
    }).catch((error) =>{
          console.log(error)
      });
  }
  catch (error) {
      Toast.show({
          text:error,
          textStyle:{textAlign:"center"},
          duration:2000,
          style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
        });
  }
}
openImage = () =>{
  if(this.state.userData.u_login_source == "facebook"){
    Toast.show({
      text:"Can't change profile pic , logged in with Social account",
      textStyle:{textAlign:"center"},
      duration:2000,
      style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
    });
  }
  else{
    this.props.navigation.navigate("updateUserPhoto")
  }
}
renderNavBar = () => (
  <View style={{height: responsiveHeight(12),alignItems: 'center', flexDirection: 'row',justifyContent:"space-between"}}>
    <View style={{flexDirection:"row"}}>
      <Image source={{uri: this.state.photoUrl}}
             style={{height:responsiveWidth(10),width:responsiveWidth(10),borderRadius:responsiveWidth(10),marginLeft:responsiveWidth(5)}} />
      <Text style={{color:"#fff",fontSize:responsiveFontSize(3),marginLeft:responsiveWidth(3)}}>
        {this.state.userData.u_first_name}
      </Text>
    </View>
    {/* <View>
      <TouchableOpacity onPress={() => alert("open setting page")}>
        <Icon name="settings" style={{color:"#fff",fontSize:responsiveFontSize(4),marginRight:responsiveWidth(3)}}/>
      </TouchableOpacity>
    </View> */}
  </View>
      
  )
renderContent = () => (
            <>
              <View style={{paddingHorizontal:responsiveWidth(3),marginTop:responsiveHeight(2)}}>
                <Text style={{fontWeight:'bold',fontSize:responsiveFontSize(2.5),color:'#000',opacity:0.6}}>GENERAL</Text>
                <TouchableOpacity onPress={()=>{this.props.navigation.navigate("profileUpdate")}}>
                  <View style={{flexDirection:'row',height:"auto",backgroundColor:'#fff',marginTop:responsiveHeight(2),
                              borderRadius:responsiveWidth(2),borderWidth:1}}>
                    <View style={{backgroundColor:'#669900',width:responsiveWidth(15),height:responsiveWidth(14),borderRadius:responsiveWidth(2),
                                justifyContent:'center',alignItems:'center',margin:responsiveWidth(1.5)}}>
                      <Icon name="settings" style={{color:"#fff",fontSize:responsiveFontSize(4)}}/>
                    </View>
                    <View style={{flex:4,justifyContent:'center',marginLeft:responsiveWidth(2)}}>
                      <Text style={{fontSize:responsiveFontSize(2),fontWeight:'bold',color:'#0652dd'}}>Profile </Text>
                      <Text style={{fontSize:responsiveFontSize(1.9),color:'#000',opacity:0.7}}>Update your profile</Text>
                    </View>
                    <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                      <Icon name="ios-arrow-forward" style={{color:"#000",fontSize:responsiveFontSize(4)}}/>
                    </View>
                  </View>
                </TouchableOpacity>
              </View>
              <View style={{paddingHorizontal:responsiveWidth(3)}}>
              <TouchableOpacity onPress={()=>{this.updateUserPassword()}}>
              <View style={{flexDirection:'row',height:"auto",backgroundColor:'#fff',marginTop:responsiveHeight(2),
                              borderRadius:responsiveWidth(2),borderWidth:1}}>
                  <View style={{backgroundColor:'#3399ff',width:responsiveWidth(15),height:responsiveWidth(14),borderRadius:responsiveWidth(2),
                              justifyContent:'center',alignItems:'center',margin:responsiveWidth(1.5)}}>
                    <Icon name="ios-lock" style={{color:"#fff",fontSize:responsiveFontSize(4)}}/>
                  </View>
                  <View style={{flex:4,justifyContent:'center',marginLeft:responsiveWidth(2)}}>
                    <Text style={{fontSize:responsiveFontSize(2),fontWeight:'bold',color:'#0652dd'}}>Privacy</Text>
                    <Text style={{fontSize:responsiveFontSize(1.9),color:'#000',opacity:0.7}}>Change your password</Text>
                  </View>
                  <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                    <Icon name="ios-arrow-forward" style={{color:"#000",fontSize:responsiveFontSize(4)}}/>
                  </View>
                </View>
              </TouchableOpacity>
            </View>
            <View style={{paddingHorizontal:responsiveWidth(3)}}>
              <TouchableOpacity onPress={()=>{this.props.navigation.navigate("userCars")}}>
                <View style={{flexDirection:'row',height:"auto",backgroundColor:'#fff',marginTop:responsiveHeight(2),
                              borderRadius:responsiveWidth(2),borderWidth:1}}>
                  <View style={{backgroundColor:'#cc6600',width:responsiveWidth(15),height:responsiveWidth(14),borderRadius:responsiveWidth(2),
                              justifyContent:'center',alignItems:'center',margin:responsiveWidth(1.5)}}>
                    <Icon name="car-side" type="MaterialCommunityIcons" style={{color:"#fff",fontSize:responsiveFontSize(4)}}/>
                  </View>
                  <View style={{flex:4,justifyContent:'center',marginLeft:responsiveWidth(2)}}>
                    <Text style={{fontSize:responsiveFontSize(2),fontWeight:'bold',color:'#0652dd'}}>Cars</Text>
                    <Text style={{fontSize:responsiveFontSize(1.9),color:'#000',opacity:0.7}}>Add your Car</Text>
                  </View>
                  <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                    <Icon name="ios-arrow-forward" style={{color:"#000",fontSize:responsiveFontSize(4)}}/>
                  </View>
                </View>
              </TouchableOpacity>
            </View>
            <View style={{paddingHorizontal:responsiveWidth(3)}}>
              <TouchableOpacity onPress={()=>this.props.navigation.navigate("userPayments")}>
                <View style={{flexDirection:'row',height:"auto",backgroundColor:'#fff',marginTop:responsiveHeight(2),
                              borderRadius:responsiveWidth(2),borderWidth:1}}>
                  <View style={{backgroundColor:'#336600',width:responsiveWidth(15),height:responsiveWidth(14),borderRadius:responsiveWidth(2),
                              justifyContent:'center',alignItems:'center',margin:responsiveWidth(1.5)}}>
                    <Icon name="ios-card" style={{color:"#fff",fontSize:responsiveFontSize(4)}}/>
                  </View>
                  <View style={{flex:4,justifyContent:'center',marginLeft:responsiveWidth(2)}}>
                    <Text style={{fontSize:responsiveFontSize(2),fontWeight:'bold',color:'#0652dd'}}>Payments</Text>
                    <Text style={{fontSize:responsiveFontSize(1.9),color:'#000',opacity:0.7}}>Select your payment method</Text>
                  </View>
                  <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                    <Icon name="ios-arrow-forward" style={{color:"#000",fontSize:responsiveFontSize(4)}}/>
                  </View>
                </View>
              </TouchableOpacity>
            </View>
            <View style={{paddingHorizontal:responsiveWidth(3)}}>
              <TouchableOpacity onPress={()=>{this.props.navigation.navigate("refer")}}>
                <View style={{flexDirection:'row',height:"auto",backgroundColor:'#fff',marginTop:responsiveHeight(2),
                              borderRadius:responsiveWidth(2),borderWidth:1}}>
                  <View style={{backgroundColor:'#ffcc00',width:responsiveWidth(15),height:responsiveWidth(14),borderRadius:responsiveWidth(2),
                              justifyContent:'center',alignItems:'center',margin:responsiveWidth(1.5)}}>
                    <Icon name="ios-cash" style={{color:"#fff",fontSize:responsiveFontSize(4)}}/>
                  </View>
                  <View style={{flex:4,justifyContent:'center',marginLeft:responsiveWidth(2)}}>
                    <Text style={{fontSize:responsiveFontSize(2),fontWeight:'bold',color:'#0652dd'}}>Refer & Earn</Text>
                    <Text style={{fontSize:responsiveFontSize(1.9),color:'#000',opacity:0.7}}>Your refering and earnings code</Text>
                  </View>
                  <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                    <Icon name="ios-arrow-forward" style={{color:"#000",fontSize:responsiveFontSize(4)}}/>
                  </View>
                </View>
              </TouchableOpacity>
            </View>
            <View style={{paddingHorizontal:responsiveWidth(3)}}>
              <TouchableOpacity onPress={()=>{this.props.navigation.navigate("ratingReview")}}>
                <View style={{flexDirection:'row',height:"auto",backgroundColor:'#fff',marginTop:responsiveHeight(2),
                              borderRadius:responsiveWidth(2),borderWidth:1}}>
                  <View style={{backgroundColor:'#990000',width:responsiveWidth(15),height:responsiveWidth(14),borderRadius:responsiveWidth(2),
                              justifyContent:'center',alignItems:'center',margin:responsiveWidth(1.5)}}>
                    <Icon name="playlist-edit" type="MaterialCommunityIcons" style={{color:"#fff",fontSize:responsiveFontSize(4)}}/>
                  </View>
                  <View style={{flex:4,justifyContent:'center',marginLeft:responsiveWidth(2)}}>
                    <Text style={{fontSize:responsiveFontSize(2),fontWeight:'bold',color:'#0652dd'}}>Ratings & Reviews</Text>
                    <Text style={{fontSize:responsiveFontSize(1.9),color:'#000',opacity:0.7}}>See your rating & reviews</Text>
                  </View>
                  <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                    <Icon name="ios-arrow-forward" style={{color:"#000",fontSize:responsiveFontSize(4)}}/>
                  </View>
                </View>
              </TouchableOpacity>
            </View>
            <View style={{paddingHorizontal:responsiveWidth(3)}}>
              <TouchableOpacity onPress={()=>{this.props.navigation.navigate("userPreferences")}}>
                <View style={{flexDirection:'row',height:"auto",backgroundColor:'#fff',marginTop:responsiveHeight(2),
                              borderRadius:responsiveWidth(2),borderWidth:1}}>
                  <View style={{backgroundColor:'#00a3cc',width:responsiveWidth(15),height:responsiveWidth(14),borderRadius:responsiveWidth(2),
                              justifyContent:'center',alignItems:'center',margin:responsiveWidth(1.5)}}>
                    <Icon name="ios-options" style={{color:"#fff",fontSize:responsiveFontSize(4)}}/>
                   </View>
                  <View style={{flex:4,justifyContent:'center',marginLeft:responsiveWidth(2)}}>
                    <Text style={{fontSize:responsiveFontSize(2),fontWeight:'bold',color:'#0652dd'}}>Preferences</Text>
                    <Text style={{fontSize:responsiveFontSize(1.9),color:'#000',opacity:0.7}}>Change your preferences</Text>
                  </View>
                  <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                    <Icon name="ios-arrow-forward" style={{color:"#000",fontSize:responsiveFontSize(4)}}/>
                  </View>
                </View>
              </TouchableOpacity>
            </View>
            <View style={{paddingHorizontal:responsiveWidth(3),marginTop:responsiveHeight(2)}}>
            <Text style={{fontWeight:'bold',fontSize:responsiveFontSize(2.5),color:'#000',opacity:0.6}}>App</Text>
              <TouchableOpacity onPress={()=>{this.props.navigation.navigate("about")}}>
                <View style={{flexDirection:'row',height:responsiveHeight(10),backgroundColor:'#fff',marginTop:responsiveHeight(2),
                              borderRadius:responsiveWidth(2),borderWidth:1}}>
                  <View style={{backgroundColor:'#999966',width:responsiveWidth(15),height:responsiveWidth(14),borderRadius:responsiveWidth(2),
                              justifyContent:'center',alignItems:'center',margin:responsiveWidth(1.5)}}>
                    <Icon name="ios-contacts" style={{color:"#fff",fontSize:responsiveFontSize(4)}}/>
                  </View>
                  <View style={{flex:4,justifyContent:'center',marginLeft:responsiveWidth(2)}}>
                    <Text style={{fontSize:responsiveFontSize(2),fontWeight:'bold',color:'#0652dd'}}>About Us</Text>
                    <Text style={{fontSize:responsiveFontSize(1.9),color:'#000',opacity:0.7}}>ShareWheelz contact details</Text>
                  </View>
                  <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                  <Icon name="ios-arrow-forward" style={{color:"#000",fontSize:responsiveFontSize(4)}}/>
                  </View>
                </View>
              </TouchableOpacity>
            </View>
            <View style={{paddingHorizontal:responsiveWidth(3)}}>
              <TouchableOpacity onPress={()=>{this.signout()}}>
                <View style={{flexDirection:'row',height:"auto",backgroundColor:'#fff',marginTop:responsiveHeight(2),
                              borderRadius:responsiveWidth(2),borderWidth:1}}>
                  <View style={{backgroundColor:'#993300',width:responsiveWidth(15),height:responsiveWidth(14),borderRadius:responsiveWidth(2),
                              justifyContent:'center',alignItems:'center',margin:responsiveWidth(1.5)}}>
                      <Icon name="ios-exit" style={{color:"#fff",fontSize:responsiveFontSize(4)}}/>
                  </View>
                  <View style={{flex:4,justifyContent:'center',marginLeft:responsiveWidth(2)}}>
                    <Text style={{fontSize:responsiveFontSize(2),fontWeight:'bold',color:'#0652dd'}}>Logout</Text>
                    <Text style={{fontSize:responsiveFontSize(1.9),color:'#000',opacity:0.7}}>Logout from ShareWhelz</Text>
                  </View>
                  <View style={{flex:1,justifyContent:'center',alignItems:'center'}}>
                    <Icon name="ios-arrow-forward" style={{color:"#000",fontSize:responsiveFontSize(4)}}/>
                  </View>
                </View>
              </TouchableOpacity>
              <Text/>
            </View>
        </>
)
 
render() {
  return (
    <View style={{flex:1}}>
      <ReactNativeParallaxHeader
        headerMinHeight={responsiveHeight(12)}
        headerMaxHeight={responsiveHeight(32)}
        navbarColor="#0652dd"
       // backgroundImage={{uri:this.state.userData.u_profile_pic}}
        backgroundColor="#0652dd"
        title={(
            <View style={{width:responsiveWidth(100),height:'auto',justifyContent:"center",
                    marginTop:responsiveHeight(-6)}}>
            <View style={{height: responsiveHeight(10),alignItems: 'center', flexDirection: 'row',justifyContent:"space-between"}}>
              <View style={{flexDirection:"row"}}>
                <Text style={{color:"#fff",fontSize:responsiveFontSize(3),marginLeft:responsiveWidth(3),fontWeight:"bold"}}>
                  Profile</Text>
              </View>
              {/* <View>
                <TouchableOpacity onPress={() => alert("open setting page")}>
                  <Icon name="settings" style={{color:"#fff",fontSize:responsiveFontSize(4),marginRight:responsiveWidth(3)}}/>
                </TouchableOpacity>
              </View> */}
            </View>
              <View style={{flexDirection:"row",alignItems:"center",justifyContent:"center"}}>
                <TouchableOpacity onPress={()=>{this.openImage()}} style={{flexDirection:"row"}}>
                  <View style={{height:responsiveWidth(20),width:responsiveWidth(20),borderColor:"#fff",
                              borderRadius:responsiveWidth(10),borderWidth:3,alignItems:"center",justifyContent:"center"}}>
                    <Image source={{uri: this.state.photoUrl}}
                          style={{height:responsiveWidth(18),width:responsiveWidth(18),borderRadius:responsiveWidth(10)}} />
                  </View>
                  <View style={{height:responsiveWidth(7),width:responsiveWidth(7),borderRadius:responsiveWidth(5),backgroundColor:"#fff",
                              marginLeft:responsiveWidth(-7),marginTop:responsiveWidth(14),alignItems:"center",justifyContent:"center"}}>
                    <Icon name="ios-add-circle-outline" style={{color:"#0652dd",fontSize:responsiveFontSize(3)}}/>
                  </View>
                </TouchableOpacity>
                <View style={{justifyContent:"center",marginLeft:responsiveWidth(5)}}>
                  <TouchableOpacity onPress={()=>{this.props.navigation.navigate("profileUpdate")}}>
                    <Text style={{color:'#fff',fontSize:responsiveFontSize(2.5),fontWeight:"bold",fontFamily:""}}>
                    {this.state.userData.u_first_name} {this.state.userData.u_last_name}
                    </Text>
                    <Text style={{color:'#fff',fontSize:responsiveFontSize(2),fontWeight:"bold"}}>
                      {this.state.userData.u_email}
                    </Text>
                  </TouchableOpacity>
                </View>
              </View>
              <View style={{justifyContent:"center",alignItems:"center"}}>
                <TouchableOpacity onPress={()=>{this.props.navigation.navigate("ratingReview")}}>
                  <View style={{marginLeft:responsiveWidth(20),
                      alignItems:"center",justifyContent:"center",borderRadius:responsiveWidth(3),flexDirection:"row"}}>
                    <Icon name="star" style={{color:"#ffbf00",fontSize:responsiveFontSize(4)}}/> 
                    <Text style={{color:"#fff",fontSize:responsiveFontSize(2.3),marginLeft:responsiveWidth(3)}}>
                      {this.state.userData.ratings}  
                    </Text>
                  </View>
                </TouchableOpacity>
              </View>
            </View>
          )}
        alwaysShowTitle={false}
       alwaysShowNavBar={false}
        renderNavBar={this.renderNavBar}
        renderContent={this.renderContent}
        // // scrollViewProps={{
        //   onScrollBeginDrag: () => this.setState({prof:this.state.prof,name:this.state.prof}),
        //  onScrollEndDrag: () =>{this.setState({prof:this.state.prof,name:this.state.name})}
        // }}
      />
      <Spinner animation="fade"
                visible={this.state.showLoader}
                textContent={'Loading...'}
                textStyle={{color:"#fff"}}
              />
    </View>
  );
}
}