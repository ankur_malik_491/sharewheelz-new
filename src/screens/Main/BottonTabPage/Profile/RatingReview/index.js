import React from 'react';
import {Text, View,TouchableOpacity,TouchableWithoutFeedback,Keyboard,ImageBackground,Image,AsyncStorage,Modal} from 'react-native';
import {Container,Icon,Input,Content,Toast,Card,CardItem,Picker,Tabs,Tab,TabHeading} from "native-base";
import {responsiveWidth,responsiveHeight,responsiveFontSize} from "react-native-responsive-dimensions";
import KeyboardSpacer from 'react-native-keyboard-spacer';
import Spinner from 'react-native-loading-spinner-overlay';
import Display from 'react-native-display';
import API from "../../../../../../allApi";
import Localstorage from "../../../../../../async";

export default class RatingReview extends React.Component {
  constructor(props){
    super(props);
    this.state = {
        userSentRating:[],
        userReceiveRrating:[],
        loading : false,
        showLoader:false,
        enable:false,
        enableClose:false,
        enableCard:false,
        enablecard2:false,


    };
    this.getUserRating();
  }
componentDidMount = () => {
   this.setState({enable:true})
    setTimeout(()=>{
      this.setState({enableCard:true}) 
      setTimeout(()=>{
        this.setState({enableClose:true}) 
    },500);
 },1500);
  } 
getUserRating = async () => {
    try{
        Localstorage.retrieveItem("UID").then((userId) =>{
            if(userId){
                API.getUserRating(userId,"sent").then((response)=>{
                    //if(response.status){
                        this.setState({userSentRating:response})
                    //   }
                    //   else{
                    //     Toast.show({
                    //       text:response.message,
                    //       textStyle:{textAlign:"center"},
                    //       duration:2000,
                    //       style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
                    //     });
                    //   }
                }).catch((error) =>{
                      console.log(error)
                  });
                API.getUserRating(userId,"receive").then((response)=>{
                     //console.log(response)
                    // if(response.status){
                        this.setState({userReceiveRrating:response})
                    // }
                    // else{
                    //     Toast.show({
                    //         text:response.message,
                    //         textStyle:{textAlign:"center"},
                    //         duration:2000,
                    //         style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
                    //     });
                    // }
   
                }).catch((error) =>{
                         console.log(error)
                    });
            }
            else{
              Toast.show({
                text:"Something Went Wrong!",
                textStyle:{textAlign:"center"},
                duration:2000,
                style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
              });
            }
        }).catch((error) =>{
                console.log(error)
            });
    }
    catch (error) {
        Toast.show({
            text:error,
            textStyle:{textAlign:"center"},
            duration:2000,
            style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
        });
    }
}
render(){
 return ( 
      <View style={{flex:1}} >
        <Container >
          <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <ImageBackground source={require("../../../../../assets/auth.png")} resizeMode="stretch" 
                            style={{width:responsiveWidth(100),height:responsiveHeight(100)}}>
              <View style={{marginTop:responsiveHeight(5),flexDirection:"row",justifyContent:"space-between"}}>
                <View style={{marginLeft:responsiveWidth(5)}}>
                  <Display enable={this.state.enable} 
                          enterDuration={2000} 
                          enter="slideInLeft">
                    <Text style={{color:'#fff',fontSize:responsiveFontSize(3.5),fontWeight:"bold",fontFamily:""}}>
                        Ratings & Reviews  </Text>
                  </Display>
                </View>
                <View style={{marginRight:responsiveWidth(2),alignItems:"center",justifyContent:"center"}}>
                  <Display enable={this.state.enableClose} 
                          enterDuration={2000} 
                          enter="fadeIn">
                    <TouchableOpacity onPress={()=> this.props.navigation.goBack()} >
                      <Icon name="ios-close" style={{fontSize:responsiveFontSize(5),marginRight:10,color:"#fff"}}/>
                    </TouchableOpacity>
                  </Display>
                </View>
              </View>
              <Content style={{flex:1}} >
                    <Display enable={this.state.enableCard} 
                          enterDuration={2000} 
                          enter="bounceInUp">
                    <View style={{justifyContent:"center",marginTop:responsiveHeight(4),backgroundColor:"#fff"}}>
                        <Tabs tabBarUnderlineStyle={{backgroundColor:"#0652dd"}}>
                            <Tab heading="Sent" tabStyle={{backgroundColor: '#ccc'}} 
                                textStyle={{color: '#000'}} activeTabStyle={{backgroundColor: '#fff'}} 
                                activeTextStyle={{color: '#0652dd',fontWeight:"bold",fontSize:responsiveFontSize(2.5)}}>
                                <View style={{backgroundColor:"#fff",height:responsiveHeight(70)}}>
                                    <View style={{alignItems:"center",justifyContent:"center",marginTop:responsiveHeight(5)}}>
                                        <Icon name="star" style={{color:"#333",fontSize:responsiveFontSize(5)}}/>
                                        <Text style={{fontSize:responsiveFontSize(3),color: '#000',textAlign:"center",marginTop:responsiveHeight(2)}}>
                                            You haven't left a rating yet
                                        </Text>
                                        <Text style={{fontSize:responsiveFontSize(3),color: '#000',textAlign:"center",marginTop:responsiveHeight(2)}}>
                                            when you leave rating for other members, you increase trust in community!
                                        </Text>
                                    </View>
                                </View>
                            </Tab>
                            <Tab heading="Receive" tabStyle={{backgroundColor: '#ccc'}} 
                                textStyle={{color: '#000'}} activeTabStyle={{backgroundColor: '#fff'}} 
                                activeTextStyle={{color: '#0652dd', fontWeight:"bold",fontSize:responsiveFontSize(2.5)}}>
                                <View style={{backgroundColor:"#fff",height:responsiveHeight(70)}}>
                                    <View style={{alignItems:"center",justifyContent:"center",marginTop:responsiveHeight(5)}}>
                                        <Icon name="star" style={{color:"#333",fontSize:responsiveFontSize(5)}}/>
                                        <Text style={{fontSize:responsiveFontSize(3),color: '#000',textAlign:"center",marginTop:responsiveHeight(2)}}>
                                            You haven't got a rating yet
                                        </Text>
                                        <Text style={{fontSize:responsiveFontSize(3),color: '#000',textAlign:"center",marginTop:responsiveHeight(2)}}>
                                            when you got rating from other members, you increase trust in community!
                                        </Text>
                                    </View>
                                </View>
                            </Tab>
                        </Tabs>
                    </View>
                  </Display>
              </Content>  
            <KeyboardSpacer/>
            </ImageBackground>
          </TouchableWithoutFeedback>
        </Container>
        <Spinner animation="fade"
                visible={this.state.showLoader}
                textContent={'Loading...'}
                textStyle={{color:"#fff"}}
              />
      </View>
 )}
}


