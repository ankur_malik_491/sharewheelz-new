import React from 'react';
import {Text, View,TouchableOpacity,AsyncStorage} from 'react-native';
import {Container,Toast, Content,Icon,Picker} from "native-base";
import {responsiveWidth,responsiveHeight,responsiveFontSize} from "react-native-responsive-dimensions";
import Display from 'react-native-display';
import Spinner from 'react-native-loading-spinner-overlay';
import ReactNativeParallaxHeader from 'react-native-parallax-header';
import UpdateProfileModal from "../../../../../components/Modals/UpdateProfileModal";
import API from "../../../../../../allApi";
import Localstorage from "../../../../../../async";
 
export default class ProfileUpdate extends React.Component {
    constructor(props){
      super(props);
      this.state={
        userData:[],
        loading:false,
        showLoader:false,
        showModal:false,
        enable:false,
        enableBase:false,
        enableImagView:false,
        enableImg:false,
        enableCard:false,
        pickvalFirstName:"",
        pickvalLastName:"",
        pickvalEmail:"",
        pickvalDL:"",
        pickvalPhone:"",
        pickvalGender:"",
        pickvalDob:"",
        userDobYearItems:[],
        userGenderItems:[{ label: 'Select your gender', value: '' },
                        { label: 'Male', value: 'Male' },
                        { label: 'Female', value: 'Female' },
                        { label: 'Other', value: 'Other' }
                      ],
      }
      this.getUserData();
      this.handleUserDobYear();
  } 
  componentDidMount = () => {
    this.setState({enable:true})
     setTimeout(()=>{
       this.setState({enableCard:true}) 
       setTimeout(()=>{
         this.setState({enableClose:true}) 
     },500);
   },1000);
   }
   handleUserDobYear = () => {
    this.state.userDobYearItems.push({label:"Select your D.O.B year",value:""}) 
    for(var i =2002;i>=1940;i--){
        var year = i.toString();
        this.state.userDobYearItems.push({label:year,value:year}) 
    }}
    
  setval = (pickup,show,value) => {
    if(value == "First Name"){this.setState({pickvalFirstName:pickup,showModal:!show})}
    else if(value == "Last Name"){this.setState({pickvalLastName:pickup,showModal:!show})} 
    else if(value == "Email"){this.setState({pickvalEmail:pickup,showModal:!show})} 
    else if(value == "Mobile Number"){this.setState({pickvalPhone:pickup,showModal:!show})}
    else{this.setState({pickvalDL:pickup,showModal:!show})}   
  }

  displaymodal =(title,value,iconname,btn) => {
    this.setState({showModal:true,modalTitle:title,modalValue:value,modalIcon:iconname,modalBtn:btn})

  }
  updateUserInfo = async ()=>{
    try{ 
        if( this.state.userData.u_first_name == this.state.pickvalFirstName && 
            this.state.userData.u_last_name == this.state.pickvalLastName &&
            this.state.userData.u_email == this.state.pickvalEmail &&
            this.state.userData.u_phone == this.state.pickvalPhone &&
            this.state.userData.u_birth_year == this.state.pickvalDob &&
            this.state.userData.u_gender == this.state.pickvalGender && 
            this.state.userData.u_bio == this.state.pickvalDL
            ){ 
              Toast.show({
                text:"You haven't change any detail yet",
                textStyle:{textAlign:"center"},
                duration:2000,
                style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"grey"}
              });
            }
        else{
            this.setState({loading:true,showLoader:true})
            Localstorage.retrieveItem("UID").then((userId) =>{
              if(userId){
                if(this.state.userData.u_email !== this.state.pickvalEmail){
                  API.updateUserEmail(userId,this.state.pickvalEmail)
                  .then((response)=>{
                   // console.log(response)
                  }).catch((error) =>{
                        console.log(error)
                    });
                }
                else if(this.state.userData.u_phone !== this.state.pickvalPhone){
                  API.updateUserPhone(userId,this.state.pickvalPhone)
                  .then((response)=>{
                    //console.log(response)
                  }).catch((error) =>{
                        console.log(error)
                    });
                }
                else{
                  API.updateUserDetails(userId,this.state.pickvalFirstName,this.state.pickvalLastName,this.state.pickvalDob,
                    this.state.pickvalGender,this.state.pickvalDL)
                  .then((response)=>{
                    //console.log(response)
                  }).catch((error) =>{
                        console.log(error)
                    });
                }
                this.setState({loading:false,showLoader:false})
                Toast.show({
                  text:"Details Updated Successfully",
                  textStyle:{textAlign:"center"},
                  duration:2000,
                  style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"green"}
                });
              }
              else{
                this.setState({loading:false,showLoader:false})
                Toast.show({
                  text:"Something Went Wrong!",
                  textStyle:{textAlign:"center"},
                  duration:2000,
                  style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
                });
              }
            }).catch((error) =>{
                  console.log(error)
              });
      }
    }
    catch (error) {
        this.setState({loading:false,showLoader:false})
        Toast.show({
            text:error,
            textStyle:{textAlign:"center"},
            duration:2000,
            style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
          });
    }
  }
  getUserData =async () =>{
    try{ 
      Localstorage.retrieveItem("UID").then((userId) =>{
        if(userId){
          API.getUserDetails(userId).then((response)=>{
            if(response.status){
              this.setState({userData:response.data})
              this.setState({pickvalFirstName:response.data.u_first_name})
              this.setState({pickvalLastName:response.data.u_last_name})
              this.setState({pickvalEmail:response.data.u_email})
              this.setState({pickvalPhone:response.data.u_phone})
              this.setState({pickvalGender:response.data.u_gender})
              this.setState({pickvalDob:response.data.u_birth_year})
              this.setState({pickvalDL:response.data.u_bio})
            }
            else{
              Toast.show({
                text:response.message,
                textStyle:{textAlign:"center"},
                duration:2000,
                style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
              });
            }
          }).catch((error) =>{
            console.log(error)
        });
        }
        else{
          alert("You are not logged in! please login first.")
          this.props.navigation.navigate('auth');
        }
      }).catch((error) =>{
            console.log(error)
        });
    }
    catch (error) {
        Toast.show({
            text:error,
            textStyle:{textAlign:"center"},
            duration:2000,
            style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
          });
    }
  }
  renderNavBar = () => (
    <View style={{height: responsiveHeight(10),alignItems: 'center', flexDirection: 'row',}}>
      <TouchableOpacity  onPress={() => {this.props.navigation.goBack()}}>
        <Icon name="md-arrow-back" style={{color:"#fff",fontSize:responsiveFontSize(4),marginLeft:responsiveWidth(3)}}/>
      </TouchableOpacity>
      <Text style={{color:"#fff",fontSize:responsiveFontSize(3),marginLeft:responsiveWidth(3)}}>Edit your profile</Text>
    </View>
)
renderContent = () => (
    <View style={{flex:1}}>
        {this.state.showModal && 
          <UpdateProfileModal title={this.state.modalTitle} value={this.state.modalValue} iconName={this.state.modalIcon} 
              btnValue={this.state.modalBtn} onAction = {(pick,show) => this.setval(pick,show,this.state.modalTitle)}/>} 
        <Content>
        <Display enable={this.state.enableCard} 
                          enterDuration={2000} 
                          enter="bounceInUp">
        <View style={{paddingHorizontal:responsiveWidth(3)}}>
            <TouchableOpacity onPress={()=>this.displaymodal("First Name",this.state.pickvalFirstName,"ios-person","Save")}>
              <View style={{flexDirection:'row',height:responsiveHeight(9),backgroundColor:'#fff',marginTop:responsiveHeight(2)}}>
                <View style={{flex:1,padding:responsiveWidth(1.2)}}>
                  <View style={{backgroundColor:'#006600',width:responsiveWidth(15),height:responsiveWidth(15),
                                borderRadius:responsiveWidth(10),justifyContent:'center',alignItems:'center'}}>
                    <Icon name="ios-person" style={{color:"#fff",fontSize:responsiveFontSize(4)}}></Icon>
                  </View>
                </View>
                <View style={{flex:4,justifyContent:'center',}}>
                  <Text style={{fontSize:responsiveFontSize(2),color:'#000',opacity:0.7}}>First Name </Text>
                  <Text style={{fontSize:responsiveFontSize(2.2),fontWeight:'bold',color:'#000',opacity:0.7}}>
                    {this.state.pickvalFirstName}
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
          </View>    
          <View style={{paddingHorizontal:responsiveWidth(3)}}>
            <TouchableOpacity onPress={()=>this.displaymodal("Last Name",this.state.pickvalLastName,"ios-person","Save")}>
              <View style={{flexDirection:'row',height:responsiveHeight(9),backgroundColor:'#fff',marginTop:responsiveHeight(2)}}>
                <View style={{flex:1,padding:responsiveWidth(1.2)}}>
                  <View style={{backgroundColor:'#cc6600',width:responsiveWidth(15),height:responsiveWidth(15),
                                borderRadius:responsiveWidth(10),justifyContent:'center',alignItems:'center'}}>
                    <Icon name="ios-person" style={{color:"#fff",fontSize:responsiveFontSize(4)}}></Icon>
                  </View>
                </View>
                <View style={{flex:4,justifyContent:'center',}}>
                  <Text style={{fontSize:responsiveFontSize(2),color:'#000',opacity:0.7}}>Last Name </Text>
                  <Text style={{fontSize:responsiveFontSize(2.2),fontWeight:'bold',color:'#000',opacity:0.7}}>
                    {this.state.pickvalLastName}
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
          </View>
          <View style={{paddingHorizontal:responsiveWidth(3)}}>
            <TouchableOpacity onPress={()=>this.displaymodal("Email",this.state.pickvalEmail,"ios-mail","Verify")}>
              <View style={{flexDirection:'row',height:responsiveHeight(9),backgroundColor:'#fff',marginTop:responsiveHeight(2)}}>
                <View style={{flex:1,padding:responsiveWidth(1.2)}}>
                  <View style={{backgroundColor:'#0099ff',width:responsiveWidth(15),height:responsiveWidth(15),
                                borderRadius:responsiveWidth(10),justifyContent:'center',alignItems:'center'}}>
                    <Icon name="ios-mail" style={{color:"#fff",fontSize:responsiveFontSize(4)}}></Icon>
                  </View>
                </View>
                <View style={{flex:4,justifyContent:'center',}}>
                  <Text style={{fontSize:responsiveFontSize(2),color:'#000',opacity:0.7}}>Email </Text>
                  <Text style={{fontSize:responsiveFontSize(2.2),fontWeight:'bold',color:'#000',opacity:0.7}}>
                    {this.state.pickvalEmail}
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
          </View>
          <View style={{paddingHorizontal:responsiveWidth(3)}}>
            <TouchableOpacity onPress={()=>this.displaymodal("Mobile Number",this.state.pickvalPhone,"ios-phone-portrait","Verify")}>
              <View style={{flexDirection:'row',height:responsiveHeight(9),backgroundColor:'#fff',marginTop:responsiveHeight(2)}}>
                <View style={{flex:1,padding:responsiveWidth(1.2)}}>
                  <View style={{backgroundColor:'#ff9900',width:responsiveWidth(15),height:responsiveWidth(15),
                                borderRadius:responsiveWidth(10),justifyContent:'center',alignItems:'center'}}>
                    <Icon name="ios-phone-portrait" style={{color:"#fff",fontSize:responsiveFontSize(4)}}></Icon>
                  </View>
                </View>
                <View style={{flex:4,justifyContent:'center',}}>
                  <Text style={{fontSize:responsiveFontSize(2),color:'#000',opacity:0.7}}>Mobile Number </Text>
                  <Text style={{fontSize:responsiveFontSize(2.2),fontWeight:'bold',color:'#000',opacity:0.7}}>
                    {this.state.pickvalPhone}
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
          </View>
          <View style={{paddingHorizontal:responsiveWidth(3)}}>
              <View style={{flexDirection:'row',height:responsiveHeight(9),backgroundColor:'#fff',marginTop:responsiveHeight(2)}}>
                <View style={{flex:1,padding:responsiveWidth(1.2)}}>
                  <View style={{backgroundColor:'#006699',width:responsiveWidth(15),height:responsiveWidth(15),
                                borderRadius:responsiveWidth(10),justifyContent:'center',alignItems:'center'}}>
                    <Icon name="ios-calendar" style={{color:"#fff",fontSize:responsiveFontSize(4)}}></Icon>
                  </View>
                </View>
                <View style={{flex:4,justifyContent:'center',}}>
                  <Text style={{fontSize:responsiveFontSize(2),color:'#000',opacity:0.7}}>D.O.B Year </Text>
                  <Picker
                          mode="dropdown"
                          style={{backgroundColor:"#fff",marginLeft:responsiveWidth(-2)}}
                          selectedValue={this.state.pickvalDob}
                          onValueChange={(value)=> this.setState({pickvalDob:value})}
                        >
                          {this.state.userDobYearItems.map((item, i) => (
                            <Picker.Item key={i} label={item.label} value={item.value} 
                            style={{fontSize:responsiveFontSize(2.2),fontWeight:'bold'}}/>
                          ))}
                        </Picker>
                </View>
              </View>
          </View>
          <View style={{paddingHorizontal:responsiveWidth(3)}}>
              <View style={{flexDirection:'row',height:responsiveHeight(9),backgroundColor:'#fff',marginTop:responsiveHeight(2)}}>
                <View style={{flex:1,padding:responsiveWidth(1.2)}}>
                  <View style={{backgroundColor:'#669900',width:responsiveWidth(15),height:responsiveWidth(15),
                                borderRadius:responsiveWidth(10),justifyContent:'center',alignItems:'center'}}>
                    <Icon name="ios-transgender" style={{color:"#fff",fontSize:responsiveFontSize(4)}}></Icon>
                  </View>
                </View>
                <View style={{flex:4,justifyContent:'center',}}>
                  <Text style={{fontSize:responsiveFontSize(2),color:'#000',opacity:0.7}}>Gender </Text>
                  <Picker
                          mode="dropdown"
                          style={{backgroundColor:"#fff",marginLeft:responsiveWidth(-2)}}
                          selectedValue={this.state.pickvalGender}
                          onValueChange={(value)=> this.setState({pickvalGender:value})}
                        >
                          {this.state.userGenderItems.map((item, i) => (
                            <Picker.Item key={i} label={item.label} value={item.value} 
                            style={{fontSize:responsiveFontSize(2.2),fontWeight:'bold'}}/>
                          ))}
                        </Picker>
                </View>
              </View>
          </View>
          <View style={{paddingHorizontal:responsiveWidth(3)}}>
            <TouchableOpacity onPress={()=>this.displaymodal("DL number",this.state.pickvalDL,"md-card","Save")}>
              <View style={{flexDirection:'row',height:responsiveHeight(9),backgroundColor:'#fff',marginTop:responsiveHeight(2)}}>
                <View style={{flex:1,padding:responsiveWidth(1.2)}}>
                  <View style={{backgroundColor:'#990000',width:responsiveWidth(15),height:responsiveWidth(15),
                                borderRadius:responsiveWidth(10),justifyContent:'center',alignItems:'center'}}>
                    <Icon name="md-card" style={{color:"#fff",fontSize:responsiveFontSize(4)}}></Icon>
                  </View>
                </View>
                <View style={{flex:4,justifyContent:'center',}}>
                  <Text style={{fontSize:responsiveFontSize(2),color:'#000',opacity:0.7}}>D.L Number </Text>
                  <Text style={{fontSize:responsiveFontSize(2.2),fontWeight:'bold',color:'#000',opacity:0.7}}>
                  {this.state.pickvalDL}
                  </Text>
                </View>
              </View>
            </TouchableOpacity>
          </View>
          <View style={{alignItems:"center",margin:responsiveHeight(5)}}>
            <TouchableOpacity onPress={()=> this.updateUserInfo()}
              style={{alignItems:"center",justifyContent:"center",width:responsiveWidth(50),height:responsiveHeight(7),
              backgroundColor:!this.state.loading?"#0652dd":"grey",borderRadius:responsiveWidth(3)}}>
              <Text style={{fontSize:responsiveFontSize(2),color: '#fff'}}>
                {this.state.loading?"Please Wait...":"Save Profile"}
              </Text>
            </TouchableOpacity>
          </View>
          </Display>
        </Content>
    </View>

        
        
)

render() {
  //console.log(this.state.userData)
return (
  <View style={{flex:1}}>
    <ReactNativeParallaxHeader
      headerMinHeight={responsiveHeight(10)}
      headerMaxHeight={responsiveHeight(25)}
      navbarColor="#0652dd"
      backgroundColor="#0652dd"
      title={(
        <View style={{width:responsiveWidth(100),height:'auto',flexDirection:"row",justifyContent:"space-between"}}>
            
            <Display enable={this.state.enable} 
                          enterDuration={2000} 
                          enter="zoomIn"
                          exit="slideInup">
                <Text style={{color:'#fff',fontSize:responsiveFontSize(4),fontWeight:"bold",marginLeft:responsiveWidth(5)}}>
                  Edit your profile
                </Text>
                </Display>
                <Display enable={this.state.enableClose} 
                          enterDuration={2000} 
                          enter="fadeIn"
                          exit="fadeOut">
                <TouchableOpacity onPress={()=> this.props.navigation.goBack()} >
                      <Icon name="ios-close" style={{fontSize:responsiveFontSize(5),marginRight:responsiveWidth(5),color:"#fff"}}/>
                    </TouchableOpacity>
                    </Display>
          </View>
        )}
       alwaysShowTitle={false}
       alwaysShowNavBar={false}
      renderNavBar={this.renderNavBar}
      renderContent={this.renderContent}
      // scrollViewProps={{
      //   onScrollBeginDrag: () => this.setState({enableClose:!this.state.enableClose}),
      //    onScrollEndDrag: () =>this.setState({enableClose:!this.state.enableClose})
      // }}
    />
    <Spinner animation="fade"
              visible={this.state.showLoader}
              textContent={'Loading...'}
              textStyle={{color:"#fff"}}
            />
  </View>
);
}
}
