import React from 'react';
import {Text, View,TouchableOpacity,AsyncStorage,TouchableWithoutFeedback,Keyboard,ImageBackground,Image,Modal} from 'react-native';
import {Container,Icon,Input, Content,Toast,Picker,Card,CardItem} from "native-base";
import {responsiveWidth,responsiveHeight,responsiveFontSize} from "react-native-responsive-dimensions";
import KeyboardSpacer from 'react-native-keyboard-spacer';
import Spinner from 'react-native-loading-spinner-overlay';
import Display from 'react-native-display';
import API from "../../../../../../allApi";
import Localstorage from "../../../../../../async";

export default class UpdatePassword extends React.Component {
  constructor(props){
    super(props);
    this.state = {
                    userPassword:"",
                    userConPassword:"",
                    userPasswordShow:"ios-eye",
                    userConPasswordShow:"ios-eye",
                    userPasswordVisibility:true,
                    userConPasswordVisibility:true,
                    showLoader:false,
                    loading:false,
                    enable:false,
                    enableCard:false,
                    enableClose:false,
                    modalVisible: true,

    };
  }
componentDidMount = () => {
    this.setState({enable:true})
        setTimeout(()=>{
        this.setState({enableCard:true}) 
        setTimeout(()=>{
            this.setState({enableClose:true}) 
        },500);
    },1500);
}

handleUserPasswordVisibility = () => {
    this.setState({
      userPasswordShow: this.state.userPasswordShow === 'ios-eye' ? 'ios-eye-off' : 'ios-eye',
      userPasswordVisibility: !this.state.userPasswordVisibility
    })
  }
  handleUserConfirmPasswordVisibility = () => {
    this.setState({
      userConPasswordShow: this.state.userConPasswordShow === 'ios-eye' ? 'ios-eye-off' : 'ios-eye',
      userConPasswordVisibility: !this.state.userConPasswordVisibility
    })
  }
  validateUserPassword = ()=>{
    var passPattern = /^(?=.*[a-z])(?=.*\d)(?=.*[!@#$%^&*_-])[A-Za-z\d!@#$%^&*_-]{8,}$/;
    if(passPattern.test(this.state.userPassword)){
        return true;
      }
    return false;
  }
changePassword = async () => {
if(this.state.userPassword == "" || this.state.userConPassword == ""){
    this.setState({uPassColor:"red",uConPassColor:"red"})
    Toast.show({
        text:"Please fill the required fields",
        textStyle:{textAlign:"center"},
        duration:2000,
        style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
      });
}
else if(!this.validateUserPassword()){
    this.setState({uPassColor:"red",uConPassColor:"#000"})
            Toast.show({
              text:"Password must include minimum eight characters, at least one letter, one number and one special character",
              textStyle:{textAlign:"center"},
              duration:2000,
              style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
            }); 
}
else if(this.state.userPassword !== this.state.userConPassword){
    this.setState({uPassColor:"red",uConPassColor:"red"})
    Toast.show({
        text:"Password must include be same",
        textStyle:{textAlign:"center"},
        duration:2000,
        style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
      }); 
}
else{
    try { 
        Localstorage.retrieveItem("UID").then((userId) =>{
          if(userId){
            this.setState({showLoader:true,loading:true})
            this.setState({uPassColor:"green",uConPassColor:"green",})
            API.updateUserPassword(userId,this.state.userPassword)
            .then((response)=>{
              //console.log(response)
              if(response.status){
                Toast.show({
                  text:"user Password Changed Successfully",
                  textStyle:{textAlign:"center"},
                  duration:2000,
                  style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"green"}
                });
                this.setState({showLoader:false,loading:false})
                this.props.navigation.goBack();
              }
              else {
              this.setState({uPassColor:"red",uConPassColor:"red",})
              Toast.show({
                  text:response.message,
                  textStyle:{textAlign:"center"},
                  duration:2000,
                  style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
                });
                this.setState({showLoader:false,loading:false})
              }
            }).catch((error) =>{
                  console.log(error)
              });
          }
          else{
            this.setState({loading:false,showLoader:false})
            Toast.show({
              text:"Something Went Wrong!",
              textStyle:{textAlign:"center"},
              duration:2000,
              style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
            });
          }
        }).catch((error) =>{
            console.log(error)
          });
    } 
    catch (error) {
      this.setState({uPassColor:"red",uConPassColor:"red"})
      this.setState({loading:false,showLoader:false})
      Toast.show({
        text:error,
        textStyle:{textAlign:"center"},
        duration:2000,
        style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
      });
    }
}
}
 render(){
 return ( 
      <View style={{flex:1}} >
        <Container >
          <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <ImageBackground source={require("../../../../../assets/auth.png")} resizeMode="stretch" 
                            style={{width:responsiveWidth(100),height:responsiveHeight(100)}}>
              <View style={{marginTop:responsiveHeight(5),flexDirection:"row",justifyContent:"space-between"}}>
                <View style={{marginLeft:responsiveWidth(5)}}>
                  <Display enable={this.state.enable} 
                          enterDuration={2000} 
                          enter="slideInLeft">
                    <Text style={{color:'#fff',fontSize:responsiveFontSize(3.5),fontWeight:"bold",fontFamily:""}}>
                    Change Password</Text>
                  </Display>
                </View>
                <View style={{marginRight:responsiveWidth(2),alignItems:"center",justifyContent:"center"}}>
                  <Display enable={this.state.enableClose} 
                          enterDuration={2000} 
                          enter="fadeIn">
                    <TouchableOpacity onPress={()=> this.props.navigation.goBack()} >
                      <Icon name="ios-close" style={{fontSize:responsiveFontSize(5),marginRight:10,color:"#fff"}}/>
                    </TouchableOpacity>
                  </Display>
                </View>
              </View>
              <Content >
                <View style={{justifyContent:"center",margin:responsiveHeight(3),marginTop:responsiveHeight(4)}}>
                  <Display enable={this.state.enableCard} 
                          enterDuration={2000} 
                          enter="bounceInUp">
                    <Card style={{backgroundColor:"#fff",borderRadius:responsiveWidth(5)}}>
                      <CardItem header 
                          style={{backgroundColor:"#fff",borderRadius:responsiveWidth(5),borderBottomLeftRadius:0,
                                  borderBottomRightRadius:0,}}>
                        <View>
                            <Text style={{fontSize:responsiveFontSize(2.3)}}>
                            Please enter the your new password to update.
                            </Text>
                            <View style={{margin:responsiveWidth(1),paddingHorizontal:responsiveWidth(3),
                                borderRadius: responsiveWidth(2), borderWidth:1,borderColor: this.state.uPassColor,
                                marginTop:responsiveHeight(4),flex:1}}>
                                <Text style={{position: 'absolute',top:responsiveHeight(-1.5),left: responsiveHeight(2),
                                            fontWeight: 'bold',backgroundColor:"#fff"}}>
                                Password
                                </Text>
                                <View style={{flex:1,flexDirection:"row",justifyContent:"center",alignItems:"center"}}>
                                    <Input value={this.state.userPassword}
                                            onChangeText={(userPassword)=>this.setState({userPassword})}
                                            secureTextEntry={this.state.userPasswordVisibility}
                                            placeholder="Enter your password"
                                            keyboardType="ascii-capable"
                                    />
                                <TouchableOpacity onPress={() => this.handleUserPasswordVisibility()}>
                                    <Icon name={this.state.userPasswordShow} style={{color:"#000",fontSize:responsiveFontSize(4)}}/>
                                </TouchableOpacity>
                                </View>
                            </View>
                        </View> 
                      </CardItem>
                      <CardItem header 
                          style={{backgroundColor:"#fff",borderRadius:responsiveWidth(5),borderBottomLeftRadius:0,
                                  borderBottomRightRadius:0,}}>
                            <View style={{margin:responsiveWidth(1),paddingHorizontal:responsiveWidth(3),borderRadius: responsiveWidth(2),
                      borderWidth:1,borderColor: this.state.uConPassColor,flex:1}}>
                        <Text style={{position: 'absolute',top:responsiveHeight(-1.5),left: responsiveHeight(2),
                                    fontWeight: 'bold',backgroundColor:"#fff"}}>
                          Confirm Password
                        </Text>
                        <View style={{flex:1,flexDirection:"row",justifyContent:"center",alignItems:"center"}}>
                            <Input value={this.state.userConPassword}
                                    onChangeText={(userConPassword)=>this.setState({userConPassword})}
                                    secureTextEntry={this.state.userConPasswordVisibility}
                                    placeholder="Enter your password"
                                    keyboardType="ascii-capable"
                              />
                          <TouchableOpacity onPress={() => this.handleUserConfirmPasswordVisibility()}>
                            <Icon name={this.state.userConPasswordShow} style={{color:"#000",fontSize:responsiveFontSize(4)}}/>
                          </TouchableOpacity>
                        </View>
                    </View>
                      </CardItem>
                      <CardItem footer bordered style={{borderRadius:responsiveWidth(5),alignItems:"center",justifyContent:"center"}}>
                        <TouchableOpacity onPress={()=>  this.changePassword()}
                            style={{alignItems:"center",justifyContent:"center",width:responsiveWidth(50),height:responsiveHeight(7),
                            backgroundColor:"#0652dd",borderRadius:responsiveWidth(3)}}>
                            <Text style={{fontSize:responsiveFontSize(2),color: '#fff'}}>
                            {this.state.loading?"Please Wait...":"Change"}
                            </Text>
                        </TouchableOpacity>
                      </CardItem>
                    </Card>
                  </Display>
                </View>
              </Content>  
            <KeyboardSpacer/>
            </ImageBackground>
          </TouchableWithoutFeedback>
        </Container>
        <Spinner animation="fade"
                visible={this.state.showLoader}
                textContent={'Loading...'}
                textStyle={{color:"#fff"}}
              />
      </View>
 )}
}



