import React from 'react';
import {Text, View,TouchableOpacity,TouchableWithoutFeedback,Keyboard,ImageBackground,Modal} from 'react-native';
import {Container,Icon,Input,Content,Toast,Card,CardItem,Thumbnail} from "native-base";
import {responsiveWidth,responsiveHeight,responsiveFontSize} from "react-native-responsive-dimensions";
import KeyboardSpacer from 'react-native-keyboard-spacer';
import Spinner from 'react-native-loading-spinner-overlay';
import Display from 'react-native-display';
import API from "../../../../../../allApi";
import Localstorage from "../../../../../../async";

export default class UserPreferences extends React.Component {
  constructor(props){
    super(props);
    this.state = {
        userPayment:[],
        userTransaction:[],
        userWallet:"",
        loading : false,
        showLoader:false,
        enable:false,
        enableClose:false,
        enableCard:false,


    };
    this.getWallet();
  }
componentDidMount = () => {
   this.setState({enable:true})
    setTimeout(()=>{
      this.setState({enableCard:true}) 
      setTimeout(()=>{
        this.setState({enableClose:true}) 
    },500);
 },1500);
  } 
getWallet = async () => {
    try{
        Localstorage.retrieveItem("UID").then((userId) =>{
            if(userId){
              API.getUserDetails(userId).then((response)=>{
                if(response.status){
                   //console.log(response)
                    this.setState({userPayment:response.data})
                    this.setState({userWallet:response.data.wallet_amount})
                }
                else{
                  Toast.show({
                    text:response.message,
                    textStyle:{textAlign:"center"},
                    duration:2000,
                    style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
                  });
                }
              }).catch((error) =>{
                console.log(error)
            });
            }
            else{
              alert("You are not logged in! please login first.")
              this.props.navigation.navigate('auth');
            }
            }).catch((error) =>{
                console.log(error)
        });
    }
    catch (error) {
        Toast.show({
            text:error,
            textStyle:{textAlign:"center"},
            duration:2000,
            style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
        });
    }
  }

render(){
 return ( 
      <View style={{flex:1}} >
        <Container >
          <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <ImageBackground source={require("../../../../../assets/auth.png")} resizeMode="stretch" 
                            style={{width:responsiveWidth(100),height:responsiveHeight(100)}}>
              <View style={{marginTop:responsiveHeight(5),flexDirection:"row",justifyContent:"space-between"}}>
                <View style={{marginLeft:responsiveWidth(5)}}>
                  <Display enable={this.state.enable} 
                          enterDuration={2000} 
                          enter="slideInLeft">
                    <Text style={{color:'#fff',fontSize:responsiveFontSize(3.5),fontWeight:"bold",fontFamily:""}}>
                        Payments</Text>
                  </Display>
                </View>
                <View style={{marginRight:responsiveWidth(2),alignItems:"center",justifyContent:"center"}}>
                  <Display enable={this.state.enableClose} 
                          enterDuration={2000} 
                          enter="fadeIn">
                    <TouchableOpacity onPress={()=> this.props.navigation.goBack()} >
                      <Icon name="ios-close" style={{fontSize:responsiveFontSize(5),marginRight:10,color:"#fff"}}/>
                    </TouchableOpacity>
                  </Display>
                </View>
              </View>
              <Content style={{flex:1}} >
                
                <Display enable={this.state.enableCard} 
                        enterDuration={2000} 
                        enter="bounceInUp">
                <View style={{justifyContent:"center",margin:responsiveHeight(3),marginTop:responsiveHeight(4)}}>
                    <Card style={{backgroundColor:"#fff",borderRadius:responsiveWidth(5),marginBottom:responsiveHeight(10)}}>
                        <CardItem header bordered 
                            style={{backgroundColor:"#fff",borderRadius:responsiveWidth(5),borderBottomLeftRadius:0,
                                    borderBottomRightRadius:0,alignItems:"center"}}>
                            <Text style={{fontWeight:"bold",fontSize:responsiveFontSize(2.5),color:"#0652dd"}}>
                                Choose your way of payment</Text>
                        </CardItem>
                        <CardItem header bordered
                          style={{backgroundColor:"#fff",alignItems:"center",justifyContent:"center",borderRadius:responsiveWidth(5)}}>
                            <View>
                                <Text style={{fontSize:responsiveFontSize(2.3)}}>
                                    Methods you can use to do your payments.
                                </Text>
                                <View style={{padding:responsiveWidth(3),borderWidth:1,marginTop:responsiveHeight(3),borderRadius:responsiveWidth(2),
                                    width:responsiveWidth(80),backgroundColor:"#e6e6e6"}}>
                                    <View style={{flexDirection:'row',height:responsiveHeight(6),flex:1}}>
                                        <Thumbnail square source={require("../../../../../assets/wallet.png")}
                                            style={{ height:responsiveWidth(10),width:responsiveWidth(10)}}/>
                                        <View style={{flex:5,justifyContent:'center',marginLeft:responsiveWidth(4)}}>
                                            <Text style={{fontSize:responsiveFontSize(2.5),color:'#000',fontWeight:"bold",}}>
                                                Wallet
                                            </Text>
                                            <Text style={{fontSize:responsiveFontSize(1.2),color:'#000',opacity:0.7,fontWeight:"bold",}}>
                                                ShareWheelz Wallet
                                            </Text>
                                        </View>
                                        <View style={{alignItems:"center",justifyContent:"center"}}>
                                            <Text style={{fontSize:responsiveFontSize(2.5),color:'#000',fontWeight:"bold",}}>
                                                ₹ {this.state.userWallet}
                                            </Text>
                                        </View>
                                    </View>
                                </View>
                                <View style={{padding:responsiveWidth(3),borderWidth:1,marginTop:responsiveHeight(3),borderRadius:responsiveWidth(2),
                                    width:responsiveWidth(80),backgroundColor:"#e6e6e6"}}>
                                    <View style={{flexDirection:'row',height:responsiveHeight(6),flex:1}}>
                                        <Thumbnail square source={require("../../../../../assets/cash.png")}
                                            style={{ height:responsiveWidth(10),width:responsiveWidth(10)}}/>
                                        <View style={{flex:5,justifyContent:'center',marginLeft:responsiveWidth(4)}}>
                                            <Text style={{fontSize:responsiveFontSize(2.5),color:'#000',fontWeight:"bold",}}>
                                                Cash
                                            </Text>
                                            <Text style={{fontSize:responsiveFontSize(1.2),color:'#000',opacity:0.7,fontWeight:"bold",}}>
                                                Default payment method
                                            </Text>
                                        </View>
                                        <View style={{flex:1,alignItems:"center",justifyContent:"center"}}>
                                            <Icon name="ios-checkmark" 
                                                style={{color:"green",fontSize:responsiveFontSize(6)}}/>
                                        </View>
                                    </View>
                                </View>
                            </View> 
                        </CardItem>
                        <CardItem header bordered 
                            style={{backgroundColor:"#fff",borderRadius:responsiveWidth(5),borderBottomLeftRadius:0,
                                    borderBottomRightRadius:0,alignItems:"center"}}>
                            <Text style={{fontWeight:"bold",fontSize:responsiveFontSize(2.5),color:"#0652dd"}}>
                                Transactions</Text>
                        </CardItem>
                        <CardItem header 
                          style={{backgroundColor:"#fff",alignItems:"center",justifyContent:"center",borderRadius:responsiveWidth(5)}}>
                            {(this.state.userTransaction.length > 0) &&
                                <View style={{alignItems:"center",justifyContent:"center",marginTop:responsiveHeight(2)}}>
                                    {/* <Icon name="bank-transfer" type="MaterialCommunityIcons" 
                                        style={{color:"#333",fontSize:responsiveFontSize(5)}}/>
                                    <Text style={{fontSize:responsiveFontSize(2.3),color: '#000',textAlign:"center",marginTop:responsiveHeight(2)}}>
                                        You haven't got any transaction yet.
                                    </Text> */}
                                </View>
                            }
                            {(this.state.userTransaction.length<=0) &&
                                <View style={{alignItems:"center",justifyContent:"center",marginTop:responsiveHeight(2)}}>
                                    <Icon name="bank-transfer" type="MaterialCommunityIcons" 
                                        style={{color:"#333",fontSize:responsiveFontSize(5)}}/>
                                    <Text style={{fontSize:responsiveFontSize(2.3),color: '#000',textAlign:"center",marginTop:responsiveHeight(2)}}>
                                        You haven't got any transaction yet.
                                    </Text>
                                </View>
                            }  
                        </CardItem>
                    </Card>
                </View>
                </Display>
            </Content>  
            <KeyboardSpacer/>
            </ImageBackground>
          </TouchableWithoutFeedback>
        </Container>
        <Spinner animation="fade"
                visible={this.state.showLoader}
                textContent={'Loading...'}
                textStyle={{color:"#fff"}}
              />
      </View>
 )}
}


