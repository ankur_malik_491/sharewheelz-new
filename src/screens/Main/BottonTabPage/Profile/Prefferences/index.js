import React from 'react';
import {Text, View,TouchableOpacity,TouchableWithoutFeedback,Keyboard,ImageBackground,Image,AsyncStorage,Modal} from 'react-native';
import {Container,Icon,Input,Content,Toast,Card,CardItem,Picker, Thumbnail} from "native-base";
import {responsiveWidth,responsiveHeight,responsiveFontSize} from "react-native-responsive-dimensions";
import KeyboardSpacer from 'react-native-keyboard-spacer';
import Spinner from 'react-native-loading-spinner-overlay';
import Display from 'react-native-display';
import API from "../../../../../../allApi";
import Localstorage from "../../../../../../async";

export default class UserPreferences extends React.Component {
  constructor(props){
    super(props);
    this.state = {
        userPref:[],
        user_chattiness:"",
        user_smoking:"",
        user_music:"",
        user_pets:"",
        chattinessColor:"",
        smokingColor:"",
        musicColor:"",
        petsColor:"",
        userChattiness:[{label:"I'm the Quite type",value:"I'm the quite type"},
                        {label:"I talk depending on my mood",value:"I talk depending on my mood"},
                        {label:"I love to chat",value:"I love to chat"}],
        userSmoking:[{label:"No smoking in the car please",value:"No smoking in the car please"},
                    {label:"Don't know",value:"Don't know"},
                    {label:"Cigarette smoke doesn't bother me",value:"Cigarette smoke doesn't bother me"}],
        userMusic:[ {label:"Silence is golden",value:"Silence is golden"},
                    {label :"Don't know",value:"Don't know"},
                    {label:"It's all about the playlist",value:"It's all about the playlist"}],
        userPets:[{label:"No pets please",value:"No pets please"},
                {label:"Don't know",value:"Don't know"},
                {label:"Pets welcome Woof!",value:"Pets welcome Woof!"}],
        showModal:false,
        loading : false,
        showLoader:false,
        enable:false,
        enableClose:false,
        enableCard:false,
        enablecard2:false,


    };
    this.getPref();
  }
componentDidMount = () => {
   this.setState({enable:true})
    setTimeout(()=>{
      this.setState({enableCard:true}) 
      setTimeout(()=>{
        this.setState({enableClose:true}) 
    },500);
 },1500);
  } 
getPref = async () => {
    try{
        Localstorage.retrieveItem("UID").then((userId) =>{
            if(userId){
              API.getUserDetails(userId).then((response)=>{
                if(response.status){
                   // console.log(response)
                    this.setState({userPref:response.data})
                    this.setState({user_chattiness:response.data.user_chattiness,user_smoking:response.data.user_smoking,
                                user_music:response.data.user_music,user_pets:response.data.user_pets})
                }
                else{
                  Toast.show({
                    text:response.message,
                    textStyle:{textAlign:"center"},
                    duration:2000,
                    style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
                  });
                }
              }).catch((error) =>{
                console.log(error)
            });
            }
            else{
              alert("You are not logged in! please login first.")
              this.props.navigation.navigate('auth');
            }
            }).catch((error) =>{
                console.log(error)
        });
    }
    catch (error) {
        Toast.show({
            text:error,
            textStyle:{textAlign:"center"},
            duration:2000,
            style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
        });
    }
  }
updatePref = async () => {
    try{ //console.log(this.state.user_smoking)
        Localstorage.retrieveItem("UID").then((userId) =>{
            if(userId){
                this.setState({chattinessColor:"green",smokingColor:"green",musicColor:"green",petsColor:"green"})
                this.setState({showLoader:true,loading:true})
                API.updateUserPref(userId,this.state.user_chattiness,this.state.user_smoking,
                                this.state.user_music,this.state.user_pets)
                .then((response)=>{
                   // console.log(response)
                    if(response.status){
                        this.setState({loading:false,showLoader:false,enableCard:true,enablecard2:false})
                        Toast.show({
                            text:"Preferences Updated Successfully",
                            textStyle:{textAlign:"center"},
                            duration:2000,
                            style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"green"}
                        });
                        this.getPref();
                        this.setState({chattinessColor:"#000",smokingColor:"#000",musicColor:"#000",petsColor:"#000"})
                    }
                    else{
                        this.setState({loading:false,showLoader:false})
                        Toast.show({
                        text:response.message,
                        textStyle:{textAlign:"center"},
                        duration:2000,
                        style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
                        });
                    }
                }).catch((error) =>{
                    console.log(error)
                });
            }
            else{
              alert("You are not logged in! please login first.")
              this.props.navigation.navigate('auth');
            }
            }).catch((error) =>{
                console.log(error)
        });
    }
    catch (error) {
        Toast.show({
            text:error,
            textStyle:{textAlign:"center"},
            duration:2000,
            style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
        });
    }
}
cancel = () =>{
    this.setState({enableCard:true,enablecard2:false})
    this.setState({chattinessColor:"#000",smokingColor:"#000",musicColor:"#000",petsColor:"#000"})
}
render(){
 return ( 
      <View style={{flex:1}} >
        <Container >
          <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <ImageBackground source={require("../../../../../assets/auth.png")} resizeMode="stretch" 
                            style={{width:responsiveWidth(100),height:responsiveHeight(100)}}>
              <View style={{marginTop:responsiveHeight(5),flexDirection:"row",justifyContent:"space-between"}}>
                <View style={{marginLeft:responsiveWidth(5)}}>
                  <Display enable={this.state.enable} 
                          enterDuration={2000} 
                          enter="slideInLeft">
                    <Text style={{color:'#fff',fontSize:responsiveFontSize(3.5),fontWeight:"bold",fontFamily:""}}>
                        Prefferences</Text>
                  </Display>
                </View>
                <View style={{marginRight:responsiveWidth(2),alignItems:"center",justifyContent:"center"}}>
                  <Display enable={this.state.enableClose} 
                          enterDuration={2000} 
                          enter="fadeIn">
                    <TouchableOpacity onPress={()=> this.props.navigation.goBack()} >
                      <Icon name="ios-close" style={{fontSize:responsiveFontSize(5),marginRight:10,color:"#fff"}}/>
                    </TouchableOpacity>
                  </Display>
                </View>
              </View>
              <Content style={{flex:1}} >
                
                  <Display enable={this.state.enableCard} 
                          enterDuration={2000} 
                          enter="bounceInUp">
                <View style={{justifyContent:"center",margin:responsiveHeight(3),marginTop:responsiveHeight(4)}}>
                    <Card style={{backgroundColor:"#fff",borderRadius:responsiveWidth(5),marginBottom:responsiveHeight(10)}}>
                      <CardItem header bordered 
                          style={{backgroundColor:"#fff",borderRadius:responsiveWidth(5),borderBottomLeftRadius:0,
                                  borderBottomRightRadius:0,alignItems:"center"}}>
                        <Text style={{fontWeight:"bold",fontSize:responsiveFontSize(2.5),color:"#0652dd"}}>
                            What you like when travelling</Text>
                      </CardItem>
                      <CardItem header bordered 
                          style={{backgroundColor:"#fff",alignItems:"center",justifyContent:"center"}}>
                        <View style={{justifyContent:"center",width:responsiveWidth(70)}}>
                            <Text style={{fontSize:responsiveFontSize(2.3)}}>
                                Update your prefferences that tell about you.
                            </Text>
                            <View style={{flexDirection:'row',height:"auto",backgroundColor:'#fff',marginTop:responsiveHeight(2),
                                        alignItems:"center"}}>
                                {(this.state.user_chattiness == this.state.userChattiness[0].value) &&
                                    <Icon name="chat" type="MaterialCommunityIcons"  
                                        style={{color:"#008ae6",fontSize:responsiveFontSize(5)}}/>
                                }
                                {(this.state.user_chattiness == this.state.userChattiness[1].value) &&
                                    <Icon name="chat-processing" type="MaterialCommunityIcons"  
                                        style={{color:"#008ae6",fontSize:responsiveFontSize(5)}}/>
                                }
                                {(this.state.user_chattiness == this.state.userChattiness[2].value) &&
                                    <Icon name="wechat" type="MaterialCommunityIcons"  
                                        style={{color:"#008ae6",fontSize:responsiveFontSize(4.5)}}/>
                                }
                                <View style={{justifyContent:'center',marginLeft:responsiveWidth(3)}}>
                                    <Text style={{fontSize:responsiveFontSize(2),color:'#000',opacity:0.7}}>
                                        Chattiness </Text>
                                    <Text style={{fontSize:responsiveFontSize(2.2),fontWeight:'bold',color:'#000',opacity:0.7}}>
                                        {this.state.userPref.user_chattiness}
                                    </Text>
                                </View>
                            </View>
                            <View style={{flexDirection:'row',height:"auto",backgroundColor:'#fff',marginTop:responsiveHeight(2),
                                        alignItems:"center"}}>
                                {(this.state.user_smoking == this.state.userSmoking[0].value ||
                                    this.state.user_smoking == this.state.userSmoking[1].value) &&
                                    <Icon name="logo-no-smoking"  style={{color:"red",fontSize:responsiveFontSize(5)}}/>
                                }
                                {(this.state.user_smoking == this.state.userSmoking[2].value) &&
                                    <Icon name="smoking" type="MaterialCommunityIcons"  
                                        style={{color:"red",fontSize:responsiveFontSize(5)}}/>
                                }
                                <View style={{justifyContent:'center',marginLeft:responsiveWidth(3)}}>
                                    <Text style={{fontSize:responsiveFontSize(2),color:'#000',opacity:0.7}}>
                                        Smoking </Text>
                                    <Text style={{fontSize:responsiveFontSize(2.2),fontWeight:'bold',color:'#000',opacity:0.7,
                                                textAlign:"left"}}>
                                        {this.state.userPref.user_smoking}
                                    </Text>
                                </View>
                            </View>
                            <View style={{flexDirection:'row',height:"auto",backgroundColor:'#fff',marginTop:responsiveHeight(2),
                            alignItems:"center",}}>
                                {(this.state.user_music == this.state.userMusic[0].value ||
                                    this.state.user_music == this.state.userMusic[1].value) &&
                                    <Icon name="music-off" type="MaterialCommunityIcons"  
                                        style={{color:"#006600",fontSize:responsiveFontSize(5)}}/>
                                }
                                {(this.state.user_music == this.state.userMusic[2].value) &&
                                    <Icon name="music" type="MaterialCommunityIcons"  
                                        style={{color:"#006600",fontSize:responsiveFontSize(5)}}/>
                                }
                                <View style={{justifyContent:'center',marginLeft:responsiveWidth(3)}}>
                                    <Text style={{fontSize:responsiveFontSize(2),color:'#000',opacity:0.7}}>
                                        Music </Text>
                                    <Text style={{fontSize:responsiveFontSize(2.2),fontWeight:'bold',color:'#000',opacity:0.7}}>
                                        {this.state.userPref.user_music}
                                    </Text>
                                </View>
                            </View>
                            <View style={{flexDirection:'row',height:"auto",backgroundColor:'#fff',marginTop:responsiveHeight(2),
                                        alignItems:"center",}}>
                                 {(this.state.user_pets == this.state.userPets[0].value ||
                                    this.state.user_pets == this.state.userPets[1].value) &&
                                    <Icon name="paw-off" type="MaterialCommunityIcons"  
                                        style={{color:"#cc3300",fontSize:responsiveFontSize(4.5)}}/>
                                }
                                {(this.state.user_pets == this.state.userPets[2].value) &&
                                    <Icon name="paw" type="MaterialCommunityIcons"  
                                        style={{color:"#cc3300",fontSize:responsiveFontSize(4.5)}}/>
                                }
                                <View style={{justifyContent:'center',marginLeft:responsiveWidth(3)}}>
                                    <Text style={{fontSize:responsiveFontSize(2),color:'#000',opacity:0.7}}>
                                        Pets </Text>
                                    <Text style={{fontSize:responsiveFontSize(2.2),fontWeight:'bold',color:'#000',opacity:0.7}}>
                                        {this.state.userPref.user_pets}
                                    </Text>
                                </View>
                            </View>
                        </View> 
                      </CardItem>
                      <CardItem footer bordered 
                        style={{borderRadius:responsiveWidth(5),alignItems:"center",justifyContent:"center",}}>
                        <TouchableOpacity onPress={()=>this.setState({enablecard2:true,enableCard:false})}
                          style={{alignItems:"center",justifyContent:"center",width:responsiveWidth(60),height:responsiveHeight(7),
                          backgroundColor:"#0652dd",borderRadius:responsiveWidth(3)}}>
                          <Text style={{fontSize:responsiveFontSize(2),color: '#fff'}}>
                              Change
                          </Text>
                        </TouchableOpacity>
                      </CardItem>
                    </Card>
                    </View>
                  </Display>
                
                  <Display enable={this.state.enablecard2} 
                          enterDuration={2000} 
                          enter="bounceInUp">
                <View style={{justifyContent:"center",margin:responsiveHeight(3),marginTop:responsiveHeight(4),flex:1}}>
                    <Card style={{backgroundColor:"#fff",borderRadius:responsiveWidth(5),marginBottom:responsiveHeight(10)}}>
                      <CardItem header bordered 
                          style={{backgroundColor:"#fff",borderRadius:responsiveWidth(5),borderBottomLeftRadius:0,
                                  borderBottomRightRadius:0}}>
                        <Text style={{fontWeight:"bold",fontSize:responsiveFontSize(2.5),color:"#0652dd"}}>
                            Update Preferences</Text>
                      </CardItem>
                      <CardItem bordered>
                        <View >
                          <Text style={{fontSize:responsiveFontSize(2.3)}}>
                            Add your preferences for better pools.
                          </Text>
                          <View style={{paddingHorizontal:responsiveWidth(3),borderRadius: responsiveWidth(2),borderWidth:1,
                                    borderColor: this.state.chattinessColor,marginBottom:responsiveHeight(4),marginTop:responsiveHeight(4)}}>
                            <Text style={{position: 'absolute',top:responsiveHeight(-1.5),left: responsiveHeight(2),
                                        fontWeight: 'bold',backgroundColor:"#fff"}}>
                                Chattiness
                            </Text>
                            <View style={{flex:1,flexDirection:"row",justifyContent:"center",alignItems:"center"}}>
                                <Picker
                                    mode="dropdown"
                                    selectedValue={this.state.user_chattiness}
                                    onValueChange={(value)=> this.setState({user_chattiness:value})}
                                    style={{backgroundColor:"#fff",marginTop:responsiveHeight(1)}}
                                    >
                                    {this.state.userChattiness.map((item, i) => (
                                        <Picker.Item key={i} label={item.label} value={item.value} 
                                        style={{fontSize:responsiveFontSize(2.2),fontWeight:'bold',marginTop:responsiveWidth(2)}}/>
                                    ))}
                                </Picker>
                                {(this.state.user_chattiness == this.state.userChattiness[0].value) &&
                                    <Icon name="chat" type="MaterialCommunityIcons"  
                                        style={{color:"#000",fontSize:responsiveFontSize(4)}}/>
                                }
                                {(this.state.user_chattiness == this.state.userChattiness[1].value) &&
                                    <Icon name="chat-processing" type="MaterialCommunityIcons"  
                                        style={{color:"#000",fontSize:responsiveFontSize(4)}}/>
                                }
                                {(this.state.user_chattiness == this.state.userChattiness[2].value) &&
                                    <Icon name="wechat" type="MaterialCommunityIcons"  
                                        style={{color:"#000",fontSize:responsiveFontSize(4)}}/>
                                }
                            </View>
                          </View> 
                          <View style={{paddingHorizontal:responsiveWidth(3),borderRadius: responsiveWidth(2),borderWidth:1,
                                    borderColor: this.state.smokingColor,marginBottom:responsiveHeight(4)}}>
                            <Text style={{position: 'absolute',top:responsiveHeight(-1.5),left: responsiveHeight(2),
                                        fontWeight: 'bold',backgroundColor:"#fff"}}>
                                Smoking
                            </Text>
                            <View style={{flex:1,flexDirection:"row",justifyContent:"center",alignItems:"center"}}>
                                <Picker
                                    mode="dropdown"
                                    selectedValue={this.state.user_smoking}
                                    onValueChange={(value)=> this.setState({user_smoking:value})}
                                    style={{backgroundColor:"#fff",marginTop:responsiveHeight(1.5)}}
                                    >
                                    {this.state.userSmoking.map((item, i) => (
                                        <Picker.Item key={i} label={item.label} value={item.value} 
                                        style={{fontSize:responsiveFontSize(2.2),fontWeight:'bold',marginTop:responsiveWidth(2)}}/>
                                    ))}
                                </Picker>
                                {(this.state.user_smoking == this.state.userSmoking[0].value ||
                                    this.state.user_smoking == this.state.userSmoking[1].value) &&
                                    <Icon name="logo-no-smoking"  style={{color:"#000",fontSize:responsiveFontSize(4)}}/>
                                }
                                {(this.state.user_smoking == this.state.userSmoking[2].value) &&
                                    <Icon name="smoking" type="MaterialCommunityIcons"  
                                        style={{color:"#000",fontSize:responsiveFontSize(4)}}/>
                                }
                            </View>
                          </View>
                          <View style={{paddingHorizontal:responsiveWidth(3),borderRadius: responsiveWidth(2),borderWidth:1,
                                    borderColor: this.state.musicColor,marginBottom:responsiveHeight(4)}}>
                            <Text style={{position: 'absolute',top:responsiveHeight(-1.5),left: responsiveHeight(2),
                                        fontWeight: 'bold',backgroundColor:"#fff"}}>
                                Music
                            </Text>
                            <View style={{flex:1,flexDirection:"row",justifyContent:"center",alignItems:"center"}}>
                                <Picker
                                    mode="dropdown"
                                    selectedValue={this.state.user_music}
                                    onValueChange={(value)=> this.setState({user_music:value})}
                                    style={{backgroundColor:"#fff",marginTop:responsiveHeight(1)}}
                                    >
                                    {this.state.userMusic.map((item, i) => (
                                        <Picker.Item key={i} label={item.label} value={item.value} 
                                        style={{fontSize:responsiveFontSize(2.2),fontWeight:'bold',marginTop:responsiveWidth(2)}}/>
                                    ))}
                                </Picker>
                                {(this.state.user_music == this.state.userMusic[0].value ||
                                    this.state.user_music == this.state.userMusic[1].value) &&
                                    <Icon name="music-off" type="MaterialCommunityIcons"  
                                        style={{color:"#000",fontSize:responsiveFontSize(4)}}/>
                                }
                                {(this.state.user_music == this.state.userMusic[2].value) &&
                                    <Icon name="music" type="MaterialCommunityIcons"  
                                        style={{color:"#000",fontSize:responsiveFontSize(4)}}/>
                                }
                            </View>
                          </View> 
                          <View style={{paddingHorizontal:responsiveWidth(3),borderRadius: responsiveWidth(2),borderWidth:1,
                                    borderColor: this.state.petsColor,marginBottom:responsiveHeight(4)}}>
                            <Text style={{position: 'absolute',top:responsiveHeight(-1.5),left: responsiveHeight(2),
                                        fontWeight: 'bold',backgroundColor:"#fff"}}>
                                Pets
                            </Text>
                            <View style={{flex:1,flexDirection:"row",justifyContent:"center",alignItems:"center"}}>
                                <Picker
                                    mode="dropdown"
                                    selectedValue={this.state.user_pets}
                                    onValueChange={(value)=> this.setState({user_pets:value})}
                                    style={{backgroundColor:"#fff",marginTop:responsiveHeight(1)}}
                                    >
                                    {this.state.userPets.map((item, i) => (
                                        <Picker.Item key={i} label={item.label} value={item.value} 
                                        style={{fontSize:responsiveFontSize(2.2),fontWeight:'bold',marginTop:responsiveWidth(2)}}/>
                                    ))}
                                </Picker>
                                {(this.state.user_pets == this.state.userPets[0].value ||
                                    this.state.user_pets == this.state.userPets[1].value) &&
                                    <Icon name="paw-off" type="MaterialCommunityIcons"  
                                        style={{color:"#000",fontSize:responsiveFontSize(4)}}/>
                                }
                                {(this.state.user_pets == this.state.userPets[2].value) &&
                                    <Icon name="paw" type="MaterialCommunityIcons"  
                                        style={{color:"#000",fontSize:responsiveFontSize(4)}}/>
                                }
                            </View>
                          </View> 
                          
                          
                        </View> 
                      </CardItem>
                      <CardItem footer bordered 
                        style={{borderRadius:responsiveWidth(5),alignItems:"center",justifyContent:"space-between",}}>
                        <TouchableOpacity onPress={()=>   this.updatePref()}
                          style={{alignItems:"center",justifyContent:"center",width:responsiveWidth(30),height:responsiveHeight(7),
                          backgroundColor:"#0652dd",borderRadius:responsiveWidth(3)}}>
                          <Text style={{fontSize:responsiveFontSize(2),color: '#fff'}}>
                              Update
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={()=>   this.cancel()}
                          style={{alignItems:"center",justifyContent:"center",width:responsiveWidth(30),height:responsiveHeight(7),
                          backgroundColor:"#0652dd",borderRadius:responsiveWidth(3)}}>
                          <Text style={{fontSize:responsiveFontSize(2),color: '#fff'}}>
                              Cancel
                          </Text>
                        </TouchableOpacity>
                      </CardItem>
                    </Card>
                    </View>
                  </Display>
              </Content>  
            <KeyboardSpacer/>
            </ImageBackground>
          </TouchableWithoutFeedback>
        </Container>
        <Spinner animation="fade"
                visible={this.state.showLoader}
                textContent={'Loading...'}
                textStyle={{color:"#fff"}}
              />
      </View>
 )}
}


