import {createStackNavigator} from "react-navigation-stack";
import {createAppContainer,createSwitchNavigator} from "react-navigation";
import ProfileMAin from "./ProfileMain";
import ProfileUpdate from "./UpdateProfile";
import UpdatePassword from './UpdatePassword';
import UserCars from "./UserCars";
import RatingReview from "./RatingReview";
import UserPreferences from "./Prefferences";
import UserPayments from "./Payments";
import UpdateUserPhoto from "./UpdatePhoto";
import Refer from "../../DrawerPage/ReferEarn";
import About from "../../DrawerPage/About";

const profileRoutes = createStackNavigator({
    profileMain : ProfileMAin,
    profileUpdate : ProfileUpdate,
    passwordUpdate : UpdatePassword,
    userCars : UserCars,
    ratingReview : RatingReview,
    userPreferences : UserPreferences,
    userPayments: UserPayments,
    updateUserPhoto : UpdateUserPhoto,
    refer:Refer,
    about:About
    
   
},{
    initialRouteName:'profileMain',
    headerMode:null
});

export default createAppContainer(profileRoutes);