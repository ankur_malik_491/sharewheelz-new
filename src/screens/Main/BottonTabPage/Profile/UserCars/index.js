import React from 'react';
import {Text, View,TouchableOpacity,TouchableWithoutFeedback,Keyboard,ImageBackground,Image,AsyncStorage,Modal} from 'react-native';
import {Container,Icon,Input,Content,Toast,Card,CardItem,Picker} from "native-base";
import {responsiveWidth,responsiveHeight,responsiveFontSize} from "react-native-responsive-dimensions";
import KeyboardSpacer from 'react-native-keyboard-spacer';
import Spinner from 'react-native-loading-spinner-overlay';
import Display from 'react-native-display';
import API from "../../../../../../allApi";
import Localstorage from "../../../../../../async";

export default class UserCars extends React.Component {
  constructor(props){
    super(props);
    this.state = {
        userCar:[],
        carRegNumber:"",
        carRegYear:"",
        carBrand:"",
        carModal:"",
        carBodyType:"",
        carBodyColor:"",
        carBrandData:[],
        carModalData:[],
        carBodyTypeData:[],
        carBodyColorData:[],
        showModal:false,
        carId:"",
        loading : false,
        showLoader:false,
        enable:false,
        enableClose:false,
        enableCard:false,
        enablecard2:false,


    };
    this.getUserCar();
  }
componentDidMount = () => {
   this.setState({enable:true})
    setTimeout(()=>{
      this.setState({enableCard:true}) 
      setTimeout(()=>{
        this.setState({enableClose:true}) 
    },500);
 },1500);
  } 
getUserCar = async () => {
    try{
        Localstorage.retrieveItem("UID").then((userId) =>{
            if(userId){
              API.getUserCars(userId).then((response)=>{
                if(response.status){
                    this.setState({userCar:response.data})
                }
                else{
                    Toast.show({
                        text:response.message,
                        textStyle:{textAlign:"center"},
                        duration:2000,
                        style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
                    });
                }
              }).catch((error) =>{
                console.log(error)
            });
            }
            else{
              alert("You are not logged in! please login first.")
              this.props.navigation.navigate('auth');
            }
          }).catch((error) =>{
                console.log(error)
            });
        }
        catch (error) {
            Toast.show({
                text:error,
                textStyle:{textAlign:"center"},
                duration:2000,
                style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
            });
        }
  }
AddNewCar = async () =>{
    try{
        this.setState({enableCard:false,enablecard2:true})
        API.getCarsBrands().then((response)=>{
            if(response.status){
                this.setState({carBrandData:response.data})
            }
            else{
                Toast.show({
                    text:response.message,
                    textStyle:{textAlign:"center"},
                    duration:2000,
                    style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
                });
            }
          }).catch((error) =>{
            console.log(error)
        });
        API.getCarsBody().then((response)=>{
            if(response.status){
                this.setState({carBodyTypeData:response.data})
            }
            else{
                Toast.show({
                    text:response.message,
                    textStyle:{textAlign:"center"},
                    duration:2000,
                    style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
                });
            }
          }).catch((error) =>{
            console.log(error)
        });
        API.getCarscolor().then((response)=>{
            if(response.status){
                this.setState({carBodyColorData:response.data})
            }
            else{
                Toast.show({
                    text:response.message,
                    textStyle:{textAlign:"center"},
                    duration:2000,
                    style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
                });
            }
          }).catch((error) =>{
            console.log(error)
        });     
            
    }
    catch (error) {
        Toast.show({
            text:error,
            textStyle:{textAlign:"center"},
            duration:2000,
            style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
        });
    }
  }
getCarModal = async (val) =>{
    try{
        this.setState({carBrand:val})
        var carbrand =val;
        API.getCarsModal(carbrand).then((response)=>{
            if(response.status){
                this.setState({carModalData:response.data})
            }
            else{
                Toast.show({
                    text:response.message,
                    textStyle:{textAlign:"center"},
                    duration:2000,
                    style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
                });
            }
          }).catch((error) =>{
            console.log(error)
        });
    }
    catch (error) {
        Toast.show({
            text:error,
            textStyle:{textAlign:"center"},
            duration:2000,
            style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
        });
    }  
}
AddCar = async () => {
    try{ 
        Localstorage.retrieveItem("UID").then((userId) =>{
            if(userId){
                if(this.state.carRegNumber == "" || this.state.carRegYear == "" || this.state.carBrand == "" || this.state.carModal == "" ||
                    this.state.carBodyType == "" || this.state.carBodyColor == "")
                {
                    this.setState({carRegNumberColor:"red",carRegYearColor:"red",carBrandColor:"red",carModalColor:"red",
                            carBodyTypeColor:"red",carColor:"red"})
                    Toast.show({
                        text:"Please fill the required fields",
                        textStyle:{textAlign:"center"},
                        duration:2000,
                        style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
                    });
                }
                else 
                {
                    this.setState({carRegNumberColor:"green",carRegYearColor:"green",carBrandColor:"green",carModalColor:"green",
                                carBodyTypeColor:"green",carColor:"green"})
                    this.setState({showLoader:true,loading:true})
                    API.addUserCars(userId,this.state.carBrand,this.state.carModal,this.state.carBodyType,
                                    this.state.carBodyColor,this.state.carRegYear,this.state.carRegNumber)
                    .then((response)=>{
                        if(response.status){
                            this.setState({loading:false,showLoader:false,enableCard:true,enablecard2:false})
                            Toast.show({
                                text:"Car Added Successfully",
                                textStyle:{textAlign:"center"},
                                duration:2000,
                                style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"green"}
                            });
                            this.getUserCar();
                            this.setState({carRegNumberColor:"#000",carRegYearColor:"#000",carBrandColor:"#000",carModalColor:"#000",
                                carBodyTypeColor:"#000",carColor:"#000"})
                            this.setState({carRegNumber :"" ,carRegYear :"",carBrand :"",carModal :"",carBodyType :"",carBodyColor:""})
                        
                        }
                        else{
                            this.setState({loading:false,showLoader:false})
                            Toast.show({
                                text:response.message,
                                textStyle:{textAlign:"center"},
                                duration:2000,
                                style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
                            });
                        }
                    }).catch((error) =>{
                        console.log(error)
                    });
                }
            }
            else{
              alert("You are not logged in! please login first.")
              this.props.navigation.navigate('auth');
            }
        }).catch((error) =>{
                console.log(error)
            });
    }
    catch (error) {
        Toast.show({
            text:error,
            textStyle:{textAlign:"center"},
            duration:2000,
            style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
        });
    }
}

deleteCar = async (val,permission) =>{
    try{
        var carid = val
        if(permission)
        {
            this.setState({showLoader:true,loading:true})
            API.deleteUserCars(carid).then((response)=>{
                if(response.status){
                    this.setState({showLoader:false,loading:false})
                    Toast.show({
                        text:"Car Deleted Successfully",
                        textStyle:{textAlign:"center"},
                        duration:2000,
                        style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"redgreen"}
                    });
                    this.setState({showModal:false})
                    this.getUserCar();
                }
                else{
                    this.setState({showLoader:false,loading:false})
                    Toast.show({
                        text:response2.Message,
                        textStyle:{textAlign:"center"},
                        duration:2000,
                        style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
                    });
                }
              }).catch((error) =>{
                console.log(error)
            });
        }
        else
        {   this.setState({enableCard:true,enablecard2:false,showModal:false})
            this.setState({carRegNumberColor:"#000",carRegYearColor:"#000",carBrandColor:"#000",carModalColor:"#000",
                            carBodyTypeColor:"#000",carColor:"#000"})
            this.setState({carRegNumber :"" ,carRegYear :"",carBrand :"",carModal :"",carBodyType :"",carBodyColor:""})
        }
    }
    catch (error) {
        Toast.show({
            text:error,
            textStyle:{textAlign:"center"},
            duration:2000,
            style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
        });
    }     
}

 render(){
 return ( 
      <View style={{flex:1}} >
        <Container >
          <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <ImageBackground source={require("../../../../../assets/auth.png")} resizeMode="stretch" 
                            style={{width:responsiveWidth(100),height:responsiveHeight(100)}}>
              <View style={{marginTop:responsiveHeight(5),flexDirection:"row",justifyContent:"space-between"}}>
                <View style={{marginLeft:responsiveWidth(5)}}>
                  <Display enable={this.state.enable} 
                          enterDuration={2000} 
                          enter="slideInLeft">
                    <Text style={{color:'#fff',fontSize:responsiveFontSize(3.5),fontWeight:"bold",fontFamily:""}}>
                        Your Cars  </Text>
                  </Display>
                </View>
                <View style={{marginRight:responsiveWidth(2),alignItems:"center",justifyContent:"center"}}>
                  <Display enable={this.state.enableClose} 
                          enterDuration={2000} 
                          enter="fadeIn">
                    <TouchableOpacity onPress={()=> this.props.navigation.goBack()} >
                      <Icon name="ios-close" style={{fontSize:responsiveFontSize(5),marginRight:10,color:"#fff"}}/>
                    </TouchableOpacity>
                  </Display>
                </View>
              </View>
              <Content style={{flex:1}} >
                
                  <Display enable={this.state.enableCard} 
                          enterDuration={2000} 
                          enter="bounceInUp">
                <View style={{justifyContent:"center",margin:responsiveHeight(3),marginTop:responsiveHeight(4)}}>
                    <Card style={{backgroundColor:"#fff",borderRadius:responsiveWidth(5),marginBottom:responsiveHeight(10)}}>
                      <CardItem header bordered 
                          style={{backgroundColor:"#fff",borderRadius:responsiveWidth(5),borderBottomLeftRadius:0,
                                  borderBottomRightRadius:0,alignItems:"center"}}>
                        <Text style={{fontWeight:"bold",fontSize:responsiveFontSize(2.5),color:"#0652dd"}}>
                            Cars you own's</Text>
                      </CardItem>
                      <CardItem header bordered 
                          style={{backgroundColor:"#fff",alignItems:"center",justifyContent:"center"}}>
                        <View>
                        { (this.state.userCar.length == 0) &&
                                <Text style={{fontSize:responsiveFontSize(2.3)}}>
                                You haven't added any car yet.
                            </Text>}
                            { (this.state.userCar.length>0) &&
                                <Text style={{fontSize:responsiveFontSize(2.3)}}>
                                Add your car for offering the pools.
                            </Text>}
                        {this.state.userCar.map((item, i) => (
                            <View key={i} style={{padding:responsiveWidth(3),borderWidth:1,marginTop:responsiveHeight(3),borderRadius:responsiveWidth(2),
                                width:responsiveWidth(80),backgroundColor:"#e6e6e6"}}>
                                <View style={{flexDirection:'row',height:"auto",flex:1}}>
                                    <View style={{backgroundColor:'#cccccc',alignItems:"center",justifyContent:"space-around"}}>
                                            <Icon name="car-hatchback" type="MaterialCommunityIcons" 
                                            style={{color:"#000",fontSize:responsiveFontSize(4)}}/>
                                    </View>
                                    <View style={{flex:5,justifyContent:'center',marginLeft:responsiveWidth(4)}}>
                                        <Text style={{fontSize:responsiveFontSize(2.5),color:'#000',fontWeight:"bold",}}>
                                            {item.car_brand} {item.car_model} </Text>
                                        <View style={{flexDirection:"row"}}>
                                            <Text style={{fontSize:responsiveFontSize(2),fontWeight:"bold",color:'#000',opacity:0.7}}>
                                                Reg.No : </Text>
                                            <Text style={{fontSize:responsiveFontSize(2),fontWeight:"bold",color:'#000',}}>
                                                {item.car_number_plate} </Text>
                                        </View>
                                        <View style={{flexDirection:"row"}}>
                                            <Text style={{fontSize:responsiveFontSize(2),fontWeight:"bold",color:'#000',opacity:0.7}}>
                                                Type : </Text>
                                            <Text style={{fontSize:responsiveFontSize(2),fontWeight:"bold",color:'#000',}}>
                                                {item.car_body_type} </Text>
                                        </View>
                                        <View style={{flexDirection:"row"}}>
                                            <Text style={{fontSize:responsiveFontSize(2),fontWeight:"bold",color:'#000',opacity:0.7}}>
                                                Year : </Text>
                                            <Text style={{fontSize:responsiveFontSize(2),fontWeight:"bold",color:'#000',}}>
                                                {item.car_reg_year} </Text>
                                        </View>
                                        <View style={{flexDirection:"row"}}>
                                            <Text style={{fontSize:responsiveFontSize(2),fontWeight:"bold",color:'#000',opacity:0.7}}>
                                                Color : </Text>
                                            <Text style={{fontSize:responsiveFontSize(2),fontWeight:"bold",color:'#000',}}>
                                                {item.car_color}  </Text>
                                        </View>
                                    </View>
                                    <View style={{alignItems:"center",justifyContent:"center",flexDirection:"row",
                                                paddingLeft:responsiveWidth(2)}}>
                                        <TouchableOpacity onPress={()=>this.setState({carId:item.car_id,showModal:true})}>
                                            <Icon name="delete-forever" type="MaterialCommunityIcons" style={{color:"red",fontSize:responsiveFontSize(4)}}></Icon>
                                        </TouchableOpacity>
                                    </View>
                                </View>
                            </View>
                          ))}
                        </View> 
                      </CardItem>
                      <CardItem footer bordered 
                        style={{borderRadius:responsiveWidth(5),alignItems:"center",justifyContent:"center",}}>
                        <TouchableOpacity onPress={()=>   this.AddNewCar()}
                          style={{alignItems:"center",justifyContent:"center",width:responsiveWidth(60),height:responsiveHeight(7),
                          backgroundColor:"#0652dd",borderRadius:responsiveWidth(3)}}>
                          <Text style={{fontSize:responsiveFontSize(2),color: '#fff'}}>
                              Add New Car
                          </Text>
                        </TouchableOpacity>
                      </CardItem>
                    </Card>
                    </View>
                  </Display>
                
                  <Display enable={this.state.enablecard2} 
                          enterDuration={2000} 
                          enter="bounceInUp">
                <View style={{justifyContent:"center",margin:responsiveHeight(3),marginTop:responsiveHeight(4),flex:1}}>
                    <Card style={{backgroundColor:"#fff",borderRadius:responsiveWidth(5),marginBottom:responsiveHeight(10)}}>
                      <CardItem header bordered 
                          style={{backgroundColor:"#fff",borderRadius:responsiveWidth(5),borderBottomLeftRadius:0,
                                  borderBottomRightRadius:0}}>
                        <Text style={{fontWeight:"bold",fontSize:responsiveFontSize(2.5),color:"#0652dd"}}>
                            Add Cars</Text>
                      </CardItem>
                      <CardItem bordered>
                        <View>
                          <Text style={{fontSize:responsiveFontSize(2.3)}}>
                            Add your car for offering the pools.
                          </Text>
                          <View style={{paddingHorizontal:responsiveWidth(3),borderRadius: responsiveWidth(2),borderWidth:1,
                                    borderColor: this.state.carRegNumberColor,marginTop:responsiveHeight(4),marginBottom:responsiveHeight(4)}}>
                            <Text style={{position: 'absolute',top:responsiveHeight(-1.5),left: responsiveHeight(2),
                                        fontWeight: 'bold',backgroundColor:"#fff"}}>
                              Reg. Number
                            </Text>
                            <View style={{flex:1,flexDirection:"row",justifyContent:"center",alignItems:"center"}}>
                              <Input value={this.state.carRegNumber}
                                    onChangeText={(carRegNumber) => this.setState({carRegNumber})}
                                    autoCapitalize = "characters"
                                    placeholder="Enter Reg. Number"
                                />
                              <Icon name="card-text" type="MaterialCommunityIcons" style={{color:"#000",fontSize:responsiveFontSize(4)}}/>
                            </View>
                          </View> 
                          <View style={{paddingHorizontal:responsiveWidth(3),borderRadius: responsiveWidth(2),borderWidth:1,
                                    borderColor: this.state.carRegYearColor,marginBottom:responsiveHeight(4)}}>
                            <Text style={{position: 'absolute',top:responsiveHeight(-1.5),left: responsiveHeight(2),
                                        fontWeight: 'bold',backgroundColor:"#fff"}}>
                              Reg. Year
                            </Text>
                            <View style={{flex:1,flexDirection:"row",justifyContent:"center",alignItems:"center"}}>
                              <Input value={this.state.carRegYear}
                                    onChangeText={(carRegYear) => this.setState({carRegYear})}
                                    autoCapitalize = "characters"
                                    keyboardType ="number-pad"
                                    placeholder="Select Reg. year"
                                />
                              <Icon name="ios-calendar" style={{color:"#000",fontSize:responsiveFontSize(4)}}/>
                            </View>
                          </View>
                          <View style={{paddingHorizontal:responsiveWidth(3),borderRadius: responsiveWidth(2),borderWidth:1,
                                    borderColor: this.state.carBrandColor,marginBottom:responsiveHeight(4)}}>
                            <Text style={{position: 'absolute',top:responsiveHeight(-1.5),left: responsiveHeight(2),
                                        fontWeight: 'bold',backgroundColor:"#fff"}}>
                              Car Brand
                            </Text>
                            <View style={{flex:1,flexDirection:"row",justifyContent:"center",alignItems:"center"}}>
                                <Picker
                                    mode="dropdown"
                                    selectedValue={this.state.carBrand}
                                    onValueChange={(value)=> this.getCarModal(value)}
                                    style={{backgroundColor:"#fff",marginTop:responsiveHeight(1)}}
                                    >
                                    {this.state.carBrandData.map((item, i) => (
                                        <Picker.Item key={i} label={item.car_brand} value={item.car_brand} 
                                        style={{fontSize:responsiveFontSize(2.2),fontWeight:'bold',marginTop:responsiveWidth(2)}}/>
                                    ))}
                                </Picker>
                                <Icon name="ios-car"  style={{color:"#000",fontSize:responsiveFontSize(4)}}/>
                            </View>
                          </View> 
                          <View style={{paddingHorizontal:responsiveWidth(3),borderRadius: responsiveWidth(2),borderWidth:1,
                                    borderColor: this.state.carModalColor,marginBottom:responsiveHeight(4)}}>
                            <Text style={{position: 'absolute',top:responsiveHeight(-1.5),left: responsiveHeight(2),
                                        fontWeight: 'bold',backgroundColor:"#fff"}}>
                              Car Modal
                            </Text>
                            <View style={{flex:1,flexDirection:"row",justifyContent:"center",alignItems:"center"}}>
                                <Picker
                                    mode="dropdown"
                                    selectedValue={this.state.carModal}
                                    onValueChange={(carModal)=> this.setState({carModal})}
                                    style={{backgroundColor:"#fff",marginTop:responsiveHeight(1)}}
                                    >
                                    {this.state.carModalData.map((item, i) => (
                                        <Picker.Item key={i} label={item.car_model} value={item.car_model} 
                                        style={{fontSize:responsiveFontSize(2.2),fontWeight:'bold',marginTop:responsiveWidth(2)}}/>
                                    ))}
                                </Picker>
                              <Icon name="car-sports" type="MaterialCommunityIcons" style={{color:"#000",fontSize:responsiveFontSize(4)}}/>
                            </View>
                          </View>
                          <View style={{paddingHorizontal:responsiveWidth(3),borderRadius: responsiveWidth(2),borderWidth:1,
                                    borderColor: this.state.carBodyTypeColor,marginBottom:responsiveHeight(4)}}>
                            <Text style={{position: 'absolute',top:responsiveHeight(-1.5),left: responsiveHeight(2),
                                        fontWeight: 'bold',backgroundColor:"#fff"}}>
                              Body Type
                            </Text>
                            <View style={{flex:1,flexDirection:"row",justifyContent:"center",alignItems:"center"}}>
                                <Picker
                                    mode="dropdown"
                                    selectedValue={this.state.carBodyType}
                                    onValueChange={(value)=> this.setState({carBodyType:value})}
                                    style={{backgroundColor:"#fff",marginTop:responsiveHeight(1)}}
                                    >
                                    {this.state.carBodyTypeData.map((item, i) => (
                                        <Picker.Item key={i} label={item.type_name} value={item.type_name} 
                                        style={{fontSize:responsiveFontSize(2.2),fontWeight:'bold',marginTop:responsiveWidth(2)}}/>
                                    ))}
                                </Picker>
                              <Icon name="car-hatchback" type="MaterialCommunityIcons" style={{color:"#000",fontSize:responsiveFontSize(4)}}/>
                            </View>
                          </View>
                          <View style={{paddingHorizontal:responsiveWidth(3),borderRadius: responsiveWidth(2),borderWidth:1,
                                    borderColor: this.state.carColor,marginBottom:responsiveHeight(4)}}>
                            <Text style={{position: 'absolute',top:responsiveHeight(-1.5),left: responsiveHeight(2),
                                        fontWeight: 'bold',backgroundColor:"#fff"}}>
                              Body Color
                            </Text>
                            <View style={{flex:1,flexDirection:"row",justifyContent:"center",alignItems:"center"}}>
                                <Picker
                                    mode="dropdown"
                                    selectedValue={this.state.carBodyColor}
                                    onValueChange={(value)=> this.setState({carBodyColor:value})}
                                    style={{backgroundColor:"#fff",marginTop:responsiveHeight(1)}}
                                    >
                                    {this.state.carBodyColorData.map((item, i) => (
                                        <Picker.Item key={i} label={item.color_name} value={item.color_name} 
                                        style={{fontSize:responsiveFontSize(2.2),fontWeight:'bold',marginTop:responsiveWidth(2)}}/>
                                    ))}
                                </Picker>
                              <Icon name="car-wash" type="MaterialCommunityIcons" style={{color:"#000",fontSize:responsiveFontSize(4)}}/>
                            </View>
                          </View>
                          
                        </View> 
                      </CardItem>
                      <CardItem footer bordered 
                        style={{borderRadius:responsiveWidth(5),alignItems:"center",justifyContent:"space-between",}}>
                        <TouchableOpacity onPress={()=>   this.AddCar()}
                          style={{alignItems:"center",justifyContent:"center",width:responsiveWidth(30),height:responsiveHeight(7),
                          backgroundColor:"#0652dd",borderRadius:responsiveWidth(3)}}>
                          <Text style={{fontSize:responsiveFontSize(2),color: '#fff'}}>
                              Add Car
                          </Text>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={()=>   this.cancel()}
                          style={{alignItems:"center",justifyContent:"center",width:responsiveWidth(30),height:responsiveHeight(7),
                          backgroundColor:"#0652dd",borderRadius:responsiveWidth(3)}}>
                          <Text style={{fontSize:responsiveFontSize(2),color: '#fff'}}>
                              Cancel
                          </Text>
                        </TouchableOpacity>
                      </CardItem>
                    </Card>
                    </View>
                  </Display>
              </Content>  
            <KeyboardSpacer/>
            </ImageBackground>
          </TouchableWithoutFeedback>
        </Container>
        <Modal
            animationType="slide"
            transparent={true}
            visible={this.state.showModal}
            onRequestClose={() => this.setState({showModal:false,})}
        >
            <View style={{alignItems:"center",justifyContent:"center",backgroundColor:"#ccc",height:responsiveHeight(30),
                        width:responsiveWidth(80),margin:responsiveWidth(10),marginTop:responsiveHeight(30),borderRadius:responsiveWidth(10)}} >
                <View style={{margin:responsiveWidth(10)}}> 
                    <Text style={{fontSize:responsiveFontSize(2.5),fontWeight:"bold"}}>Are you sure! Want to delete car.</Text>
                </View>
                <View style={{flexDirection:"row",justifyContent:"space-around",alignItems:"center"}}>
                    <TouchableOpacity onPress={()=>  this.deleteCar(this.state.carId,false)}
                        style={{alignItems:"center",justifyContent:"center",width:responsiveWidth(30),height:responsiveHeight(7),
                                backgroundColor:"#0652dd",borderRadius:responsiveWidth(3),marginRight:responsiveWidth(5)}}>
                        <Text style={{fontSize:responsiveFontSize(2),color: '#fff'}}>
                            Cancel
                        </Text>
                    </TouchableOpacity>
                    <TouchableOpacity onPress={()=> this.deleteCar(this.state.carId,true)}
                        style={{alignItems:"center",justifyContent:"center",width:responsiveWidth(30),height:responsiveHeight(7),
                        backgroundColor:"#0652dd",borderRadius:responsiveWidth(3),marginLeft:responsiveWidth(5)}}>
                        <Text style={{fontSize:responsiveFontSize(2),color: '#fff'}}>
                            Ok
                        </Text>
                    </TouchableOpacity>
                </View>
            </View>
        </Modal>
        <Spinner animation="fade"
                visible={this.state.showLoader}
                textContent={'Loading...'}
                textStyle={{color:"#fff"}}
              />
      </View>
 )}
}


