import React from 'react';
import {DrawerItems} from "react-navigation-drawer";
import { StyleSheet, View,TouchableOpacity, Image,ImageBackground ,AsyncStorage} from 'react-native';
import {Container, Content, Text, List, ListItem ,Icon, Button, Right,Thumbnail} from "native-base";
import {responsiveWidth,responsiveHeight,responsiveFontSize} from "react-native-responsive-dimensions";
import { LinearGradient } from 'expo-linear-gradient';
import API from "../../../allApi";
import Localstorage from "../../../async";

export default class Drawer extends React.Component {
  constructor(props){
      super(props);
      this.state={
        userData:[],
        photo:"https://i.imgur.com/jeCVrJJ.png"
      }

  } 
// getdata=async()=>{
//   try{ 
//     Localstorage.retrieveItem("UID").then((userId) =>{
//       if(userId){
//         API.getUserDetails(userId).then((response)=>{
//           if(response.status){
//               this.setState({userData:response.data})
//               //console.log(response.data)
//               if(!response.data.u_profile_pic == "")
//                 { this.setState({photo:response.data.u_profile_pic})
//               }
//               else{
//                 this.setState({photo:"https://i.ya-webdesign.com/images/profile-avatar-png-4.png"})}
//               }
//             else{
//               Toast.show({
//                 text:response.message,
//                 textStyle:{textAlign:"center"},
//                 duration:2000,
//                 style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
//               });
//             }
//         }).catch((error) =>{
//           console.log(error)
//       });
//       }
//       else{
//         alert("You are not logged in! please login first.")
//         this.props.navigation.navigate('auth');
//       }
//     }).catch((error) =>{
//           console.log(error)
//       });
//   }
//   catch (error) {
//       Toast.show({
//           text:error,
//           textStyle:{textAlign:"center"},
//           duration:2000,
//           style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
//         });
//   }
// }
  
  render() {
    //this.getdata();
    return (
      <Container>
        <Content>
        
              <LinearGradient start={{x: 0, y: 0}} end={{x: 1, y: 0}} colors={['#1CB5E0', '#000046']}>
                <View style={{margin:responsiveHeight(2),marginTop:responsiveHeight(5),marginBottom:responsiveHeight(5),
                        flexDirection:"row",alignItems:"center"}}  >
                      <Image  source={{uri:this.state.photo}}
                              style={{width:responsiveWidth(18),height:responsiveWidth(18),borderRadius:responsiveWidth(10)}} />
                  
                  <View style={{marginLeft:responsiveWidth(5),width:responsiveWidth(50)}}>
                    <Text style={{color:"#fff",fontSize:responsiveFontSize(2.5),fontWeight:"bold"}}>
                      {this.state.userData.u_first_name} {this.state.userData.u_last_name}
                    </Text>
                    <Text style={{color:"#fff",fontSize:responsiveFontSize(2)}}>{this.state.userData.u_email}</Text>
                  </View>
                </View>
              </LinearGradient>
            
                <Content>
                  <DrawerItems {...this.props}/>
                </Content>
           
                </Content>
            <View style={{height:responsiveHeight(5),backgroundColor:"#d9d9d9",alignItems:"center",justifyContent:"center"}}>
                <Text style={style.textstyle}>
                    version:1.0.0
                </Text>
            </View>
            
        
      </Container>
    );
  }
}
const style = StyleSheet.create({
    Container:{
               flex: 1,
            },
            textstyle:{
              fontSize:responsiveFontSize(2),
              marginLeft:responsiveWidth(5)
            
            
            
            }
            

});