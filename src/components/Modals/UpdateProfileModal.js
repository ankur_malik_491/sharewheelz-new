import React from 'react';
import {Text, View,TouchableOpacity,TouchableWithoutFeedback,Keyboard,ImageBackground,Modal} from 'react-native';
import {Container,Icon,Input, Content,Toast,Card,CardItem} from "native-base";
import {responsiveWidth,responsiveHeight,responsiveFontSize} from "react-native-responsive-dimensions";
import KeyboardSpacer from 'react-native-keyboard-spacer';
import Display from 'react-native-display';

export default class UpdateProfileModal extends React.Component {
  constructor(props){
    super(props);
    this.state = {
                    val:this.props.value,
                    color:"#000",
                    enable:false,
                    enableBase:false,
                    enableImagView:false,
                    enableImg:false,
                    enableCard:false,
                    modalVisible: true,

    };
  }
componentDidMount = () => {
    this.setState({enable:true})
        setTimeout(()=>{
        this.setState({enableCard:true}) 
        setTimeout(()=>{
            this.setState({enableClose:true}) 
        },500);
    },1500);
}
validateUserEmail = ()=>{
    var emailPattern = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/;
    if(emailPattern.test(this.state.val)){
        return true;
        }
    return false;
}
saveval = () => {
    if(this.props.title == "Email")
    { if(!this.validateUserEmail()){
        this.setState({color:"red"})
        alert("Please fill email in correct format (abc@email.com)");
      }
      else{
        this.props.onAction(this.state.val,this.state.modalVisible)
      }
    }
    else{
        this.props.onAction(this.state.val,this.state.modalVisible)
    }
}
 render(){
   //console.log(this.state.userDobYearItems)
 return ( 
    <Modal
    animationType="slide"
    transparent={true}
    visible={this.state.modalVisible}
    onRequestClose={() => {this.props.onAction(this.props.value,this.state.modalVisible)}}
  >
      <View style={{flex:1}} >
        <Container >
          <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
            <ImageBackground source={require("../../assets/auth.png")} resizeMode="stretch" 
                            style={{width:responsiveWidth(100),height:responsiveHeight(100)}}>
              <View style={{marginTop:responsiveHeight(5),flexDirection:"row",justifyContent:"space-between"}}>
                <View style={{marginLeft:responsiveWidth(5)}}>
                  <Display enable={this.state.enable} 
                          enterDuration={2000} 
                          enter="slideInLeft">
                    <Text style={{color:'#fff',fontSize:responsiveFontSize(3.5),fontWeight:"bold",fontFamily:""}}>
                    Edit {this.props.title}</Text>
                  </Display>
                </View>
                <View style={{marginRight:responsiveWidth(2),alignItems:"center",justifyContent:"center"}}>
                  <Display enable={this.state.enableClose} 
                          enterDuration={2000} 
                          enter="fadeIn">
                    <TouchableOpacity onPress={()=> this.props.onAction(this.props.value,this.state.modalVisible)} >
                      <Icon name="ios-close" style={{fontSize:responsiveFontSize(5),marginRight:10,color:"#fff"}}/>
                    </TouchableOpacity>
                  </Display>
                </View>
              </View>
              <Content >
                <View style={{justifyContent:"center",margin:responsiveHeight(3),marginTop:responsiveHeight(4)}}>
                  <Display enable={this.state.enableCard} 
                          enterDuration={2000} 
                          enter="bounceInUp">
                    <Card style={{backgroundColor:"#fff",borderRadius:responsiveWidth(5)}}>
                      <CardItem header bordered 
                          style={{backgroundColor:"#fff",borderRadius:responsiveWidth(5),borderBottomLeftRadius:0,
                                  borderBottomRightRadius:0}}>
                        <View>
                          <Text style={{fontSize:responsiveFontSize(2.3)}}>
                            Please enter the {this.props.title} you want to update.
                          </Text>
                          <View style={{paddingHorizontal:responsiveWidth(3),borderRadius: responsiveWidth(2),borderWidth:1,
                                    borderColor: this.state.color,marginTop:responsiveHeight(4),marginBottom:responsiveHeight(4)}}>
                            <Text style={{position: 'absolute',top:responsiveHeight(-1.5),left: responsiveHeight(2),
                                        fontWeight: 'bold',backgroundColor:"#fff"}}>
                              {this.props.title}
                            </Text>
                            <View style={{flex:1,flexDirection:"row",justifyContent:"center",alignItems:"center"}}>
                              <Input value={this.state.val}
                                    autoFocus={true}
                                    onChangeText={(val) => this.setState({val:val})}
                                    autoCapitalize = "none"
                                />
                              <Icon name={this.props.iconName} style={{color:"#000",fontSize:responsiveFontSize(4)}}/>
                            </View>
                          </View> 
                        </View> 
                      </CardItem>
                      <CardItem footer bordered style={{borderRadius:responsiveWidth(5),alignItems:"center",justifyContent:"center"}}>
                        <TouchableOpacity onPress={()=>this.saveval()}
                          style={{alignItems:"center",justifyContent:"center",width:responsiveWidth(50),height:responsiveHeight(7),
                          backgroundColor:"#0652dd",borderRadius:responsiveWidth(3)}}>
                          <Text style={{fontSize:responsiveFontSize(2.5),color: '#fff'}}>
                             {this.props.btnValue}
                          </Text>
                        </TouchableOpacity>
                      </CardItem>
                    </Card>
                  </Display>
                </View>
              </Content>  
            <KeyboardSpacer/>
            </ImageBackground>
          </TouchableWithoutFeedback>
        </Container>
      </View>
      </Modal>
 )}
}



