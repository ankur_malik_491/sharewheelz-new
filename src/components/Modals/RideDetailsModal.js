import React from 'react';
import {Text, View,TouchableOpacity,TouchableWithoutFeedback,Keyboard,ImageBackground,Modal,Image} from 'react-native';
import { Container, Header, Content, List, ListItem, Left, Body, Right, Thumbnail, Icon,Separator  } from 'native-base';
import {responsiveWidth,responsiveHeight,responsiveFontSize} from "react-native-responsive-dimensions";
import * as Location from 'expo-location';
import KeyboardSpacer from "react-native-keyboard-spacer";
import Loader from 'react-native-loading-spinner-overlay';
import API from "../../../allApi";

var date,time;
export default class RideDetailsModal extends React.Component {
  constructor(props){
    super(props);
    this.state = {
                    rideDetails:this.props.details,
                    showLoader:false,
                    loading:false,
                    modalVisible: true,
                    photoUrl:"https://i.ya-webdesign.com/images/profile-avatar-png-4.png",
                    userChattiness:[{label:"I'm the Quite type",value:"I'm the quite type"},
                                    {label:"I talk depending on my mood",value:"I talk depending on my mood"},
                                    {label:"I love to chat",value:"I love to chat"}],
                    userSmoking:[{label:"No smoking in the car please",value:"No smoking in the car please"},
                                    {label:"Don't know",value:"Don't know"},
                                    {label:"Cigarette smoke doesn't bother me",value:"Cigarette smoke doesn't bother me"}],
                    userMusic:[ {label:"Silence is golden",value:"Silence is golden"},
                                {label :"Don't know",value:"Don't know"},
                                {label:"It's all about the playlist",value:"It's all about the playlist"}],
                    userPets:[{label:"No pets please",value:"No pets please"},
                                {label:"Don't know",value:"Don't know"},
                                {label:"Pets welcome Woof!",value:"Pets welcome Woof!"}],
    };
    
    
  }
 

 render(){
   //console.log(this.props.mode)
 return (
    <Modal
    animationType="slide"
    transparent={true}
    visible={this.state.modalVisible}
    onRequestClose={() => this.props.onAction(this.state.modalVisible)}
  >
      <View style={{flex:1}} >
        <Container >
        <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <>
        {(this.props.bookedMode) &&
        <View style={{flex:1,}}>
            <View style={{flexDirection:"row",alignItems:"center",backgroundColor:"#0652dd",paddingBottom:responsiveHeight(1),paddingHorizontal:responsiveWidth(5)}}>
                <View>
                    <TouchableOpacity onPress={()=>this.props.onAction(this.state.modalVisible)}>
                        <Icon name="ios-arrow-back" style={{color:"#fff",fontSize:responsiveFontSize(4)}}/>
                    </TouchableOpacity>
                </View>
                {this.state.rideDetails.ride_detail.map((item,i) =>(
                <View key={i}>
                    <Text style={{color:"#fff",fontSize:responsiveFontSize(2.5),fontWeight:"bold",marginLeft:responsiveWidth(5)}}>
                        { date = new Date(item.ride_pick_timestamp*1),
                            date.toDateString().slice(4)}  ({ time = new Date(item.ride_pick_timestamp*1),
                            time.toTimeString().slice(0,5)})
                    </Text>
                    {/* <View style={{flexDirection:"row",marginBottom:responsiveHeight(1)}}>
                        <Text style={{color:"#fff",fontSize:responsiveFontSize(2),marginLeft:responsiveWidth(5)}}>
                            {item.ride_pick_city}
                        </Text>
                        <Text style={{color:"#fff",fontSize:responsiveFontSize(2),marginLeft:responsiveWidth(3)}}>
                            >>
                        </Text>
                        <Text style={{color:"#fff",fontSize:responsiveFontSize(2),marginLeft:responsiveWidth(3)}}>
                            {item.ride_drop_city}
                        </Text>
                    </View> */}
                </View>
                ))}
                
                
            </View>
            <Content>
            <View>
              <ImageBackground source={require('../../assets/googlemap.png')} 
                                style={{width:responsiveWidth(100),height:responsiveHeight(25)}}/>
            </View>
            {this.state.rideDetails.ride_detail.map((item,i) =>(
            < View key={i}>
            <ListItem thumbnail style={{margin:responsiveHeight(2)}} >
              
                <View key={i} style={{flexDirection:"row",alignItems:"center"}}>
                    <Left>
                        { (item.userDataObject.u_profile_pic != "") &&
                        <Thumbnail source={{uri:item.userDataObject.u_profile_pic}}
                            style={{height:responsiveWidth(14),width:responsiveWidth(14)}}/>
                    }
                    { (item.userDataObject.u_profile_pic == "") &&
                        <Thumbnail source={{uri:this.state.photoUrl}}
                            style={{height:responsiveWidth(14),width:responsiveWidth(14)}}/>
                    } 
                    </Left>
                    <Body>
                        <Text style={{fontSize:responsiveFontSize(2.3),fontWeight:"bold"}}>
                            {item.userDataObject.u_first_name} {item.userDataObject.u_last_name}
                        </Text>
                        <Text style={{fontSize:responsiveFontSize(2.3),opacity:0.7}}>
                            {item.userDataObject.u_phone}
                        </Text>
                    </Body> 
                </View>
            </ListItem>
            <ListItem thumbnail>
                <Left>
                    <Thumbnail square source={require('../../assets/car.jpg')}
                        style={{height:responsiveWidth(10),width:responsiveWidth(14)}}/>
                </Left>
                <Body>
                    <Text style={{fontSize:responsiveFontSize(2.3),fontWeight:"bold",opacity:0.7}}>
                        {item.userCarDetailObject.car_color} {item.userCarDetailObject.car_brand} {item.userCarDetailObject.car_model}
                    </Text>
                    <Text style={{fontSize:responsiveFontSize(2.3),opacity:0.7}}>
                        {item.userCarDetailObject.car_number_plate} ({item.userCarDetailObject.car_reg_year})
                    </Text>
                    <Text style={{fontSize:responsiveFontSize(2.3),opacity:0.7}}>
                        {item.userCarDetailObject.car_body_type}
                    </Text>
                </Body>
            </ListItem>
            <ListItem thumbnail>
              <Left>
                <Thumbnail square source={require('../../assets/cash.png')} 
                    style={{height:responsiveWidth(12),width:responsiveWidth(14)}}/>
              </Left>
              <Body>
                <Text style={{fontSize:responsiveFontSize(2.3),fontWeight:"bold"}}>
                    ₹ {item.ride_seat_price}/Seat
                </Text>
             </Body>
            </ListItem>
            <ListItem thumbnail>
              <Left>
                <Thumbnail square source={require('../../assets/seats.png')} 
                    style={{height:responsiveWidth(14),width:responsiveWidth(14)}}/>
              </Left>
              <Body>
                <Text style={{fontSize:responsiveFontSize(2.3),fontWeight:"bold",opacity:0.7}}>
                    Bkd/Avl/off seats
                </Text>
                <Text style={{fontSize:responsiveFontSize(2.3),fontWeight:"bold"}}>
                    {item.ride_seats_offered - item.ride_seats_available} / {item.ride_seats_available} / {item.ride_seats_offered}
                </Text>
             </Body>
            </ListItem>
            <Separator/>
            <ListItem last >
              <Body>
                <View style={{}}>
                    <View style={{marginLeft:responsiveWidth(1),marginBottom:responsiveHeight(3)}}>
                        <Text style={{fontSize:responsiveFontSize(2.5),fontWeight:"bold",}}>
                            { date = new Date(item.ride_pick_timestamp*1),
                                date.toDateString().slice(4)}  ({ time = new Date(item.ride_pick_timestamp*1),
                                time.toTimeString().slice(0,5)})
                        </Text>
                    </View>
                    <View style={{flexDirection:"row",alignItems:"center"}}>
                        <Icon active name='map-marker-alt'  type="FontAwesome5" style={{color:"green",fontSize:responsiveFontSize(4)}}/>
                        <Text style={{fontSize:responsiveFontSize(2),fontWeight:"bold",opacity:0.7,marginLeft:responsiveWidth(3)}}>
                            {item.ride_pick_address}
                        </Text>
                    </View>
                    <View style={{flexDirection:"row",alignItems:"center",justifyContent:"space-between"}}>
                        <Icon active name='dots-vertical'  type="MaterialCommunityIcons" 
                                style={{color:"#000",marginLeft:responsiveWidth(-1),fontSize:responsiveFontSize(4)}}/>
                    </View>
                    <View style={{flexDirection:"row",alignItems:"center"}}>
                        <Icon active name='map-marker-alt'  type="FontAwesome5" style={{color:"red",fontSize:responsiveFontSize(4)}}/>
                        <Text style={{fontSize:responsiveFontSize(2),fontWeight:"bold",opacity:0.7,marginLeft:responsiveWidth(3)}}>
                            {item.ride_drop_address}
                        </Text>
                    </View>
                </View>
             </Body>
            </ListItem>
            <Separator/>
            <ListItem last >
              <Body>
                <View style={{}}>
                    <View style={{marginLeft:responsiveWidth(1),marginBottom:responsiveHeight(3)}}>
                        <Text style={{fontSize:responsiveFontSize(2.5),fontWeight:"bold",}}>
                            Prefferences
                        </Text>
                    </View>
                    <View style={{flexDirection:"row",alignItems:"center",paddingHorizontal:responsiveWidth(5),
                                    justifyContent:"space-between"}}>
                        <View style={{flexDirection:"row",alignItems:"center",width:responsiveWidth(30)}}>
                            {(item.userDataObject.user_chattiness == this.state.userChattiness[0].value) &&
                                <Icon name="chat" type="MaterialCommunityIcons"  
                                    style={{color:"#008ae6",fontSize:responsiveFontSize(4)}}/>
                            }
                            {(item.userDataObject.user_chattiness == this.state.userChattiness[1].value) &&
                                <Icon name="chat-processing" type="MaterialCommunityIcons"  
                                    style={{color:"#008ae6",fontSize:responsiveFontSize(4)}}/>
                            }
                            {(item.userDataObject.user_chattiness == this.state.userChattiness[2].value) &&
                                <Icon name="wechat" type="MaterialCommunityIcons"  
                                    style={{color:"#008ae6",fontSize:responsiveFontSize(4)}}/>
                            }
                            <View style={{alignItems:"center"}}>
                                <Text style={{marginLeft:responsiveWidth(3),fontSize:responsiveFontSize(2),fontWeight:"bold",opacity:0.5}}>
                                    {item.userDataObject.user_chattiness}
                                </Text> 
                            </View>
                        </View>
                        <View style={{flexDirection:"row",alignItems:"center",width:responsiveWidth(35)}}>
                            {(item.userDataObject.user_smoking == this.state.userSmoking[0].value ||
                                item.userDataObject.user_smoking == this.state.userSmoking[1].value) &&
                                <Icon name="logo-no-smoking"  style={{color:"red",fontSize:responsiveFontSize(4)}}/>
                            }
                            {(item.userDataObject.user_smoking == this.state.userSmoking[2].value) &&
                                <Icon name="smoking" type="MaterialCommunityIcons"  
                                    style={{color:"red",fontSize:responsiveFontSize(4)}}/>
                            }
                            <View style={{alignItems:"center"}}>
                                <Text style={{marginLeft:responsiveWidth(3),fontSize:responsiveFontSize(2),fontWeight:"bold",opacity:0.5}}>
                                    {item.userDataObject.user_smoking}
                                </Text> 
                            </View>
                        </View>
                    </View>
                    <View style={{flexDirection:"row",alignItems:"center",paddingHorizontal:responsiveWidth(5),
                                justifyContent:"space-between"}}>
                        <View style={{flexDirection:"row",alignItems:"center",width:responsiveWidth(30)}}>
                            {(item.userDataObject.user_music == this.state.userMusic[0].value ||
                                item.userDataObject.user_music == this.state.userMusic[1].value) &&
                                <Icon name="music-off" type="MaterialCommunityIcons"  
                                    style={{color:"#006600",fontSize:responsiveFontSize(4)}}/>
                            }
                            {(item.userDataObject.user_music == this.state.userMusic[2].value) &&
                                <Icon name="music" type="MaterialCommunityIcons"  
                                    style={{color:"#006600",fontSize:responsiveFontSize(4)}}/>
                            }
                            <View style={{alignItems:"center"}}>
                                <Text style={{marginLeft:responsiveWidth(3),fontSize:responsiveFontSize(2),fontWeight:"bold",opacity:0.5}}>
                                    {item.userDataObject.user_music}
                                </Text> 
                            </View>
                        </View>
                        <View style={{flexDirection:"row",alignItems:"center",width:responsiveWidth(35)}}>
                            {(item.userDataObject.user_pets == this.state.userPets[0].value ||
                                item.userDataObject.user_pets == this.state.userPets[1].value) &&
                                <Icon name="paw-off" type="MaterialCommunityIcons"  
                                    style={{color:"#cc3300",fontSize:responsiveFontSize(4)}}/>
                            }
                            {(item.userDataObject.user_pets == this.state.userPets[2].value) &&
                                <Icon name="paw" type="MaterialCommunityIcons"  
                                    style={{color:"#cc3300",fontSize:responsiveFontSize(4)}}/>
                            }
                            <View style={{alignItems:"center"}}>
                                <Text style={{marginLeft:responsiveWidth(3),fontSize:responsiveFontSize(2),fontWeight:"bold",opacity:0.5}}>
                                    {item.userDataObject.user_pets}
                                </Text> 
                            </View>
                        </View>
                    </View>
                </View>
             </Body>
            </ListItem>
            <Separator/>
            <ListItem itemHeader>
              <Text style={{fontWeight:"bold",color:"#000",fontSize:responsiveFontSize(2.5)}}>Bill Details</Text>
            </ListItem>
            <ListItem>
                <Left>
                    <Text style={{fontWeight:"bold",color:"#000",opacity:0.7,fontSize:responsiveFontSize(2)}}>
                        Fare/Seat
                    </Text>
                </Left>
                <Body/>
                <Right>
                    <Text style={{fontWeight:"bold",color:"#000",opacity:0.7,fontSize:responsiveFontSize(2)}}>
                        ₹ {item.ride_seat_price}
                    </Text>
                </Right>
            </ListItem>
            <ListItem>
                <Left>
                    <Text style={{fontWeight:"bold",color:"#000",opacity:0.7,fontSize:responsiveFontSize(2)}}>
                        Seat Booked
                    </Text>
                </Left>
                <Body/>
                <Right>
                    <Text style={{fontWeight:"bold",color:"#000",opacity:0.7,fontSize:responsiveFontSize(2)}}>
                        0{item.ride_seats_offered - item.ride_seats_available}
                    </Text>
                </Right>
            </ListItem>           
            <ListItem>
                <Left>
                    <View>
                        <Text style={{fontWeight:"bold",color:"#000",opacity:0.7,fontSize:responsiveFontSize(2)}}>
                            Total Amount
                        </Text>
                        <Text style={{fontWeight:"bold",color:"#000",opacity:0.7,fontSize:responsiveFontSize(1.5)}}>
                            Includes all taxes.
                        </Text>
                    </View>
                </Left>
                <Right>
                    <View style={{flexDirection:"row"}}>
                        <Text style={{fontWeight:"bold",color:"#000",opacity:0.7,fontSize:responsiveFontSize(2)}}>
                            ₹ {(item.ride_seats_offered - item.ride_seats_available)*item.ride_seat_price}
                        </Text>
                    </View>
                </Right>
            </ListItem>
            <ListItem>
                <Left>
                    <View>
                        <Text style={{fontWeight:"bold",color:"#000",opacity:0.7,fontSize:responsiveFontSize(2)}}>
                            Payment
                        </Text>
                        <Text style={{fontWeight:"bold",color:"#000",opacity:0.7,fontSize:responsiveFontSize(1.5)}}>
                            Cash
                        </Text>
                    </View>
                </Left>
                <Right>
                    <View style={{flexDirection:"row"}}>
                        <Text style={{fontWeight:"bold",color:"#000",opacity:0.7,fontSize:responsiveFontSize(2)}}>
                            ₹ {(item.ride_seats_offered - item.ride_seats_available)*item.ride_seat_price}
                        </Text>
                    </View>
                </Right>
            </ListItem>
            <Separator/>
            <ListItem last >
              <Body>
                <View style={{}}>
                    <View style={{marginLeft:responsiveWidth(1),marginBottom:responsiveHeight(1)}}>
                        <Text style={{fontSize:responsiveFontSize(2.5),fontWeight:"bold",}}>
                            Co-Passengers
                        </Text>
                    </View>
                    {item.alreadyBookings.map((item,i)=>(
                        <View key ={i} style={{backgroundColor:"#e6e6e6",
                                margin:responsiveWidth(2),padding:responsiveWidth(2)}}>
                            <View style={{flexDirection:"row",alignItems:"center",justifyContent:"space-between"}}>
                                <View style={{flexDirection:"row"}}>
                                    { (item.booking_profile_pic != "") &&
                                        <Thumbnail source={{uri:item.booking_profile_pic}}
                                            style={{height:responsiveWidth(12),width:responsiveWidth(12)}}/>
                                    }
                                    { (item.booking_profile_pic == "") &&
                                        <Thumbnail source={{uri:this.state.photoUrl}}
                                            style={{height:responsiveWidth(12),width:responsiveWidth(12)}}/>
                                    }
                                    <View>
                                        <Text style={{marginLeft:responsiveWidth(3),fontSize:responsiveFontSize(2.3),fontWeight:"bold"}}>
                                            {item.booking_username}
                                        </Text>
                                        <Text style={{marginLeft:responsiveWidth(3),fontSize:responsiveFontSize(2),fontWeight:"bold",opacity:0.7}}>
                                            {item.booking_contact}
                                        </Text>
                                    </View>
                                </View>
                                <View style={{flexDirection:"row",alignItems:"center"}}>
                                    <View style={{alignItems:"center",marginRight:responsiveWidth(3)}}>
                                        <Text style={{marginLeft:responsiveWidth(3),fontSize:responsiveFontSize(2),fontWeight:"bold",opacity:0.5}}>
                                            Bkd. Seats
                                        </Text> 
                                        <Text style={{marginLeft:responsiveWidth(3),fontSize:responsiveFontSize(2.3),fontWeight:"bold"}}>
                                            0{item.booking_seats}
                                        </Text>
                                    </View>
                                </View>
                            </View>
                            <View style={{flexDirection:"row",alignItems:"center",justifyContent:"space-between"}}>
                                <View style={{justifyContent:"space-between",margin:responsiveWidth(5)}}>
                                    <View style={{flexDirection:"row",alignItems:"center"}}>
                                        <Icon active name='map-marker-alt'  type="FontAwesome5" style={{color:"green",fontSize:responsiveFontSize(3)}}/>
                                        <Text style={{fontSize:responsiveFontSize(1.5),fontWeight:"bold",opacity:0.7,marginLeft:responsiveWidth(3)}}>
                                            {item.booking_src_city}
                                        </Text>
                                    </View>
                                    <View style={{flexDirection:"row",alignItems:"center",justifyContent:"space-between"}}>
                                        <Icon active name='dots-vertical'  type="MaterialCommunityIcons" 
                                                style={{color:"#000",marginLeft:responsiveWidth(-1),fontSize:responsiveFontSize(3)}}/>
                                    </View>
                                    <View style={{flexDirection:"row",alignItems:"center"}}>
                                        <Icon active name='map-marker-alt'  type="FontAwesome5" style={{color:"red",fontSize:responsiveFontSize(3)}}/>
                                        <Text style={{fontSize:responsiveFontSize(1.5),fontWeight:"bold",opacity:0.7,marginLeft:responsiveWidth(3)}}>
                                            {item.booking_dest_city}
                                        </Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                        
                    ))}
                </View>
             </Body>
            </ListItem>
            <Separator/>
            </View>
            ))}
            </Content>
        </View>
        }
        {(this.props.offerMode) &&
            <View style={{flex:1,}}>
            <View style={{flexDirection:"row",alignItems:"center",backgroundColor:"#0652dd",paddingBottom:responsiveHeight(1),paddingHorizontal:responsiveWidth(5)}}>
                <View>
                    <TouchableOpacity onPress={()=>this.props.onAction(this.state.modalVisible)}>
                        <Icon name="ios-arrow-back" style={{color:"#fff",fontSize:responsiveFontSize(4)}}/>
                    </TouchableOpacity>
                </View>
                <View >
                    <Text style={{color:"#fff",fontSize:responsiveFontSize(2.5),fontWeight:"bold",marginLeft:responsiveWidth(5)}}>
                        { date = new Date(this.state.rideDetails.ride_pick_timestamp*1),
                            date.toDateString().slice(4)}  ({ time = new Date(this.state.rideDetails.ride_pick_timestamp*1),
                            time.toTimeString().slice(0,5)})
                    </Text>
                    {/* <View style={{flexDirection:"row",marginBottom:responsiveHeight(1)}}>
                        <Text style={{color:"#fff",fontSize:responsiveFontSize(2),marginLeft:responsiveWidth(5)}}>
                            {item.ride_pick_city}
                        </Text>
                        <Text style={{color:"#fff",fontSize:responsiveFontSize(2),marginLeft:responsiveWidth(3)}}>
                            >>
                        </Text>
                        <Text style={{color:"#fff",fontSize:responsiveFontSize(2),marginLeft:responsiveWidth(3)}}>
                            {item.ride_drop_city}
                        </Text>
                    </View> */}
                </View>
                
                
            </View>
            <Content>
            <View>
              <ImageBackground source={require('../../assets/googlemap.png')} 
                                style={{width:responsiveWidth(100),height:responsiveHeight(25)}}/>
            </View>
            < View >
            <ListItem thumbnail style={{margin:responsiveHeight(2)}} >
              
                <View style={{flexDirection:"row",alignItems:"center"}}>
                    <Left>
                        { (this.state.rideDetails.userDataObject.u_profile_pic != "") &&
                        <Thumbnail source={{uri:this.state.rideDetails.userDataObject.u_profile_pic}}
                            style={{height:responsiveWidth(14),width:responsiveWidth(14)}}/>
                    }
                    { (this.state.rideDetails.userDataObject.u_profile_pic == "") &&
                        <Thumbnail source={{uri:this.state.photoUrl}}
                            style={{height:responsiveWidth(14),width:responsiveWidth(14)}}/>
                    } 
                    </Left>
                    <Body>
                        <Text style={{fontSize:responsiveFontSize(2.3),fontWeight:"bold"}}>
                            {this.state.rideDetails.userDataObject.u_first_name} {this.state.rideDetails.userDataObject.u_last_name}
                        </Text>
                        <Text style={{fontSize:responsiveFontSize(2.3),opacity:0.7}}>
                            {this.state.rideDetails.userDataObject.u_phone}
                        </Text>
                    </Body> 
                </View>
            </ListItem>
            <ListItem thumbnail>
                <Left>
                    <Thumbnail square source={require('../../assets/car.jpg')}
                        style={{height:responsiveWidth(10),width:responsiveWidth(14)}}/>
                </Left>
                <Body>
                    <Text style={{fontSize:responsiveFontSize(2.3),fontWeight:"bold",opacity:0.7}}>
                        {this.state.rideDetails.userCarDetailObject.car_color} {this.state.rideDetails.userCarDetailObject.car_brand} {this.state.rideDetails.userCarDetailObject.car_model}
                    </Text>
                    <Text style={{fontSize:responsiveFontSize(2.3),opacity:0.7}}>
                        {this.state.rideDetails.userCarDetailObject.car_number_plate} ({this.state.rideDetails.userCarDetailObject.car_reg_year})
                    </Text>
                    <Text style={{fontSize:responsiveFontSize(2.3),opacity:0.7}}>
                        {this.state.rideDetails.userCarDetailObject.car_body_type}
                    </Text>
                </Body>
            </ListItem>
            <ListItem thumbnail>
              <Left>
                <Thumbnail square source={require('../../assets/cash.png')} 
                    style={{height:responsiveWidth(12),width:responsiveWidth(14)}}/>
              </Left>
              <Body>
                <Text style={{fontSize:responsiveFontSize(2.3),fontWeight:"bold"}}>
                    ₹ {this.state.rideDetails.ride_seat_price}/Seat
                </Text>
             </Body>
            </ListItem>
            <ListItem thumbnail>
              <Left>
                <Thumbnail square source={require('../../assets/seats.png')} 
                    style={{height:responsiveWidth(14),width:responsiveWidth(14)}}/>
              </Left>
              <Body>
                <Text style={{fontSize:responsiveFontSize(2.3),fontWeight:"bold",opacity:0.7}}>
                    Bkd/Avl/Off seats
                </Text>
                <Text style={{fontSize:responsiveFontSize(2.3),fontWeight:"bold"}}>
                    {this.state.rideDetails.ride_seats_offered - this.state.rideDetails.ride_seats_available}/ {this.state.rideDetails.ride_seats_available} / {this.state.rideDetails.ride_seats_offered}
                </Text>
             </Body>
            </ListItem>
            <Separator/>
            <ListItem last >
              <Body>
                <View style={{}}>
                    <View style={{marginLeft:responsiveWidth(1),marginBottom:responsiveHeight(3)}}>
                        <Text style={{fontSize:responsiveFontSize(2.5),fontWeight:"bold",}}>
                            { new  Date(this.state.rideDetails.ride_pick_timestamp*1),
                                date.toDateString().slice(4)}  ({ time = new Date(this.state.rideDetails.ride_pick_timestamp*1),
                                time.toTimeString().slice(0,5)})
                        </Text>
                    </View>
                    <View style={{flexDirection:"row",alignItems:"center"}}>
                        <Icon active name='map-marker-alt'  type="FontAwesome5" style={{color:"green",fontSize:responsiveFontSize(4)}}/>
                        <Text style={{fontSize:responsiveFontSize(2),fontWeight:"bold",opacity:0.7,marginLeft:responsiveWidth(3)}}>
                            {this.state.rideDetails.ride_pick_address}
                        </Text>
                    </View>
                    <View style={{flexDirection:"row",alignItems:"center",justifyContent:"space-between"}}>
                        <Icon active name='dots-vertical'  type="MaterialCommunityIcons" 
                                style={{color:"#000",marginLeft:responsiveWidth(-1),fontSize:responsiveFontSize(4)}}/>
                    </View>
                    <View style={{flexDirection:"row",alignItems:"center"}}>
                        <Icon active name='map-marker-alt'  type="FontAwesome5" style={{color:"red",fontSize:responsiveFontSize(4)}}/>
                        <Text style={{fontSize:responsiveFontSize(2),fontWeight:"bold",opacity:0.7,marginLeft:responsiveWidth(3)}}>
                            {this.state.rideDetails.ride_drop_address}
                        </Text>
                    </View>
                </View>
             </Body>
            </ListItem>
            <Separator/>
            <ListItem itemHeader>
              <Text style={{fontWeight:"bold",color:"#000",fontSize:responsiveFontSize(2.5)}}>Bill Details</Text>
            </ListItem>
            <ListItem>
                <Left>
                    <Text style={{fontWeight:"bold",color:"#000",opacity:0.7,fontSize:responsiveFontSize(2)}}>
                        Fare/Seat
                    </Text>
                </Left>
                <Body/>
                <Right>
                    <Text style={{fontWeight:"bold",color:"#000",opacity:0.7,fontSize:responsiveFontSize(2)}}>
                        ₹ {this.state.rideDetails.ride_seat_price}
                    </Text>
                </Right>
            </ListItem>
            <ListItem>
                <Left>
                    <Text style={{fontWeight:"bold",color:"#000",opacity:0.7,fontSize:responsiveFontSize(2)}}>
                        Seat Booked
                    </Text>
                </Left>
                <Body/>
                <Right>
                    <Text style={{fontWeight:"bold",color:"#000",opacity:0.7,fontSize:responsiveFontSize(2)}}>
                        0{this.state.rideDetails.ride_seats_offered - this.state.rideDetails.ride_seats_available}
                    </Text>
                </Right>
            </ListItem>           
            <ListItem>
                <Left>
                    <View>
                        <Text style={{fontWeight:"bold",color:"#000",opacity:0.7,fontSize:responsiveFontSize(2)}}>
                            Total Amount
                        </Text>
                        <Text style={{fontWeight:"bold",color:"#000",opacity:0.7,fontSize:responsiveFontSize(1.5)}}>
                            Includes all taxes.
                        </Text>
                    </View>
                </Left>
                <Right>
                    <View style={{flexDirection:"row"}}>
                        <Text style={{fontWeight:"bold",color:"#000",opacity:0.7,fontSize:responsiveFontSize(2)}}>
                            ₹ {(this.state.rideDetails.ride_seats_offered - this.state.rideDetails.ride_seats_available)*this.state.rideDetails.ride_seat_price}
                        </Text>
                    </View>
                </Right>
            </ListItem>
            <ListItem>
                <Left>
                    <View>
                        <Text style={{fontWeight:"bold",color:"#000",opacity:0.7,fontSize:responsiveFontSize(2)}}>
                            Payment
                        </Text>
                        <Text style={{fontWeight:"bold",color:"#000",opacity:0.7,fontSize:responsiveFontSize(1.5)}}>
                            Cash
                        </Text>
                    </View>
                </Left>
                <Right>
                    <View style={{flexDirection:"row"}}>
                        <Text style={{fontWeight:"bold",color:"#000",opacity:0.7,fontSize:responsiveFontSize(2)}}>
                            ₹ {(this.state.rideDetails.ride_seats_offered - this.state.rideDetails.ride_seats_available)*this.state.rideDetails.ride_seat_price}
                        </Text>
                    </View>
                </Right>
            </ListItem>
            <Separator/>
            <ListItem last >
              <Body>
                <View style={{}}>
                    <View style={{marginLeft:responsiveWidth(1),marginBottom:responsiveHeight(1)}}>
                        <Text style={{fontSize:responsiveFontSize(2.5),fontWeight:"bold",}}>
                            Co-Passengers
                        </Text>
                    </View>
                    {this.state.rideDetails.alreadyBookings.map((item,i)=>(
                        <View key ={i} style={{backgroundColor:"#e6e6e6",
                                margin:responsiveWidth(2),padding:responsiveWidth(2)}}>
                            <View style={{flexDirection:"row",alignItems:"center",justifyContent:"space-between"}}>
                                <View style={{flexDirection:"row"}}>
                                    { (item.booking_profile_pic != "") &&
                                        <Thumbnail source={{uri:item.booking_profile_pic}}
                                            style={{height:responsiveWidth(12),width:responsiveWidth(12)}}/>
                                    }
                                    { (item.booking_profile_pic == "") &&
                                        <Thumbnail source={{uri:this.state.photoUrl}}
                                            style={{height:responsiveWidth(12),width:responsiveWidth(12)}}/>
                                    }
                                    <View>
                                        <Text style={{marginLeft:responsiveWidth(3),fontSize:responsiveFontSize(2.3),fontWeight:"bold"}}>
                                            {item.booking_username}
                                        </Text>
                                        <Text style={{marginLeft:responsiveWidth(3),fontSize:responsiveFontSize(2),fontWeight:"bold",opacity:0.7}}>
                                            {item.booking_contact}
                                        </Text>
                                    </View>
                                </View>
                                <View style={{flexDirection:"row",alignItems:"center"}}>
                                    <View style={{alignItems:"center",marginRight:responsiveWidth(3)}}>
                                        <Text style={{marginLeft:responsiveWidth(3),fontSize:responsiveFontSize(2),fontWeight:"bold",opacity:0.5}}>
                                            Bkd. Seats
                                        </Text> 
                                        <Text style={{marginLeft:responsiveWidth(3),fontSize:responsiveFontSize(2.3),fontWeight:"bold"}}>
                                            0{item.booking_seats}
                                        </Text>
                                    </View>
                                </View>
                            </View>
                            <View style={{flexDirection:"row",alignItems:"center",justifyContent:"space-between"}}>
                                <View style={{justifyContent:"space-between",margin:responsiveWidth(5)}}>
                                    <View style={{flexDirection:"row",alignItems:"center"}}>
                                        <Icon active name='map-marker-alt'  type="FontAwesome5" style={{color:"green",fontSize:responsiveFontSize(3)}}/>
                                        <Text style={{fontSize:responsiveFontSize(1.5),fontWeight:"bold",opacity:0.7,marginLeft:responsiveWidth(3)}}>
                                            {item.booking_src_city}
                                        </Text>
                                    </View>
                                    <View style={{flexDirection:"row",alignItems:"center",justifyContent:"space-between"}}>
                                        <Icon active name='dots-vertical'  type="MaterialCommunityIcons" 
                                                style={{color:"#000",marginLeft:responsiveWidth(-1),fontSize:responsiveFontSize(3)}}/>
                                    </View>
                                    <View style={{flexDirection:"row",alignItems:"center"}}>
                                        <Icon active name='map-marker-alt'  type="FontAwesome5" style={{color:"red",fontSize:responsiveFontSize(3)}}/>
                                        <Text style={{fontSize:responsiveFontSize(1.5),fontWeight:"bold",opacity:0.7,marginLeft:responsiveWidth(3)}}>
                                            {item.booking_dest_city}
                                        </Text>
                                    </View>
                                </View>
                            </View>
                        </View>
                        
                    ))}
                </View>
             </Body>
            </ListItem>
            <Separator/>
            </View>
            </Content>
        </View>

        }
        </>
        </TouchableWithoutFeedback>
        </Container>
        <Loader animation="fade"
                visible={this.state.showLoader}
                textContent={'Loading...'}
                textStyle={{color:"#fff"}}
              />
      </View> 
      </Modal>
)}
} 




