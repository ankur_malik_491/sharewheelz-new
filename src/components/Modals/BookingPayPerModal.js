import React from 'react';
import {Text, View,TouchableOpacity,TouchableWithoutFeedback,Keyboard,ImageBackground,Modal,Image,StyleSheet} from 'react-native';
import { Container,  Icon,Segment,Button ,Content,DatePicker} from 'native-base';
import {responsiveWidth,responsiveHeight,responsiveFontSize} from "react-native-responsive-dimensions";
import Loader from 'react-native-loading-spinner-overlay';

export default class BookingPayPerModal extends React.Component {
  constructor(props){
    super(props);
    
     var todayDate=new Date();
     var tommorowDate = new Date();
    this.state = {
                   modalVisible:true ,
                   activePage:this.props.activeTab,
                   activeSubPage:0,
                   openview:false,
                   openviewSub2:false,
                   rooms:1,
                   adults:1,
                   childs:0,
                   currentDate:todayDate,
                   nextDate: tommorowDate,
                   roomDetails:this.props.roomDetails,


    };
    //this.setNewDate = this.setNewDate.bind(this);
    
    
  }
  setNewDate= (newDate)=> {
    this.setState({currentDate:newDate})
  }
  setNewDate1= (newDate)=> {
    this.setState({nextDate:newDate})
  }
  setSelectValue= ()=> {
      const {currentDate,nextDate,rooms,adults,childs,activeSubPage,modalVisible} = this.state;
      if(currentDate == "" || nextDate == ""){
          alert("Please select a valid date!")
      }
      else if (activeSubPage == ""){
        //console.log(activeSubPage)
        alert("Please select a Rooms first!")
        this.setState({activePage:2})
      }
      else{
        const night = new Date(nextDate).getDate() - new Date(currentDate).getDate();
        const roomID = activeSubPage-1;
        this.props.onAction(modalVisible,currentDate,nextDate,night,rooms,adults,childs,roomID)

      }
  }
  setAdults = (val) =>{
      let a = this.state.adults;
      if(val=="inc" && a<this.state.roomDetails[this.state.activeSubPage-1].max_adult){
        a = a+1;
          this.setState({adults:a})
      }
      else if(val=="dec" && a>1){
        a = a-1;
          this.setState({adults:a})
      }
      else if(a==this.state.roomDetails[this.state.activeSubPage-1].max_adult){
          alert("Max Adults allowed for this room: " + a)
      }
      //console.log(a)

  }
  setChilds = (val) =>{
    let c = this.state.childs;
    if(val=="inc" && c<this.state.roomDetails[this.state.activeSubPage-1].max_child){
      c = c+1;
        this.setState({childs:c})
    }
    else if(val=="dec" && c>0){
      c = c-1;
      this.setState({childs:c})
    }
    else if(c==this.state.roomDetails[this.state.activeSubPage-1].max_child){
        alert("Max Childs allowed for this room: " + c)
    }
    //console.log(c)
}
setRooms = (val) =>{
    let r = this.state.rooms;
    if(val=="inc" && r<this.state.roomDetails[this.state.activeSubPage-1].available_rooms){
      r = r+1;
        this.setState({rooms:r})
    }
    else if(val=="dec" && r>1){
      r = r-1;
      this.setState({rooms:r})
    }
    else if(r==this.state.roomDetails[this.state.activeSubPage-1].available_rooms){
        alert("Max Childs allowed for this room: " + r)
    }
    
    //console.log(c)

}
  selectComponent = (activePage) => () => this.setState({activePage})
  selectSubComponent = (activeSubPage) => () => this.setState({activeSubPage:activeSubPage,rooms:1,adults:1,childs:0})
  _renderComponent = () => {
    if(this.state.activePage === 1)
      return (
          <View style={{flex:1,marginTop:responsiveHeight(3),alignItems:"center"}}>
              <Segment style={{backgroundColor: "#fff",height:"auto",}}>
                        <View style={{flexDirection:"row"}}>
                            <TouchableOpacity 
                                style={{flexDirection:"row",alignItems:"center",justifyContent:"space-between",padding:responsiveHeight(2),
                                height:"auto",backgroundColor:"#fff",borderWidth:1,borderColor:"#e6e6e6",width:responsiveWidth(40),
                                borderRadius:responsiveWidth(4)}}>
                                <Icon name="ios-calendar" style={{color:"green",fontSize:responsiveFontSize(4)}}/>
                                <DatePicker
                                    defaultDate={this.state.currentDate}
                                    minimumDate={new Date()}
                                    locale={"en"}
                                    timeZoneOffsetInMinutes={undefined}
                                    modalTransparent={false}
                                    animationType={"fade"}
                                    androidMode={"default"}
                                    onDateChange={this.setNewDate}
                                    disabled={false}
                                />
                            </TouchableOpacity>
                            <View style={{alignItems:"center",justifyContent:"center",marginHorizontal:responsiveWidth(3)}}>
                                <Icon name="md-arrow-round-forward" style={{color:"#000",fontSize:responsiveFontSize(4)}}/>
                            </View>
                            <TouchableOpacity 
                                style={{flexDirection:"row",alignItems:"center",justifyContent:"space-between",padding:responsiveHeight(2),
                                height:"auto",backgroundColor:"#fff",borderWidth:1,borderColor:"#e6e6e6",width:responsiveWidth(40),
                                borderRadius:responsiveWidth(4)}}>
                                <Icon name="ios-calendar" style={{color:"red",fontSize:responsiveFontSize(4)}}/>
                                <DatePicker
                                    defaultDate={this.state.nextDate}
                                    minimumDate={this.state.currentDate}
                                    locale={"en"}
                                    timeZoneOffsetInMinutes={undefined}
                                    modalTransparent={false}
                                    animationType={"fade"}
                                    androidMode={"default"}
                                    onDateChange={this.setNewDate1}
                                    disabled={false}
                                />
                            </TouchableOpacity>
                        </View>
                    </Segment>
          </View>
      )
    else 
      return (
        <View style={{flex:1,marginTop:responsiveHeight(3),alignItems:"center",}}>
            <Segment style={{backgroundColor: "#fff",height:"auto",}}>
                <View style={{flexDirection:"row"}}>
                    
                {this.state.roomDetails.length>0 &&
                this.state.roomDetails.map((item,i)=>(
                    <TouchableOpacity key={i} onPress={this.selectSubComponent(i+1)}
                            style={{alignItems:"center",justifyContent:"space-between",padding:responsiveHeight(2),
                            height:"auto",backgroundColor:this.state.activeSubPage==i+1?"#da4453":"#fff",borderWidth:1,borderColor:"#e6e6e6",width:responsiveWidth(40),
                            borderTopLeftRadius:i==0?responsiveWidth(4):0,borderBottomLeftRadius:i==0?responsiveWidth(4):0,
                            borderTopRightRadius:i==this.state.roomDetails.length?responsiveWidth(4):0,borderBottomRightRadius:i==this.state.roomDetails.length?responsiveWidth(4):0}}>
                        <Text style={{fontSize:responsiveFontSize(2.3),color:this.state.activeSubPage==i+1?"#fff":"#000",textAlign:"center"}}>
                            {item.room_type}
                        </Text>
                        <Text style={{fontWeight:"bold",fontSize:responsiveFontSize(2.3),color:this.state.activeSubPage==i+1?"#fff":"#000",textAlign:"center",marginTop:responsiveHeight(1)}}>
                            ₹ {item.pay_per_hour*item.same_day_price/100} / Hour
                        </Text>
                    </TouchableOpacity>
                ))}
                    {/* <TouchableOpacity onPress={this.selectSubComponent(2)}
                            style={{alignItems:"center",justifyContent:"space-between",padding:responsiveHeight(2),
                            height:"auto",backgroundColor:this.state.activeSubPage==2?"#da4453":"#fff",borderWidth:1,borderColor:"#e6e6e6",width:responsiveWidth(40),
                            borderTopRightRadius:responsiveWidth(4),borderBottomRightRadius:responsiveWidth(4)}}>
                        <Text style={{fontSize:responsiveFontSize(2.3),color:this.state.activeSubPage==2?"#fff":"#000",textAlign:"center"}}>
                            classic (3x)
                        </Text>
                        <Text style={{fontWeight:"bold",fontSize:responsiveFontSize(2.3),color:this.state.activeSubPage==2?"#fff":"#000",textAlign:"center",marginTop:responsiveHeight(1)}}>
                            ₹ 3000
                        </Text>
                    </TouchableOpacity> */}
                </View>
            </Segment>
            <Content padder>
                {this._renderSubComponent()}
            </Content>
        </View>
      )
  }

  _renderSubComponent = () => {
    return (
        <View style={{flex:1}}>
            <View style={{display:this.state.activeSubPage?"flex":"none",flex:1,marginTop:responsiveHeight(3),width:responsiveWidth(90),borderWidth:1,borderColor:"#e6e6e6"}}>
                <TouchableOpacity onPress={()=>this.setState({openview:!this.state.openview})}>
                    <View style={{flexDirection:"row",alignItems:"center",justifyContent:"space-between",paddingHorizontal:responsiveWidth(5),
                                paddingVertical:responsiveHeight(2),borderBottomWidth:1,borderBottomColor:"#e6e6e6"}}>
                        <Text style={{fontSize:responsiveFontSize(2.5),fontWeight:"bold"}}>
                            {this.state.rooms} Room ({this.state.adults} Guest, {this.state.childs} Child)
                        </Text>
                        <Icon name="ios-arrow-down" style={{color:"#000",fontSize:responsiveFontSize(3)}}/>
                    </View>
                </TouchableOpacity>
                <View style={{display:this.state.openview?"flex":"none",flexDirection:"row",alignItems:"center",justifyContent:"space-between",marginHorizontal:responsiveWidth(5),
                            paddingVertical:responsiveHeight(2),borderBottomWidth:1,borderColor:"#e6e6e6"}}>
                    <View>
                        <Text style={{fontSize:responsiveFontSize(2.5),fontWeight:"bold",opacity:0.7}}>
                            Adults
                        </Text>
                        <Text style={{fontSize:responsiveFontSize(2),opacity:0.7}}>
                            (5+ years)
                        </Text>
                    </View>
                    <View style={{width:responsiveWidth(30),flexDirection:"row",alignItems:"center",justifyContent:"space-between"}}>
                        <View style={{backgroundColor:"#0246b1",paddingHorizontal:responsiveWidth(3),paddingVertical:responsiveHeight(1),
                                    marginRight:responsiveWidth(5),borderRadius:responsiveWidth(3)}}>
                            <Text style={{fontSize:responsiveFontSize(2.5),color:"#fff"}}>
                                {this.state.adults}
                            </Text>
                        </View>
                        <TouchableOpacity onPress={()=>this.setAdults("inc")}>
                            <Icon name="ios-add-circle" style={{color:"#da4453",fontSize:responsiveFontSize(4)}}/>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={()=>this.setAdults("dec")}>
                            <Icon name="ios-remove-circle" style={{color:"#da4453",fontSize:responsiveFontSize(4)}}/>
                        </TouchableOpacity>
                       
                    </View>
                </View>
                <View style={{display:this.state.openview?"flex":"none",flexDirection:"row",alignItems:"center",justifyContent:"space-between",marginHorizontal:responsiveWidth(5),
                            paddingVertical:responsiveHeight(2),borderBottomWidth:1,borderColor:"#e6e6e6"}}>
                    <View>
                        <Text style={{fontSize:responsiveFontSize(2.5),fontWeight:"bold",opacity:0.7}}>
                            Childs
                        </Text>
                        <Text style={{fontSize:responsiveFontSize(2),opacity:0.7}}>
                            (0-5 years)
                        </Text>
                    </View>
                    <View style={{width:responsiveWidth(30),flexDirection:"row",alignItems:"center",justifyContent:"space-between"}}>
                        <View style={{backgroundColor:"#0246b1",paddingHorizontal:responsiveWidth(3),paddingVertical:responsiveHeight(1),
                                    marginRight:responsiveWidth(5),borderRadius:responsiveWidth(3)}}>
                            <Text style={{fontSize:responsiveFontSize(2.5),color:"#fff"}}>
                                {this.state.childs}
                            </Text>
                        </View>
                        <TouchableOpacity onPress={()=>this.setChilds("inc")}>
                            <Icon name="ios-add-circle" style={{color:"#da4453",fontSize:responsiveFontSize(4)}}/>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={()=>this.setChilds("dec")}>
                            <Icon name="ios-remove-circle" style={{color:"#da4453",fontSize:responsiveFontSize(4)}}/>
                        </TouchableOpacity>
                       
                    </View>
                </View>
                <View style={{display:this.state.openview?"flex":"none",flexDirection:"row",alignItems:"center",justifyContent:"space-between",marginHorizontal:responsiveWidth(5),
                            paddingVertical:responsiveHeight(2)}}>
                    <View>
                        <Text style={{fontSize:responsiveFontSize(2.5),fontWeight:"bold",opacity:0.7,color:"red"}}>
                            Add Rooms
                        </Text>
                    </View>
                    <View style={{width:responsiveWidth(30),flexDirection:"row",alignItems:"center",justifyContent:"space-between"}}>
                        <View style={{backgroundColor:"#0246b1",paddingHorizontal:responsiveWidth(3),paddingVertical:responsiveHeight(1),
                                    marginRight:responsiveWidth(5),borderRadius:responsiveWidth(3)}}>
                            <Text style={{fontSize:responsiveFontSize(2.5),color:"#fff"}}>
                                {this.state.rooms}
                            </Text>
                        </View>
                        <TouchableOpacity onPress={()=>this.setRooms("inc")}>
                            <Icon name="ios-add-circle" style={{color:"#da4453",fontSize:responsiveFontSize(4)}}/>
                        </TouchableOpacity>
                        <TouchableOpacity onPress={()=>this.setRooms("dec")}>
                            <Icon name="ios-remove-circle" style={{color:"#da4453",fontSize:responsiveFontSize(4)}}/>
                        </TouchableOpacity>
                       
                    </View>
                </View>
            </View>
            
        </View>
      )
  }

 render(){
   //console.log(this.props.roomDetails)
 return (
    <Modal
    animationType="slide"
    transparent={true}
    visible={this.state.modalVisible}
    onRequestClose={() => this.props.onClose(this.state.modalVisible)}
  >
        <View style={{flex:1,backgroundColor:"#fff"}} >
            <Container >
                <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                <>
                    <View style={{flexDirection:"row",alignItems:"center",justifyContent:"space-between",borderBottomWidth:1,
                        borderBottomColor:"#a6a6a6",padding:responsiveWidth(3),height:"auto",backgroundColor: "#fff"}}>
                        <TouchableOpacity onPress={()=>this.props.onClose(this.state.modalVisible)}>
                            <Icon name='arrow-back'  style={{color:"#000",fontSize:responsiveFontSize(4)}}/>
                        </TouchableOpacity>
                        <TouchableOpacity style={{width:responsiveWidth(85)}}>
                            <Text style={{fontSize:responsiveFontSize(2.5),fontWeight:"bold"}}>
                                Select Rooms & Guests
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{backgroundColor: "#fff",marginTop:responsiveHeight(5),paddingHorizontal:responsiveWidth(5)}}>
                    <Segment style={{backgroundColor: "#fff",height:responsiveHeight(12),}}>
                        <View style={{flexDirection:"row"}}>
                            <TouchableOpacity onPress={this.selectComponent(1)}
                                style={{alignItems:"center",justifyContent:"center",paddingVertical:responsiveHeight(2),width:responsiveWidth(45),
                                height:responsiveHeight(10),backgroundColor:this.state.activePage==1?"#0246b1":"#e6e6e6",
                                borderTopLeftRadius:responsiveWidth(5),borderBottomLeftRadius:responsiveWidth(5)}}>
                                <Text style={{fontWeight:'bold',fontSize:responsiveFontSize(2.5),color:this.state.activePage==1?"#fff":"#0246b1",textAlign:"center"}}>
                                    Check-In & Check-Out
                                </Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={this.selectComponent(2)}
                                style={{alignItems:"center",justifyContent:"center",paddingVertical:responsiveHeight(2),width:responsiveWidth(45),
                                        height:responsiveHeight(10),backgroundColor:this.state.activePage==2?"#0246b1":"#e6e6e6",
                                        borderTopRightRadius:responsiveWidth(5),borderBottomRightRadius:responsiveWidth(5)}}>
                                <Text style={{fontWeight:'bold',fontSize:responsiveFontSize(2.5),color:this.state.activePage==2?"#fff":"#0246b1",textAlign:"center"}}>
                                    Rooms & Guests
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </Segment>
                    </View>
                    <View style={{flex:1}}>
                        {this._renderComponent()}
                    </View>
                    <View style={{alignItems:"center",justifyContent:"center",padding:responsiveWidth(3),height:"auto",
                                backgroundColor: "#fff",borderTopWidth:1,borderTopColor:"#e6e6e6"}}>
                        <TouchableOpacity onPress={() => this.setSelectValue()}
                            style={{alignItems:"center",justifyContent:"center",paddingVertical:responsiveHeight(2),width:responsiveWidth(80),
                            backgroundColor:this.state.loader?"grey":"#0246B1",borderRadius:responsiveWidth(5)}}>
                            <Text style={{fontWeight:'bold',fontSize:responsiveFontSize(2.5),color:'#fff',}}>
                                Done
                            </Text>
                        </TouchableOpacity>
                    </View>
                </>
                </TouchableWithoutFeedback>
            </Container>
            <Loader animation="fade"
                    visible={this.state.showLoader}
                    textContent={'Loading...'}
                    textStyle={{color:"#fff"}}
                />
        </View> 
    </Modal>
)}
} 
const styles = StyleSheet.create({
    segmentButton: {
      padding:10,
      borderColor:"#ccc",

    }
  })



