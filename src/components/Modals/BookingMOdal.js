import React from 'react';
import {Text, View,TouchableOpacity,TouchableWithoutFeedback,Keyboard,ImageBackground,Modal,Image,StyleSheet,FlatList} from 'react-native';
import { Container,  Icon,Segment,Button ,Content,DatePicker,Root} from 'native-base';
import {responsiveWidth,responsiveHeight,responsiveFontSize} from "react-native-responsive-dimensions";
import Loader from 'react-native-loading-spinner-overlay';
import {Calendar} from 'react-native-calendars';

var h,m;

export default class BookingModal extends React.Component {
  constructor(props){
    super(props);
    var todayDate=new Date();
    var nextDate = new Date(); 
    var time =  (("0" + todayDate.getHours()).slice(-2) + ":" + ("0" + todayDate.getMinutes()).slice(-2));
    nextDate.setHours(nextDate.getHours()+24,nextDate.getMinutes(),0,0);
    var sd = todayDate.getTime();
    var ed = nextDate.getTime();
    var start = (todayDate.getFullYear() + "-" + ("0"+(todayDate.getMonth()+1)).slice(-2) + "-" +("0"+(todayDate.getDate())).slice(-2));
    var end =(todayDate.getFullYear() + "-" + ("0"+(todayDate.getMonth()+1)).slice(-2) + "-" +("0"+(todayDate.getDate()+1)).slice(-2));
    //console.log(start + end)
    this.state = {
                   modalVisible:true ,
                   activePage:this.props.activeTab,
                   activeSubPage:0,
                   openview:null,
                   rooms:1,
                   Rooms:[],
                   adults:1,
                   Adults:[],
                   childs:0,
                   Childs:[],
                   roomDetails:this.props.roomDetails,
                   selectedRoomDetails:[],
                   startDateSelected:{date:start,[start]:{customStyles: {container: {backgroundColor: 'green'},text: {color: '#fff'}}}},
                   endDateSelected:{date:end,[end]:{customStyles: {container: {backgroundColor: 'red'},text: {color: '#fff'}}}},
                   showcalenderStart:false,
                   showcalenderEnd:false,
                   showTimeCalender:false,
                   startDate:sd,
                   endDate:ed,
                   startDateTime:time,
                   endDateTime:time,
                   defaultTimeDate:start,
                   timeData:[],
                   slcttimeColor:"",
                   Hours:[3,6,9,12],
                   activeHour:0,
                   dateChanged:false,
                };
                this.setTimeData();
}
setTimeData = () =>{
    var h = 0;
    var m = "00";
    this.state.timeData.push('00'+':'+"00");
    for(let i=1;i<48;i++){
        if(i%2==0){
            h = h+1;
            m = "00";
        }
        else{
            m = "30";
        }
        this.state.timeData.push(('0'+h).slice(-2)+':'+(m))
    }
    //console.log(this.state.timeData)
}
setDate = async(day) => {
     //console.log(this.state.showcalenderStart)
    if(this.state.showcalenderStart){
        await this.setState({showcalenderStart:!this.state.showcalenderStart})
        this.setState({
            startDateSelected:{ date:day.dateString,
                                [day.dateString]:{customStyles: {container: {backgroundColor: 'green'},
                                                                            text: {color: '#fff',}}
                            }}
            })
        this.setState({startDate:day.timestamp,dateChanged:true})  
        //console.log(day)
        this.setState({showcalenderEnd:!this.state.showcalenderEnd})
     }
     else if(this.state.showcalenderEnd){
        this.setState({showcalenderEnd:!this.state.showcalenderEnd})
        this.setState({
            endDateSelected:{date:day.dateString,
                            [day.dateString]:{customStyles: {container: {backgroundColor: 'red'},
                                                                        text: {color: '#fff',}}
                        }}
            })
        this.setState({endDate:day.timestamp,dateChanged:true})
     }
     else{
        this.setState({
            startDateSelected:{date:day.dateString,
                            [day.dateString]:{customStyles: {container: {backgroundColor: 'green'},
                                                                        text: {color: '#fff',}}
                        }}
            })
        
        //this.setState({showTimeCalender:!this.state.showTimeCalender})
     }
    
}
setTime = (time) =>{
    this.setState({startDateTime:time,slcttimeColor:time})
    this.setState({showTimeCalender:!this.state.showTimeCalender})
}
selectComponent = (activePage) => () => this.setState({activePage})
selectSubComponent = (activeSubPage) => async() => {
        await this.setState({activeSubPage:activeSubPage,rooms:1,adults:1,childs:0})
        if(this.state.Rooms.length==0){
            this.state.Rooms.push({rooms:1,adults:1,childs:0})
            this.setState({Rooms:this.state.Rooms})
            this.setDefaultValue()
        }
        else{
            this.setState({Rooms:[]})
            this.state.Rooms.push({rooms:1,adults:1,childs:0})
            this.setState({Rooms:this.state.Rooms})
            this.setDefaultValue()
            
        }
    //console.log(this.state.Rooms)
}
selectHourComponent = (activeHour) => async() => {
    const st = this.state.startDateTime.slice(0,2);
    const stm = this.state.startDateTime.slice(3);
    const et = parseInt(st)+parseInt(activeHour);
    var  endtime = this.state.endDateTime;
    if(et>24){
        const t = et - 24;
        const x = ("0"+t)
        if(stm>=30){
            
            endtime = x.slice(-2) + ':' +"30"
        }
        else{
            endtime = x.slice(-2) + ':' +"00"
        }
        const sd = this.state.startDateSelected.date.split('-');
        var sy = sd[0];
        var sm= sd[1];
        var sdd =sd[2];
        if(sm=="01" ||sm=="03"|| sm=="05" || sm=="07" || sm=="08" || sm=="10"){
            const d = sdd
            if(d==31){
                sm = parseInt(sm)+parseInt(1);
                sdd = "01";
            }
            else{
                sdd = parseInt(sdd)+parseInt(1);
            }
        }
        else if(sm=="04" || sm=="06"|| sm=="09" || sm=="11"){
            const d = sdd
            if(d==30){
                sm = parseInt(sm)+parseInt(1);
                sdd = "01";
            }
            else{
                sdd = parseInt(sdd)+parseInt(1);
            }
        }
        else if(sm=="12"){
            const d = sdd
            if(d==31){
                sy = parseInt(sy)+parseInt(1);;
                sm = parseInt(sm)+parseInt(1);
                sdd = "01";
            }
            else{
                sdd = parseInt(sdd)+parseInt(1);
            }

        }
        else{
            const d = sdd
            if(d==29){
                sm = parseInt(sm)+parseInt(1);
                sdd = "01";
            }
            else{
                sdd = parseInt(sdd)+parseInt(1);
            }
        }
       enddate= sy+'-'+sm+'-'+sdd; 
        
    }
    else{
        if(stm>30){
            endtime = et + ':' +"30"
        }
        else{
            endtime = et + ':' +"00"
        }
        const sd = this.state.startDateSelected.date.split('-');
        var sy = sd[0];
        var sm= sd[1];
        var sdd =sd[2];
        if(sm=="01" ||sm=="03"|| sm=="05" || sm=="07" || sm=="08" || sm=="10"){
            const d = sdd
            if(d==31){
                sm = parseInt(sm)+parseInt(1);
                sdd = "01";
            }
            else{
                sdd = parseInt(sdd);
            }
        }
        else if(sm=="04" || sm=="06"|| sm=="09" || sm=="11"){
            const d = sdd
            if(d==30){
                sm = parseInt(sm)+parseInt(1);
                sdd = "01";
            }
            else{
                sdd = parseInt(sdd);
            }
        }
        else if(sm=="12"){
            const d = sdd
            if(d==31){
                sy = parseInt(sy)+parseInt(1);;
                sm = parseInt(sm)+parseInt(1);
                sdd = "01";
            }
            else{
                sdd = parseInt(sdd);
            }

        }
        else{
            const d = sdd
            if(d==29){
                sm = parseInt(sm)+parseInt(1);
                sdd = "01";
            }
            else{
                sdd = parseInt(sdd);
            }
        }
       enddate= sy+'-'+sm+'-'+sdd;
    }
    //console.log(enddate)
    this.setState({activeHour:activeHour,endDateTime:endtime})
    await this.setState({
        endDateSelected:{date:enddate,
                        [enddate]:{customStyles: {container: {backgroundColor: 'green'},
                                                                    text: {color: '#fff',}}
                    }}
        })
}
setDefaultValue = ()=> {
    //this.setState({openview:!this.state.openview})
    const ma= this.props.roomDetails[this.state.activeSubPage-1].max_adult
    const mc= this.props.roomDetails[this.state.activeSubPage-1].max_child
    if(this.state.Adults.length<ma){
        for(let i = 1;i<=ma;i++){
            this.state.Adults.push(i)
        }
    }
    if(this.state.Childs.length<mc){
        for(let i = 0;i<=mc;i++){
            this.state.Childs.push(i)
        }
    }
    //console.log(this.state.Adults)
  }
expandView = (i) => {
    const r = this.state.rooms[i]
    this.setState({openview:i})
}
setGuest = async (val,g,i) =>{
    const ma= this.props.roomDetails[this.state.activeSubPage-1].max_adult
    const mc= this.props.roomDetails[this.state.activeSubPage-1].max_child
    if(val=="adults" && g<=ma){
         await this.setState({adults:g})
    }
    if(val=="childs" && g<=mc){
        await this.setState({childs:g})
   }
    await this.state.Rooms.splice(i,1,{rooms:1,adults:this.state.adults,childs:this.state.childs})
    this.setState({Rooms:this.state.Rooms})
}
AddRoom = async()=> {
   const mr= this.props.roomDetails[this.state.activeSubPage-1].available_rooms
   if(this.state.rooms<mr){
    await this.setState({rooms:this.state.rooms+1,adults:1,childs:0})
    await this.state.Rooms.push({rooms:1,adults:this.state.adults,childs:this.state.childs})
   }
   this.setState({Rooms:this.state.Rooms})
   //console.log(this.state.Rooms)
}
RemoveRoom = async()=> {
    if(this.state.rooms>0){
     await this.setState({rooms:this.state.rooms-1,adults:1,childs:0})
     await this.state.Rooms.pop();
    }
    this.setState({Rooms:this.state.Rooms})
}
setSelectValue = async()=> {
    const {startDateSelected,endDateSelected,startDateTime,endDateTime,activeSubPage,activeHour,modalVisible,} = this.state;
    if(this.props.bookingType=="normal") {
        if(startDateSelected == "" || endDateSelected == ""){
          alert("Please select a valid date!")
        }
        else if (startDateSelected.date == endDateSelected.date ){
            alert("Check-in date & Check-out Date can't be same!")
        
        }
        else if (activeSubPage == ""){
            alert("Please select a Rooms first!")
            this.setState({activePage:2})
        }
        else{
            var tRooms = this.state.Rooms;
            var tRoomsLeng = this.state.Rooms.length;
            var tAdults = 0;
            var tChilds =0;
            for(let i=0;i<tRooms.length;i++){
                tAdults = tAdults + this.state.Rooms[i].adults
            }
            for(let i=0;i<tRooms.length;i++){
                tChilds = tChilds + this.state.Rooms[i].childs
            }
            const ed = new Date(this.state.endDate).getDate();
            const sd = new Date(this.state.startDate).getDate();
            var nights = 0;
            if(ed>sd){
                nights =  ed - sd;
            }
            else{ 
                const em = new Date(this.state.startDate).getMonth()+1;
                if(em=="1" || em=="3"|| em=="5" || em=="7" || em=="8" || em=="10" ||em=="12"){
                    nights =  (31 - sd)+ed;
                }
                else if(em=="4" || em=="6"|| em=="9" || em=="11"){
                    nights =  (30 - sd)+ed;
                }
                else{
                    nights =  (29 - sd)+ed;
                }
                
            }
            const roomID = this.state.roomDetails[activeSubPage-1].id;
            const roomType = this.state.roomDetails[activeSubPage-1].room_type;
            await this.setState({selectedRoomDetails:{srtDate:startDateSelected.date,
                                                      endDate:endDateSelected.date,
                                                      tRooms:tRooms,
                                                      tRoomsLeng:tRoomsLeng,
                                                      tAdults:tAdults,
                                                      tChilds:tChilds,
                                                      tNights:nights,
                                                      roomID:roomID,
                                                      roomType:roomType
                                                }})
            this.props.onAction(modalVisible,this.state.selectedRoomDetails)
        }
    }
    else{
        if(startDateSelected == "" || startDateTime == "" ){
            alert("Please select a valid date and time!")
        }
        else if(activeHour == ""){
            alert("Please select valid hours!")
        }
        else if (activeSubPage == ""){
              alert("Please select a Rooms first!")
              this.setState({activePage:2})
        }
        else{
              var tRooms = this.state.Rooms;
              var tRoomsLeng = this.state.Rooms.length;
              var tAdults = 0;
              var tChilds =0;
              for(let i=0;i<tRooms.length;i++){
                  tAdults = tAdults + this.state.Rooms[i].adults
              }
              for(let i=0;i<tRooms.length;i++){
                  tChilds = tChilds + this.state.Rooms[i].childs
              }
              const roomID = this.state.roomDetails[activeSubPage-1].id;
              const roomType = this.state.roomDetails[activeSubPage-1].room_type;
              await this.setState({selectedRoomDetails:{srtDate:startDateSelected.date,
                                                        endDate:endDateSelected.date,
                                                        startTime:startDateTime,
                                                        endTime:endDateTime,
                                                        tRooms:tRooms,
                                                        tRoomsLeng:tRoomsLeng,
                                                        tAdults:tAdults,
                                                        tChilds:tChilds,
                                                        tHours:activeHour,
                                                        roomID:roomID,
                                                        roomType:roomType
                                                }})
              this.props.onAction(modalVisible,this.state.selectedRoomDetails)
          }
    }
  }
  
_renderTimeItem = ({item}) => (
    h = new Date(this.state.startDate).getHours(),
    m = new Date(this.state.startDate).getHours(),
    <TouchableOpacity
        onPress={()=>{this.setTime(item)}}
        style={{backgroundColor:this.state.slcttimeColor==item?"green":"#fff"}}
    >
      {(h<=item.slice(0,2)) && 
            <View style={{alignItems:"center",justifyContent:"center",borderBottomWidth:1,
                borderBottomColor:"#ccc",paddingHorizontal:responsiveWidth(5),paddingVertical:responsiveHeight(1),height:"auto"}}>
                <Text style={{fontSize:responsiveFontSize(2),fontWeight:"bold",
                                color:"#000"}}>
                    {item}
                </Text>
            </View>
      }
      {(this.state.startDateSelected.date != this.state.defaultTimeDate) &&
            <View style={{alignItems:"center",justifyContent:"center",borderBottomWidth:1,
                borderBottomColor:"#ccc",paddingHorizontal:responsiveWidth(5),paddingVertical:responsiveHeight(1),height:"auto"}}>
                <Text style={{fontSize:responsiveFontSize(2),fontWeight:"bold",
                                color:"#000"}}>
                    {item}
                </Text>
            </View>
      }
    </TouchableOpacity>
    
  ) 
 
  _renderComponent = () => {
      //console.log(this.state.currentDate)
    if(this.state.activePage === 1)
      return (
          <View style={{flex:1,marginTop:responsiveHeight(3),alignItems:"center"}}>
              <Segment style={{backgroundColor: "#fff",height:"auto",}}>
                    { this.props.bookingType=="normal"  &&
                        <View style={{flexDirection:"row"}}>
                            <TouchableOpacity  onPress={()=>this.setState({showcalenderStart:!this.state.showcalenderStart,showcalenderEnd:false})}
                                style={{flexDirection:"row",alignItems:"center",justifyContent:"space-between",padding:responsiveHeight(1),
                                height:"auto",backgroundColor:"#fff",borderWidth:1,borderColor:"#e6e6e6",width:"auto",
                                borderRadius:responsiveWidth(4)}}>
                                <Icon name="ios-calendar" style={{color:"green",fontSize:responsiveFontSize(4)}}/>
                               <Text style={{fontSize:responsiveFontSize(2.3),fontWeight:"bold",marginLeft:responsiveWidth(2)}}>
                                    {this.state.startDateSelected.date}
                                </Text> 
                            </TouchableOpacity>
                            <View style={{alignItems:"center",justifyContent:"center",marginHorizontal:responsiveWidth(3)}}>
                                <Icon name="md-arrow-round-forward" style={{color:"#000",fontSize:responsiveFontSize(4)}}/>
                            </View>
                            <TouchableOpacity onPress={()=>this.setState({showcalenderEnd:!this.state.showcalenderEnd,showcalenderStart:false})}
                                style={{flexDirection:"row",alignItems:"center",justifyContent:"space-between",padding:responsiveHeight(1),
                                height:"auto",backgroundColor:"#fff",borderWidth:1,borderColor:"#e6e6e6",width:"auto",
                                borderRadius:responsiveWidth(4)}}>
                                <Icon name="ios-calendar" style={{color:"red",fontSize:responsiveFontSize(4)}}/>
                                <Text style={{fontSize:responsiveFontSize(2.3),fontWeight:"bold",marginLeft:responsiveWidth(2)}}>
                                    {this.state.endDateSelected.date}
                                </Text>
                            </TouchableOpacity>
                         
                        </View>
                    }
                    { this.props.bookingType=="section"  &&
                        <View style={{flexDirection:"row"}}>
                            <TouchableOpacity onPress={()=>this.setState({showcalenderEnd:false,showcalenderStart:false,showTimeCalender:!this.state.showTimeCalender})}
                                style={{flexDirection:"row",alignItems:"center",justifyContent:"center",padding:responsiveHeight(1),
                                height:"auto",backgroundColor:"#f2f2f2",borderWidth:1,borderColor:"#e6e6e6",width:"auto",
                                borderRadius:responsiveWidth(4)}}>
                                <Icon name="ios-clock" style={{color:"green",fontSize:responsiveFontSize(4)}}/>
                                <Text style={{fontSize:responsiveFontSize(2.5),fontWeight:"bold",marginLeft:responsiveWidth(3)}}>
                                    {this.state.startDateSelected.date}  {this.state.startDateTime}
                                </Text>
                                
                            </TouchableOpacity>
                            
                        </View>
                    }
                </Segment>
                <Content padder>
                {(this.state.showcalenderStart || this.state.showcalenderEnd || this.state.showTimeCalender) &&
                <View style={{marginTop:responsiveHeight(-4)}}>
                    <View style={{justifyContent:"space-between",flexDirection:"row",paddingHorizontal:responsiveWidth(15),alignItems:"center"}}>
                        {this.state.showcalenderStart &&
                            <Icon name="md-arrow-dropup" style={{fontSize:responsiveFontSize(6),color:"#ccc"}}/>
                        }
                        {this.state.showcalenderEnd &&
                        <>
                            <Icon/>
                            <Icon name="md-arrow-dropup" style={{fontSize:responsiveFontSize(6),color:"#ccc"}}/>
                        </>
                        }
                        {this.state.showTimeCalender &&
                            <Icon name="md-arrow-dropup" style={{fontSize:responsiveFontSize(6),color:"#ccc",marginLeft:responsiveWidth(20)}}/>
                        }
                    </View>
                    <View style={{flexDirection:"row",marginTop:responsiveHeight(-2.5),}}>
                        <Calendar
                            style={{height:"auto",width:this.state.showTimeCalender?responsiveWidth(70):responsiveWidth(90),
                                    borderWidth:1}}
                            current={new Date()}
                            minDate={new Date()}
                            disabledDaysIndexes={[0, 6]}
                            markingType={'custom'}
                            onDayPress={(day) => {this.setDate(day)}}
                            hideExtraDays={true}
                            disableMonthChange={true}
                            disableAllTouchEventsForDisabledDays={true}
                            markedDates={((this.state.showcalenderStart || this.state.showTimeCalender) && this.state.startDateSelected) || 
                                        ((this.state.showcalenderEnd) && this.state.endDateSelected) }
                            theme={{
                                backgroundColor: '#fff',
                                calendarBackground: '#fff',
                                dayTextColor: '#000',
                                textDisabledColor:"#a6a6a6",
                                arrowColor: '#000',
                                disabledArrowColor: '#d9e1e8',
                                // //textDayFontWeight: 'bold',
                            
                            'stylesheet.calendar.header': {
                                week: {
                                flexDirection: 'row',
                                justifyContent: 'space-between',
                                backgroundColor:"#f2f2f2",
                                },
                                header: {
                                    flexDirection: 'row',
                                    justifyContent: 'space-between',
                                    alignItems: 'center',
                                    backgroundColor:"#f2f2f2"
                                },
                                monthText: {
                                    fontSize: responsiveFontSize(2.3),
                                    fontWeight: "bold",
                                    color: "#000",
                                },
                                dayHeader: {
                                    margin:responsiveWidth(1),
                                    textAlign: 'center',
                                    fontSize: responsiveFontSize(2),
                                    fontWeight: "bold",
                                    color: "#000"
                                },
                                disabledDayHeader: {
                                    margin:responsiveWidth(1),
                                    textAlign: 'center',
                                    fontSize: responsiveFontSize(2),
                                    fontWeight: "bold",
                                    color: "#000"
                                },
                                }
                            }}
                        />
                       { this.state.showTimeCalender &&
                            <View style={{borderWidth:1,borderColor:"#ccc",height:"auto"}}>
                                <FlatList
                                    data={this.state.timeData}
                                    renderItem={this._renderTimeItem}
                                    keyExtractor={item => item}
                                />
                            </View>
                        }
                        
                    </View>
                </View>
                }
                </Content>
                {this.props.bookingType=="section"  &&
                <Segment style={{backgroundColor: "#fff",height:"auto",marginBottom:responsiveHeight(2)}}>
                    <View style={{flexDirection:"row"}}>
                    {this.state.Hours.length>0 &&
                    this.state.Hours.map((item,i)=>(
                        <TouchableOpacity key={i} onPress={this.selectHourComponent((i+1)*3)}
                                style={{alignItems:"center",justifyContent:"space-between",padding:responsiveHeight(1),paddingHorizontal:responsiveWidth(3),
                                height:"auto",backgroundColor:this.state.activeHour==((i+1)*3)?"#da4453":"#fff",borderWidth:1,borderColor:"#e6e6e6",width:"auto",
                                borderTopLeftRadius:i==0?responsiveWidth(4):0,borderBottomLeftRadius:i==0?responsiveWidth(4):0,
                                borderTopRightRadius:i==this.state.Hours.length-1?responsiveWidth(4):0,borderBottomRightRadius:i==this.state.Hours.length-1?responsiveWidth(4):0}}>
                            <Text style={{fontSize:responsiveFontSize(2.3),color:this.state.activeHour==((i+1)*3)?"#fff":"#000",textAlign:"center"}}>
                                {item} Hours
                            </Text>
                        </TouchableOpacity>
                    ))}
                    </View>
                </Segment>
                }     
          </View>
      )
    else 
      return (
        <View style={{flex:1,marginTop:responsiveHeight(3),alignItems:"center",}}>
            <Segment style={{backgroundColor: "#fff",height:"auto",}}>
                <View style={{flexDirection:"row"}}>
                    
                {this.state.roomDetails.length>0 &&
                this.state.roomDetails.map((item,i)=>(
                    <TouchableOpacity key={i} onPress={this.selectSubComponent(i+1)}
                            style={{alignItems:"center",justifyContent:"space-between",padding:responsiveHeight(1),paddingHorizontal:responsiveWidth(3),
                            height:"auto",backgroundColor:this.state.activeSubPage==i+1?"#da4453":"#fff",borderWidth:1,borderColor:"#e6e6e6",width:"auto",
                            borderTopLeftRadius:i==0?responsiveWidth(4):0,borderBottomLeftRadius:i==0?responsiveWidth(4):0,
                            borderTopRightRadius:i==this.state.roomDetails.length-1?responsiveWidth(4):0,borderBottomRightRadius:i==this.state.roomDetails.length-1?responsiveWidth(4):0}}>
                        <Text style={{fontSize:responsiveFontSize(2.3),color:this.state.activeSubPage==i+1?"#fff":"#000",textAlign:"center"}}>
                            {item.room_type}
                        </Text>
                        {this.props.bookingType=="normal" && !this.state.dateChanged &&
                            <Text style={{fontWeight:"bold",fontSize:responsiveFontSize(2.3),color:this.state.activeSubPage==i+1?"#fff":"#000",textAlign:"center",marginTop:responsiveHeight(1)}}>
                                ₹ {item.same_day_price}
                            </Text>
                        }
                        {this.props.bookingType=="normal" && this.state.dateChanged &&
                            <Text style={{fontWeight:"bold",fontSize:responsiveFontSize(2.3),color:this.state.activeSubPage==i+1?"#fff":"#000",textAlign:"center",marginTop:responsiveHeight(1)}}>
                                ₹ {item.price}
                            </Text>
                        }
                        {this.props.bookingType=="section" &&
                            <Text style={{fontWeight:"bold",fontSize:responsiveFontSize(2.3),color:this.state.activeSubPage==i+1?"#fff":"#000",textAlign:"center",marginTop:responsiveHeight(1)}}>
                                ₹ {item.same_day_price*item.pay_per_hour/100}
                            </Text>
                        }
                    </TouchableOpacity>
                ))}
                {this.state.roomDetails == 0 && 
                    <TouchableOpacity 
                            style={{alignItems:"center",justifyContent:"space-between",padding:responsiveHeight(5),backgroundColor:"#fff",}}>
                        <Text style={{fontSize:responsiveFontSize(2.5),color:"#000",textAlign:"center",fontWeight:"bold"}}>
                            Sorry no rooms found vacant for this hotel!
                        </Text>
                    </TouchableOpacity>}
                </View>
            </Segment>
            <Content padder>
                {this._renderSubComponent()}
            </Content>
        </View>
      )
  }

  _renderSubComponent = () => {
    return (
        <View style={{flex:1}}>
            {this.state.Rooms.length>0 &&
                this.state.Rooms.map((item,i)=>(
            <View key={i} style={{display:"flex",flex:1,marginTop:responsiveHeight(3),width:responsiveWidth(90),borderWidth:1,borderColor:"#e6e6e6"}}>
                <TouchableOpacity onPress={()=>this.expandView(i)}>
                    <View style={{flexDirection:"row",alignItems:"center",justifyContent:"space-between",paddingHorizontal:responsiveWidth(5),
                                paddingVertical:responsiveHeight(2),borderBottomWidth:1,borderBottomColor:"#e6e6e6"}}>
                        <Text style={{fontSize:responsiveFontSize(2.5),fontWeight:"bold"}}>
                            Room {i+1} ({item.adults} Guest, {item.childs} Child)
                        </Text>
                        <Icon name="ios-arrow-down" style={{color:"#000",fontSize:responsiveFontSize(3)}}/>
                    </View>
                </TouchableOpacity>
                <View style={{display:this.state.openview==i?"flex":"none",flexDirection:"row",alignItems:"center",justifyContent:"space-between",marginHorizontal:responsiveWidth(5),
                            paddingVertical:responsiveHeight(2),borderBottomWidth:1,borderColor:"#e6e6e6"}}>
                    <View>
                        <Text style={{fontSize:responsiveFontSize(2.5),fontWeight:"bold",opacity:0.7}}>
                            Adults 
                        </Text>
                        <Text style={{fontSize:responsiveFontSize(2),opacity:0.7}}>
                            (5+ years)
                        </Text>
                    </View>
                    <View style={{width:responsiveWidth(50),flexDirection:"row",alignItems:"center",}}>
                    {this.state.Adults.length>0 &&
                        this.state.Adults.map((itmk,k)=>(
                            <TouchableOpacity key={k} onPress={()=>this.setGuest("adults",k+1,i)}
                                    style={{alignItems:"center",justifyContent:"space-between",padding:responsiveHeight(1),paddingHorizontal:responsiveWidth(3),
                                    height:"auto",backgroundColor:item.adults==k+1?"#0246b1":"#fff",borderWidth:1,borderColor:"#e6e6e6",width:"auto",
                                    borderTopLeftRadius:k==0?responsiveWidth(4):0,borderBottomLeftRadius:k==0?responsiveWidth(4):0,
                                    borderTopRightRadius:k==this.state.Adults.length-1?responsiveWidth(4):0,borderBottomRightRadius:k==this.state.Adults.length-1?responsiveWidth(4):0}}>
                                <Text style={{fontWeight:"bold",fontSize:responsiveFontSize(2.3),color:item.adults==k+1?"#fff":"#000",textAlign:"center",}}>
                                     {itmk}
                                    </Text>
                                
                            </TouchableOpacity>
                        ))}
                       
                    </View>
                </View>
                <View style={{display:this.state.openview==i?"flex":"none",flexDirection:"row",alignItems:"center",justifyContent:"space-between",marginHorizontal:responsiveWidth(5),
                            paddingVertical:responsiveHeight(2),borderBottomWidth:1,borderColor:"#e6e6e6"}}>
                    <View>
                        <Text style={{fontSize:responsiveFontSize(2.5),fontWeight:"bold",opacity:0.7}}>
                            Childs
                        </Text>
                        <Text style={{fontSize:responsiveFontSize(2),opacity:0.7}}>
                            (0-5 years)
                        </Text>
                    </View>
                    <View style={{width:responsiveWidth(50),flexDirection:"row",alignItems:"center"}}>
                    {this.state.Childs.length>0 &&
                        this.state.Childs.map((itmm,m)=>(
                            <TouchableOpacity key={m} onPress={()=>this.setGuest("childs",m,i)}
                                    style={{alignItems:"center",justifyContent:"space-between",padding:responsiveHeight(1),paddingHorizontal:responsiveWidth(3),
                                    height:"auto",backgroundColor:item.childs==m?"#0246b1":"#fff",borderWidth:1,borderColor:"#e6e6e6",width:"auto",
                                    borderTopLeftRadius:m==0?responsiveWidth(4):0,borderBottomLeftRadius:m==0?responsiveWidth(4):0,
                                    borderTopRightRadius:m==this.state.Childs.length-1?responsiveWidth(4):0,borderBottomRightRadius:m==this.state.Childs.length-1?responsiveWidth(4):0}}>
                                <Text style={{fontWeight:"bold",fontSize:responsiveFontSize(2.3),color:item.childs==m?"#fff":"#000",textAlign:"center",}}>
                                     {itmm}
                                    </Text>
                                
                            </TouchableOpacity>
                        ))}
                       
                    </View>
                </View>
                <View style={{display:this.state.openview==i?"flex":"none",flexDirection:"row",alignItems:"center",justifyContent:"center",marginHorizontal:responsiveWidth(5),
                            paddingVertical:responsiveHeight(2)}}>
                    {i==0 &&
                        <View>
                            <TouchableOpacity onPress={()=>this.AddRoom()}>
                                <Text style={{fontSize:responsiveFontSize(2.5),fontWeight:"bold",opacity:0.7,color:"red"}}>
                                    Add Rooms
                                </Text>
                            </TouchableOpacity>
                        </View>
                    }
                    {i!=0 &&
                        <View>
                            <TouchableOpacity onPress={()=>this.RemoveRoom(i)}>
                                <Text style={{fontSize:responsiveFontSize(2.5),fontWeight:"bold",opacity:0.7,color:"red"}}>
                                    Remove Rooms
                                </Text>
                            </TouchableOpacity>
                        </View>
                    }

                </View>
            </View>
                ))}
        </View>
      )
  }

 render(){
   //console.log(this.props.roomDetails)
 return (
    
    <Modal
    animationType="slide"
    transparent={true}
    statusBarTranslucent={true}
    hardwareAccelerated={true}
    onRequestClose={() => this.props.onClose(this.state.modalVisible)}
  >
    
        <View style={{flex:1,backgroundColor:"#fff"}} >
            <Container >
                <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
                <>
                    <View style={{flexDirection:"row",alignItems:"center",justifyContent:"space-between",borderBottomWidth:1,
                        borderBottomColor:"#a6a6a6",padding:responsiveWidth(3),height:"auto",backgroundColor: "#fff"}}>
                        <TouchableOpacity onPress={()=>this.props.onClose(this.state.modalVisible)}>
                            <Icon name='arrow-back'  style={{color:"#000",fontSize:responsiveFontSize(4)}}/>
                        </TouchableOpacity>
                        <TouchableOpacity style={{width:responsiveWidth(85)}}>
                            <Text style={{fontSize:responsiveFontSize(2.5),fontWeight:"bold"}}>
                                Select Rooms & Guests
                            </Text>
                        </TouchableOpacity>
                    </View>
                    <View style={{backgroundColor: "#fff",marginTop:responsiveHeight(3),paddingHorizontal:responsiveWidth(5)}}>
                    <Segment style={{backgroundColor: "#fff",height:"auto"}}>
                        <View style={{flexDirection:"row"}}>
                            <TouchableOpacity onPress={this.selectComponent(1)}
                                style={{alignItems:"center",justifyContent:"center",paddingVertical:responsiveHeight(1),paddingHorizontal:responsiveWidth(3),width:"auto",
                                height:"auto",backgroundColor:this.state.activePage==1?"#0246b1":"#e6e6e6",
                                borderTopLeftRadius:responsiveWidth(5),borderBottomLeftRadius:responsiveWidth(5)}}>
                                <Text style={{fontWeight:'bold',fontSize:responsiveFontSize(2.3),color:this.state.activePage==1?"#fff":"#0246b1"}}>
                                    Check-In & Check-Out
                                </Text>
                            </TouchableOpacity>
                            <TouchableOpacity onPress={this.selectComponent(2)}
                                style={{alignItems:"center",justifyContent:"center",paddingVertical:responsiveHeight(1),paddingHorizontal:responsiveWidth(3),width:"auto",
                                        height:"auto",backgroundColor:this.state.activePage==2?"#0246b1":"#e6e6e6",
                                        borderTopRightRadius:responsiveWidth(5),borderBottomRightRadius:responsiveWidth(5)}}>
                                <Text style={{fontWeight:'bold',fontSize:responsiveFontSize(2.5),color:this.state.activePage==2?"#fff":"#0246b1",textAlign:"center"}}>
                                    Rooms & Guests
                                </Text>
                            </TouchableOpacity>
                        </View>
                    </Segment>
                    </View>
                    <View style={{flex:1}}>
                        {this._renderComponent()}
                    </View>
                    <View style={{alignItems:"center",justifyContent:"center",padding:responsiveWidth(3),height:"auto",
                                backgroundColor: "#fff",borderTopWidth:1,borderTopColor:"#e6e6e6"}}>
                        <TouchableOpacity onPress={() => this.setSelectValue()}
                            style={{alignItems:"center",justifyContent:"center",paddingVertical:responsiveHeight(2),width:responsiveWidth(80),
                            backgroundColor:this.state.loader?"grey":"#0246B1",borderRadius:responsiveWidth(5)}}>
                            <Text style={{fontWeight:'bold',fontSize:responsiveFontSize(2.5),color:'#fff',}}>
                                Done
                            </Text>
                        </TouchableOpacity>
                    </View>
                </>
                </TouchableWithoutFeedback>
            </Container>
            <Loader animation="fade"
                    visible={this.state.showLoader}
                    textContent={'Loading...'}
                    textStyle={{color:"#fff"}}
                />
        </View>
    
    </Modal>
     
)}
} 
const styles = StyleSheet.create({
    segmentButton: {
      padding:10,
      borderColor:"#ccc",

    }
  })



