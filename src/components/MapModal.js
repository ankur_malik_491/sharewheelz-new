import React from 'react';
import {Text, View,TouchableOpacity,TouchableWithoutFeedback,Keyboard,ImageBackground,Modal,FlatList} from 'react-native';
import {Container,Icon,Input, Content,Toast,Card,CardItem,Spinner} from "native-base";
import {responsiveWidth,responsiveHeight,responsiveFontSize} from "react-native-responsive-dimensions";
import * as Location from 'expo-location';
import KeyboardSpacer from "react-native-keyboard-spacer";
import Loader from 'react-native-loading-spinner-overlay';
import API from "../../allApi";

export default class MapModal extends React.Component {
  constructor(props){
    super(props);
    this.state = {
                    modalVisible:true,
                    loading:true,
                    searchText: "",
                    data: [],
                    filteredData: []
    };
    this.getCity()
    
  }
 
  // getHistory = () => {
  //   if(this.props.history == ""){}
  //   else{
  //     this.setState({searchHistory:this.props.history})
  //   }
//}
getCity = async () => {
    // this.setState({loading:true})
    if(this.state.data.length<=0){
        await fetch('https://app.yoyorooms.net/Appapi/get_all_city')
        .then(response => response.json())
      .then(response2 => {
        
        if(response2!=0){
            this.setState({data: response2})
            this.setState({loading:false})
        }
        else {
          this.setState({loading:false})
          Toast.show({
            text:"Something went wrong!",
            textStyle:{textAlign:"center"},
            duration:2000,
            style:{borderRadius:10,margin:responsiveWidth(10),backgroundColor:"red"}
          });
        }
      }).catch((error) =>{
        console.log(error)
        this.setState({loading:false})
      }).catch((error) =>{
        console.log(error)
        this.setState({loading:false})
    })
  }
  else{
    this.setState({loading:false})
  }
   
}
search = (searchText) => {
  this.setState({searchText: searchText,loading:true});

  let filteredData = this.state.data.filter(function (item) {
    return item.city_name.includes(searchText);
  });

  this.setState({filteredData: filteredData,loading:false});
};

_renderItem = ({item}) => (
  <TouchableOpacity onPress={()=>this.props.onAction(this.state.modalVisible,item.city_name)}>
    <View style={{flexDirection:"row",alignItems:"center",justifyContent:"space-between",borderBottomWidth:1,
              borderBottomColor:"#e6e6e6",paddingHorizontal:responsiveWidth(5),height:responsiveHeight(8)}}>
      <Text style={{fontSize:responsiveFontSize(1.8),marginLeft:responsiveWidth(5)}}>{item.city_name}</Text>
      <Icon name='ios-arrow-forward'   style={{color:"#a6a6a6",fontSize:responsiveFontSize(3)}}/>
    </View>
  </TouchableOpacity>
  
)
 render(){
   //console.log(this.state.searchHistory)
 return (
    <Modal
      animationType="slide"
      transparent={true}
      visible={this.state.modalVisible}
      onRequestClose={() => this.props.onAction(this.state.modalVisible)}
    >
      <View style={{flex:1}} >
      <TouchableWithoutFeedback onPress={Keyboard.dismiss}>
        <Container >
          
              <View >
                  <View style={{flexDirection:"row",alignItems:"center",justifyContent:"space-between",borderBottomWidth:1,
                            borderBottomColor:"#e6e6e6",paddingHorizontal:responsiveWidth(5),height:responsiveHeight(10)}}>
                    <TouchableOpacity onPress={()=>this.props.onAction(this.state.modalVisible)}>
                      <Icon name='arrow-back'  style={{color:"#000",fontSize:responsiveFontSize(4)}}/>
                    </TouchableOpacity>
                    <Input placeholder="Search Hotels, Location" 
                        placeholderTextColor="#a6a6a6"
                        value={this.state.searchText}
                        onChangeText={(val)=>this.search(val)}
                        style={{width:responsiveWidth(60),marginLeft:responsiveWidth(5)}}
                    />
                    <TouchableOpacity onPress={()=>this.setState({searchText:""})}>
                      <Icon name='close'   style={{color:"red",fontSize:responsiveFontSize(4)}}/>
                    </TouchableOpacity>
                  </View>
                  <View style={{flexDirection:"row",alignItems:"center",justifyContent:"space-between",borderBottomWidth:4,
                            borderBottomColor:"#e6e6e6",paddingHorizontal:responsiveWidth(5),height:responsiveHeight(10)}}>
                    <Text style={{fontSize:responsiveFontSize(2.2),fontWeight:"900"}}>Current Location</Text>
                    <Icon name='md-locate'   style={{color:"#000",fontSize:responsiveFontSize(3)}}/>
                  </View>
                  <View style={{flexDirection:"row",alignItems:"center",justifyContent:"space-between",borderBottomWidth:4,
                            borderBottomColor:"#e6e6e6",paddingHorizontal:responsiveWidth(5),height:responsiveHeight(10)}}>
                    <Text style={{fontSize:responsiveFontSize(2),fontWeight:"bold"}}>All Cities</Text>
                    <Icon name='ios-pin'   style={{color:"#000",fontSize:responsiveFontSize(3)}}/>
                  </View>
              </View>
              <Content>
                <View style={{flex:1}}>
                      {(!this.state.loading) &&
                        <FlatList
                        data={this.state.filteredData && this.state.filteredData.length > 0 ? this.state.filteredData : this.state.data}
                        renderItem={this._renderItem}
                        keyExtractor={item => item.id}
                      />}
                      {(this.state.loading) &&
                       <View  style={{alignItems:"center",justifyContent:"center",height:responsiveHeight(60)}}>
                       {/* <Image source={require("../../../../../assets/splash.png")} style={{height:responsiveHeight(40),width:responsiveWidth(60),}} /> */}
                          <Spinner color="#0246b1" size="large"/>
                        </View>
                       }
                </View>
              </Content>
        </Container>
        </TouchableWithoutFeedback>
      </View> 
  </Modal>
)}
} 




