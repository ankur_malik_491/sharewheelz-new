import {createAppContainer} from "react-navigation";
import {createStackNavigator} from 'react-navigation-stack';
import {Login,Signup,Forgot } from "../../screens/Auth";

const AuthRoutes = createStackNavigator({
       login:Login,
    //    signup:Signup,
    //    forgot:Forgot
       
    },{
        initialRouteName:'login',
        headerMode:null
    });
    
    export default createAppContainer(AuthRoutes);