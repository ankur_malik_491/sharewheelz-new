import React from 'react';
import {View, Text,AsyncStorage,Image} from "react-native";
import {Icon, Thumbnail} from "native-base";
import { createBottomTabNavigator,createMaterialTopTabNavigator } from 'react-navigation-tabs';
import {responsiveHeight,responsiveFontSize,responsiveWidth} from 'react-native-responsive-dimensions';
import {createStackNavigator} from "react-navigation-stack";
import {createAppContainer,createSwitchNavigator} from "react-navigation";
import {HomeScreen,Saved,Bookings,ShareEarn} from "../../screens/Main/BottonTabPage";
import searchHotelRoutes from "../../screens/Main/BottonTabPage/HomeScreen/SearchHotels/index";
import HotelDetailsOuter from "../../screens/Main/BottonTabPage/HomeScreen/SearchHotels/hotelDetails";
import BookingDetail from "../../screens/Main/BottonTabPage/Bookings/BookingDetails";

const BottomTabContainer = createMaterialTopTabNavigator({
    homescreen:{screen:HomeScreen,
        navigationOptions:{
            tabBarLabel: ({ tintColor, focused }) => (
              <View style={{alignItems:"center",justifyContent:"center"}}>
                <Icon name="ios-home" 
                  style={{ fontSize:responsiveFontSize(3.3),
                        marginBottom:responsiveHeight(1),
                        color: focused ? "#0246b1" : "#808080",}}/>
                <Text style={{ fontSize: responsiveFontSize(1.4),
                              color: focused ? "#0246b1" : "#808080"}}>Home</Text>
              </View>
            )
          }
      },
    history:{screen:Saved,
        navigationOptions:{
            tabBarLabel: ({ tintColor, focused }) => (
            <View style={{alignItems:"center",justifyContent:"center"}}>
              <Icon name="ios-heart"  
                  style={{ fontSize:responsiveFontSize(3.3),
                        marginBottom:responsiveHeight(1),
                        color: focused ? "#0246b1" : "#808080",}}/>
              <Text style={{ fontSize: responsiveFontSize(1.4),
                          color: focused ? "#0246b1" : "#808080"}}>Saved</Text>
            </View>
            )
          }
      },
    ride:{screen:Bookings,
        navigationOptions:{
            tabBarLabel: ({ tintColor, focused }) => (
              <View style={{alignItems:"center",justifyContent:"center"}}>
              <Icon name="ios-briefcase" 
                  style={{ fontSize: responsiveFontSize(3.3),
                        marginBottom:responsiveHeight(1),
                        color: focused ? "#0246b1" : "#808080",}}/>
              <Text style={{ fontSize: responsiveFontSize(1.4),
                          color: focused ? "#0246b1" : "#808080"}}>Bookings</Text>
            </View>
        
            ),
            // tabBarOnPress: (scene, jumpToIndex) => {
            //   console.log('onPress:', jumpToIndex);
            //   //jumpToIndex(scene.index);
            // },
            // tabBarOnPress: (scene) => {
            //  // console.log(scene.navigation)
            //   // if (args.scene.focused) { // if tab currently focused tab
            //   // if (args.scene.route.index !== 0) { // if not on first screen of the StackNavigator in focused tab.
            //   //     navigation.dispatch(NavigationActions.reset({
            //   //     index: 0,
            //   //     actions: [
            //   //         NavigationActions.navigate({ routeName: args.scene.route.routes[0].routeName }) // go to first screen of the StackNavigator
            //   //     ]
            //   //     }))
            //   // }
            //   // } else {
            //   //     args.jumpToIndex(args.scene.index) // go to another tab (the default behavior)
            //   // }
          //}
          },

      },
    chat:{screen:ShareEarn,
        navigationOptions:{
            tabBarLabel: ({ tintColor, focused }) => (
              <View style={{alignItems:"center",justifyContent:"center"}}>
                <Icon name="ios-share-alt"  
                    style={{ fontSize:responsiveFontSize(3.3),
                          marginBottom:responsiveHeight(1),
                          color: focused ? "#0246b1" : "#808080",}}/>
                <Text style={{ fontSize: responsiveFontSize(1.4),
                            color: focused ? "#0246b1" : "#808080",textAlign:"center"}}>Share & Earn</Text>
              </View>
            ),
          },
      },
},{
   
    tabBarPosition:"bottom",
    swipeEnabled:false,
    tabBarOptions:{style:{backgroundColor:"#fff"}},
    navigationOptions: ({ navigation }) => ({
      
      tabBarOnPress: (scene, jumpToIndex) => {
        console.log(scene.route);
        //jumpToIndex(scene.index);
        },
      }),
  
    
    
    
    
    
    
});

const homeRoutes = createStackNavigator({
  bottomTab:BottomTabContainer,
  searchHotel: searchHotelRoutes,
  hotelDeatilsOuter:HotelDetailsOuter,
  bookingDetails : BookingDetail,

  
 
},{
  initialRouteName:'bottomTab',
  headerMode:null
});

export default createAppContainer(homeRoutes);