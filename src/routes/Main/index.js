import {createStackNavigator} from "react-navigation-stack";
import {createAppContainer,createSwitchNavigator} from "react-navigation";
import DrawerContainer from '../Drawer';
import BottomTabContainer from "../BottomTab"

const mainRoutes = createSwitchNavigator({
    drawer:DrawerContainer,
    //bottomTab:BottomTabContainer
    
   
},{
    initialRouteName:'drawer',
    headerMode:null
});

export default createAppContainer(mainRoutes);