import React from 'react';
import {View,Icon,Text,Thumbnail} from "native-base";
import {responsiveWidth,responsiveHeight,responsiveFontSize} from "react-native-responsive-dimensions";
import {createDrawerNavigator} from "react-navigation-drawer";
import {createAppContainer, createSwitchNavigator} from "react-navigation";
import Drawer from "../../components/Drawer";
import {Main,ReferEarn,Setting,About,Support} from "../../screens/Main/DrawerPage";

const DrawerContainer = createDrawerNavigator({
    main:{screen:Main,
        navigationOptions:{
           drawerLabel:({ tintColor, focused }) => 
                        <Text  style={{fontWeight: focused ? 'bold' : "normal",fontSize:responsiveFontSize(2.5)}}>
                            Home
                        </Text>,
           drawerIcon:() => <Thumbnail square source={require("../../assets/icon/home.png")} 
                                    style={{height:responsiveWidth(10),width:responsiveWidth(10),margin:responsiveWidth(3)}}/>,
          },
        },
    referearn:{screen:ReferEarn,
        navigationOptions:{
           drawerLabel:({ focused }) => 
                        <Text  style={{fontWeight: focused ? 'bold' : "normal",fontSize:responsiveFontSize(2.5)}}>
                            Refer and Earn
                        </Text>,
           drawerIcon:() => <Thumbnail square source={require("../../assets/icon/gift.png")} 
                                    style={{height:responsiveWidth(8),width:responsiveWidth(8),margin:responsiveWidth(3)}}/> ,
          },
        },
    
   
},{
    initialRouteName : "main",
    hideStatusBar:true,
    contentComponent: props => <Drawer {...props} />,
    
    
    
});

export default createAppContainer(DrawerContainer);