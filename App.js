import React from 'react';
import {Root} from "native-base";
import { SafeAreaProvider } from 'react-native-safe-area-context';
import AppContainer from "./src/routes/index";
import { StatusBar } from 'react-native';

class App extends React.Component {
  render(){
    return (
      <Root>
        <SafeAreaProvider>
          <StatusBar barStyle="dark-content"/>
          <AppContainer />
        </SafeAreaProvider>
      </Root>
    );
  }
}
export default App;
